package com.eab.rest.ease.report;

import com.google.gson.JsonArray;

public class SupervisorReportData {
	
	private String CaseStatus;
	private String CaseNumber_PolicyNumber;
	private String Casecreationdate;
	private String AgentCaseSubmissiondate;
	private String Supervisorapprovaldate;
	private String Firmapprovaldate;
	private String ProposerICDocumentType;
	private String ProposerIC;
	private String ProposerName;
	private String LifeAssuredICDocumentType;
	private String LifeAssuredIC;
	private String LifeAssuredName;
	private String ProposerMobileNo;
	public String getCaseStatus() {
		return CaseStatus;
	}
	public void setCaseStatus(String caseStatus) {
		CaseStatus = caseStatus;
	}
	public String getCaseNumber_PolicyNumber() {
		return CaseNumber_PolicyNumber;
	}
	public void setCaseNumber_PolicyNumber(String caseNumber_PolicyNumber) {
		CaseNumber_PolicyNumber = caseNumber_PolicyNumber;
	}
	public String getCasecreationdate() {
		return Casecreationdate;
	}
	public void setCasecreationdate(String casecreationdate) {
		Casecreationdate = casecreationdate;
	}
	public String getAgentCaseSubmissiondate() {
		return AgentCaseSubmissiondate;
	}
	public void setAgentCaseSubmissiondate(String agentCaseSubmissiondate) {
		AgentCaseSubmissiondate = agentCaseSubmissiondate;
	}
	public String getSupervisorapprovaldate() {
		return Supervisorapprovaldate;
	}
	public void setSupervisorapprovaldate(String supervisorapprovaldate) {
		Supervisorapprovaldate = supervisorapprovaldate;
	}
	public String getFirmapprovaldate() {
		return Firmapprovaldate;
	}
	public void setFirmapprovaldate(String firmapprovaldate) {
		Firmapprovaldate = firmapprovaldate;
	}
	public String getProposerICDocumentType() {
		return ProposerICDocumentType;
	}
	public void setProposerICDocumentType(String proposerICDocumentType) {
		ProposerICDocumentType = proposerICDocumentType;
	}
	public String getProposerIC() {
		return ProposerIC;
	}
	public void setProposerIC(String proposerIC) {
		ProposerIC = proposerIC;
	}
	public String getProposerName() {
		return ProposerName;
	}
	public void setProposerName(String proposerName) {
		ProposerName = proposerName;
	}
	public String getLifeAssuredICDocumentType() {
		return LifeAssuredICDocumentType;
	}
	public void setLifeAssuredICDocumentType(String lifeAssuredICDocumentType) {
		LifeAssuredICDocumentType = lifeAssuredICDocumentType;
	}
	public String getLifeAssuredIC() {
		return LifeAssuredIC;
	}
	public void setLifeAssuredIC(String lifeAssuredIC) {
		LifeAssuredIC = lifeAssuredIC;
	}
	public String getLifeAssuredName() {
		return LifeAssuredName;
	}
	public void setLifeAssuredName(String lifeAssuredName) {
		LifeAssuredName = lifeAssuredName;
	}
	public String getProposerMobileNo() {
		return ProposerMobileNo;
	}
	public void setProposerMobileNo(String proposerMobileNo) {
		ProposerMobileNo = proposerMobileNo;
	}
	public String getProposerEmailaddress() {
		return ProposerEmailaddress;
	}
	public void setProposerEmailaddress(String proposerEmailaddress) {
		ProposerEmailaddress = proposerEmailaddress;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getManagerName() {
		return ManagerName;
	}
	public void setManagerName(String managerName) {
		ManagerName = managerName;
	}
	public String getDirectorName() {
		return DirectorName;
	}
	public void setDirectorName(String directorName) {
		DirectorName = directorName;
	}
	public String getCaseApprovedby() {
		return CaseApprovedby;
	}
	public void setCaseApprovedby(String caseApprovedby) {
		CaseApprovedby = caseApprovedby;
	}
	public String getFirmName() {
		return FirmName;
	}
	public void setFirmName(String firmName) {
		FirmName = firmName;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getPremiumamount_Basicplan() {
		return Premiumamount_Basicplan;
	}
	public void setPremiumamount_Basicplan(String premiumamount_Basicplan) {
		Premiumamount_Basicplan = premiumamount_Basicplan;
	}
	public String getSumassured_BasicPlan() {
		return Sumassured_BasicPlan;
	}
	public void setSumassured_BasicPlan(String sumassured_BasicPlan) {
		Sumassured_BasicPlan = sumassured_BasicPlan;
	}
	public String getPremiumType_RP_SP() {
		return PremiumType_RP_SP;
	}
	public void setPremiumType_RP_SP(String premiumType_RP_SP) {
		PremiumType_RP_SP = premiumType_RP_SP;
	}
	public String getPremiumFrequency() {
		return PremiumFrequency;
	}
	public void setPremiumFrequency(String premiumFrequency) {
		PremiumFrequency = premiumFrequency;
	}
	public String getTop_up() {
		return Top_up;
	}
	public void setTop_up(String top_up) {
		Top_up = top_up;
	}
	public String getRSP() {
		return RSP;
	}
	public void setRSP(String rSP) {
		RSP = rSP;
	}
	public String getRSPfrequency() {
		return RSPfrequency;
	}
	public void setRSPfrequency(String rSPfrequency) {
		RSPfrequency = rSPfrequency;
	}
	public String getPaymentMethod() {
		return PaymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		PaymentMethod = paymentMethod;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public JsonArray getRiders() {
		return riders;
	}
	public void setRiders(JsonArray riders) {
		this.riders = riders;
	}
	private String ProposerEmailaddress;
	private String AgentName;
	private String ManagerName;
	private String DirectorName;
	private String CaseApprovedby;
	private String FirmName;
	private String ProductName;
	private String Premiumamount_Basicplan;
	private String Sumassured_BasicPlan;
	private String PremiumType_RP_SP;
	private String PremiumFrequency;
	private String Top_up;
	private String RSP;
	private String RSPfrequency;
	private String PaymentMethod;
	private String Currency;
	private JsonArray riders;

}
