package com.eab.rest.ease.report;

import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/report/supervisor")
public class SupervisorReportController {

	private SupervisorReportService supervisorReportService = new SupervisorReportService();
	private SupervisorReportProvider reportProvider = new SupervisorReportProvider();

	@GET
	@Path("{userId}")
	@Produces("application/json")
	public Response report(@PathParam("userId") String userId, @QueryParam("startTime") String startTime,
			@QueryParam("endTime") String endTime,@QueryParam("dateType") String dateType, @Context HttpHeaders headers, @Context UriInfo uriInfo) {
		Log.info("Report / supervisor");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		Log.debug("*** audID=" + audID);
		Log.debug("*** userId=" + userId);
		Log.debug("*** startTime=" + startTime);
		Log.debug("*** endTime=" + endTime);

		Response output = null;

		String errorString = "";

		if (userId == null) {
			errorString += "userId is null";
		}

		if (startTime == null) {
			startTime = "1970-01-01T02:23:24.598Z";
		}
		
		if (endTime == null) {
			endTime = Function.getDateUTCNowStr();
		}
		
		if(dateType == null) {
			dateType = "C";
		}

		boolean valid = Function.dateFormatValidator(startTime, Constant.DATETIME_PATTERN_AS_ISO8601);
		if (!valid) {
			errorString += "startTime date format is incorrect";
		}
		
		valid = Function.dateFormatValidator(endTime, Constant.DATETIME_PATTERN_AS_ISO8601);
		if (!valid) {
			errorString += "endTime date format is incorrect";
		}
		
		String[] optionValues = new String[] { "C", "S", "A" };
		valid = Arrays.asList(optionValues).contains(dateType);
		
		if (!valid) {
			errorString += "dateType is incorrect";
		}

		if (errorString.length() > 0) { // found errors after validation
			output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
					RestUtil.genErrorJsonStr(false, errorString));
		} else {
			
			JSONArray agencyDownlineUserProfileJsonArray = supervisorReportService.getAgencyDownlineUserProfile(userId);
			Log.debug("*** agencyDownlineUserProfileJsonArray=" + agencyDownlineUserProfileJsonArray);

			if (agencyDownlineUserProfileJsonArray != null) {
				ArrayList<String> agentCodeArrayList = supervisorReportService
						.extractAgentCodeArray(agencyDownlineUserProfileJsonArray);
				Log.debug("*** agentCodeArrayList=" + agentCodeArrayList);
				
				JsonArray mergedRecordJsonArray = supervisorReportService.getRecordsOnCouchBase(agentCodeArrayList, dateType, startTime, endTime);
				
				try {
					String excelFileData = null;
					
					if(mergedRecordJsonArray.size() >0) {
						String[][] excelDataArray = supervisorReportService.parseToExcelDataArray(mergedRecordJsonArray);
						excelFileData = reportProvider.createExcelReportInBase64Format(excelDataArray, userId);
						
					}else {
						excelFileData = reportProvider.createEmptyReport(userId);
					}
					
					if(excelFileData != null) {
						JsonObject jsonBody = supervisorReportService.createJsonBody(excelFileData);
						
						if(jsonBody != null) {
							HttpPost postRequest = supervisorReportService.createEmailPostRequest(jsonBody);
							Response emailOutput = HttpUtil.send(postRequest);
							
							boolean isCompleted = emailOutput.getStatus() == 204? true:false;
							
							Log.debug("*** emailOutput="+isCompleted);
						}else {
							Log.error("ERROR - supervisorReportService.createJsonBody jsonBody is null");
						}
					}else {
						Log.error("ERROR - excelFileData is null");
					}
					
				}catch(Exception ex){
					Log.error(ex);
				}
				
				output = RestUtil.toResponse(audID, Response.Status.OK,
						mergedRecordJsonArray.toString());
			}else {
				Log.error("ERROR - agencyDownlineUserProfileJsonArray = null");
			}
		}

		return output;
	}

}
