package com.eab.rest.ease.sms;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_EMS_SMS;
import com.eab.database.jdbc.DBAccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/*
 * URL--> http://ease-api:8080/ease-api/sms/sendSMS
 * Expected Body -->
 * {
 *   "mobileNo": "98765432",				//SG Mobile Only
 *   "smsMsg": "SMS Message in ASCII"		//Max. 160 ASCII chars
 * }
 * 
 */

@Path("/sms")
public class Ems {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendSMS")
	public Response sendSMS(@Context HttpHeaders headers, String body) {
		Log.info("Landing /sendSMS");
		String audID = headers.getRequestHeader("aud-id").get(0);	// provided by filter
		
		String headerStr = RestUtil.showLogRequest(headers, body);		// for debug
		
		boolean hasError = false;
		String msgError = "";
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement jelem = null;
		JsonObject jobj = null;
		Response output = null;
		Connection conn = null;
		API_EMS_SMS dao = new API_EMS_SMS();
		try {
			conn = DBAccess.getConnection();
			
			// Create Transaction Log into Database
			dao.create(audID, headerStr, body, conn);
			
			// Convert JSON Body to Object
			if (!hasError) {
				jelem = gson.fromJson(body, JsonElement.class);
				if (jelem != null) {
					jobj = jelem.getAsJsonObject();
					if (jobj == null) {
						hasError = true;
						msgError += "INVALID BODY;";
					}
				} else {
					hasError = true;
					msgError += "EMPTY BODY;";
				}
			}
			
			// Get known JSON objects
			if (!hasError) {
				String mobileNo = jobj.get("mobileNo")!=null?jobj.get("mobileNo").getAsString():null;
				String smsMsg = jobj.get("smsMsg")!=null?jobj.get("smsMsg").getAsString():null;
				String smsHost = EnvVariable.get("WEB_API_SMS_HOST");
				String smsSource = EnvVariable.get("WEB_API_SMS_SOURCE");
				String smsUsername = EnvVariable.get("WEB_API_SMS_USERNAME");
				String smsPassoword = EnvVariable.get("WEB_API_SMS_PASSWORD");
				
				// Parmater Validation
				if (mobileNo == null || mobileNo.length() < 8) {
					hasError = true;
					msgError += "INVALID mobileNo which is less than 8 digit;";
				} else {
					String temp = mobileNo;
					if (mobileNo.indexOf("+852") == 0) {
						temp = mobileNo.substring(4);
					} else if (mobileNo.indexOf("+65") == 0) {
						temp = mobileNo.substring(3);
					} else {
						// Add +65 as default to Mobile No. for SG project
						mobileNo = "+65" + mobileNo;
					}
					if (temp.trim().length() < 8 || !Function.isNumeric(temp.trim())) {
						hasError = true;
						msgError += "INVALID mobileNo which is not 8 digit numbers;";
					}
				}
				
//				if (smsMsg == null || checkSmsMsgSize(smsMsg) < 0 || checkSmsMsgSize(smsMsg) > 160) {
//					hasError = true;
//					msgError += "INVALID smsMsg which size is " + checkSmsMsgSize(smsMsg) + ";";
//				}
				
				if (smsHost == null || smsUsername == null || smsPassoword == null) {
					hasError = true;
					msgError += "INVALID SMS Host Config;";
				}
				
				// Make request to webservice
				if (!hasError) {
					output = EmsUtil.send(mobileNo, smsMsg);
					
					// Update Transaction Log in Database
					dao.update(audID, RestUtil.convertHeaderToStr(output.getStringHeaders()), output.getEntity().toString(), String.valueOf(output.getStatus()), "C", "", conn);
				}
			}
			
			if (hasError) {
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, msgError));
			}
			
		} catch(Exception ex) {
			Log.error(ex);
			hasError = true;
			msgError = ex.getMessage();
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, msgError));
		} finally {
			if (hasError) {
				try {
					// Update Transaction Log in Database
					dao.update(audID, "", "", "", "F", "", conn);
				} catch(Exception ex) {
					Log.error(ex);
				}
			}
			gson = null;
			jelem = null;
			jobj = null;
			try {if (conn != null && !conn.isClosed()) conn.close();} catch(Exception e) {}
			conn = null;
			dao = null;
		}
		
		// Reply the response to caller
		return output;
	}
	
	private int checkSmsMsgSize(String text) throws UnsupportedEncodingException {
		int size = 0;
		if (text != null) {
			size = text.getBytes("UTF-8").length;
		}
		return size;
	}
}
