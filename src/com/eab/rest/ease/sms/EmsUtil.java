package com.eab.rest.ease.sms;

import java.net.URLEncoder;

import javax.ws.rs.core.Response;

import org.apache.commons.codec.net.URLCodec;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;

import com.eab.common.EnvVariable;
import com.eab.common.HttpUtil;
import com.eab.common.Log;

public class EmsUtil {
	public static Response send(String destination, String message) throws Exception {
		Log.info("Goto EmsUtil.send");
		
		String smsHost = EnvVariable.get("WEB_API_SMS_HOST");
		String smsSource = EnvVariable.get("WEB_API_SMS_SOURCE");
		String smsUsername = EnvVariable.get("WEB_API_SMS_USERNAME");
		String smsPassoword = EnvVariable.get("WEB_API_SMS_PASSWORD");
		
		URLCodec codec = new URLCodec();
		
		URIBuilder builder = new URIBuilder(smsHost);
		builder.addParameter("username", smsUsername);
		builder.addParameter("password", smsPassoword);
		builder.addParameter("destination", destination);
		builder.addParameter("message", message);
		if (smsSource != null && smsSource.length() > 0)
			builder.addParameter("source", smsSource);
		
		Log.debug(builder.toString());
		
		HttpGet request = new HttpGet(builder.build());
		return HttpUtil.send(request);
	}
}
