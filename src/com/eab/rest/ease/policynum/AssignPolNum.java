package com.eab.rest.ease.policynum;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.eab.common.EnvVariable;
import com.eab.common.Log;

/*
 * URL--> http://ease-api:8080/ease-api/policyNumber/assignPolicyNumber
 * Expected Body -->
 * {
 *   "companyCD": "3",
 *   "requestedPolicyNO": "1000"
 * }
 * 
 */

@Path("/policyNumber")
public class AssignPolNum {
	private static boolean IS_GET_FROM_POOL = ("Y".equals(EnvVariable.get("SWITCH_GET_FROM_POOL"))?true:false);
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/assignPolicyNumber")
	public Response assignPolicyNumber(@Context HttpHeaders headers, String body) {
		Response output = null;
		
		Log.info("Landing /assignPolicyNumber");
		String audID = headers.getRequestHeader("aud-id").get(0);	// provided by filter
		
		// This API is called by WEB.
		// This API may get policy num from AXA RO API or Policy Num Pool.
		if (IS_GET_FROM_POOL) {
			output = PolicyNumUtil.getFromPool(audID, headers, body);
		} else {
			output = PolicyNumUtil.getFromROAPI(audID, headers, body);
		}
		
		// Reply the response to caller
		return output;
	}
}
