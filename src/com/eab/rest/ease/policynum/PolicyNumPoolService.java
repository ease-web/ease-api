package com.eab.rest.ease.policynum;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Log;
import com.eab.dao.API_GET_POL_TRX;
import com.eab.dao.POOL_NON_SHIELD;
import com.eab.dao.POOL_SHIELD;
import com.eab.dao.POOL_USED_NON_SHIELD;
import com.eab.dao.POOL_USED_SHIELD;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class PolicyNumPoolService {

	private final int RETRY_TIMES = 100;

	public JsonObject convertAXAJsonFormat(JsonArray policyNumJsonArray) {
		Log.info("PolicyNumPoolService.convertAXAJsonFormat");

		JsonObject retrievePolicyNumberResponse = new JsonObject();

		retrievePolicyNumberResponse.add("policyNO", policyNumJsonArray);

		JsonObject exception = new JsonObject();
		exception.addProperty("reason", "");
		retrievePolicyNumberResponse.add("exception", exception);

		JsonObject jsonBody = new JsonObject();
		jsonBody.add("retrievePolicyNumberResponse", retrievePolicyNumberResponse);
		return jsonBody;
	}

	public JsonArray convertPolicyNumberArray(JSONArray policyNumJsonArrayFromDB) {
		Log.info("PolicyNumPoolService.convertPolicyNumberArray");

		JsonArray policyNO = new JsonArray();

		for (Object item : policyNumJsonArrayFromDB) {
			JSONObject policyNumObject = (JSONObject) item;
			String policyNumString = policyNumObject.get("POL_NUM").toString();

			policyNO.add(policyNumString);
		}

		return policyNO;
	}

	public JSONArray getPolicyNumberFromPool(String audID, int requestedPolicyNum, boolean isHealth) throws Exception {
		Log.info("PolicyNumPoolService.getPolicyNumberFromPool");

		JSONArray policyNumJsonArrayFromDB = null;

		POOL_SHIELD poolShieldDao = new POOL_SHIELD();
		POOL_NON_SHIELD poolNonShieldDao = new POOL_NON_SHIELD();

		int policyCount = 0;
		int retryCount = 0;

		while (retryCount <= RETRY_TIMES) {
			Log.debug("**** Start loop ***");
			boolean result = false;
			if (isHealth) {
				result = poolShieldDao.updateTargetPolicyNumber(audID);
			} else {
				result = poolNonShieldDao.updateTargetPolicyNumber(audID);
			}

			if (!result) {
				retryCount++;

				Random rand = new Random();
				int sleepTime = rand.nextInt(50) + 1;

				try {
					Log.debug("**** sleepTime =" + sleepTime);
					Thread.sleep(sleepTime); // sleep for a while and then try again
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
			} else {
				policyCount++;
			}

			Log.debug("**** policyCount=" + policyCount + ", retryCount=" + retryCount);

			if (policyCount >= requestedPolicyNum) {
				break; // Marked enough policy numbers, then break
			}
		}

		if (retryCount >= RETRY_TIMES) {
			// rollback AUD_ID if getting policy number fail
			if (isHealth) {
				poolShieldDao.rollbackTargetPolicyNumber(audID);
				Log.debug("**** poolShieldDao.rollbackTargetPolicyNumber");
			} else {
				poolNonShieldDao.rollbackTargetPolicyNumber(audID);
				Log.debug("**** poolNonShieldDao.rollbackTargetPolicyNumber");
			}

		} else {
			if (isHealth) {
				policyNumJsonArrayFromDB = poolShieldDao.selectTargetPolicyNumbers(audID);
			} else {
				policyNumJsonArrayFromDB = poolNonShieldDao.selectTargetPolicyNumbers(audID);
			}
		}

		return policyNumJsonArrayFromDB;
	}

	public void moveUsedPolicyNumbersToArchive(String audID, JsonArray policyNumJsonArray, boolean isHealth) {
		Log.info("Goto PolicyNumPoolService.moveUsedPolicyNumbersToArchive");
		ExecutorService executor = Executors.newSingleThreadExecutor();

		executor.submit(() -> {
			try {
				if (isHealth) {
					POOL_SHIELD poolShieldDao = new POOL_SHIELD();
					poolShieldDao.removeRecords(audID);
					
					POOL_USED_SHIELD usedPoolShieldDao = new POOL_USED_SHIELD();
					usedPoolShieldDao.insertRecordsFromArray(audID, policyNumJsonArray);
				} else {
					POOL_NON_SHIELD poolNonShieldDao = new POOL_NON_SHIELD();
					poolNonShieldDao.removeRecords(audID);
					
					POOL_USED_NON_SHIELD usedPoolNonShieldDao = new POOL_USED_NON_SHIELD();
					usedPoolNonShieldDao.insertRecordsFromArray(audID, policyNumJsonArray);
				}
			} catch (Exception ex) {
				Log.error(ex);
			}
		});
	}

	public boolean createTransactionRecord(String audID, String headerAsString, String bodyAsString) throws Exception {
		Log.info("Goto PolicyNumPoolService.createTransactionRecord");
		API_GET_POL_TRX trxGetPolicyNumDao = new API_GET_POL_TRX();
		return trxGetPolicyNumDao.create(audID, headerAsString, bodyAsString);
	}

	public boolean updateTransactionRecord(String audID, String responseHeaderAsString, String responseAsString,
			int responsePolicyNumCount, boolean isCompleted) throws Exception {
		Log.info("Goto PolicyNumPoolService.updateTransactionRecord");
		API_GET_POL_TRX trxGetPolicyNumDao = new API_GET_POL_TRX();
		return trxGetPolicyNumDao.update(audID, responseHeaderAsString, responseAsString, responsePolicyNumCount,
				isCompleted);
	}
}
