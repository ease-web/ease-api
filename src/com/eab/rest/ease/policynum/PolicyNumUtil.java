package com.eab.rest.ease.policynum;

import java.sql.Connection;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import com.eab.auth.ease.Maam;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_ASSIGN_POL_TRX;
import com.eab.database.jdbc.DBAccess;
import com.eab.rest.ease.email.EmailNotificationHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class PolicyNumUtil {
	public static HttpPost createRequest(String msgid, boolean isHealth, String companyCD, int requestedPolicyNO) throws Exception {
		Log.info("Goto PolicyNumUtil.createRequest");
		
		Gson gson = new Gson();
		PolicyNumBean pol = new PolicyNumBean();
		
		String policyNumberHost = EnvVariable.get("WEB_API_APWSG_PN");
		HttpPost request = new HttpPost(policyNumberHost);
		request.addHeader("Content-Type", "application/json");
		
		// Headers
		request.addHeader("Authorization", "Bearer "+ Maam.getToken());
		request.addHeader("x-axa-entity", "SG");
		request.addHeader("x-axa-msgid", msgid);
		request.addHeader("x-axa-initialmsgid", msgid);
		request.addHeader("x-axa-lob", (isHealth)?"Health":"Life");
		request.addHeader("Accept", "application/json");
		
		// Body
		pol.retrievePolicyNumberRequest.companyCD = companyCD;
		pol.retrievePolicyNumberRequest.requestedPolicyNO = String.valueOf(requestedPolicyNO);
		String polStr = gson.toJson(pol);
		
		RestUtil.showLogRequest(request.getAllHeaders(), polStr);		// for debug
		
		StringEntity stringEntity = new StringEntity(polStr);
		request.setEntity(stringEntity);
		
		return request;
	}
	
	public static Response getFromPool(String audID, HttpHeaders headers, String body) {
		Log.info("--> Get Policy Num from Pool ***");
		
		Response output = null;
		PolicyNumPoolService policyNumPoolService = new PolicyNumPoolService();
		int responsePolicyNumCount = 0;
		boolean isCompleted = false;
		
		try {
			boolean result = policyNumPoolService.createTransactionRecord(audID,
					RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body);

			Log.debug("*****policyNumPoolService.createTransactionRecord create result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		
		try {
			String headerStr = RestUtil.showLogRequest(headers, body);
			
			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate Json format

			if (bodyJson == null) {
				// Request body is not in JSON format
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
						RestUtil.genErrorJsonStr(false, "Json Syntax Incorrect;"));
			} else {

				boolean hasError = false;
				String msgError = "";
				boolean isHealth = false;

				String companyCD = bodyJson.get("companyCD") != null ? bodyJson.get("companyCD").getAsString() : null;
				int requestedPolicyNO = bodyJson.get("requestedPolicyNO") != null
						? bodyJson.get("requestedPolicyNO").getAsInt()
						: -1;
				Log.debug("companyCD=" + companyCD);
				Log.debug("requestedPolicyNO=" + requestedPolicyNO);

				// Parmater Validation
				// companyCD only can 3 or 5
				if (!"3".equals(companyCD) && !"5".equals(companyCD)) {
					hasError = true;
					msgError += "INVALID companyCD;";
				} else {
					if ("3".equals(companyCD))
						isHealth = true;
				}

				// requestedPolicyNO only can 1 to 1000
				if (requestedPolicyNO < 1 || requestedPolicyNO > 1000) {
					hasError = true;
					msgError += "INVALID requestedPolicyNO;";
				}

				if (hasError) {
					output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
							RestUtil.genErrorJsonStr(false, msgError));
				} else {
					JSONArray policyNumJsonArrayFromDB = policyNumPoolService.getPolicyNumberFromPool(audID,requestedPolicyNO,
							isHealth);

					if (policyNumJsonArrayFromDB != null) {

						responsePolicyNumCount = policyNumJsonArrayFromDB.length();

						JsonArray policyNumJsonArray = policyNumPoolService
								.convertPolicyNumberArray(policyNumJsonArrayFromDB);

						JsonObject jsonBody = policyNumPoolService.convertAXAJsonFormat(policyNumJsonArray);
						output = RestUtil.toResponse(audID, Response.Status.OK, jsonBody.toString());

						Log.debug("**** policyNumJsonArray=" + policyNumJsonArray.toString());
						
						policyNumPoolService.moveUsedPolicyNumbersToArchive(audID, policyNumJsonArray, isHealth);
						
					}else {
						output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
								RestUtil.genErrorJsonStr(false, "Do not have enough policy numbers"));
					}

				}
			}
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
					RestUtil.genErrorJsonStr(false, ex.toString()));
		}
		
		try {

			if (output.getStatus() == 200) { // 202 = success
				isCompleted = true;
			}

			String responseAsString = output.getEntity().toString();
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());

			boolean result = policyNumPoolService.updateTransactionRecord(audID, responseHeaderAsString,
					responseAsString, responsePolicyNumCount, isCompleted);

			Log.debug("*****policyNumPoolService.updateTransactionRecord result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return output;
	}
	
	public static Response getFromROAPI(String audID, HttpHeaders headers, String body) {
		Log.info("--> Get Policy Num from RO API ***");
		
		boolean hasError = false;
		String msgError = "";
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement jelem = null;
		JsonObject jobj = null;
		boolean isHealth = false;
		Response output = null;
		API_ASSIGN_POL_TRX trxPol = new API_ASSIGN_POL_TRX();
		Connection conn = null;
		
		try {
			conn = DBAccess.getConnection();
			
			String headerStr = RestUtil.showLogRequest(headers, body);
			
			// Create Transaction Log into Database
			trxPol.create(audID, headerStr, body, conn);
			
			// Convert JSON Body to Object
			if (!hasError) {
				jelem = gson.fromJson(body, JsonElement.class);
				if (jelem != null) {
					jobj = jelem.getAsJsonObject();
					if (jobj == null) {
						hasError = true;
						msgError += "INVALID BODY;";
					}
				} else {
					hasError = true;
					msgError += "EMPTY BODY;";
				}
			}
			
			// Get known JSON objects
			if (!hasError) {
				String companyCD = jobj.get("companyCD")!=null?jobj.get("companyCD").getAsString():null;
				int requestedPolicyNO = jobj.get("requestedPolicyNO")!=null?jobj.get("requestedPolicyNO").getAsInt():-1;
				Log.debug("companyCD=" + companyCD);
				Log.debug("requestedPolicyNO=" + requestedPolicyNO);
				
				// Parmater Validation
				//companyCD only can 3 or 5
				if ( !"3".equals(companyCD) && !"5".equals(companyCD)) {
					hasError = true;
					msgError += "INVALID companyCD;";
				} else {
					if ("3".equals(companyCD))
						isHealth = true;
				}
				
				//requestedPolicyNO only can 1 to 1000
				if ( requestedPolicyNO<1 || requestedPolicyNO>1000 ) {
					hasError = true;
					msgError += "INVALID requestedPolicyNO;";
				}
				
				// Make request to webservice
				if (!hasError) {
					HttpPost postRequest = PolicyNumUtil.createRequest(audID, isHealth, companyCD, requestedPolicyNO);
					
					output = HttpUtil.send(postRequest);
					
					String responseStatus = output.getStatus() == 200?"C":"F";
					
					String requestHeader = postRequest == null ? "" : HttpUtil.convertHeaderToStr(postRequest.getAllHeaders());
					String requestParam = postRequest == null ? "" : EntityUtils.toString(postRequest.getEntity());
					
					// Update Transaction Log in Database
					trxPol.update(audID, requestHeader, requestParam, RestUtil.convertHeaderToStr(output.getStringHeaders()), output.getEntity().toString(), String.valueOf(output.getStatus()), responseStatus, "", conn);
				}
			}
			
			if (hasError) {
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, msgError));
			}
			
		} catch(Exception ex) {
			Log.error(ex);
			hasError = true;
			msgError = ex.getMessage();
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, msgError));
			
		} finally {
			if (hasError) {
				try {
					// Update Transaction Log in Database
					trxPol.update(audID,"", "", "", "", "", "F", "", conn);
				} catch(Exception ex) {
					Log.error(ex);
				}
			}
			gson = null;
			jelem = null;
			jobj = null;
			trxPol = null;
			try {if (conn != null && !conn.isClosed()) conn.close();} catch(Exception e) {}
			conn = null;
		}
		
		if (output.getStatus() != 200) { // 200 = success
			EmailNotificationHelper.sendErrorNotificationPolicyNumber();
		}
		
		return output;
	}
}
