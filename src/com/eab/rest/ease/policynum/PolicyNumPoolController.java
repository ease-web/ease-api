package com.eab.rest.ease.policynum;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.eab.common.Log;

@Path("/PolicyNumRequest")
public class PolicyNumPoolController {

	private PolicyNumPoolService policyNumPoolService = new PolicyNumPoolService();

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response policyNumPool(@Context HttpHeaders headers, String body) {
		Log.info("/PolicyNumRequest");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		int responsePolicyNumCount = 0;
		Response output = null;
		
		try {
			// Get from RO API
			// This API "ONLY" get Policy Num from AXA RO.
			// And this API should be called by Policy Pool Batch Job.
			output = PolicyNumUtil.getFromROAPI(audID, headers, body);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}

}
