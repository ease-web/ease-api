package com.eab.rest.ease.submission;

public class PolicyRemarks {
	String[] remarklist = new String[99];	
		
	public void addpolicyRemarks(String policyRemark) {		
		for (int i = 0; i < remarklist.length; i++) {
			if(remarklist[i] != null && (remarklist[i].equals(policyRemark))) {
				break;
			} 
			if(remarklist[i] == null) {
				remarklist[i] =  policyRemark;
				break;
			}
		}
	}
	
	public String[] getremarklist() {
		return remarklist;
	}
	
	public String getRMbyIndex(Integer rm_index) {	
		String target_rm = "";
		for (int i = 0; i < remarklist.length; i++) { 
			if(rm_index == i) {
				target_rm = remarklist[i];
				break;
			}
		}
		return target_rm;
	}	
}