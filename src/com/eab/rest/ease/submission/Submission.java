package com.eab.rest.ease.submission;

import java.sql.Connection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.eab.rest.ease.submission.model.SubBean;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_RLS_ESUB_TRX;
import com.eab.database.jdbc.DBAccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/submission")
public class Submission {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{polNum}")
	public Response submission(@Context HttpHeaders headers, @PathParam("polNum") String polNum) throws Exception {
		Log.info("submission");
		
		Response output = null;
		Gson gson = null;
		SubmissionBean obj = null;
		SubmissionUtil util = null;
		
		String audID = headers.getRequestHeader("aud-id").get(0);
		JSONObject bundleDoc = null;
		
		Connection conn = null;
		API_RLS_ESUB_TRX trxPol = new API_RLS_ESUB_TRX();
				
		try {
			conn = DBAccess.getConnection();
			
			//Create Transaction Log into Database
			trxPol.create(audID, headers.toString(), polNum, "P", conn);
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Get policy json file - S
			JSONObject applicationDoc = null;
			JSONObject bundleFNADoc = null;
			JSONObject policyDoc = Constant.CBUTIL.getDoc(polNum);
			
			if (policyDoc == null) {
				return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Policy Number - " + polNum));
				
			//Get policy json file - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			} else {
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Get application json file - S
				if (policyDoc.has("applicationId")) {
					//Get application json file
					applicationDoc = Constant.CBUTIL.getDoc(policyDoc.getString("applicationId"));
					
					if (applicationDoc == null) {
						Log.error("Error - Invalid Application Number - " + policyDoc.getString("applicationId"));
					 			
						return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Application Number - " + policyDoc.getString("id")));
					}
				} else {
					applicationDoc = policyDoc;
					policyDoc = null;
				}
			}
			//Get application json file - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			String bundleID = "";
			String bundleFNAID = "";

			if (applicationDoc != null) {
				bundleID = applicationDoc.getString("bundleId");
				bundleFNAID = bundleID +"-NA";
				
				if (StringUtils.isNotEmpty(bundleID)) {
					try {
						bundleDoc = Constant.CBUTIL.getDoc(bundleID);
					} catch(Exception et) {
						throw new Exception("Failure to get bundle:" + bundleID);
					}
				} else {
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Missing Bundle Number"));
				}
				
				if (StringUtils.isNotEmpty(bundleFNAID)) {
					try {
						bundleFNADoc = Constant.CBUTIL.getDoc(bundleFNAID);
					} catch(Exception et) {
						throw new Exception("Failure to get bundle FNA:" + bundleFNAID);
					}
				} else {
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Missing Bundle FNA Document"));
				}
			}
						
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Get client json file - S
			JSONObject clientDoc = null;
			
			if (applicationDoc.has("iCid")) {
				//Get client json file
				clientDoc = Constant.CBUTIL.getDoc(applicationDoc.getString("iCid"));
				
				if (clientDoc == null) {
					Log.error("Error - Invalid Client Number 1 - " + applicationDoc.getString("iCid"));
				 			
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Client Number 2 - " + policyDoc.getString("id")));
				}
			} else {
				return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Client Number 3 - " + policyDoc.getString("id")));
			}
			//Get client json file - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			JSONObject proposerClientDoc = null;
			
			if (applicationDoc.has("pCid")) {
				//Get client json file
				proposerClientDoc = Constant.CBUTIL.getDoc(applicationDoc.getString("pCid"));
				
				if (proposerClientDoc == null) {
					Log.error("Error - Invalid Proposer Client Number 1 - " + applicationDoc.getString("pCid"));
				 			
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Proposer Client Number 2 - " + policyDoc.getString("id")));
				}
			} else {
				return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Proposer Client Number 3 - " + policyDoc.getString("id")));
			}
			
			obj = new SubmissionBean();
			util = new SubmissionUtil(policyDoc, applicationDoc, clientDoc, bundleDoc, bundleFNADoc, proposerClientDoc);
			gson = new GsonBuilder().setPrettyPrinting().create();
		
			obj = util.setRecordApplicationRequest();
						
			output = RestUtil.toResponse(audID, Response.Status.OK, gson.toJson(obj));
			
			// Update Transaction Log in Database for completed case
			trxPol.update(audID, output.getEntity().toString(), "C", conn);
		} catch(Exception ex) {
			Log.error(ex);
			
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, ex.getMessage()));
			
			try {
				// Update Transaction Log in Database for failed case
				trxPol.update(audID, output.getEntity().toString(), "F", conn);
			} catch(Exception e) {
				Log.error(e);
			}
		} finally {			
			try {
				if (conn != null && !conn.isClosed()) 
					conn.close();
			} catch(Exception e) {
				Log.error(e);
			}
			
			conn = null;
			gson = null;
			obj = null;
			util = null;
		}
		
		return output;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/new/{polNum}")
	public Response submission2(@Context HttpHeaders headers, @PathParam("polNum") String polNum) throws Exception {
		Log.info("submission");
		
		Response output = null;
		Gson gson = null;
		SubBean obj = null;
		SubmissionUtil2 util = null;
		
		String audID = headers.getRequestHeader("aud-id").get(0);
		JSONObject bundleDoc = null;
		
		Connection conn = null;
		API_RLS_ESUB_TRX trxPol = new API_RLS_ESUB_TRX();
				
		try {
			conn = DBAccess.getConnection();
			
			//Create Transaction Log into Database
			trxPol.create(audID, headers.toString(), polNum, "P", conn);
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Get policy json file - S
			JSONObject applicationDoc = null;
			JSONObject bundleFNADoc = null;
			JSONObject policyDoc = Constant.CBUTIL.getDoc(polNum);
			
			if (policyDoc == null) {
				return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Policy Number - " + polNum));
				
			//Get policy json file - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			} else {
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Get application json file - S
				if (policyDoc.has("applicationId")) {
					//Get application json file
					applicationDoc = Constant.CBUTIL.getDoc(policyDoc.getString("applicationId"));
					
					if (applicationDoc == null) {
						Log.error("Error - Invalid Application Number - " + policyDoc.getString("applicationId"));
					 			
						return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Application Number - " + policyDoc.getString("id")));
					}
				} else {
					applicationDoc = policyDoc;
					policyDoc = null;
				}
			}
			//Get application json file - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			String bundleID = "";
			String bundleFNAID = "";

			if (applicationDoc != null) {
				bundleID = applicationDoc.getString("bundleId");
				bundleFNAID = bundleID +"-NA";
				
				if (StringUtils.isNotEmpty(bundleID)) {
					try {
						bundleDoc = Constant.CBUTIL.getDoc(bundleID);
					} catch(Exception et) {
						throw new Exception("Failure to get bundle:" + bundleID);
					}
				} else {
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Missing Bundle Number"));
				}
				
				if (StringUtils.isNotEmpty(bundleFNAID)) {
					try {
						bundleFNADoc = Constant.CBUTIL.getDoc(bundleFNAID);
					} catch(Exception et) {
						throw new Exception("Failure to get bundle FNA:" + bundleFNAID);
					}
				} else {
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Missing Bundle FNA Document"));
				}
			}
						
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Get client json file - S
			JSONObject clientDoc = null;
			
			if (applicationDoc.has("iCid")) {
				//Get client json file
				clientDoc = Constant.CBUTIL.getDoc(applicationDoc.getString("iCid"));
				
				if (clientDoc == null) {
					Log.error("Error - Invalid Client Number 1 - " + applicationDoc.getString("iCid"));
				 			
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Client Number 2 - " + policyDoc.getString("id")));
				}
			} else {
				return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Client Number 3 - " + policyDoc.getString("id")));
			}
			//Get client json file - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			JSONObject proposerClientDoc = null;
			
			if (applicationDoc.has("pCid")) {
				//Get client json file
				proposerClientDoc = Constant.CBUTIL.getDoc(applicationDoc.getString("pCid"));
				
				if (proposerClientDoc == null) {
					Log.error("Error - Invalid Proposer Client Number 1 - " + applicationDoc.getString("pCid"));
				 			
					return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Proposer Client Number 2 - " + policyDoc.getString("id")));
				}
			} else {
				return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Invalid Proposer Client Number 3 - " + policyDoc.getString("id")));
			}
			
			obj = new SubBean();
			util = new SubmissionUtil2(policyDoc, applicationDoc, clientDoc, bundleDoc, bundleFNADoc, proposerClientDoc);
			gson = new GsonBuilder().setPrettyPrinting().create();
		
			obj = util.setRecordApplicationRequest();
						
			output = RestUtil.toResponse(audID, Response.Status.OK, gson.toJson(obj));
			
			// Update Transaction Log in Database for completed case
			trxPol.update(audID, output.getEntity().toString(), "C", conn);
		} catch(Exception ex) {
			Log.error(ex);
			
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, ex.getMessage()));
			
			try {
				// Update Transaction Log in Database for failed case
				trxPol.update(audID, output.getEntity().toString(), "F", conn);
			} catch(Exception e) {
				Log.error(e);
			}
		} finally {			
			try {
				if (conn != null && !conn.isClosed()) 
					conn.close();
			} catch(Exception e) {
				Log.error(e);
			}
			
			conn = null;
			gson = null;
			obj = null;
			util = null;
		}
		
		return output;
	}
}
