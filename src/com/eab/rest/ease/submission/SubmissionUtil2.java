package com.eab.rest.ease.submission;

import com.eab.rest.ease.submission.model.SubBean;
import org.json.JSONObject;
import com.eab.rest.ease.submission.applicationBean.ApplicationBean;

/**
 * The function to retrieve EASE eSubmission Json Object
 */
class SubmissionUtil2 {
    //The following flag set to true during testing, this is to avoid the Json preparation broken
    public static boolean TESTING_FLAG = true;

    private ApplicationBean appBean = new ApplicationBean();

	//SubmissionBean model = new SubmissionBean();

	SubmissionUtil2 (JSONObject _policyDoc, JSONObject _applicationDoc, JSONObject _clientDoc, JSONObject _bundleAppDoc, JSONObject _bundleFNADoc, JSONObject _proposerClientDoc) {
        appBean.policyDoc = _policyDoc;
        appBean.applicationDoc = _applicationDoc;
        appBean.clientDoc = _clientDoc;
        appBean.bundleAppDoc = _bundleAppDoc;
        appBean.bundleFNADoc = _bundleFNADoc;
        appBean.proposerClientDoc = _proposerClientDoc;
	}

	public SubBean setRecordApplicationRequest() throws Exception  {
		ComposeJsonUtil jsonUtil = new ComposeJsonUtil();
        RetrieveApplicationUtil appUtil = new RetrieveApplicationUtil();
	    SubBean subBean = null;

		try {
		    //To fill in the appBean object from Json source data
            appUtil.retreiveApplicationData(this.appBean);
            //Business logic to compose the recordApplication Object with appBean data
            subBean = jsonUtil.prepareSubBean(this.appBean);
		} catch (Exception ex) {			
			throw ex;
		}

		return subBean;
	}

}
