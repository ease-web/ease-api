package com.eab.rest.ease.submission;

import java.util.Date;
import java.util.List;

public class SubmissionBean {

	public RecordApplicationRequestType recordApplicationRequest;
	public String lob;

	public SubmissionBean() {

		this.recordApplicationRequest = new RecordApplicationRequestType();
		//this.lob = "Life";
	}

	public class RecordApplicationRequestType {
		public PolicyType policy;
		//public HasDetailsOfPolicyInItemType hasDetailsOfPolicyInItem;
		
		public RecordApplicationRequestType() {
			this.policy = new PolicyType();	
			//this.hasDetailsOfPolicyInItem = new HasDetailsOfPolicyInItemType();
		}
	}

	public class PolicyType {
		public String applicationCaptureDT;
		public String companyCD;
		public String policyNO;
		public String sourceChannelCD;
		public String policySubmitDT;
		public String applicationSignatureDT;
		public String policyStatusCD;
		public String policyEffectiveDTTM;
		public String medicalIND;
		public String languageCD;
		public String paymentModeCD;
		public String paymentMethodCD;
		public String currencyCD;
		public String minPremiumAMT;
		public String premiumAMT;
		public String singlePremiumPolicyFLG;
		
		
		
		
		

		public String depositAMT;
		public String policyExpirationDTTM;
		public String policyNo_2;
		public String autoSetFLG;
		public String communicationMode;
		public String policySubmitDT1;
		public String initialLumpsumPremiumAMT;
		public String lumpsumPremiumAMT;
		public String topupRegularModalPremium;
		public String lobRk;
		public String applicationSubmitTM;
		public String declineOptionIND;
		public String termConversionFLG;
		public String termConversionPolicyNO;
		public String indexationFLG;
		public String nonForfeitureFLG;
		public String captureUserDeptNM;
		public String captureUserSourceNM;
		public String lastUpdateUserDeptNM;
		public String lastUpdateUserSourceNM;
		public String statusUpdateDeptNM;
		public String statusUpdateSourceNM;
		public List<HasDetailsOfPolicyInItemType> hasDetailsOfPolicyIn;
		public List<HasProducerInformationInItemType> hasProducerInformationIn;
		
		
		
		public List<HasDetailsOfRisksInItemType> hasDetailsOfRisksIn;
		
		
		public List<HasCustomerInformationInItemType> hasCustomerInformationIn;
		
		public List<HasBenificialOwnerDetailsInItemType> HasBenificialOwnerDetailsIn;
		
		
		public List<HasAccountDetailsInItemType> hasAccountDetailsIn;
		
		public List<HasPolicyChargeInfoInItemType> hasPolicyChargeInfoIn;

		public List<HasLifePolicyTransactionDetailsInItemType> hasLifePolicyTransactionDetailsIn;

		public List<ForLifeUnitofExposureItemType> forLifeUnitofExposure;
		public List<IsAssociatedWithLifeInsuranceProductItemType> isAssociatedWithLifeInsuranceProduct;
		public List<HasNotificationsDetailsInItemType> hasNotificationsDetailsIn;

		public List<HasSharedProducerInformationInItemType> hasSharedProducerInformationIn;
		public List<HasDetailsRecordInItemType> hasDetailsRecordIn;
		public List<HasPolicyRemarkDetailsInItemType> hasPolicyRemarkDetailsIn;
		public List<HasCaseUnderwriterDetailsInItemType> hasCaseUnderwriterDetailsIn;
		public List<HasPolicyAssociationDetailsInItemType> hasPolicyAssociationDetailsIn;
		public List<HasAssessmentHeadersInItemType> hasAssessmentHeadersIn;
		public List<HasAXAAsiaPolicyPaymentAccountItemType> hasAXAAsiaPolicyPaymentAccount;
		public List<HasBenificialOwnerDetailsInItemType> hasBenificialOwnerDetailsIn;
		public List<HasAXAAsiaPolicyHeaderPaymentAccountItemType> hasAXAAsiaPolicyHeaderPaymentAccount;
		public List<HasUnderwritingDecisionsInItemType> hasUnderwritingDecisionsIn;
		public List<HasDependentInformationInItemType> hasDependentInformationIn;
		public HasCreditAuthorizationDetailsInType hasCreditAuthorizationDetailsIn;
		public List<HasPreviousPolicyDetailsInItemType> hasPreviousPolicyDetailsIn;

		public String projectionControlRequiredIND;
//		public PolicyType() {
//		}
	}
	
	public HasDetailsOfPolicyInItemType getHasDetailsOfPolicyInItemType (){
		return new HasDetailsOfPolicyInItemType();
	}

	public class HasDetailsOfPolicyInItemType {

		public String dvdoIND;
		public String uwRemark1;
		public String cpfAcctTypeCD;
		public String rtuPaymentMethodCD;
		public String rtuPaymentModeCD;
		public String dboIND;
		public String ownerAgeAtIssue;

		public HasDetailsOfPolicyInItemType() {
		}
	}

	public HasPolicyChargeInfoInItemType getHasPolicyChargeInfoInItemType(){
		return new HasPolicyChargeInfoInItemType();
	}
	
	public class HasPolicyChargeInfoInItemType {

		public String chargeCD;
		public String chargePCT;

		public HasPolicyChargeInfoInItemType() {
		}
	}

	public HasCustomerInformationInItemType getHasCustomerInformationInItemType(){
		return new HasCustomerInformationInItemType();
	}
		
	public class HasCustomerInformationInItemType {

		public String ownerIND;
		public String smsTextableFlg;
		public String dependentNO;
		public String customerID;
		public String familyMembersNO;
		public List<String> prevPolicyNO;
		public String documentID;
		public String documentTypeCD;
		public CanBeIndividualType canBeIndividual;
		public HasRiskRatingInType hasRiskRatingIn;
		public String vulnerableAgeFLG;
		public String vulnerableEducationFLG;

		public HasCustomerInformationInItemType() {
			this.canBeIndividual = new CanBeIndividualType();
			this.hasRiskRatingIn = new HasRiskRatingInType();
		}
	}
	
	public HasLifePolicyTransactionDetailsInItemType getHasLifePolicyTransactionDetailsInItemType(){
		return new HasLifePolicyTransactionDetailsInItemType();
	}

	public class HasLifePolicyTransactionDetailsInItemType {

		public String paymentDueDT;
		public String premiumAMT;
		public String transTypeCD;
		public String transCurrencyCD;
		public String receiptNO;
		public List<HasLifePolicyTransactionHeaderDetailsInItemType> hasLifePolicyTransactionHeaderDetailsIn;
		public List<HasPaymentDetailsInItemType> hasPaymentDetailsIn;

		public HasLifePolicyTransactionDetailsInItemType() {
		}
	}
	
	public HasDetailsOfRisksInItemType getHasDetailsOfRisksInItemType(){
		return new HasDetailsOfRisksInItemType();
	}
	
	public class HasDetailsOfRisksInItemType {

		public String issueAge;
		public String relationshipToOwnerCD;
		public List<String> insuredPrevPolicyNO;
		public List<HasPersonalDetailsInItemType> hasPersonalDetailsIn;
		public HasUndewritingResultsInType hasUndewritingResultsIn;

		public HasDetailsOfRisksInItemType() {
			this.hasUndewritingResultsIn = new HasUndewritingResultsInType();
		}
	}
	
	public ForLifeUnitofExposureItemType getForLifeUnitofExposureItemType(){
		return new ForLifeUnitofExposureItemType();
	}

	public class ForLifeUnitofExposureItemType {

		public String basicRegularPremiumAMT;
		public String currAMT;
		public String basicPremiumRt;
		public String loadingPremiumRt;
		public String tempLoadingRT;
		public Date tempExtraEndDt;
		public String tempLoadingYrCNT;
		public String referenceTO;
		public String packagedTO;
		public HasCoverageDetailsInType hasCoverageDetailsIn;
		public List<HasBeneficiaryInformationInItemType> hasBeneficiaryInformationIn;

		public ForLifeUnitofExposureItemType() {
		}
	}
	
	public IsAssociatedWithLifeInsuranceProductItemType getIsAssociatedWithLifeInsuranceProductItemType(){
		return new IsAssociatedWithLifeInsuranceProductItemType();
	}

	public class IsAssociatedWithLifeInsuranceProductItemType {

		public String insuranceProductCd;
		public String planClassCD;

		public IsAssociatedWithLifeInsuranceProductItemType() {
		}
	}

	public class HasNotificationsDetailsInItemType {

		public String recipientTypeCD;
		public String recipientEmailAddress;

		public HasNotificationsDetailsInItemType() {
		}
	}
	
	
	public HasProducerInformationInItemType getHasProducerInformationInItemType (){
		return new HasProducerInformationInItemType();
	}
	
	public class HasProducerInformationInItemType {

		public String producerID;
		public List<HasAssociationWithItemType> hasAssociationWith;

		public HasProducerInformationInItemType() {
		}
	}

	public class HasSharedProducerInformationInItemType {

		public String producerID;

		public HasSharedProducerInformationInItemType() {
		}
	}
	
	public HasDetailsRecordInItemType getHasDetailsRecordInItemType(){
		return new HasDetailsRecordInItemType();
	}

	public class HasDetailsRecordInItemType {
		public List<HasAccountDetailsInItemType> hasAccountDetailsIn;

		public HasDetailsRecordInItemType() {
		}
	}

	public class HasPolicyRemarkDetailsInItemType {

		public String lineNO;
		public String remarks;

		public HasPolicyRemarkDetailsInItemType() {
		}
	}

	public class HasCaseUnderwriterDetailsInItemType {

		public String notificationTypeCD;
		public String referenceNO;
		public String remarkCD;
		public String remarkDESC;
		public String remarkDESC2;
		public String remarkDESC3;
		public String deptCD;
		public String assessorUserID;
		public Date lastUpdateDT;
		public Date lastUpdateUserID;
		public Date completionDT;
		public String completionStatusCD;
		public Date remarkIssueDT;

		public HasCaseUnderwriterDetailsInItemType() {
		}
	}

	public HasPolicyAssociationDetailsInItemType getHasPolicyAssociationDetailsInItemType(){
		return new HasPolicyAssociationDetailsInItemType();
	}
	
	public class HasPolicyAssociationDetailsInItemType {

		public String linkedPlanCode;
		public String assocPolicyStatusCD;

		public HasPolicyAssociationDetailsInItemType() {
		}
	}
	
	public HasAssessmentHeadersInItemType getHasAssessmentHeadersInItemType(){
		return new HasAssessmentHeadersInItemType();
	}

	public class HasAssessmentHeadersInItemType {

		public String referenceNO;
		public List<HasAssessmentDetailsInItemType> hasAssessmentDetailsIn;

		public HasAssessmentHeadersInItemType() {
		}
	}

	public HasAXAAsiaPolicyPaymentAccountItemType getHasAXAAsiaPolicyPaymentAccountItemType(){
		return new HasAXAAsiaPolicyPaymentAccountItemType();
	}
	
	public class HasAXAAsiaPolicyPaymentAccountItemType {

		public String paymentTypeCD;
		public String statusCD;
		public String bankNO;
		public String branchNO;
		public String validFromDTTM;
		public String factoringHouseCD;
		public String bankIssuerNM;
		public String authProcessDT;
		public String sectionCD;
		public String modDT;
		public String paymentMethodChangeFLG;
		public List<HasAXAAsiaPartyAccountItemType> hasAXAAsiaPartyAccount;

		public HasAXAAsiaPolicyPaymentAccountItemType() {
		}
	}

	public HasBenificialOwnerDetailsInItemType getHasBenificialOwnerDetailsInItemType(){
		return new HasBenificialOwnerDetailsInItemType();
	}
	
	public class HasBenificialOwnerDetailsInItemType {

		public String ownershipFLG;
		public String sharesPCT;
		public String insurableInterestAMT;
		public String monthlyIncomeAMT;
		public String sourceFundDTL;
		public String additionalIncomeAMT;
		public String purposeDESC;
		public String totMonAddIncmAMT;
		public String positionDESC;
		public String registrationLetterTXT;
		public String assigneeLastNM;
		public String assigneeFirstNM;
		public String powerAttorneyDESC;
		public String domRegistrationNO;
		public String comRegistrationNO;
		public String deedEstdDT;
		public Date decreeLetterDT;
		public String comWebsiteTXT;
		public String businessEntityNM;
		public String totalComAssetAMT;
		public String relationshipToInsuredCD;
		public List<CanBeIndividualItemType> canBeIndividual;

		public HasBenificialOwnerDetailsInItemType() {
		}
	}
	
	public HasAXAAsiaPolicyHeaderPaymentAccountItemType getHasAXAAsiaPolicyHeaderPaymentAccountItemType(){
		return new HasAXAAsiaPolicyHeaderPaymentAccountItemType();
	}

	public class HasAXAAsiaPolicyHeaderPaymentAccountItemType {

		public String transCD;
		public String transDetailCD;
		public String detailTransactionDESC;

		public HasAXAAsiaPolicyHeaderPaymentAccountItemType() {
		}
	}

	public class HasUnderwritingDecisionsInItemType {

		public String underwritingRemarks;
		public Date underwritingDate;

		public HasUnderwritingDecisionsInItemType() {
		}
	}

	public class HasDependentInformationInItemType {

		public String dependentNO;
		public String planCD;
		public String classCD;
		public String premiumAMT;
		public String offerCD;
		public List<String> prevPolicyNO;
		public String familyMembersNO;
		public List<CanBeIndividualItemType> canBeIndividual;

		public HasDependentInformationInItemType() {
		}
	}

	public class HasCreditAuthorizationDetailsInType {

		public Date caEffectiveDT;
		public String status;
		public String accountHolderFullName;
		public String bankID;
		public String branchCD;
		public String accountNO;
		public String limit;
		public String accountBankCD;
		public Date processDT;
		public String subTransactionNO;

		public HasCreditAuthorizationDetailsInType() {
		}
	}

	public class HasPreviousPolicyDetailsInItemType {

		public String insuredIND;
		public String prevInsuredIND;
		public String dependentNO;
		public String prevPolicyNO;
		public String lifeNO;

		public HasPreviousPolicyDetailsInItemType() {
		}
	}

	public CanBeIndividualType getCanBeIndividualType(){
		return new CanBeIndividualType();
	}			

	public class CanBeIndividualType {

		public String height;
		public String weight;
		public String annualIncomeAMT;
		public String firstNM;
		public String lastNM;
		public String genderCD;
		public String maritalStatusCD;
		public String birthDT;
		public String countryOfResidenceCD;
		public String documentID;
		public String checkDocumentID;
		public String stdOccupationCD;
		public String industryCD;
		public String occupationClassCd;
		public String citizenCD;
		public String religionCD;
		public String countryOfOriginDESC;
		public String nationality;
		public String occupation;
		public String placeOfBirth;
		public String ckaEducationFLG;
		public String ckaInvestmentExperienceFLG;
		public String ckaWorkExperienceFLG;
		public String ckaFLG;
		public String vulnerableAgeFLG;
		public String vulnerableEducationFLG;		
		public List<HasIndividualUWInfoInItemType> hasIndividualUWInfoIn;
		public List<HasAddressesInItemType> hasAddressesIn;
		public HasCRSInformationInType hasCRSInformationIn;

		public CanBeIndividualType() {
			this.hasCRSInformationIn = new HasCRSInformationInType();
		}
	}

	public class HasRiskRatingInType {

		public String productRiskRatingLVL;
		public String customerRiskRatingLVL;
		public String geographicalRiskRatingLVL;
		public String regulatoryRiskRatingLVL;

		public HasRiskRatingInType() {
		}
	}
	
	public HasLifePolicyTransactionHeaderDetailsInItemType getHasLifePolicyTransactionHeaderDetailsInItemType(){
		return new HasLifePolicyTransactionHeaderDetailsInItemType();
	}

	public class HasLifePolicyTransactionHeaderDetailsInItemType {

		public String creditCardNO;
		public String bankAccountNO;
		public String cardExpirationDT;
		public String authenticationCD;
		public String paymentRemarks;
		public String cbrDT;
		public String bankCD;
		public String branchCD;
		public String chequeNO;
		public String chequeDT;
		public String bankFeeAMT;
		public String ccLocalAMT;
		public String amalgatedAccountNO;

		public HasLifePolicyTransactionHeaderDetailsInItemType() {
		}
	}

	public class HasPaymentDetailsInItemType {

		public String serviceFromIND;

		public HasPaymentDetailsInItemType() {
		}
	}

	public HasPersonalDetailsInItemType getHasPersonalDetailsInItemType(){
		return new HasPersonalDetailsInItemType();
	}
	
	public HasAddressesInItemType getHasAddressesInItemType(){
		return new HasAddressesInItemType();
	}
	
	public class HasPersonalDetailsInItemType {

		public String stdBusinessCD;
		public String stdOccupationCD;
		public String industryCD;
		public String occupationClassCd;
		public String citizenCD;
		public String religionCD;
		public String firstNM;
		public String lastNM;
		public String genderCD;
		public String documentTypeCD;
		public String documentID;
		public String birthDT;
		public String maritalStatusCD;
		public String countryOfResidenceCD;
		public String height;
		public String weight;
		public String annualIncomeAMT;
		public String externalShieldIND;
		public String takeOverBusinessIND;
		public String projectionStatusIND;		
		public List<HasAddressesInItemType> hasAddressesIn;
		public List<HasIndividualUWInfoInItemType> hasIndividualUWInfoIn;

		public HasPersonalDetailsInItemType() {
		}
	}
	
	public HasUndewritingResultsInType getHasUndewritingResultsInType(){
		return new HasUndewritingResultsInType();
	}

	public class HasUndewritingResultsInType {

		public String insurableInterestRT;
		public String signatureConsistanatFLG;
		public String salesIllustrationFLG;
		public String capturedID;
		public String financialUWResultFLG;
		public String largeCaseIND;
		public String largeCaseAMT;
		public String amlSubjectPlanDESC;
		public String caseAcceptanceFLG;
		public List<String> previousSubStdPNO;
		public List<String> remarksDESC;
		public String sourceID;
		public String splitCommissionAMT;

		public HasUndewritingResultsInType() {
		}
	}
	
	public HasCoverageDetailsInType getHasCoverageDetailsInType(){
		return new HasCoverageDetailsInType();
	}

	public class HasCoverageDetailsInType {

		public String baseCoverageIndicatorCd;
		public List<IsAssociatedWithLifeInsuranceProductItemType> isAssociatedWithLifeInsuranceProduct;

		public HasCoverageDetailsInType() {
		}
	}

	public class HasBeneficiaryInformationInItemType {

		public String beneficiaryFLG;
		public List<CanBeIndividualItemType> canBeIndividual;
		public List<HasBeneficiaryXUoeDetailsInItemType> hasBeneficiaryXUoeDetailsIn;

		public HasBeneficiaryInformationInItemType() {
		}
	}

	public HasAssociationWithItemType getHasAssociationWithItemType(){
		return new HasAssociationWithItemType();
	}
	
	public class HasAssociationWithItemType {

		public String bankBranchCD;
		public String stateRegionCD;
		public String areaCD;
		public String bankCD;
		public List<HasStaffInformationInItemType> hasStaffInformationIn;

		public HasAssociationWithItemType() {
		}
	}
	
	
	public HasAccountDetailsInItemType getHasAccountDetailsInItemType(){
		return new HasAccountDetailsInItemType();
	}

	
	public class HasAccountDetailsInItemType {

		public String allocPct;
		public String lumpsumAllocPCT;
		
		public List<HasInvestmentFundDetailsInItemType> hasInvestmentFundDetailsIn;

		public HasAccountDetailsInItemType() {
		}
	}
	
	
	public HasInvestmentFundDetailsInItemType getHasInvestmentFundDetailsInItemType(){
		return new HasInvestmentFundDetailsInItemType();
	}

	public class HasInvestmentFundDetailsInItemType {

		public String investmentFundCd;

		public HasInvestmentFundDetailsInItemType() {
		}
	}
	
	public HasAssessmentDetailsInItemType getHasAssessmentDetailsInItemType(){
		return new HasAssessmentDetailsInItemType();
	}

	public class HasAssessmentDetailsInItemType {

		public String lineNO;
		public String assessmentDetails;

		public HasAssessmentDetailsInItemType() {
		}
	}

	public HasAXAAsiaPartyAccountItemType getHasAXAAsiaPartyAccountItemType(){
		return new HasAXAAsiaPartyAccountItemType();
	}
	
	public class HasAXAAsiaPartyAccountItemType {

		public String payerID;
		public String mandateStatusCD;
		public String payorLastNM;
		public String accountNO;
		public String creditCardNO;
		public String cardExpirationDT;
		public String accountBankCD;
		public String payorFirstNM;
		public String branchCD;
		public String cpfBankEffectiveDT;
		public String cpfBankAbbr;
		public String cpfINVAccountNO;
		public String bankID;
		public String status;
		public String processDT;
		public String amalgametedAccountNO;
		public String accountTypeCD;

		public HasAXAAsiaPartyAccountItemType() {
		}
	}

	public class CanBeIndividualItemType {

		public String height;
		public String weight;
		public String annualIncomeAMT;
		public String firstNM;
		public String lastNM;
		public String chineseNM;
		public String genderCD;
		public String maritalStatusCD;
		public String birthDT;	
		public String countryOfResidenceCD;
		public String passportNo;
		public String ownershipFLG;
		public String documentID;
		public String checkDocumentID;
		public String stdOccupationCD;
		public String industryCD;
		public String occupationClassCd;
		public String citizenCD;
		public String religionCD;
		public String countryOfOriginDESC;
		public String nationality;
		public String occupation;
		public String placeOfBirth;
		public List<HasIndividualUWInfoInItemType> hasIndividualUWInfoIn;
		public List<HasAddressesInItemType> hasAddressesIn;
	//	public HasCRSInformationInType hasCRSInformationIn;

		public CanBeIndividualItemType() {
	//		this.hasCRSInformationIn = new HasCRSInformationInType();
		}
	}
	

	public CanBeIndividualItemType getCanBeIndividualItemType(){
		return new CanBeIndividualItemType();
	}
	
	public HasIndividualUWInfoInItemType getHasIndividualUWInfoInItemType(){
		return new HasIndividualUWInfoInItemType();
	}

	public class HasIndividualUWInfoInItemType {

		public String declineQuestionIND;
		public String avocationQuestionIND;
		public String healthQuestionIND;
		public String previousHealthExamQuestionIND;
		public String previousHealthExamReasonIND;
		public String previousHealthExamResultIND;
		public String alcoholQuestionIND;
		public String alcoholUnitsPerWeekNO;
		public String tobaccoQuestionIND;
		public String tobaccoUnitsPerDayNO;
		public String smokingQuestionIND;
		public String cigarettesPerDayNO;
		public List<String> previousSubStdPNO;
		public String ePolicyDeliveryFLG;
		public String customerProtectionDeclarationIND;
		public String agentReportIND;
		public String residenceDeclCD;
		public String trustDeclarationIND;
		public String diQuestionIND;
		public String dobProofIND;
		public String previousSumInsured;
		
		public HasIndividualUWInfoInItemType() {
		}

		public HasIndividualUWInfoInItemType(String declineQuestionIND, String avocationQuestionIND,
				String healthQuestionIND, String previousHealthExamQuestionIND, String previousHealthExamReasonIND,
				String previousHealthExamResultIND, String alcoholQuestionIND, String alcoholUnitsPerWeekNO,
				String tobaccoQuestionIND, String tobaccoUnitsPerDayNO, String smokingQuestionIND,
				String cigarettesPerDayNO, List<String> previousSubStdPNO, String ePolicyDeliveryFLG, 
				String diQuestionIND, String dobProofIND, String previousSumInsured) {

			this.declineQuestionIND = declineQuestionIND;
			this.avocationQuestionIND = avocationQuestionIND;
			this.healthQuestionIND = healthQuestionIND;
			this.previousHealthExamQuestionIND = previousHealthExamQuestionIND;
			this.previousHealthExamReasonIND = previousHealthExamReasonIND;
			this.previousHealthExamResultIND = previousHealthExamResultIND;
			this.alcoholQuestionIND = alcoholQuestionIND;
			this.alcoholUnitsPerWeekNO = alcoholUnitsPerWeekNO;
			this.tobaccoQuestionIND = tobaccoQuestionIND;
			this.tobaccoUnitsPerDayNO = tobaccoUnitsPerDayNO;
			this.smokingQuestionIND = smokingQuestionIND;
			this.cigarettesPerDayNO = cigarettesPerDayNO;
			this.previousSubStdPNO = previousSubStdPNO;
			this.ePolicyDeliveryFLG = ePolicyDeliveryFLG;
			this.diQuestionIND = diQuestionIND;
			this.dobProofIND = dobProofIND;
			this.previousSumInsured = previousSumInsured;
		}

		public String getDeclineQuestionIND() {
			return declineQuestionIND;
		}

		public String getAvocationQuestionIND() {
			return avocationQuestionIND;
		}

		public String getHealthQuestionIND() {
			return healthQuestionIND;
		}

		public String getPreviousHealthExamQuestionIND() {
			return previousHealthExamQuestionIND;
		}

		public String getPreviousHealthExamReasonIND() {
			return previousHealthExamReasonIND;
		}

		public String getPreviousHealthExamResultIND() {
			return previousHealthExamResultIND;
		}

		public String getAlcoholQuestionIND() {
			return alcoholQuestionIND;
		}

		public String getAlcoholUnitsPerWeekNO() {
			return alcoholUnitsPerWeekNO;
		}

		public String getTobaccoQuestionIND() {
			return tobaccoQuestionIND;
		}

		public String getTobaccoUnitsPerDayNO() {
			return tobaccoUnitsPerDayNO;
		}

		public String getSmokingQuestionIND() {
			return smokingQuestionIND;
		}

		public String getCigarettesPerDayNO() {
			return cigarettesPerDayNO;
		}

		public List<String> getPreviousSubStdPNO() {
			return previousSubStdPNO;
		}

		public String getEPolicyDeliveryFLG() {
			return ePolicyDeliveryFLG;
		}
		
		public String getDiQuestionIND() {
			return diQuestionIND;
		}
		
		public String getDobProofIND() {
			return dobProofIND;
		}
		
		public String getPreviousSumInsured() {
			return previousSumInsured;
		}

		public void setDeclineQuestionIND(String declineQuestionIND) {
			this.declineQuestionIND = declineQuestionIND;
		}

		public void setAvocationQuestionIND(String avocationQuestionIND) {
			this.avocationQuestionIND = avocationQuestionIND;
		}

		public void setHealthQuestionIND(String healthQuestionIND) {
			this.healthQuestionIND = healthQuestionIND;
		}

		public void setPreviousHealthExamQuestionIND(String previousHealthExamQuestionIND) {
			this.previousHealthExamQuestionIND = previousHealthExamQuestionIND;
		}

		public void setPreviousHealthExamReasonIND(String previousHealthExamReasonIND) {
			this.previousHealthExamReasonIND = previousHealthExamReasonIND;
		}

		public void setPreviousHealthExamResultIND(String previousHealthExamResultIND) {
			this.previousHealthExamResultIND = previousHealthExamResultIND;
		}

		public void setAlcoholQuestionIND(String alcoholQuestionIND) {
			this.alcoholQuestionIND = alcoholQuestionIND;
		}

		public void setAlcoholUnitsPerWeekNO(String alcoholUnitsPerWeekNO) {
			this.alcoholUnitsPerWeekNO = alcoholUnitsPerWeekNO;
		}

		public void setTobaccoQuestionIND(String tobaccoQuestionIND) {
			this.tobaccoQuestionIND = tobaccoQuestionIND;
		}

		public void setTobaccoUnitsPerDayNO(String tobaccoUnitsPerDayNO) {
			this.tobaccoUnitsPerDayNO = tobaccoUnitsPerDayNO;
		}

		public void setSmokingQuestionIND(String smokingQuestionIND) {
			this.smokingQuestionIND = smokingQuestionIND;
		}

		public void setCigarettesPerDayNO(String cigarettesPerDayNO) {
			this.cigarettesPerDayNO = cigarettesPerDayNO;
		}

		public void setPreviousSubStdPNO(List<String> previousSubStdPNO) {
			this.previousSubStdPNO = previousSubStdPNO;
		}

		public void setEPolicyDeliveryFLG(String ePolicyDeliveryFLG) {
			this.ePolicyDeliveryFLG = ePolicyDeliveryFLG;
		}
		
		public void setDiQuestionIND(String diQuestionIND) {
			this.diQuestionIND = diQuestionIND;
		}
		
		public void setDobProofIND(String dobProofIND) {
			this.dobProofIND = dobProofIND;
		}
		
		public void setPreviousSumInsured(String previousSumInsured) {
			this.previousSumInsured = previousSumInsured;
		}
	}
	
	public class HasAddressesInItemType {

		public String officeTelNo;
		public String addressLine1;
		public String addressLine2;
		public String addressLine3;
		public String addressLine4;
		public String cityNM;
		public String countryCD;
		public String stateRegionCd;
		public String telephoneNO;
		public String emailAddress;
		public String mobilePhoneNO;
		public String addressTypeInfoCD;
		public String addressTypeCD;
		public String postalCD;
		public String mailingStatusCD;
		public String phoneStatusCd;
		public String emailStatusCD;
		public String phoneTypeInfoCD;
		public String emailTypeInfoCD;

		public HasAddressesInItemType() {
		}
	}
	
	public class HasCRSInformationInType {

		public String CRSType;
		public String followUpIndicator;
		public List<String> taxIdentificationNO;
		public List<String> countryOfTaxResidence;
		public String remarks;
		public String entityFlag;
		public String CRSStatus;
		public String FATCAStatus;
		public String entityType;
		
		
		

		public HasCRSInformationInType() {
		}
	}
	
	public HasCRSInformationInType getHasCRSInformationInType (){
		return new HasCRSInformationInType();
	}

	public class HasBeneficiaryXUoeDetailsInItemType {

		public String relationshipToInsuredCD;
		public String benefitDistributionPCT;
		public String beneficiaryTypeCD;

		public HasBeneficiaryXUoeDetailsInItemType() {
		}
	}

	public HasStaffInformationInItemType getHasStaffInformationInItemType (){
		return new HasStaffInformationInItemType();
	}
	
	public class HasStaffInformationInItemType {

		public String channelID;
		public String staffCD;
		public String staffIND;
		public List<HasBankReferrerInfoInItemType> hasBankReferrerInfoIn;

		public HasStaffInformationInItemType() {
		}
	}
	
	public HasBankReferrerInfoInItemType getHasBankReferrerInfoInItemType (){
		return new HasBankReferrerInfoInItemType();
	}

	public class HasBankReferrerInfoInItemType {

		public String leadID;
		public String leadCategoryCD;

		public HasBankReferrerInfoInItemType() {
		}
	}
	
}
