package com.eab.rest.ease.submission.applicationBean;

import com.eab.rest.ease.submission.AgeCrossDateCal;
import com.eab.rest.ease.submission.PaymentCodes;
import com.eab.rest.ease.submission.PolicyRemarks;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class posse the source data of Application including Application, Quotation, Client, Approval Object
 */
public class ApplicationBean {

    //Application JSON Object Document ID
    public String appId = "";

    //Other Logical Class to possess data
    public PolicyRemarks Pol_remarks = new PolicyRemarks();
    public PaymentCodes initialPayment = new PaymentCodes();
    public AgeCrossDateCal ageCrossDateCal = new AgeCrossDateCal();

    //JSON Object as initial Setup
    public JSONObject policyDoc = new JSONObject();
    public JSONObject applicationDoc = new JSONObject();
    public JSONObject clientDoc = new JSONObject();
    public JSONObject bundleAppDoc = new JSONObject();
    public JSONObject bundleFNADoc = new JSONObject();
    public JSONObject proposerClientDoc = new JSONObject();


    //breakdown Json Object derived from ApplicationDoc
    public JSONObject payment = new JSONObject();
    public JSONObject quotation = new JSONObject();
    public JSONObject quotation_agent = new JSONObject();
    public JSONObject quotation_policyOptions = new JSONObject();
    public JSONArray quotation_plans = new JSONArray();
    public JSONObject quotation_basicPlan = new JSONObject();
    public JSONObject quotation_RiderPlan = new JSONObject();


    public JSONObject appForm = new JSONObject();
    public JSONObject appForm_basicValues = new JSONObject();
    public JSONObject appForm_basicValues_planDetails = new JSONObject();

    public JSONArray applicationForm_values_insured = new JSONArray();
    public JSONObject applicationForm_values_planDetails = new JSONObject();

    public JSONArray app_plans = new JSONArray();
    public JSONObject appForm_BasicPlan = new JSONObject();

    //Personal Information
    public JSONObject LifeAssured = null;
    public JSONObject LifeAssured_personalInfo = null;
    public JSONObject LifeAssured_residency = null;
    public JSONObject LifeAssured_insurability = null;
    public JSONObject LifeAssured_policies= null;
    public JSONObject LifeAssured_declaration= null;

    public JSONObject Owner = null;
    public JSONObject Owner_personalInfo = null;
    public JSONObject Owner_residency = null;
    public JSONObject Owner_insurability = null;
    public JSONObject Owner_policies= null;
    public JSONObject Owner_declaration= null;
    public JSONObject Owner_personalInfo_branchInfo = null;
    public JSONObject proposer_dependant = null;
    public JSONArray  LifeAssured_personalInfo_dependants = null;
    public JSONObject CPFpaymentBlockNM = null;

    public String ownerIND = "";

    //Application Status
    public String applicationStatus = null;
    public String approvalStatus = null;
    public boolean isFirstParty = false;
    public boolean isThirdParty = false;
    public boolean isSameAnswer = false;
    public String riskCommenceDate = "";

    //Sub Status of Application
    public boolean isINVALIDATED = false;
    public boolean isAPPROVE_REJECT = false;
    public boolean isAPPROVE = false;
    public boolean isREJECT = false;
    public boolean isEXPIRED = false;

    //Date and Time of Application Event
    public String invalidatedDate = null;
    public String approveRejectDate = null;
    public String expiredDate = null;
    public String signatureDate = "";
    public String approveDate = "";
    public String policySubmitDT = "";
    
    public String invalidatedTime = null;
    public String approveRejectTime = null;
    public String expiredTime = null;
    public String signatureTime = "";
    
    public boolean hasFundReqDate = false;
    
    //Product Information
    public boolean isInspireProduct = false;
    public boolean isBandAid = false;
    public boolean hasFundCharge = false;
    
    //Payment Information
    public String basicPlanCode = "";
    public String paymentMethod  = null;
    public boolean isCreditCardPayment = false;
    public boolean isPaid = false;
    public boolean isCashPayment = false;
    public boolean isCheqPayment = false;
    public boolean isCPFPayment = false;
    public String paymentType		= "INIT";
    //public String parm_PlanCD			= "";
    //public String parm_PaymentMethod	= "";
    public String paymentMode		= "M";
    public String useGiroInd			= "N";
    //public String Parm_paymode = "";
    public String payorPaymentMethodCode = "";
    public boolean withCPFAccount = false;
    public String paymentModeCodeMapped = "";  //when a field end with mapped, it is convert to EIP format already
    public String bankNamePickerFieldName = "";
    public String investmentAcctNoFieldName = "";
    public String singlePremiumPolicyFLG = "";

    //address indicator
    public boolean ismAddrdifferent = false;

    //medical indicator
    public boolean isPolicyPMED = false;
    public boolean isLAPMED = false;
    public boolean isOWPMED = false;

    //Singpost
    public boolean isSINGPOST = false;

	public String[] medicalQNString = {
	"LIFESTYLE03","LIFESTYLE04","FAMILY01","HEALTH01",
	"HEALTH02", "HEALTH03", "HEALTH04", 
	"HEALTH05", "HEALTH06", "HEALTH07", 
	"HEALTH08", "HEALTH09" , "HEALTH10", 
	"HEALTH11", "HEALTH12" , "HEALTH13", 
	"HEALTH14", "HEALTH15" , "HEALTH16", 
	"HEALTH17", "HEALTH_GIO01" , "HEALTH_GIO02","HEALTH_GIO03",
	"INS02", 
	};


    public ClientBean la = new ClientBean();
    public ClientBean ow = new ClientBean();
    
    public String bankName = "";
    public boolean hasPPEPS = false; //hasPPEPS = Rider = Smart Payer PremiumEraser Plus
    
}
