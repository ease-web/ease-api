package com.eab.rest.ease.submission;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.eab.common.Log;

public class AgeCrossDateCal {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@SuppressWarnings("deprecation")
	public String getAgeXDate(String commDateStr, String dobStr, boolean getLastBD) {
		Calendar commDate = null;  // Backdate
		Calendar dob = null;      // Date Of Birth
		Calendar axDate = null;  //
		Calendar cyDate = null; // G12
		Calendar lastBirthDT = null; 	// G13
		Calendar nextBirthDT = null; // G14

		String[] commDateParts = null;
		String[] dobParts = null;

		try {
			if (commDateStr != null && dobStr != null) {
				commDateParts = commDateStr.split("-");
				dobParts = dobStr.split("-");

				// Assume Date String must be YYYY-MM-DD
				commDate = new GregorianCalendar(Integer.parseInt(commDateParts[0]), Integer.parseInt(commDateParts[1])-1, Integer.parseInt(commDateParts[2]));
				dob = new GregorianCalendar(Integer.parseInt(dobParts[0]), Integer.parseInt(dobParts[1])-1, Integer.parseInt(dobParts[2]));

				Log.debug("Comm Date: " + sdf.format(commDate.getTime()));
				Log.debug("Birth Date: " + sdf.format(dob.getTime()));

				cyDate = new GregorianCalendar(commDate.get(Calendar.YEAR), dob.get(Calendar.MONTH), dob.get(Calendar.DATE));
				Log.debug("CY Date: " + sdf.format(cyDate.getTime()));

				Log.debug("Compare Comm Date ==? CY Date: " + commDate.compareTo(cyDate));
				if (commDate.compareTo(cyDate) >= 0) {
					lastBirthDT = (Calendar) cyDate.clone();
				} else {
					lastBirthDT = new GregorianCalendar(commDate.get(Calendar.YEAR) - 1, dob.get(Calendar.MONTH), dob.get(Calendar.DATE));
				}
				Log.debug("Last Birthd Date: " + sdf.format(lastBirthDT.getTime()));

				if (commDate.compareTo(cyDate) >= 0) {
					nextBirthDT = new GregorianCalendar(commDate.get(Calendar.YEAR) + 1, dob.get(Calendar.MONTH), dob.get(Calendar.DATE));
				} else {
					nextBirthDT = (Calendar) cyDate.clone();
				}
				Log.debug("Next Birthd Date: " + sdf.format(nextBirthDT.getTime()));

				axDate = Calendar.getInstance();
				axDate.setTimeInMillis((lastBirthDT.getTimeInMillis() + nextBirthDT.getTimeInMillis() ) / 2 );
				Log.debug("Age Crossed Date: " + sdf.format(axDate.getTime()));
			}

			if (getLastBD) {
				if (lastBirthDT != null)
					return sdf.format(lastBirthDT.getTime());
			} else {
				if (axDate != null)
					return sdf.format(axDate.getTime());
			}
		} catch (Exception e) {
			Log.error(e);
		}

		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static int calAgeNearestBirthday(String inputDate, String dobStr) {
		Calendar commDate = null;  // Backdate
		Calendar dob = null;      // Date Of Birth
		Calendar axDate = null;  //
		Calendar cyDate = null; // G12
		Calendar lastBirthDT = null; 	// G13
		Calendar nextBirthDT = null; // G14

		String[] commDateParts = null;
		String[] dobParts = null;

		int anb =  -1;

		try {
			if (inputDate != null && dobStr != null) {
				commDateParts = inputDate.split("-");
				dobParts = dobStr.split("-");

				// Assume Date String must be YYYY-MM-DD
				commDate = new GregorianCalendar(Integer.parseInt(commDateParts[0]), Integer.parseInt(commDateParts[1])-1, Integer.parseInt(commDateParts[2]));
				dob = new GregorianCalendar(Integer.parseInt(dobParts[0]), Integer.parseInt(dobParts[1])-1, Integer.parseInt(dobParts[2]));
				
				cyDate = new GregorianCalendar(commDate.get(Calendar.YEAR), dob.get(Calendar.MONTH), dob.get(Calendar.DATE));
				Log.debug("CY Date: " + sdf.format(cyDate.getTime()));

				Log.debug("Compare Comm Date ==? CY Date: " + commDate.compareTo(cyDate));
				if (commDate.compareTo(cyDate) >= 0) {
					lastBirthDT = (Calendar) cyDate.clone();
				} else {
					lastBirthDT = new GregorianCalendar(commDate.get(Calendar.YEAR) - 1, dob.get(Calendar.MONTH), dob.get(Calendar.DATE));
				}
				Log.debug("Last Birthd Date: " + sdf.format(lastBirthDT.getTime()));

				if (commDate.compareTo(cyDate) >= 0) {
					nextBirthDT = new GregorianCalendar(commDate.get(Calendar.YEAR) + 1, dob.get(Calendar.MONTH), dob.get(Calendar.DATE));
				} else {
					nextBirthDT = (Calendar) cyDate.clone();
				}
				Log.debug("Next Birthd Date: " + sdf.format(nextBirthDT.getTime()));

				axDate = Calendar.getInstance();
				axDate.setTimeInMillis((lastBirthDT.getTimeInMillis() + nextBirthDT.getTimeInMillis() ) / 2 );
				

				
				Log.debug("Age Crossed Date: " + sdf.format(axDate.getTime()));
			}
			
			if (commDate.compareTo(axDate) > 0)
				anb = nextBirthDT.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
			else
				anb = lastBirthDT.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

			return anb;

		} catch (Exception e) {
			Log.error(e);
		}

		return -1;
	}
}
