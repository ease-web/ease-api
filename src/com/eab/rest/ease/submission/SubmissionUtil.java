package com.eab.rest.ease.submission;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.rest.ease.submission.SubmissionBean.CanBeIndividualItemType;
import com.eab.rest.ease.submission.SubmissionBean.CanBeIndividualType;
import com.eab.rest.ease.submission.SubmissionBean.ForLifeUnitofExposureItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAXAAsiaPartyAccountItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAXAAsiaPolicyHeaderPaymentAccountItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAXAAsiaPolicyPaymentAccountItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAccountDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAddressesInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAssessmentDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAssessmentHeadersInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasAssociationWithItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasBankReferrerInfoInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasBenificialOwnerDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasCRSInformationInType;
import com.eab.rest.ease.submission.SubmissionBean.HasCoverageDetailsInType;
import com.eab.rest.ease.submission.SubmissionBean.HasCustomerInformationInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasDetailsOfPolicyInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasDetailsOfRisksInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasDetailsRecordInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasIndividualUWInfoInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasInvestmentFundDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasLifePolicyTransactionDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasLifePolicyTransactionHeaderDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasPersonalDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasPolicyAssociationDetailsInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasPolicyChargeInfoInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasProducerInformationInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasStaffInformationInItemType;
import com.eab.rest.ease.submission.SubmissionBean.HasUndewritingResultsInType;
import com.eab.rest.ease.submission.SubmissionBean.IsAssociatedWithLifeInsuranceProductItemType;

public class SubmissionUtil {

	JSONObject policyDoc = new JSONObject();
	JSONObject applicationDoc = new JSONObject();
	JSONObject clientDoc = new JSONObject();
	JSONObject bundleAppDoc = new JSONObject();
	JSONObject bundleFNADoc = new JSONObject();
	SubmissionBean submissionBean = new SubmissionBean();
	JSONObject proposerClientDoc = new JSONObject();

	public SubmissionUtil (JSONObject _policyDoc, JSONObject _applicationDoc, JSONObject _clientDoc, JSONObject _bundleAppDoc, JSONObject _bundleFNADoc, JSONObject _proposerClientDoc) {
		this.policyDoc = _policyDoc;
		this.applicationDoc = _applicationDoc;
		this.clientDoc = _clientDoc;
		this.bundleAppDoc = _bundleAppDoc;
		this.bundleFNADoc = _bundleFNADoc;
		this.proposerClientDoc = _proposerClientDoc;
	}

	public SubmissionBean setRecordApplicationRequest() throws Exception  {
		
		Log.debug("*** applicationDoc="+this.applicationDoc);
		
		SubmissionBean obj = new SubmissionBean();
		
		List<HasDetailsOfPolicyInItemType> HasDetailsOfPolicyInItemTypeList = new ArrayList<HasDetailsOfPolicyInItemType>();
		HasDetailsOfPolicyInItemType HasDetailsOfPolicyInItemType = obj.getHasDetailsOfPolicyInItemType();

		List<HasProducerInformationInItemType> HasProducerInformationInItemTypeList = new ArrayList<HasProducerInformationInItemType>();
		HasProducerInformationInItemType HasProducerInformationInItemType = obj.getHasProducerInformationInItemType();

		List<HasDetailsOfRisksInItemType> HasDetailsOfRisksInItemTypeList = new ArrayList<HasDetailsOfRisksInItemType>();
		HasDetailsOfRisksInItemType HasDetailsOfRisksInItemType = obj.getHasDetailsOfRisksInItemType();
		
		List<HasPersonalDetailsInItemType> HasPersonalDetailsInItemTypeList = new ArrayList<HasPersonalDetailsInItemType>();
		HasPersonalDetailsInItemType HasPersonalDetailsInItemType = obj.getHasPersonalDetailsInItemType();

		List<HasAddressesInItemType> HasAddressesInItemTypeList = new ArrayList<HasAddressesInItemType>();
		HasAddressesInItemType HasAddressesInItemType = obj.getHasAddressesInItemType();

		List<HasCustomerInformationInItemType> HasCustomerInformationInItemTypeList = new ArrayList<HasCustomerInformationInItemType>();
		HasCustomerInformationInItemType HasCustomerInformationInItemType = obj.getHasCustomerInformationInItemType();

		List<HasBenificialOwnerDetailsInItemType> HasBenificialOwnerDetailsInItemTypeList = new ArrayList<HasBenificialOwnerDetailsInItemType>();
		HasBenificialOwnerDetailsInItemType HasBenificialOwnerDetailsInItemType = obj.getHasBenificialOwnerDetailsInItemType();
		
		//List<CanBeIndividualType> CanBeIndividualTypeList = new ArrayList<CanBeIndividualType>();
		CanBeIndividualType CanBeIndividualType = obj.getCanBeIndividualType();

		List<CanBeIndividualItemType> CanBeIndividualItemTypeList = new ArrayList<CanBeIndividualItemType>();
		CanBeIndividualItemType CanBeIndividualItemType = obj.getCanBeIndividualItemType();
		
		List<IsAssociatedWithLifeInsuranceProductItemType> isAssociatedWithLifeInsuranceProductList = new ArrayList<IsAssociatedWithLifeInsuranceProductItemType>();
		IsAssociatedWithLifeInsuranceProductItemType IsAssociatedWithLifeInsuranceProductItemType = obj.getIsAssociatedWithLifeInsuranceProductItemType();

		HasCoverageDetailsInType HasCoverageDetailsInType = obj.getHasCoverageDetailsInType();

		List<ForLifeUnitofExposureItemType> ForLifeUnitofExposureItemTypeList = new ArrayList<ForLifeUnitofExposureItemType>();
		ForLifeUnitofExposureItemType ForLifeUnitofExposureItemType = obj.getForLifeUnitofExposureItemType();

		List<HasLifePolicyTransactionDetailsInItemType> HasLifePolicyTransactionDetailsInItemTypeList = new ArrayList<HasLifePolicyTransactionDetailsInItemType>();
		HasLifePolicyTransactionDetailsInItemType HasLifePolicyTransactionDetailsInItemType = obj.getHasLifePolicyTransactionDetailsInItemType();

		List<HasLifePolicyTransactionHeaderDetailsInItemType> HasLifePolicyTransactionHeaderDetailsInItemTypeList = new ArrayList<HasLifePolicyTransactionHeaderDetailsInItemType>();
		HasLifePolicyTransactionHeaderDetailsInItemType HasLifePolicyTransactionHeaderDetailsInItemType = obj.getHasLifePolicyTransactionHeaderDetailsInItemType();

		List<HasIndividualUWInfoInItemType> HasIndividualUWInfoInItemTypeList = new ArrayList<HasIndividualUWInfoInItemType>();
		HasIndividualUWInfoInItemType HasIndividualUWInfoInItemType = obj.getHasIndividualUWInfoInItemType();

		//List<HasInvestmentFundDetailsInItemType> HasInvestmentFundDetailsInItemTypeList = new ArrayList<HasInvestmentFundDetailsInItemType>();
		//HasInvestmentFundDetailsInItemType HasInvestmentFundDetailsInItemType = obj.getHasInvestmentFundDetailsInItemType();

		//List<HasAccountDetailsInItemType> HasAccountDetailsInItemTypeList = new ArrayList<HasAccountDetailsInItemType>();
		//HasAccountDetailsInItemType HasAccountDetailsInItemType = obj.getHasAccountDetailsInItemType();

		List<HasDetailsRecordInItemType> HasDetailsRecordInItemTypeList = new ArrayList<HasDetailsRecordInItemType>();
		HasDetailsRecordInItemType HasDetailsRecordInItemType = obj.getHasDetailsRecordInItemType();

		List<HasPolicyChargeInfoInItemType> HasPolicyChargeInfoInItemTypeList = new ArrayList<HasPolicyChargeInfoInItemType>();
		HasPolicyChargeInfoInItemType HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();

		List<HasAXAAsiaPolicyPaymentAccountItemType> HasAXAAsiaPolicyPaymentAccountItemTypeList = new ArrayList<HasAXAAsiaPolicyPaymentAccountItemType>();
		HasAXAAsiaPolicyPaymentAccountItemType HasAXAAsiaPolicyPaymentAccountItemType = obj.getHasAXAAsiaPolicyPaymentAccountItemType();
		
		List<HasAssessmentHeadersInItemType> HasAssessmentHeadersInItemTypeList = new ArrayList<HasAssessmentHeadersInItemType>();
		HasAssessmentHeadersInItemType HasAssessmentHeadersInItemType = obj.getHasAssessmentHeadersInItemType();

		List<HasAssessmentDetailsInItemType> HasAssessmentDetailsInItemTypeList = new ArrayList<HasAssessmentDetailsInItemType>();
		HasAssessmentDetailsInItemType HasAssessmentDetailsInItemType = obj.getHasAssessmentDetailsInItemType();

		List<HasStaffInformationInItemType> HasStaffInformationInItemTypeList = new ArrayList<HasStaffInformationInItemType>();
		HasStaffInformationInItemType HasStaffInformationInItemType = obj.getHasStaffInformationInItemType();

		List<HasAssociationWithItemType> HasAssociationWithItemTypeList = new ArrayList<HasAssociationWithItemType>();
		HasAssociationWithItemType HasAssociationWithItemType = obj.getHasAssociationWithItemType();

		List<HasBankReferrerInfoInItemType> HasBankReferrerInfoInItemTypeList = new ArrayList<HasBankReferrerInfoInItemType>();
		HasBankReferrerInfoInItemType HasBankReferrerInfoInItemType = obj.getHasBankReferrerInfoInItemType();

		List<HasAXAAsiaPartyAccountItemType> HasAXAAsiaPartyAccountItemTypeList = new ArrayList<HasAXAAsiaPartyAccountItemType>();
		HasAXAAsiaPartyAccountItemType HasAXAAsiaPartyAccountItemType = obj.getHasAXAAsiaPartyAccountItemType();
		
		List<HasPolicyAssociationDetailsInItemType> HasPolicyAssociationDetailsInItemTypeList = new ArrayList<HasPolicyAssociationDetailsInItemType>();
		HasPolicyAssociationDetailsInItemType HasPolicyAssociationDetailsInItemType = obj.getHasPolicyAssociationDetailsInItemType();
		
		HasCRSInformationInType HasCRSInformationInType = obj.getHasCRSInformationInType();
		
		List<HasAXAAsiaPolicyHeaderPaymentAccountItemType> HasAXAAsiaPolicyHeaderPaymentAccountItemTypeList = new ArrayList<HasAXAAsiaPolicyHeaderPaymentAccountItemType>();
		HasAXAAsiaPolicyHeaderPaymentAccountItemType HasAXAAsiaPolicyHeaderPaymentAccountItemType = obj.getHasAXAAsiaPolicyHeaderPaymentAccountItemType();
		
		List<String> taxIdentificationNOType = new ArrayList<String>();
		List<String> countryOfTaxResidenceType = new ArrayList<String>();
	
		boolean is_1stparty = false;
		boolean is_3rdparty = false;
//		JSONObject policyDocument = this.policyDoc;
		PolicyRemarks Pol_remarks = new PolicyRemarks();
		
		JSONObject quotation = this.applicationDoc.getJSONObject("quotation");
		JSONObject quotation_agent = quotation.getJSONObject("agent");
		//JSONObject quotation_policyOptions = quotation.getJSONObject("policyOptions");
		
		//For Shield
		boolean isShield = false; 
		
		if (quotation.has("quotType")) {
			if (quotation.getString("quotType").equals("SHIELD")) {//Shield
				isShield = true;
			}
		}
		
		JSONArray quotation_plans = null; 
		JSONObject quotation_BasicPlan = null;
				
		if (!isShield) {
			quotation_plans = quotation.getJSONArray("plans");
			
			if (quotation_plans.length() > 0)
				quotation_BasicPlan = quotation_plans.getJSONObject(0);
		}
					
		JSONObject appform = this.applicationDoc.getJSONObject("applicationForm");
		JSONObject appform_basicvalue = appform.getJSONObject("values"); 
		JSONObject appform_basicvalue_plandetails = appform_basicvalue.getJSONObject("planDetails");
		
		JSONObject payment = null;
		Boolean FundReqDate = false;
		JSONObject cpfisoaBlock = null;
		JSONObject cpfissaBlock = null;
		JSONObject srsBlock = null;
		String bankName = "";
		boolean hasPayorBenefitRider = false; 
				
		if (this.applicationDoc.has("payment")) {
			payment = this.applicationDoc.getJSONObject("payment");	
			
			if (payment.has("cpfisoaBlock"))
				cpfisoaBlock = payment.getJSONObject("cpfisoaBlock");
			
			if (payment.has("cpfissaBlock"))
				cpfissaBlock = payment.getJSONObject("cpfissaBlock");
			
			if (payment.has("srsBlock"))
				srsBlock = payment.getJSONObject("srsBlock");
			
			//Process Fund Call on Specific Date
			if (cpfisoaBlock != null) {
				if (cpfisoaBlock.has("cpfisoaFundReqDate")) {
					if (cpfisoaBlock.getString("cpfisoaFundReqDate").equals("Y")) {
						FundReqDate = true;
						
						LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(cpfisoaBlock.getLong("cpfisoaFundProcessDate")), TimeZone.getDefault().toZoneId());

						String FundProcessDate =  date.getYear() + "-" + String.format("%02d", date.getMonthValue()) + "-" + String.format("%02d", date.getDayOfMonth());
						
						Pol_remarks.addpolicyRemarks("Process Fund Call on " + FundProcessDate);
					} else {
						Pol_remarks.addpolicyRemarks("CPF a/c details received, Process Fund Call");
					}
				} else if (cpfisoaBlock.has("cpfisoaAcctDeclare")) {
					if (cpfisoaBlock.getString("cpfisoaAcctDeclare").equals("Y"))
						Pol_remarks.addpolicyRemarks("CPF a/c opening form pending for submission at AXA Office");
					else {
						Pol_remarks.addpolicyRemarks("Client will submit CPF Account Opening form directly to the Bank.");
						Pol_remarks.addpolicyRemarks("To require SuppForm to indicate CPF Account Number and Bank Name.");
					}
				}
				
				if(cpfisoaBlock.has("cpfisoaBankNamePicker"))
					bankName = cpfisoaBlock.getString("cpfisoaBankNamePicker");
			}
			
			if (cpfissaBlock != null) {
				if(cpfissaBlock.has("cpfissaFundReqDate")) {
					if (cpfissaBlock.getString("cpfissaFundReqDate").equals("Y")) {
						FundReqDate = true;
												
						LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(cpfissaBlock.getLong("cpfissaFundProcessDate")), TimeZone.getDefault().toZoneId());

						String FundProcessDate =  date.getYear() + "-" + String.format("%02d", date.getMonthValue()) + "-" + String.format("%02d", date.getDayOfMonth());
						
						Pol_remarks.addpolicyRemarks("Process Fund Call on " + FundProcessDate);
					} else {
						Pol_remarks.addpolicyRemarks("CPF a/c details received, Process Fund Call");
					}
				}
				
				if(cpfissaBlock.has("cpfissaBankNamePicker"))
					bankName = cpfissaBlock.getString("cpfissaBankNamePicker");
			}
			
			if (srsBlock != null) {
				if(srsBlock.has("srsFundReqDate")) {
					if (srsBlock.getString("srsFundReqDate").equals("Y")) {
						FundReqDate = true;
						
						LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(srsBlock.getLong("srsFundProcessDate")), TimeZone.getDefault().toZoneId());

						String FundProcessDate =  date.getYear() + "-" + String.format("%02d", date.getMonthValue()) + "-" + String.format("%02d", date.getDayOfMonth());
						
						Pol_remarks.addpolicyRemarks("Process Fund Call on " + FundProcessDate);
					} else {
						Pol_remarks.addpolicyRemarks("CPF a/c details received, Process Fund Call");
					}
				} else if (srsBlock.has("srsAcctDeclare")) {
					if (srsBlock.getString("srsAcctDeclare").equals("Y"))
						Pol_remarks.addpolicyRemarks("CPF a/c opening form pending for submission at AXA Office");
					else {
						Pol_remarks.addpolicyRemarks("Client will submit CPF Account Opening form directly to the Bank.");
						Pol_remarks.addpolicyRemarks("To require SuppForm to indicate CPF Account Number and Bank Name.");
					}
				}
				
				if(srsBlock.has("srsBankNamePicker"))
					bankName = srsBlock.getString("srsBankNamePicker");
			}
		}
		
		JSONObject applicationForm = this.applicationDoc.getJSONObject("applicationForm");
		JSONObject applicationForm_values = applicationForm.getJSONObject("values");
		
		JSONArray applicationForm_values_insured = applicationForm_values.getJSONArray("insured");
		JSONObject applicationForm_values_planDetails = applicationForm_values.getJSONObject("planDetails");		
		
		JSONArray app_plans = applicationForm_values_planDetails.getJSONArray("planList");
		JSONObject appForm_BasicPlan = null;
		
		if (app_plans.length() > 0) {
			appForm_BasicPlan = app_plans.getJSONObject(0);
		}
	
//		boolean has_appForm_RiderPlan = false;
//	
//		if (app_plans.length() > 1) {
//			has_appForm_RiderPlan = true;
//		}

		JSONObject LifeAssured = null;
		JSONObject LifeAssured_personalInfo = null; 	
		JSONObject LifeAssured_insurability = null; 
		JSONObject LifeAssured_policies= null; 
		JSONObject LifeAssured_declaration= null;
		JSONObject LifeAssured_residency = null;
		JSONObject Owner = null;
		JSONObject Owner_personalInfo = null;
		JSONObject Owner_insurability = null; 
		JSONObject Owner_declaration= null;
		JSONObject Owner_policies= null;
		JSONObject Owner_residency = null;
		
 		String ApplicationStatus = null;
		String ApprovalStatus = null;
		
		//String SUBMITTED_date = null;
		String INVALIDATED_date = null;
		String APPROVE_REJECT_date = null;
		String EXPIRED_date = null;
		
		//String SUBMITTED_time = null;
		String INVALIDATED_time = null;
		String APPROVE_REJECT_time = null;
		String EXPIRED_time = null;
		
		//boolean isSUBMITTED = false;
		boolean isINVALIDATED = false;
		boolean isAPPROVE_REJECT = false;
		boolean isAPPROVE = false;
		boolean isREJECT = false;
		boolean isEXPIRED = false;
		
		//Policy Level -start  TODO
		//MEDICAL CODE
		boolean isPolicyPMED = false;
		boolean isLAPMED = false;
		boolean isOWPMED = false;
//		boolean isLAHLTH = false;
//		boolean isOWHLTH = false;	

		AgeCrossDateCal ageCrossDateCal = new AgeCrossDateCal();
		//String riskCommDate = "";
				
		String[] medicalQNString = {
		"HEALTH01",
		"HEALTH02", "HEALTH03", "HEALTH04", 
		"HEALTH05", "HEALTH06", "HEALTH07", 
		"HEALTH08", "HEALTH09" , "HEALTH10", 
		"HEALTH11", "HEALTH12" , "HEALTH13", 
		"HEALTH14", "HEALTH15" , "HEALTH16", 
		"HEALTH17", "HEALTH_GIO01" , "HEALTH_GIO02","HEALTH_GIO03" 
		};
		
		String[] medicalShieldQNString = {
		"HEALTH_SHIELD_01","HEALTH_SHIELD_02","HEALTH_SHIELD_03","HEALTH_SHIELD_04",
		"HEALTH_SHIELD_05","HEALTH_SHIELD_06","HEALTH_SHIELD_07","HEALTH_SHIELD_08",
		"HEALTH_SHIELD_09","HEALTH_SHIELD_10","HEALTH_SHIELD_11","HEALTH_SHIELD_12", 
		"HEALTH_SHIELD_13","HEALTH_SHIELD_14","HEALTH_SHIELD_15","HEALTH_SHIELD_16", 
		"HEALTH_SHIELD_17","HEALTH_SHIELD_18","HEALTH_SHIELD_19","HEALTH_SHIELD_20", 
		"HEALTH_SHIELD_21","HEALTH_SHIELD_22","HEALTH_SHIELD_23","HEALTH_SHIELD_24", 
		"HEALTH13","HEALTH14","HEALTH15","HEALTH16" 
		};

		obj.recordApplicationRequest.policy.medicalIND = "N";
		
		//policy level - end
		
		String appId = applicationDoc.getString("_id");
		
		
		if (applicationDoc.has("parentId")) //For Shield
			appId = applicationDoc.getString("parentId");
		
			
		if (bundleAppDoc.has("applications")) {
			JSONArray apps = bundleAppDoc.getJSONArray("applications");
			
			for (int i = 0; i < apps.length(); i++) {
				JSONObject app = apps.getJSONObject(i);
				if(app.has("applicationDocId")){
					if (appId.equals(app.getString("applicationDocId"))) {
						ApplicationStatus = app.getString("appStatus");
						break;
					}
				}
			}
		}
		
		
		
		//The processing application must be signed
		//Sign date is getting from eApp applicationSignedDate
		String Signature_date = "";
		String Signature_time = "";
		
		if (applicationDoc.has("applicationSignedDate")) {
			try
			{
				Signature_date = applicationDoc.getString("applicationSignedDate").substring(0, 10);
				Signature_time = applicationDoc.getString("applicationSignedDate").substring(11,19);	
				
				if (Signature_date.isEmpty() || Signature_time.isEmpty()) {
					throw new IOException("Signature date/time missing for processing application!");
				}
			} catch(Exception ex) { }	
		}
		
		if (StringUtils.isNotEmpty(ApplicationStatus)) {
//			ApplicationStatus = bundleAppDoc.getString("appStatus");
//			status: 'APPLYING','SUBMITTED','INVALIDATED','INVALIDATED_SIGNED'
			
//			if ("SUBMITTED".equals(ApplicationStatus)) {
////				Date submit_tmp_date = new Date(applicationDoc.getLong("applicationSubmittedDate")); // TODO
////				SUBMITTED_date = getDummyDate();// TODO
//				SUBMITTED_date = applicationDoc.getString("applicationSubmittedDate");
//				if (SUBMITTED_date.isEmpty()) {
//					throw new IOException("Policy is SUBMITTED, while Date is missing");
//				}
//				isSUBMITTED = true;
//			}
			
			if ("INVALIDATED_SIGNED".equals(ApplicationStatus) && applicationDoc.getBoolean("isInitialPaymentCompleted")) {
				if (INVALIDATED_date!= null && INVALIDATED_date.isEmpty()) {
					throw new IOException("Policy is INVALIDATED, while Date is missing");
				} else {
					if (bundleAppDoc.has("applications")) {
						
						JSONArray applicationsArray = bundleAppDoc.getJSONArray("applications");
						JSONObject applicationJson = new JSONObject();
						
						for(int i = 0; i < applicationsArray.length(); i++) {
							applicationJson = applicationsArray.getJSONObject(i);
							
							if (applicationJson.has("applicationDocId")) {
								if (applicationJson.getString("applicationDocId").equals(appId)) {
									
									//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
									//LocalDateTime date = LocalDateTime.parse((applicationJson.getLong("invalidateDate"), formatter);
									
									LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(applicationJson.getLong("invalidateDate")), TimeZone.getDefault().toZoneId());
									
									LocalDateTime _date = date.plusHours(8);
									
									INVALIDATED_date =  _date.getYear() + "-" + String.format("%02d", _date.getMonthValue()) + "-" + String.format("%02d", _date.getDayOfMonth());
									//INVALIDATED_date = bundleAppDoc.getString("invalidateDate").substring(0, 10);
									
									INVALIDATED_time =  String.format("%02d", _date.getHour()) + ":" +  String.format("%02d", _date.getMinute()) + ":" + String.format("%02d", _date.getSecond());
									//INVALIDATED_time = bundleAppDoc.getString("invalidateDate").substring(11,19);
									
									Integer date_figure = Integer.valueOf(INVALIDATED_date.substring(8,10));
									
									if (date_figure > 28)
										INVALIDATED_date = INVALIDATED_date.substring(0,8) + "28";
									break;
								}
							}
						}
					}
				}
				if ("".equals(INVALIDATED_date) || "".equals(INVALIDATED_time)) {
					throw new Exception("Policy is invalided, while Date/time is missing\"");
				}
				isINVALIDATED = true;
			}
		}
		
		//set default value to APPROVE_REJECT_time to walk around 
		String approveDate = "";
		
		if (policyDoc != null && policyDoc.has("approvalStatus")){
			ApprovalStatus = policyDoc.getString("approvalStatus"); //status: "A" - approved, "R" - Rejected, "E" - expired
			
			if ("A".equals(ApprovalStatus) || "R".equals(ApprovalStatus)) {				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				LocalDateTime date = LocalDateTime.parse(policyDoc.getString("approveRejectDate"), formatter);
				LocalDateTime _date = date.plusHours(8);
				
				APPROVE_REJECT_date =  _date.getYear() + "-" + String.format("%02d", _date.getMonthValue()) + "-" + String.format("%02d", _date.getDayOfMonth());
				APPROVE_REJECT_time =  String.format("%02d", _date.getHour()) + ":" + String.format("%02d", _date.getMinute()) + ":" +  String.format("%02d", _date.getSecond());
				
				if (APPROVE_REJECT_date.isEmpty()||APPROVE_REJECT_time.isEmpty())
					throw new IOException("Policy is approved/Rejected, while Date/time is missing");
				
				isAPPROVE_REJECT = true;
				
				if ("A".equals(ApprovalStatus)) {
					isAPPROVE = true;
					approveDate = APPROVE_REJECT_date;
				} else {
					isREJECT = true;
				}
					
				Integer date_figure = Integer.valueOf(APPROVE_REJECT_date.substring(8,10));
				
				if (date_figure > 28)
					APPROVE_REJECT_date = APPROVE_REJECT_date.substring(0,8) + "28";
			} else if ("E".equals(ApprovalStatus)) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				LocalDateTime date = LocalDateTime.parse(policyDoc.getString("expiredDate"), formatter);
				LocalDateTime _date = date.plusHours(8);
				
				EXPIRED_date =  _date.getYear() + "-" + String.format("%02d", _date.getMonthValue()) + "-" + String.format("%02d", _date.getDayOfMonth());
				EXPIRED_time =  String.format("%02d", _date.getHour()) + ":" + String.format("%02d", _date.getMinute()) + ":" +  String.format("%02d", _date.getSecond());
				
				if (EXPIRED_date.isEmpty() || EXPIRED_time.isEmpty())
					throw new IOException("Policy is expired, while Date/time is missing");
				
				isEXPIRED = true;
				
				Integer date_figure = Integer.valueOf(EXPIRED_date.substring(8,10));
				
				if (date_figure > 28)
					EXPIRED_date = EXPIRED_date.substring(0,8) + "28";
			}
		}
		
		
		if (applicationForm_values_insured.length() > 0){
			LifeAssured = applicationForm_values_insured.getJSONObject(0);
	//		Owner = applicationForm_values.getJSONObject("proposer");
	//		Owner_personalInfo = Owner.getJSONObject("personalInfo");
	//		Owner_insurability = Owner.getJSONObject("insurability");
			is_3rdparty = true;
		} else {
			LifeAssured = applicationForm_values.getJSONObject("proposer");
			is_1stparty = true;
		}
		
		if(applicationForm_values.has("proposer")) { 
			Owner = applicationForm_values.getJSONObject("proposer");
		}else {
			Owner = new JSONObject();
		}
		
		//personal infomation OWsection
		if(Owner.has("personalInfo")) { 
			Owner_personalInfo = Owner.getJSONObject("personalInfo");
		}else {
			Owner_personalInfo = new JSONObject();
		}
		
		Integer OW_issueage = 0;
		String 	OW_Gender = "";
		if (is_3rdparty) {
			if (Owner_personalInfo.has("age")) {
				OW_issueage = Owner_personalInfo.getInt("age");
			}
			if(Owner_personalInfo.has("gender")) {
				OW_Gender = Owner_personalInfo.getString("gender");
			}else {
				throw new Exception("owner gender missing");
			}
		}
		
		if(Owner.has("residency")) {
			Owner_residency = Owner.getJSONObject("residency");
		}
		else {
			Owner_residency = new JSONObject();
		}
		
		if (Owner.has("insurability")) {
			Owner_insurability = Owner.getJSONObject("insurability");
		} else {
			Owner_insurability = new JSONObject();
		}
		
		if (Owner.has("policies")) {
			Owner_policies = Owner.getJSONObject("policies");
		} else {
			Owner_policies = new JSONObject();
		}
		
		if (Owner.has("declaration")) {
			Owner_declaration = Owner.getJSONObject("declaration");	
		} else {
			Owner_declaration = new JSONObject();
		}
		
		
		//personal info common field Section. S
		LifeAssured_personalInfo = LifeAssured.getJSONObject("personalInfo");
		String LA_nationality = "";
		Integer LA_issueage = 0;
		String LA_Occupation = "";
		String LA_Gender = "";
		

		if (is_1stparty || is_3rdparty) {
			if (LifeAssured_personalInfo.has("nationality")) {
				LA_nationality = NationalityConversion(LifeAssured_personalInfo.getString("nationality"), false); 
			}
			if (isShield) {
				if (LifeAssured_personalInfo.has("age")) {
					LA_issueage = LifeAssured_personalInfo.getInt("age");
				}
			}  else {
				if (quotation.has("iAge")) {
					LA_issueage = quotation.getInt("iAge");
				}
			}
	
			if (LifeAssured_personalInfo.has("occupation")) {
				LA_Occupation = LifeAssured_personalInfo.getString("occupation");
			}
			if (LifeAssured_personalInfo.has("gender")) {
				LA_Gender = LifeAssured_personalInfo.getString("gender");
			}
		}
		
		
		//personal info commom field LASection. E
		
		if (LifeAssured.has("insurability")) {
			LifeAssured_insurability = LifeAssured.getJSONObject("insurability");
		} else {
			JSONObject json = new JSONObject();
			LifeAssured_insurability = json;
		}
		
		if (LifeAssured.has("residency")) {
			LifeAssured_residency = LifeAssured.getJSONObject("residency");	
		} else {
			JSONObject json = new JSONObject();
			LifeAssured_residency = json;
		}
		
		LifeAssured_policies = LifeAssured.getJSONObject("policies");
		LifeAssured_declaration = LifeAssured.getJSONObject("declaration");	
		
		boolean ismAddrdifferent = false;
		
		if (Owner_personalInfo.has("mAddrCountry")&&(!("".equals(Owner_personalInfo.getString("mAddrCountry"))))) {
			ismAddrdifferent = true;
		}

		JSONArray LifeAssured_personalInfo_dependants = LifeAssured_personalInfo.getJSONArray("dependants");
		JSONObject proposer_dependant = null;
		
		if (LifeAssured_personalInfo_dependants.length() > 0) {	
			for(int i = 0; i < LifeAssured_personalInfo_dependants.length(); i++) {
				if (Owner_personalInfo.get("cid").equals(LifeAssured_personalInfo_dependants.getJSONObject(i).get("cid"))){
					proposer_dependant = LifeAssured_personalInfo_dependants.getJSONObject(i);
					break;
				}
			}	
		}

		try {
			JSONObject quotation_policyOptions = null;
			
			String ShieldBasicCode = "";
			String ShieldBasicPolicyNo = "";
			String ShieldRiderCode = "";
			String ShieldRiderPolicyNo = "";
			
			if (isShield) {
				quotation = quotation.getJSONObject("insureds");
				
				quotation_policyOptions = quotation.getJSONObject("policyOptions");
				
				JSONArray plansArray = quotation.getJSONArray("plans");
				
				quotation_plans = plansArray;
				
				ShieldBasicCode = plansArray.getJSONObject(0).getString("planCode");
				
				quotation_BasicPlan = plansArray.getJSONObject(0);
				
				String parentId = this.applicationDoc.getString("parentId");
				String iCid = this.applicationDoc.getString("iCid");
				
				JSONObject masterShieldJson = Constant.CBUTIL.getDoc(parentId);
				
				JSONObject iCidMapping = masterShieldJson.getJSONObject("iCidMapping");
				
				JSONArray iCidMappingArray = iCidMapping.getJSONArray(iCid);
				
				for (int i = 0; i < iCidMappingArray.length(); i++) {
					if (i == 0) {
						//ShieldBasicCode = iCidMappingArray.getJSONObject(i).getString("covCode");
						ShieldBasicPolicyNo = iCidMappingArray.getJSONObject(i).getString("policyNumber");	
					} else {
						ShieldRiderCode = iCidMappingArray.getJSONObject(i).getString("covCode");
						ShieldRiderPolicyNo = iCidMappingArray.getJSONObject(i).getString("policyNumber");	
					}
				}
				
				obj.lob = "Health";
			} else {
				quotation_policyOptions = quotation.getJSONObject("policyOptions");
				obj.lob = "Life";
			}			
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy - S

			//CAPTURE DATE *
			//if (applicationDoc.has("applicationSubmittedDate")) {
			//	Date applicationStartedDate = new Date(applicationDoc.getLong("applicationSubmittedDate"));				
			//	obj.recordApplicationRequest.policy.applicationCaptureDT = changeDateFormat(applicationStartedDate);
			//} 
			//else {
			//	throw new IOException("Mandatory field missing - application capture date");
			//}
			
			//Capture date: Date on which Supervisor Approved/Rejected or case expired or invalidated with payment made
			//Submit date:  Date on which Supervisor Approved/Rejected or case expired or invalidated with payment made
			
			obj.recordApplicationRequest.policy.applicationCaptureDT = "";
			obj.recordApplicationRequest.policy.policySubmitDT = "";
			obj.recordApplicationRequest.policy.applicationSubmitTM = "";
			
			if (isAPPROVE_REJECT) {
				obj.recordApplicationRequest.policy.applicationCaptureDT = APPROVE_REJECT_date;	
				obj.recordApplicationRequest.policy.policySubmitDT = APPROVE_REJECT_date;
				obj.recordApplicationRequest.policy.applicationSubmitTM = APPROVE_REJECT_time;
				
				if (isShield)
					obj.recordApplicationRequest.policy.policySubmitDT1 = APPROVE_REJECT_date;
			}
			
	        if (isINVALIDATED) {
				obj.recordApplicationRequest.policy.applicationCaptureDT = INVALIDATED_date;
				obj.recordApplicationRequest.policy.policySubmitDT = INVALIDATED_date;
				obj.recordApplicationRequest.policy.applicationSubmitTM = INVALIDATED_time;
				
				if (isShield)
					obj.recordApplicationRequest.policy.policySubmitDT1 = INVALIDATED_date;
			}

			if (isEXPIRED) {
				obj.recordApplicationRequest.policy.applicationCaptureDT = EXPIRED_date;
				obj.recordApplicationRequest.policy.policySubmitDT = EXPIRED_date;
				obj.recordApplicationRequest.policy.applicationSubmitTM = EXPIRED_time;
				
				if (isShield)
					obj.recordApplicationRequest.policy.policySubmitDT1 = EXPIRED_date;
			}
			
			obj.recordApplicationRequest.policy.applicationSignatureDT = Signature_date;
		
			//COMPANY CODE *
			if (isShield)//For Shield
				obj.recordApplicationRequest.policy.companyCD = "3";
			else 
				obj.recordApplicationRequest.policy.companyCD = "5";
				
			//POLICY NO. *
			if (applicationDoc.has("policyNumber")) {
				obj.recordApplicationRequest.policy.policyNO = applicationDoc.getString("policyNumber");
			}
			
			//SOURCE CODE
			obj.recordApplicationRequest.policy.sourceChannelCD = "";

			//AUTO SET FLAG
			String riskCommenceDate = "";
			Boolean isBackDate = false;
			
			obj.recordApplicationRequest.policy.autoSetFLG = "N";
			if (appform_basicvalue_plandetails.has("isBackDate")) {	
				if ("Y".equals(appform_basicvalue_plandetails.getString("isBackDate"))) {
					if (appform_basicvalue_plandetails.has("riskCommenDate")) {	
						
						Integer date_figure = Integer.valueOf(appform_basicvalue_plandetails.getString("riskCommenDate").substring(8,10));
						
						if (date_figure > 28)
							riskCommenceDate = appform_basicvalue_plandetails.getString("riskCommenDate").substring(0,8) + "28";
						else 
							riskCommenceDate = appform_basicvalue_plandetails.getString("riskCommenDate");
						
						Pol_remarks.addpolicyRemarks("Backdated to " + riskCommenceDate);
						obj.recordApplicationRequest.policy.autoSetFLG = "N";
						isBackDate = true;
					}		 
				} else {
					obj.recordApplicationRequest.policy.autoSetFLG = "Y";
				}
			} 	
			
			String paymentmethod = "";
			boolean isCreditCardPayment = false;
			boolean isPaid = false;
			boolean isCashPayment = false;
			boolean isCheqPayment = false;
			boolean isCPFPayment = false;
			
			if (payment != null && payment.has("initPayMethod")) {//TODO
				paymentmethod = payment.getString("initPayMethod");
				if ("crCard".equals(paymentmethod)||"eNets".equals(paymentmethod)||"dbsCrCardIpp".equals(paymentmethod)){
					isCreditCardPayment = true;
					isPaid = true;
				}
				
				if ("cpfisoa".equals(paymentmethod) || "cpfissa".equals(paymentmethod) ||"srs".equals(paymentmethod)) {
					isCPFPayment = true;
				}
				
				if ("cash".equals(paymentmethod)) {
					isCashPayment = true;
				}
				
				if ("chequeCashierOrder".equals(paymentmethod)) {
					isCheqPayment = true;
				}
			}	
						
			if (applicationDoc.getString("policyNumber").equals(ShieldBasicPolicyNo)) {
				isCPFPayment = true;
				
			}
				
			//COMMUNICATION MEANS *
			obj.recordApplicationRequest.policy.communicationMode = "E";			
			
			if ("".equals(obj.recordApplicationRequest.policy.communicationMode)) {
				throw new IOException("Mandatory field missing - communication mode");
			}
			 
//			//ePolicy
//			if (Owner_declaration.has("epolicy01")) {
//				if (Owner_declaration.get("Owner_declaration").equals("Y"))
//					obj.recordApplicationRequest.policy.communicationMode = "E";
//				else
//					obj.recordApplicationRequest.policy.communicationMode = "B";
//			} else {
//				obj.recordApplicationRequest.policy.communicationMode = "B";
//			}
			
//			if (isShield)
//				obj.recordApplicationRequest.policy.communicationMode = "E";

			//APP STATUS CODE
			//"11": 
			//1. (Reject Case):					SupervisorApproval.status is "R"  
			//2. (Paid Case):					Paid && (Application.status is Invalidated 
			//3. (approval status is expired):	SupervisorApproval.status is expired)
			obj.recordApplicationRequest.policy.policyStatusCD = "";	
			
			if ("A".equals(ApprovalStatus))
				obj.recordApplicationRequest.policy.policyStatusCD = "00";
			else {
				if (isShield) 
					obj.recordApplicationRequest.policy.policyStatusCD = "03";
				else
					obj.recordApplicationRequest.policy.policyStatusCD = "11";
			}

//			if ("R".equals(ApprovalStatus)||(isPaid && ("INVALIDATED".equals(ApplicationStatus)))||"E".equals(ApprovalStatus)) {
//				obj.recordApplicationRequest.policy.policyStatusCD = "11";
//			}
			
			//DEP AMOUNT *
			obj.recordApplicationRequest.policy.depositAMT = "0";
			
			if (isCheqPayment) {
				if (quotation.has("premium")) {
					obj.recordApplicationRequest.policy.depositAMT = Double.toString(quotation.getDouble("premium"));
				}
			}
			
			//Boolean paid = true;
			//paid = application.paid;
			//if applicaton.paymentMethod in (Credit Card / eNet / IPP)
			//          paid = true;
			//else
			//          paid = false;
			
			//POLICY DATE
			
//			1- Date of Supervisor approval/Rejection at front end
//			2- Date of application expired at front end
//			3. eApp expired/invalid (only for cases with payment collected/paid)
//			4- For Month end 29,30,31, set 28th of every month 
			
			obj.recordApplicationRequest.policy.policyEffectiveDTTM = "";
			//String riskCommenceDate = "";
//			//fixing for riskCommenceDate position change
//			//if (appform_basicvalue_plandetails.has("riskCommenDate")) {				
			//  if (quotation.has("riskCommenDate")) {
//				String Tmp_riskCommenceDate = quotation.getString("riskCommenDate");
//				obj.recordApplicationRequest.policy.policyEffectiveDTTM = quotation.getString("riskCommenDate");
			//	riskCommenceDate = quotation.getString("riskCommenDate");
//				Integer date_figure = Integer.valueOf(Tmp_riskCommenceDate.substring(8,10));
//				
//				if (date_figure > 28) {
//					obj.recordApplicationRequest.policy.policyEffectiveDTTM = Tmp_riskCommenceDate.substring(0,8) + "28";
//					//riskCommDate = obj.recordApplicationRequest.policy.policyEffectiveDTTM ;
//				}
		//	} 
			  
		  	if (isAPPROVE_REJECT) {
				obj.recordApplicationRequest.policy.policyEffectiveDTTM = APPROVE_REJECT_date;	
//				Integer date_figure = Integer.valueOf(APPROVE_REJECT_date.substring(8,10));
//				
//				if (date_figure > 28)
//					obj.recordApplicationRequest.policy.policyEffectiveDTTM = APPROVE_REJECT_date.substring(0,8) + "28";
			}
		  	
		  	
		  	if (isBackDate) {
		  		obj.recordApplicationRequest.policy.policyEffectiveDTTM = riskCommenceDate;	
		  	}
			
	        if (isINVALIDATED) {
				obj.recordApplicationRequest.policy.policyEffectiveDTTM = INVALIDATED_date;
//				Integer date_figure = Integer.valueOf(INVALIDATED_date.substring(8,10));
//				
//				if (date_figure > 28)
//					obj.recordApplicationRequest.policy.policyEffectiveDTTM = INVALIDATED_date.substring(0,8) + "28";
			}

			if (isEXPIRED) {
				obj.recordApplicationRequest.policy.policyEffectiveDTTM = EXPIRED_date;
//				Integer date_figure = Integer.valueOf(EXPIRED_date.substring(8,10));
//				
//				if (date_figure > 28)
//					obj.recordApplicationRequest.policy.policyEffectiveDTTM = EXPIRED_date.substring(0,8) + "28";	
			}
			  			
			if (isShield) {
				String LAdob = LifeAssured.getJSONObject("personalInfo").getString("dob");
                                                                                                                                                                                                                                                                                                                                
				int LADOBYear = Integer.parseInt(LAdob.substring(0,4));
				int	expirationYear =  LADOBYear + 99; 
						
				Integer date_figure = Integer.valueOf(LAdob.substring(8,10));

				if (date_figure > 28)
					LAdob = LAdob.substring(5,8) + "28";
				else 
					LAdob = LAdob.substring(5,10);

				obj.recordApplicationRequest.policy.policyExpirationDTTM = expirationYear + "-" + LAdob;
			}
			  
			obj.recordApplicationRequest.policy.languageCD = "E";

			String Basic_planCD = "";
			String _covCode = "";
			
			boolean changibleIND_PROD = false;
			
			boolean is_bandAid = false;
			boolean is_inspireDuo = false;
			boolean is_savvySaver = false;			
			boolean is_flexiProduct = false;
			boolean is_awtProduct = false;
			boolean is_pulsarProduct = false;
			
			//Is investment link product
			boolean is_ILPProduct = false;
			
			
			if (appForm_BasicPlan.has("planCode"))
			   Basic_planCD = appForm_BasicPlan.getString("planCode");
			
			if (appForm_BasicPlan.has("covCode")) {
				_covCode = appForm_BasicPlan.getString("covCode");
				
				switch(_covCode)
				{
				  case "BAA"://Band Aid
					  is_bandAid = true;
					  break;
				  case "IND"://Inspire Duo
					  changibleIND_PROD = true;
					  is_inspireDuo = true;
					  is_ILPProduct = true;
					  break;
				  case "SAV"://Savvy Saver
					  is_savvySaver = true;
					  break;
				  case "FPX"://Flexi Protector
					  changibleIND_PROD = true;
					  is_flexiProduct = true;					  
					  is_ILPProduct = true;
					  break;
				  case "FSX"://Flexi Saver
					  changibleIND_PROD = true;
					  is_flexiProduct = true;
					  is_ILPProduct = true;
					  break;
				  case "AWT"://AWT Protector
					  changibleIND_PROD = true;
					  is_awtProduct = true;
					  is_ILPProduct = true;
					  break;
				  case "PUL"://Pulsar
					  changibleIND_PROD = true;
					  is_pulsarProduct = true;
					  is_ILPProduct = true;
					  break;
				}
			} else {
				throw new IOException("Mandatory field missing - basic plan code 2");
			}
			
			//Payor Benefit rider
			if (app_plans.length() > 1 && !isShield) {
				for (int i = 1; i < app_plans.length(); i++) {
					JSONObject app = app_plans.getJSONObject(i);
					
					if (isPayorBenefitRider(_covCode, app.getString("covCode"))) {
						hasPayorBenefitRider = true;
						break;
					}
				}
			}
			
			String quotation_paymentMethod = "";
		
			if (quotation_policyOptions.has("paymentMethod"))
				quotation_paymentMethod = quotation_policyOptions.getString("paymentMethod");
		
			//PaymentCodes - call 1
			String parm_PaymentType		= "INIT";
			String parm_PlanCD			= Basic_planCD;
			String parm_PaymentMethod	= quotation_paymentMethod;
			String parm_PaymentMode		= "M";
			String parm_GiroIND			= "N";
			
			PaymentCodes initialPayment = new PaymentCodes(); 
			//"INIT" : initial payment "M" is paymode for ispireD, "N": GIRO, no GIRO for initial payment
			initialPayment.setPayKeys(parm_PaymentType, parm_PlanCD, parm_PaymentMethod, parm_PaymentMode, parm_GiroIND);
			initialPayment.retrievePaymentRecord();
			//boolean isInspireProduct = initialPayment.isInspirePROD();
			
			//PAYMENT MODE
			String Parm_paymode = null;
			obj.recordApplicationRequest.policy.paymentModeCD = "";
			
			if (appform_basicvalue_plandetails.has("paymentMode")) {
				Parm_paymode = appform_basicvalue_plandetails.getString("paymentMode");	
				obj.recordApplicationRequest.policy.paymentModeCD =	getPaymentModeCD(Parm_paymode);
			} else {
				throw new IOException("Mandatory field missing - payment mode code");
			}
			
			if (isShield) {
				if (!applicationDoc.getString("policyNumber").equals(ShieldBasicPolicyNo)) {
					Parm_paymode = quotation_plans.getJSONObject(1).getString("payFreq");	
					obj.recordApplicationRequest.policy.paymentModeCD =	getPaymentModeCD(Parm_paymode);
				}
			}
			
			if (is_inspireDuo) {
				obj.recordApplicationRequest.policy.paymentModeCD = "M";
			}
			
			if (Parm_paymode.equals("L"))
				obj.recordApplicationRequest.policy.singlePremiumPolicyFLG = "Y";
			else 
				obj.recordApplicationRequest.policy.singlePremiumPolicyFLG = "";
			
			//PAYMENT METHOD	
			obj.recordApplicationRequest.policy.paymentMethodCD = "";
			
			if (is_inspireDuo || is_awtProduct) {
				obj.recordApplicationRequest.policy.paymentMethodCD = initialPayment.getPaymentMethod();	
			} 
		
			//CURRENCY
			if (payment != null && payment.has("policyCcy")) {
				obj.recordApplicationRequest.policy.currencyCD = payment.getString("policyCcy");
			} else {
				obj.recordApplicationRequest.policy.currencyCD = "";
			}
			
			if(isShield)
				obj.recordApplicationRequest.policy.currencyCD = quotation.getString("ccy");
				
			//ANNUAL PREM EXCLUDE TOP-UP
			if (appform_basicvalue_plandetails.has("totYearPrem")) {
				obj.recordApplicationRequest.policy.minPremiumAMT = appform_basicvalue_plandetails.getBigDecimal("totYearPrem").toString();
			} else {
				throw new IOException("Mandatory field missing - minPremiumAMT.");
			}
			
			//MODAL PREM EXCLUDE TOP-UP R
			switch(Parm_paymode)
			{
			  case "L":
				  if (quotation.has("totYearPrem")) {
					obj.recordApplicationRequest.policy.premiumAMT = quotation.getBigDecimal("totYearPrem").toString();	
				  }
				  break;
			  case "A":
				  if (quotation.has("totYearPrem")) {
					obj.recordApplicationRequest.policy.premiumAMT = quotation.getBigDecimal("totYearPrem").toString();	
				  }
				  break;
			  case "S":
				  if (quotation.has("totHalfyearPrem")) {
					obj.recordApplicationRequest.policy.premiumAMT = quotation.getBigDecimal("totHalfyearPrem").toString();	
				  }
				  break;
			  case "Q":
				  if (quotation.has("totQuarterPrem")) {
					obj.recordApplicationRequest.policy.premiumAMT = quotation.getBigDecimal("totQuarterPrem").toString();		
				  }
				  break;
			  case "M":
				  if (quotation.has("totMonthPrem")) {
					obj.recordApplicationRequest.policy.premiumAMT = quotation.getBigDecimal("totMonthPrem").toString();	
				  }
				  break;
			  default:
				  Log.error("invalid Pay_Frequency");
				  break;
			}

			//TOP-UP REGULAR MODAL PREMIUM
			//sample: obj.recordApplicationRequest.policy.topupRegularModalPremium = "0";
			try {
				if (appform_basicvalue_plandetails != null && appform_basicvalue_plandetails.has("rspSelect") && appform_basicvalue_plandetails.has("rspSelect") && appform_basicvalue_plandetails.getBoolean("rspSelect")) {
					if (appform_basicvalue_plandetails.has("rspAmount")) {
						obj.recordApplicationRequest.policy.topupRegularModalPremium = appform_basicvalue_plandetails.getBigDecimal("rspAmount").toString();
						Pol_remarks.addpolicyRemarks("W/ RSP");
					}
				}
			} catch (Exception e) {}

			
			//INITIAL LUMP SUM PREMIUM
			//hard code 5000 firstly from feedback on 10/12/2017 11:50 AM
			//obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = "5000";
			//obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = "0";
			 
			//boolean is_bandAid = false;
			//boolean is_inspireDuo = false;
			if (is_inspireDuo || is_bandAid || is_savvySaver)
				obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = quotation.getBigDecimal("premium").toString();
			else 
				obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = "";
			//if (payment.has("initBasicPrem")) {
			//	obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = Double.toString(payment.getDouble("initBasicPrem"));
			//}

			//LUMP SUM PREMIUM
			//sample: obj.recordApplicationRequest.policy.lumpsumPremiumAMT = "0";
			if (appform_basicvalue_plandetails != null && appform_basicvalue_plandetails.has("topUpSelect") && appform_basicvalue_plandetails.getBoolean("topUpSelect")) {
				obj.recordApplicationRequest.policy.lumpsumPremiumAMT = appform_basicvalue_plandetails.getBigDecimal("topUpAmt").toString();
				Pol_remarks.addpolicyRemarks("W/ Top Up");
			}
			
			//RecordApplicationRequest > policy - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasDetailsOfPolicyIn - S

			//U/W REMARKS 1
			String uwRemark1 = "";
			HasDetailsOfPolicyInItemType.uwRemark1 = "";

			if ("R".equals(ApprovalStatus)) {
				uwRemark1 += "eApp Rejected by Supervisor,";
				Pol_remarks.addpolicyRemarks("eApp Rejected by Supervisor");
			}
				
			if (isPaid && ("INVALIDATED".equals(ApplicationStatus))) {
				uwRemark1 += "eApp Invalid (paid)," ;	
				Pol_remarks.addpolicyRemarks("Eapp Invalidated Case");
			} else if (isPaid && ("INVALIDATED_SIGNED".equals(ApplicationStatus))) {
				uwRemark1 += "eApp Invalid (paid)," ;	
				Pol_remarks.addpolicyRemarks("Eapp Invalidated Case");
			}
						
			if ("E".equals(ApprovalStatus)) {
				uwRemark1 += "eApp Expired (paid)," ;
				Pol_remarks.addpolicyRemarks("Supervisor Approval Expired");
			}		
			
			if (!paymentmethod.equals("")) {
				uwRemark1 += getRLSPaymentMethodCD(paymentmethod, true) + "," ;
				Pol_remarks.addpolicyRemarks(getRLSPaymentMethodCD(paymentmethod, false));
			}
			
			if (is_awtProduct || is_flexiProduct) {				
				if (applicationForm_values_planDetails.has("wdSelect")) {
					if (applicationForm_values_planDetails.getBoolean("wdSelect")) {
						uwRemark1 += "w/ RW," ;
						Pol_remarks.addpolicyRemarks("W/ Regular Withdrawal Option");
					}	
				}
			}
			
			if (uwRemark1.endsWith(","))
				HasDetailsOfPolicyInItemType.uwRemark1 = uwRemark1.substring(0, uwRemark1.length() - 1);
			
//			if (appform_basicvalue_plandetails != null && appform_basicvalue_plandetails.has("wdInd") && appform_basicvalue_plandetails.getBoolean("wdInd"))
//				Pol_remarks.addpolicyRemarks("W/ regular withdrawal option");
			
			
			//HasDetailsOfPolicyInItemType.uwRemark1 = HasDetailsOfPolicyInItemType.uwRemark1 //TODO
					
					
			
			//add policy status/approval status to policy summary remark: BD19-008
//			if (!("".equals(HasDetailsOfPolicyInItemType.uwRemark1))) {
//				Pol_remarks.addpolicyRemarks(HasDetailsOfPolicyInItemType.uwRemark1);
//			}

			//OWNER AGE
			//sample:HasDetailsOfPolicyInItemType.ownerAgeAtIssue = "54";

			if (is_3rdparty) {
				HasDetailsOfPolicyInItemType.ownerAgeAtIssue = Integer.toString(OW_issueage);
			}
			


			//DIVIDEND OPTION
			//sample HasDetailsOfPolicyInItemType.dvdoIND = "2";
			if (quotation.has("divInd")) {
				HasDetailsOfPolicyInItemType.dvdoIND = quotation.getString("divInd");
			} else {
				HasDetailsOfPolicyInItemType.dvdoIND = "";
			}

			//DEATH BENEFIT OPTION
			//not in release 1
			//HasDetailsOfPolicyInItemType.dboIND = "C";
			if (quotation.has("deathBenF")) {
				HasDetailsOfPolicyInItemType.dboIND = quotation.getString("deathBenF");
			} else {
				HasDetailsOfPolicyInItemType.dboIND = "";
			}	
			
			
			if (quotation_policyOptions.has("deathBenefit")) {
				if (quotation_policyOptions.getString("deathBenefit").toUpperCase().equals("BASIC"))
					HasDetailsOfPolicyInItemType.dboIND = "D";
				else if (quotation_policyOptions.getString("deathBenefit").toUpperCase().equals("ENHANCED")) 
					HasDetailsOfPolicyInItemType.dboIND = "C";
				else 
					HasDetailsOfPolicyInItemType.dboIND = "";
			}
			
			JSONObject CPFpaymentBlockNM = null;
			String Block_fieldNM = null;
			String IsHaveCpfAcct_fieldNM =  null;
			String InvmAcctNo_fieldNM = null;
			String BankNamePicker_fieldNM  = null;
			String withCPFAccountFlagNM = null;
			boolean withCPFAccount = false;
			String Payor_paymentMethod = "";
			
			if (quotation_policyOptions.has("paymentMethod")) {
				if (isCPFPayment) {	
					Payor_paymentMethod = quotation_policyOptions.getString("paymentMethod");
					Block_fieldNM  = Payor_paymentMethod+ "Block";

					if (payment.has(Block_fieldNM)) {
						CPFpaymentBlockNM	= payment.getJSONObject(Block_fieldNM);
					} else {
						throw new IOException(Payor_paymentMethod + " payment block missing");
				  	}
					
					IsHaveCpfAcct_fieldNM = Payor_paymentMethod + "IsHaveCpfAcct"; 
					InvmAcctNo_fieldNM = Payor_paymentMethod + "InvmAcctNo";
					BankNamePicker_fieldNM  = Payor_paymentMethod + "BankNamePicker";
					withCPFAccountFlagNM = Payor_paymentMethod + "IsHaveCpfAcct";
					
					if (CPFpaymentBlockNM.has(withCPFAccountFlagNM) &&("Y".equals(CPFpaymentBlockNM.getString(withCPFAccountFlagNM)))){
						withCPFAccount = true;
					}
				}
			}
					
			//CPF A/C TYPE
			HasDetailsOfPolicyInItemType.cpfAcctTypeCD = "";
			
			if (isCPFPayment) {
				switch(Payor_paymentMethod)
				{
				  case "cpfisoa":
					  HasDetailsOfPolicyInItemType.cpfAcctTypeCD = "O";
					  break;
				  case "cpfissa":
					  HasDetailsOfPolicyInItemType.cpfAcctTypeCD = "S";	
					  break;
				  case "srs":
					  HasDetailsOfPolicyInItemType.cpfAcctTypeCD = "R";	
					  break;
				  default:
					  HasDetailsOfPolicyInItemType.cpfAcctTypeCD = "";
					  break;
				}
			}	
		
			//Do not consider without CPY payment scenario.
			//if (isCPFPayment&&(!withCPFAccount)) {
			//	HasDetailsOfPolicyInItemType.cpfAcctTypeCD = "";	
			//}
		
			//PAYMENT MODE OF DE-LINK RTU
			HasDetailsOfPolicyInItemType.rtuPaymentModeCD = "";
	
			//PAYMENT METHOD OF DE-LINK RTU
			HasDetailsOfPolicyInItemType.rtuPaymentMethodCD = "";
			
			if (is_inspireDuo || is_awtProduct || is_flexiProduct) {
				if (payment != null && payment.getDouble("initRspPrem") > 0) {
					 parm_PaymentType		= "RSP";
					 parm_PlanCD			= Basic_planCD;
					 parm_PaymentMethod		= paymentmethod;
					 parm_PaymentMode		= "";
					 
					 if (appform_basicvalue_plandetails.has("rspPayFreq")) {  
						 switch(appform_basicvalue_plandetails.getString("rspPayFreq").toUpperCase()) {
						 	case "ANNUAL":
						 		parm_PaymentMode = "A";
						 		break;
						 	case "SEMIANNUAL":
						 		parm_PaymentMode = "S";
						 		break;
						 	case "QUARTERLY":
						 		parm_PaymentMode = "Q";
						 		break;
						 	default:
						 		parm_PaymentMode = "M";
						 		break;
						 }
						 HasDetailsOfPolicyInItemType.rtuPaymentModeCD = parm_PaymentMode;
					 }  
					 parm_GiroIND = "N";
					 
					 if (payment.has("subseqPayMethod")) {
						 if ("Y".equals(payment.getString("subseqPayMethod"))) {
							 parm_GiroIND = "Y";
						 }	 
					 }
					 
					 PaymentCodes rspPayment = new PaymentCodes(); 
					 //"INIT" : initial payment ; GIRO
					 rspPayment.setPayKeys(parm_PaymentType, parm_PlanCD, parm_PaymentMethod, parm_PaymentMode, parm_GiroIND);
					 rspPayment.retrievePaymentRecord();
					 HasDetailsOfPolicyInItemType.rtuPaymentMethodCD = rspPayment.getPaymentMethod();	 
				}
			}
			
			//RecordApplicationRequest > policy > hasDetailsOfPolicyIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasProducerInformationIn - S

			//AGENT 1 CODE  
		
			//JSONObject agentDoc = null;
			String tmp_agentcode = ""; 
			String agentID = "";
			
			if (quotation_agent.has("agentCode")) {
				tmp_agentcode = quotation_agent.getString("agentCode");
			} else {
				throw new IOException("Mandatory field missing - agent code not found");
			}
			
			//direct get from bundle
			JSONObject agentView =  Constant.CBUTIL.getDoc("_design/main/_view/agents?startkey=[\"01\",\"0\"]&endkey=[\"01\",\"ZZ\"]&stale=ok");
			JSONArray agentArray = agentView.getJSONArray("rows");	
			JSONObject agentDoc_tmp = null;	

			for (int i = 0; i < agentArray.length(); i++) {
				JSONObject agentObject =  agentArray.getJSONObject(i).getJSONObject("value");
					if (agentObject.getString("agentCode").equals(tmp_agentcode)) {
						agentID =  agentArray.getJSONObject(i).getString("id"); //for easy testing
						//if (agentObject.has("agentCodeDisp")) {  				//comment to get it from agent profile as dev do not has this field
						//	agentID = agentObject.getString("agentCodeDisp");
						//}
						agentDoc_tmp = Constant.CBUTIL.getDoc(agentID); 
						
						if (agentDoc_tmp.has("agentCodeDisp")) {
							agentID = agentDoc_tmp.getString("agentCodeDisp");
							break;
						}
				}
			}
			
			if (agentDoc_tmp == null) {
				//throw new IOException("Agent Profile Missing.");
			} else {
				HasProducerInformationInItemType.producerID = agentID.replaceAll("-", "");
				//HasProducerInformationInItemType.producerID = add_leading_zero(agentID).replaceAll("-", "");
				//HasProducerInformationInItemType.producerID = tmp_producerID.substring(tmp_producerID.length() -16, 16);
			}
			
			//RecordApplicationRequest > policy > hasProducerInformationIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith - S
			JSONObject Owner_personalInfo_branchInfo = null;
			boolean isSINGPOST = false;
			 
			if (quotation.has("agent")) {
				 if ("SINGPOST".equals(quotation_agent.getString("dealerGroup").toUpperCase())) {
					 isSINGPOST = true;
					 Owner_personalInfo_branchInfo = Owner_personalInfo.getJSONObject("branchInfo"); 
				 }
			}
			
			

			//Bank Branch Code
			//HasAssociationWithItemType.bankBranchCD = "";
			if (isSINGPOST) {
				HasAssociationWithItemType.bankBranchCD = getBankCD(Owner_personalInfo_branchInfo.getString("branch"));
				
				//Bank Code
				HasAssociationWithItemType.bankCD = "1";
				
				//Region Code
				HasAssociationWithItemType.stateRegionCD = "00";
				
				//Area Code
				HasAssociationWithItemType.areaCD = "00";
				
				//Lead category
				HasBankReferrerInfoInItemType.leadCategoryCD = "";
			} else {
				HasAssociationWithItemType.bankBranchCD = "";
				
				//Bank Code
				HasAssociationWithItemType.bankCD = "";
				
				//Region Code
				HasAssociationWithItemType.stateRegionCD = "";
				
				//Area Code
				HasAssociationWithItemType.areaCD = "";
				
				//Lead category
				HasBankReferrerInfoInItemType.leadCategoryCD = "";
			}
			
			//RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith > hasStaffInformationIn - S

			//Channel ID
			/*if (1 == 1) {
				HasStaffInformationInItemType.channelID = "1";
			} else {
				throw new IOException("Mandatory field missing - channel ID");
			}*/
			
		
			if (isSINGPOST) {		
				HasStaffInformationInItemType.channelID = "1";
				HasStaffInformationInItemType.staffIND = "S";				
				HasStaffInformationInItemType.staffCD = Owner_personalInfo_branchInfo.getString("bankRefId");
			} else {
				HasStaffInformationInItemType.channelID = "";
				HasStaffInformationInItemType.staffIND = "";				
				HasStaffInformationInItemType.staffCD = "";
			}
			
//			if (isShield)
//				HasStaffInformationInItemType.channelID = "1";

			//} else {
			//	throw new IOException("Mandatory field missing - Bank Staff ID");
			//}

			//RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith > hasStaffInformationIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith > hasStaffInformationIn > hasBankReferrerInfoIn- S

			//leadID	
			//if (1 == 1) {
			if (isSINGPOST) {
				HasBankReferrerInfoInItemType.leadID = "1";
			} else {
				HasBankReferrerInfoInItemType.leadID = "";
			}
			//} 
			//else {
			//	throw new IOException("Mandatory field missing - lead ID");
			//}

			//Lead category
			//if (1 == 1) {
				//HasBankReferrerInfoInItemType.leadCategoryCD = "";
			//} 
			//else {
			//	throw new IOException("Mandatory field missing - lead category");
			//}
			
			//RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith > hasStaffInformationIn > hasBankReferrerInfoIn- E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn - S

			//INSURED NAME			
			//sample:	HasPersonalDetailsInItemType.lastNM = quotation.getString("iLastName");


			String LA_fullNM = "";	
			String LA_ChineseNM = "";
			String LA_othName = "";
			String LA_lastNM = "";
			String LA_firstNM = "";
			if (is_1stparty || is_3rdparty)
			{
//				String LA_lastNM = "";
//				String LA_firstNM = "";
				if (LifeAssured_personalInfo.has("lastName"))
				{
					LA_lastNM = spaceRemover(truncateSpecialChar(LifeAssured_personalInfo.getString("lastName").toUpperCase()));
				}
				if (LifeAssured_personalInfo.has("firstName"))
				{
					LA_firstNM = spaceRemover(truncateSpecialChar(LifeAssured_personalInfo.getString("firstName").toUpperCase()));
				}
				if (LifeAssured_personalInfo.has("fullName")) {
					LA_fullNM = spaceRemover(truncateSpecialChar(LifeAssured_personalInfo.getString("fullName").toUpperCase()));
				}
				
				if (LifeAssured_personalInfo.has("hanyuPinyinName")) {
					LA_ChineseNM = spaceRemover(truncateSpecialChar(LifeAssured_personalInfo.getString("hanyuPinyinName").toUpperCase()));
				}
				
				if (LifeAssured_personalInfo.has("othName")) {
					LA_othName = spaceRemover(truncateSpecialChar(LifeAssured_personalInfo.getString("othName").toUpperCase()));
				}
				
			    HasPersonalDetailsInItemType.lastNM = LA_fullNM;
			    
			    if (!isShield) {
			    	HasPersonalDetailsInItemType.firstNM = "";
			    } else {
			    	if (LifeAssured_policies.has("ROP_01")) {
			    		if (LifeAssured_policies.getString("ROP_01").equals("Y")) {
			    			HasPersonalDetailsInItemType.externalShieldIND = "Y";
				    		Pol_remarks.addpolicyRemarks("ROP declaration");
			    		} else { 
			    			HasPersonalDetailsInItemType.externalShieldIND = "N";
			    		}
			    	}
			    	
			    	HasPersonalDetailsInItemType.takeOverBusinessIND = "N";
			    	HasPersonalDetailsInItemType.projectionStatusIND = "P";
			    }
			}

			//SEX
			//sample: HasPersonalDetailsInItemType.genderCD = "M";
			HasPersonalDetailsInItemType.genderCD = LA_Gender;
		

			//BIRTH DATE
			//HasPersonalDetailsInItemType.birthDT = quotation.getString("iDob");
			HasPersonalDetailsInItemType.birthDT = "";
			String LA_birthDate = "";
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_personalInfo.has("dob")) {
					HasPersonalDetailsInItemType.birthDT = LifeAssured_personalInfo.getString("dob");
					LA_birthDate = HasPersonalDetailsInItemType.birthDT;
				}
			}
			
			if ("".equals(LA_birthDate)) {
				throw new IOException("client birth date missing");
			}
									
			//MARITAL STATUS
			//sample: HasPersonalDetailsInItemType.maritalStatusCD = "M";
			HasPersonalDetailsInItemType.maritalStatusCD = "";
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_personalInfo.has("marital")) {
					HasPersonalDetailsInItemType.maritalStatusCD = LifeAssured_personalInfo.getString("marital");
				}
			}

			//HEIGHT IN METRE FOR INS/SPO
			
			//can not move if 0 : if input, then must not be 0;
			//HasPersonalDetailsInItemType.height = "0";
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_insurability.has("HW01") && (LifeAssured_insurability.getDouble("HW01")>0) ) {
					HasPersonalDetailsInItemType.height = Double.toString(LifeAssured_insurability.getDouble("HW01"));
				}
			}
			//for bandAid, height default to be 1.5 
			//mail at 10:20; 19 Oct
			if(is_bandAid) {
				HasPersonalDetailsInItemType.height = "1.5";
			}
			
			//WEIGHT IN KG FOR INS/SPO/DE
			//sample:HasPersonalDetailsInItemType.weight = "67.1";
			Double LA_weight = 0.0;
			
			if (LifeAssured_insurability.has("HW02")&&(LifeAssured_insurability.getDouble("HW02")>0)) {
				LA_weight = LifeAssured_insurability.getDouble("HW02");
			}
			
			//can not move if 0 : if input, then must not be 0;
			//HasPersonalDetailsInItemType.weight = "0.0";
			if (is_1stparty || is_3rdparty) {		
				if (LA_weight > 0) {
					HasPersonalDetailsInItemType.weight = Double.toString(LA_weight);	
				}
			}
			if(is_bandAid) {
				HasPersonalDetailsInItemType.weight = "40";
			}
			
			//OCC. CLASS FOR INS/SPO
			HasPersonalDetailsInItemType.stdOccupationCD = "";
			HasPersonalDetailsInItemType.stdOccupationCD = getOccupationClass(LA_Occupation,LA_issueage,LA_Gender);
				
			//MONTHLY INCOME FOR INS/SPO
			HasPersonalDetailsInItemType.annualIncomeAMT = "0";
				
			String LA_income = "0";
			if (is_1stparty || is_3rdparty) {
				if (clientDoc.has("allowance")) {
//					if (isShield) {//For Shield
//						double allowance = clientDoc.getDouble("allowance") / 12;
//						HasPersonalDetailsInItemType.annualIncomeAMT = Double.toString(Math.round(allowance * 100.0) / 100.0);
//					} else {
					
					LA_income = clientDoc.get("allowance").toString();
						HasPersonalDetailsInItemType.annualIncomeAMT = LA_income;
//					}
				}
			}
			
			if (HasPersonalDetailsInItemType.weight != null && HasPersonalDetailsInItemType.height != null) {
				if (isShield) {
					if (!BMIforSheild(LA_issueage, HasPersonalDetailsInItemType.weight, HasPersonalDetailsInItemType.height)) {
						Pol_remarks.addpolicyRemarks("Build");
						isPolicyPMED = true;
					}
				} else {
					if (!BMI(LA_issueage, HasPersonalDetailsInItemType.weight, HasPersonalDetailsInItemType.height)) {
						Pol_remarks.addpolicyRemarks("LA: Check BMI");
						isPolicyPMED = true;
					}
				}
			}

			if (isShield) {
				HasPersonalDetailsInItemType.stdOccupationCD = getOccupationClassForSheild(LA_Occupation, LA_income);
				
				//Proposal Form Signing Date to submission
				//More than 21 days from PF signing date
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				LocalDateTime date = LocalDateTime.parse(applicationDoc.getString("biSignedDate"), formatter);
				LocalDateTime _date = date.plusHours(8);
				
				String biSignedDate =  _date.getYear() + "-" + String.format("%02d", _date.getMonthValue()) + "-" + String.format("%02d", _date.getDayOfMonth());
				
				SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

				try {
					Date dateBefore = myFormat.parse(biSignedDate);
					Date dateAfter = myFormat.parse(APPROVE_REJECT_date);
					long difference = dateAfter.getTime() - dateBefore.getTime();
					float daysBetween = (difference / (1000*60*60*24));
					
					if (daysBetween > 21) {
						Pol_remarks.addpolicyRemarks("Appl Sign date > 21 days");
						isPolicyPMED = true;
					}
				} catch (Exception e) {
					//e.printStackTrace();
				}				
			}
			
			if("4".equals(HasPersonalDetailsInItemType.stdOccupationCD) || "6".equals(HasPersonalDetailsInItemType.stdOccupationCD)){
				HasPersonalDetailsInItemType.annualIncomeAMT = "0";
			}
			
			boolean isSameAnswer = false;
			//only when the question answer are same for Singaporean Citizen,Singapore PR / Employment Pass / Work Permit,Dependent / Student / Long Term Visit Pass
			//it can be SIT
			String LA_P6_Q1 = "";
			String LA_P6_Q2 = "";
			String LA_P6_Q3 = "";
			String LA_P6_Q4 = "";
			String OW_P6_Q1 = "";
			String OW_P6_Q2 = "";
			String OW_P6_Q3 = "";
			String OW_P6_Q4 = "";
			
			//if (is_3rdparty) {
				//Insured
				if (LifeAssured_residency.has("EAPP-P6-F1")) {
					LA_P6_Q1 = LifeAssured_residency.getString("EAPP-P6-F1");
				}	
				if (LifeAssured_residency.has("EAPP-P6-F2")) {
					LA_P6_Q2 = LifeAssured_residency.getString("EAPP-P6-F2");
				}
				if (LifeAssured_residency.has("EAPP-P6-F3")) {
					LA_P6_Q3 = LifeAssured_residency.getString("EAPP-P6-F3");
				}
				if (LifeAssured_residency.has("EAPP-P6-F4")) {
					LA_P6_Q4 = LifeAssured_residency.getString("EAPP-P6-F4");
				}
	
				//Proposer
				if (Owner_residency.has("EAPP-P6-F1")) {
					OW_P6_Q1 = Owner_residency.getString("EAPP-P6-F1");
				}	
				if (Owner_residency.has("EAPP-P6-F2")) {
					OW_P6_Q2 = Owner_residency.getString("EAPP-P6-F2");
				}
				if (Owner_residency.has("EAPP-P6-F3")) {
					OW_P6_Q3 = Owner_residency.getString("EAPP-P6-F3");
				}
				if (Owner_residency.has("EAPP-P6-F4")) {
					OW_P6_Q4 = Owner_residency.getString("EAPP-P6-F4");
				}
			//}
//			if(LA_P6_Q1.equals(OW_P6_Q1) && LA_P6_Q2.equals(OW_P6_Q2) && LA_P6_Q3.equals(OW_P6_Q3) && LA_P6_Q4.equals(OW_P6_Q4)) {
//				HasPersonalDetailsInItemType.countryOfResidenceCD = "LR";
//				isSameAnswer = true;
//			} else {
//				HasPersonalDetailsInItemType.countryOfResidenceCD = "FR";
//			}
			
					
			//CITIZEN TYPE
			//sample: HasPersonalDetailsInItemType.documentID = "S78654321";
			HasPersonalDetailsInItemType.documentID = "S00000000";
			
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_personalInfo.has("idCardNo")) {
					HasPersonalDetailsInItemType.documentID = truncateSpecialChar(LifeAssured_personalInfo.getString("idCardNo"));
				}
			}

			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasUndewritingResultsIn - S			
			
			if(isShield) {
				HasUndewritingResultsInType HasUndewritingResultsInType = obj.getHasUndewritingResultsInType();
				HasUndewritingResultsInType.sourceID = "C";
				
				HasDetailsOfRisksInItemType.hasUndewritingResultsIn = HasUndewritingResultsInType;
			}
			
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasUndewritingResultsIn - E
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn - S

			//Issue Ages  
			HasDetailsOfRisksInItemType.issueAge = "0";
			if (is_1stparty || is_3rdparty) {
				HasDetailsOfRisksInItemType.issueAge = Integer.toString(LA_issueage);
			}
			
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
			//AGE AT ISSUE
			if (is_1stparty || is_3rdparty) {
				if (Owner_personalInfo.has("age")) {
					HasDetailsOfPolicyInItemType.ownerAgeAtIssue = Integer.toString(Owner_personalInfo.getInt("age"));
				}
			}
		
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasAddressIn - S

			//CORR ADDRESS
			//Get mail address for this owner's address if proposer's mail addresss is different with resident address, if same with the resident address, get the resident address.

			if (is_1stparty||is_3rdparty) {
				//Address 1
				if (ismAddrdifferent){
					String ow_mail_block = "";
					String ow_mail_unit = "";	
					
					if (Owner_personalInfo.has("mAddrBlock")) {
						ow_mail_block = Owner_personalInfo.getString("mAddrBlock").toUpperCase();
					}
					
					if (Owner_personalInfo.has("mAddrnitNum")) {
						ow_mail_unit = Owner_personalInfo.getString("mAddrnitNum").toUpperCase();
					}
					
					if ("".equals(ow_mail_block)&&"".equals(ow_mail_unit)) {
						HasAddressesInItemType.addressLine1 = "";
					} else if ("".equals(ow_mail_block)){
						HasAddressesInItemType.addressLine1 = truncateSpecialChar(ow_mail_unit);
					} else if ("".equals(ow_mail_unit)) {
						HasAddressesInItemType.addressLine1 = truncateSpecialChar(ow_mail_block);
					} else {
						HasAddressesInItemType.addressLine1 = truncateSpecialChar(ow_mail_block) + " " + truncateSpecialChar(ow_mail_unit);
					}
					
					//Address 2
					HasAddressesInItemType.addressLine2 = "";
					
					if (Owner_personalInfo.has("mAddrStreet")) {
						HasAddressesInItemType.addressLine2 = truncateSpecialChar(Owner_personalInfo.getString("mAddrStreet").toUpperCase());
					}
					
					//Address 3
					String ow_mail_building = "";
					String ow_mail_city = "";
					
					if (Owner_personalInfo.has("mAddrEstate")) {
						ow_mail_building = truncateSpecialChar(Owner_personalInfo.getString("mAddrEstate").toUpperCase());
					}
					
					if (Owner_personalInfo.has("mAddrCity")) {
						ow_mail_city = truncateSpecialChar(Owner_personalInfo.getString("mAddrCity").toUpperCase());
					}
					
					if ("".equals(ow_mail_building)&&"".equals(ow_mail_city)) {
						HasAddressesInItemType.addressLine3 = "";
					} else if ("".equals(ow_mail_building)){
						HasAddressesInItemType.addressLine3 = CityConversion(ow_mail_city);
					} else if ("".equals(ow_mail_city)) {
						HasAddressesInItemType.addressLine3 = ow_mail_building;
					} else {
						HasAddressesInItemType.addressLine3 = ow_mail_building + " " + CityConversion(ow_mail_city);
					}
					
					//Address 4
					String ow_mail_country = "";
					HasAddressesInItemType.addressLine4 = "";
					
					if (Owner_personalInfo.has("mAddrCountry")) {
						ow_mail_country = CountryConversion(Owner_personalInfo.getString("mAddrCountry").toUpperCase());
						
						if (ow_mail_country.toLowerCase().contains("China".toLowerCase())) {
							HasAddressesInItemType.addressLine4 = "CHINA" + " " + truncateSpecialChar(Owner_personalInfo.getString("mPostalCode"));
						} else {
							HasAddressesInItemType.addressLine4 = truncateSpecialChar(ow_mail_country) + " " + truncateSpecialChar(Owner_personalInfo.getString("mPostalCode"));
						}
					}
					
					//Address 5
					HasAddressesInItemType.cityNM = "";
					
					if (Owner_personalInfo.has("addrCountry")) {
						HasAddressesInItemType.cityNM = getCountryCD(truncateSpecialChar(ow_mail_country)); 
					}
				}
				else {
					String ow_tmp_block = "";
					String ow_tmp_unit = "";	
					
					if (Owner_personalInfo.has("addrBlock")) {
						ow_tmp_block = truncateSpecialChar(Owner_personalInfo.getString("addrBlock").toUpperCase());
					}
					
					if (Owner_personalInfo.has("unitNum")) {
						ow_tmp_unit = truncateSpecialChar(Owner_personalInfo.getString("unitNum").toUpperCase());
					}
					
					if ("".equals(ow_tmp_block) &&"".equals(ow_tmp_unit)) {
						HasAddressesInItemType.addressLine1 = "";
					} else if ("".equals(ow_tmp_block)){
						HasAddressesInItemType.addressLine1 = ow_tmp_unit;
					} else if ("".equals(ow_tmp_unit)) {
						HasAddressesInItemType.addressLine1 = ow_tmp_block;
					} else {
						HasAddressesInItemType.addressLine1 = ow_tmp_block + " " + ow_tmp_unit;
					}
					
					//Address 2
					HasAddressesInItemType.addressLine2 = "";
					
					if (Owner_personalInfo.has("addrStreet")) {
						HasAddressesInItemType.addressLine2 = truncateSpecialChar(Owner_personalInfo.getString("addrStreet").toUpperCase());
					}
					
					//Address 3
					String ow_tmp_building = "";
					String ow_tmp_city = "";
					
					if (Owner_personalInfo.has("addrEstate")) {
						ow_tmp_building = truncateSpecialChar(Owner_personalInfo.getString("addrEstate").toUpperCase());
					}
					
					if (Owner_personalInfo.has("addrCity")) {
						ow_tmp_city = truncateSpecialChar(Owner_personalInfo.getString("addrCity").toUpperCase());
					}
					
					if ("".equals(ow_tmp_building)&&"".equals(ow_tmp_city)) {
						HasAddressesInItemType.addressLine3 = "";
					} else if ("".equals(ow_tmp_building)){
						HasAddressesInItemType.addressLine3 = CityConversion(ow_tmp_city);
					} else if ("".equals(ow_tmp_city)) {
						HasAddressesInItemType.addressLine3 = ow_tmp_building;
					} else {
						HasAddressesInItemType.addressLine3 = ow_tmp_building + " " + CityConversion(ow_tmp_city);
					}
					
					//Address 4
					String ow_tmp_country = "";
					HasAddressesInItemType.addressLine4 = "";
					if (Owner_personalInfo.has("addrCountry")) {
						ow_tmp_country = CountryConversion(Owner_personalInfo.getString("addrCountry").toUpperCase());
						if (ow_tmp_country.toLowerCase().contains("China".toLowerCase())) {
							HasAddressesInItemType.addressLine4 = "CHINA" + " " + truncateSpecialChar(Owner_personalInfo.getString("postalCode").toUpperCase());
						} else {
							HasAddressesInItemType.addressLine4 = truncateSpecialChar(ow_tmp_country) + " " + truncateSpecialChar(Owner_personalInfo.getString("postalCode").toUpperCase());
						}
					}

					//Address 5
					HasAddressesInItemType.cityNM = "";
					
					if (Owner_personalInfo.has("addrCountry")) {
						HasAddressesInItemType.cityNM = getCountryCD(truncateSpecialChar(ow_tmp_country)); 
					}
				}
			}	

			//MOBILE'NUMBER OF OWNER
			//sample HasAddressesInItemType.mobilePhoneNO = "9876512345";
			if (is_3rdparty) {	
				if (Owner_personalInfo.has("mobileNo")) {
					try {
						HasAddressesInItemType.mobilePhoneNO = Long.toString(Owner_personalInfo.getLong("mobileNo")); 
					} catch(Exception ex001) {
						HasAddressesInItemType.mobilePhoneNO = Owner_personalInfo.getString("mobileNo");
					}
				}
			}
			
			//EMAIL ADDRESS OF OWNER			
			if (is_3rdparty) {
				if (Owner_personalInfo.has("email")) {
					HasAddressesInItemType.emailAddress = truncateSpecialChar(Owner_personalInfo.getString("email").toUpperCase());
				}
			}

			//TEL-RES
			//sample:HasAddressesInItemType.telephoneNO = "9876543210";
			//Remark: need to enhanced to fix the issue while toString(002378474) becomes 2378474.
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_personalInfo.has("mobileNo")) {
					try {
						HasAddressesInItemType.telephoneNO = Long.toString(LifeAssured_personalInfo.getLong("mobileNo")); 
					} catch(Exception ex003) {
						HasAddressesInItemType.telephoneNO = LifeAssured_personalInfo.getString("mobileNo");
					}
				}
			}

			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasAddressIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasIndividualUWInfoIn - S
			//Owner level Indicator:
			HasIndividualUWInfoInItemType.declineQuestionIND = "";
			HasIndividualUWInfoInItemType.avocationQuestionIND = "";
			HasIndividualUWInfoInItemType.healthQuestionIND = "";
			HasIndividualUWInfoInItemType.previousHealthExamQuestionIND = "";
			HasIndividualUWInfoInItemType.previousHealthExamResultIND = "";
			HasIndividualUWInfoInItemType.alcoholQuestionIND = "";
			HasIndividualUWInfoInItemType.alcoholUnitsPerWeekNO = "0";
			HasIndividualUWInfoInItemType.tobaccoQuestionIND = "";
			HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "0";
			HasIndividualUWInfoInItemType.smokingQuestionIND = "";
			//HasIndividualUWInfoInItemType.previousHealthExamQuestionIND = "";
			HasIndividualUWInfoInItemType.previousHealthExamReasonIND = "";
			//HasIndividualUWInfoInItemType.previousHealthExamResultIND = "";	
			HasIndividualUWInfoInItemType.ePolicyDeliveryFLG = "P";
			
			//ePolicy
			if (Owner_declaration.has("epolicy01")) {
				if (Owner_declaration.get("Owner_declaration").equals("Y"))
					HasIndividualUWInfoInItemType.ePolicyDeliveryFLG = "E";
				else
					HasIndividualUWInfoInItemType.ePolicyDeliveryFLG = "B";
			}
			
			Double OW_weight = 0.0;
			
			
			if (is_3rdparty && hasPayorBenefitRider && !isShield) {
				HasIndividualUWInfoInItemType.declineQuestionIND = "N";
				
				//if ((!isOWPMED)||(!isPolicyPMED)) {
					if (Owner_insurability.has("INS01")) { //as payor, user owner 
						HasIndividualUWInfoInItemType.declineQuestionIND = Owner_insurability.getString("INS01");
						
						if ("Y".equals(Owner_insurability.getString("INS01"))) {
			//				isOWPMED = true;
							isPolicyPMED = true;
							Pol_remarks.addpolicyRemarks("PH: INS History = Y");
						}
					}
				//}
				
				//if ((!isOWPMED)||(!isPolicyPMED)) {
					if (Owner_insurability.has("INS02")) {
						if ("Y".equals(Owner_insurability.getString("INS02"))){
							isOWPMED = true;
							isPolicyPMED = true;
							
							Pol_remarks.addpolicyRemarks("PH: INS History = Y");
						}
					}
				//}

				if ((!isOWPMED)) {
					if (Owner_insurability.has("LIFESTYLE06")) {
						if ("Y".equals(Owner_insurability.getString("LIFESTYLE06"))){
							isOWPMED = true;
						}
					}
				}
				
				String OW_medicalQN = null;
				
				for(int QN_COUNT = 0; QN_COUNT < medicalQNString.length; QN_COUNT++) {
					OW_medicalQN = medicalQNString[QN_COUNT];
					
					if (Owner_insurability.has(OW_medicalQN))
						if ("Y".equals(Owner_insurability.getString(OW_medicalQN))) {
							isOWPMED = true;
							isPolicyPMED = true;
							Pol_remarks.addpolicyRemarks("PH: Health Qn = Y");
						}
					if (isOWPMED&&isPolicyPMED) {
						break;
					}
				}
				
				//Details of Regular Doctor
				if (Owner_insurability.has("REG_DR01") && "Y".equals(Owner_insurability.getString("REG_DR01"))) {
					if (Owner_insurability.has("REG_DR01_DATA")) {
						JSONArray REG_DR01_DATA_array = Owner_insurability.getJSONArray("REG_DR01_DATA");
						
						for(int i = 0; i < REG_DR01_DATA_array.length(); i++) {
							JSONObject REG_DR01_DATA = REG_DR01_DATA_array.getJSONObject(i);
							
							if (REG_DR01_DATA.has("REG_DR01d")) {
								if (REG_DR01_DATA.getString("REG_DR01d").toUpperCase().equals("OTHER")) {
									isOWPMED = true;
									isPolicyPMED = true;
									
									Pol_remarks.addpolicyRemarks("PH: Check Reason of Consultation");
									
									break;
								}
							}
						}
					}	
				}
				
				//AVOCATION QUESTION FOR PAYOR
				HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
				
				if (is_1stparty || is_3rdparty) {
					if (Owner_insurability.has("LIFESTYLE05")) {
						if ("Y".equals(Owner_insurability.getString("LIFESTYLE05"))) {
							HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
						} else {
							HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
						}
					}
				}
				
				if (Owner_insurability.has("HW02")) {
					OW_weight = Owner_insurability.getDouble("HW02");
				}
				
				Double OW_weight_change = 0.00;
				Double OW_weight_GainLost = 0.00;
				Double OW_weightchangePC = 0.0;
				
				//if ((!isOWPMED)||(!isPolicyPMED)) {
					if (Owner_insurability.has("HW03")&&("Y".equals(Owner_insurability.getString("HW03")))) {
						if (Owner_insurability.has("HW03b")) {
							OW_weight_change = Owner_insurability.getDouble("HW03b");
						}
						
						if (OW_weight_change > 0) {
							if ("L".equals(Owner_insurability.getString("HW03a"))) {
								OW_weight_GainLost = OW_weight_change *(-1);
							} else {
								OW_weight_GainLost = OW_weight_change;
							}
						}
						
						OW_weightchangePC = OW_weight_change/(OW_weight + OW_weight_GainLost);
						
						if (OW_weightchangePC > 0.05) {
							isOWPMED = true;
							isPolicyPMED = true;
							
							Pol_remarks.addpolicyRemarks("PH: Weight change = Y");
						} else if (Owner_insurability.has("HW03c") && ("Other".equals(Owner_insurability.getString("HW03c")))) {
							isOWPMED = true;
							isPolicyPMED = true;
							
							Pol_remarks.addpolicyRemarks("PH: Weight change = Y");
						}
					}
				//}

				//PREVIOUS EXAM FOR PAYOR
				HasIndividualUWInfoInItemType.previousHealthExamQuestionIND = "N";

				//PREVIOUS EXAM REASON FOR PA
				HasIndividualUWInfoInItemType.previousHealthExamReasonIND = "";

				//PREVIOUS EXAM RESULT FOR PA
				//Confirmed by Frankie, delete field
				HasIndividualUWInfoInItemType.previousHealthExamResultIND = "";

				//SMOKING QUESTION FOR PAYOR
				HasIndividualUWInfoInItemType.smokingQuestionIND = "N";
				if (is_1stparty || is_3rdparty) {
					if (Owner_insurability.has("LIFESTYLE01")) {
						HasIndividualUWInfoInItemType.smokingQuestionIND = Owner_insurability.getString("LIFESTYLE01");
					}
				}

				//NO. OF CIGARETTE PER DAY FO
				//sample: HasIndividualUWInfoInItemType.cigarettesPerDayNO = "12";
				if (Owner_insurability.has("LIFESTYLE01b")) {
					int cigarettesPerDayNO = Owner_insurability.getInt("LIFESTYLE01b");
										
					if (cigarettesPerDayNO > 99)
						cigarettesPerDayNO = 99;
						
					HasIndividualUWInfoInItemType.cigarettesPerDayNO = Integer.toString(cigarettesPerDayNO);
					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = Integer.toString(cigarettesPerDayNO);
					
					if (cigarettesPerDayNO > 30) {
						isOWPMED = true;
						isPolicyPMED = true;
												
						Pol_remarks.addpolicyRemarks("PH: Cigarette > 30");
					}	
				}
				
				if (is_inspireDuo || is_pulsarProduct) {
					if (quotation.has("pSmoke") && quotation.getString("pSmoke").equals("Y")) {
						HasIndividualUWInfoInItemType.smokingQuestionIND = "Y";
						HasIndividualUWInfoInItemType.tobaccoQuestionIND = "Y";
						HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "1";
						HasIndividualUWInfoInItemType.cigarettesPerDayNO = "1";	
					}
				}

				
				//AVOCATION QUESTION
				HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
				
				if (is_1stparty || is_3rdparty) {
					if (Owner_insurability.has("LIFESTYLE05")) {
						if ("Y".equals(Owner_insurability.getString("LIFESTYLE05"))) {
							HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
						} else {
							HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
						}
					}
				}
				
				//ALCOHOL QUESTION FOR PAYOR
				HasIndividualUWInfoInItemType.alcoholQuestionIND = "N";
				boolean OW_is_drinker = false;
				
				if (is_1stparty || is_3rdparty) {
					if (Owner_insurability.has("LIFESTYLE02")) {
						HasIndividualUWInfoInItemType.alcoholQuestionIND = Owner_insurability.getString("LIFESTYLE02");
						
						if (Owner_insurability.getString("LIFESTYLE02").equals("Y")) {
							OW_is_drinker = true;	
						}
					}
				}
				
				if (Owner_insurability.has("LIFESTYLE03")) {
					if (Owner_insurability.getString("LIFESTYLE03").equals("Y")) {
						Pol_remarks.addpolicyRemarks("PH: Drug = Y");
						isOWPMED = true;
						isPolicyPMED = true;
					}
				}
				
				if (Owner_insurability.has("LIFESTYLE04")) {
					if (Owner_insurability.getString("LIFESTYLE04").equals("Y")) {
						isPolicyPMED = true;
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
						Pol_remarks.addpolicyRemarks("PH: Intention to reside outside = Y");
					}
				}
				
				if (Owner_insurability.has("LIFESTYLE05")) {
					if (Owner_insurability.getString("LIFESTYLE05").equals("Y")) {
						Pol_remarks.addpolicyRemarks("PH: Avocation = Y");
						isPolicyPMED = true;
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
					}
				}
				
				if (Owner_insurability.has("LIFESTYLE06")) {
					if (Owner_insurability.getString("LIFESTYLE06").equals("Y")) {
						Pol_remarks.addpolicyRemarks("PH: Travel for > 3 months");
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
					}
				}
				
				if (Owner_insurability.has("FAMILY01")) {
					if (Owner_insurability.getString("FAMILY01").equals("Y")) {
						isOWPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("PH: Family Hx = Y");
					}
				}	
				
				//ALCOHOL UNITS/WEEK FOR PAYO
				//sample: HasIndividualUWInfoInItemType.alcoholUnitsPerWeekNO = "27";
				Integer OW_alcohol_unit_weekly = 0;
				
				if (OW_is_drinker) {
					if (Owner_insurability.has("LIFESTYLE02a_1")) {
						if ("Y".equals(Owner_insurability.getString("LIFESTYLE02a_1"))) {
							OW_alcohol_unit_weekly = OW_alcohol_unit_weekly + Owner_insurability.getInt("LIFESTYLE02a_2");
						}		
					}
					
					if (Owner_insurability.has("LIFESTYLE02b_1")) {
						if ("Y".equals(Owner_insurability.getString("LIFESTYLE02b_1"))) {
							OW_alcohol_unit_weekly = OW_alcohol_unit_weekly + Owner_insurability.getInt("LIFESTYLE02b_2");
						}
					}
					
					if (Owner_insurability.has("LIFESTYLE02b_1")) {
						if ("Y".equals(Owner_insurability.getString("LIFESTYLE02c_1"))) {
							OW_alcohol_unit_weekly = OW_alcohol_unit_weekly + Owner_insurability.getInt("LIFESTYLE02c_2");
						}
					}				
					
					if (OW_alcohol_unit_weekly > 99)
						OW_alcohol_unit_weekly = 99;

					if (!isShield && OW_alcohol_unit_weekly > 30) {
						isOWPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("PH: Alcohol > 30");
					}		
				}
				
				HasIndividualUWInfoInItemType.alcoholUnitsPerWeekNO = Integer.toString(OW_alcohol_unit_weekly);

				//TOBACCO QUESTION FOR PAYOR
				//sample: HasIndividualUWInfoInItemType.tobaccoQuestionIND = "N";
				HasIndividualUWInfoInItemType.tobaccoQuestionIND = "N";
				
				if (is_1stparty || is_3rdparty) {
					if (Owner_insurability.has("LIFESTYLE01")) {
						HasIndividualUWInfoInItemType.tobaccoQuestionIND = Owner_insurability.getString("LIFESTYLE01");
							
//						if (Owner_insurability.getString("LIFESTYLE01").equals("Y"))
//							isPolicyPMED = true;
					}
				}
				
				//TOBACCO UNITS/DAY FOR PAYOR
				//mandatory is controlled by UI
//				HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "0";
				
				
//				if (Owner_insurability.has("LIFESTYLE01b")) {
//					int cigarettesPerDayNO = Owner_insurability.getInt("LIFESTYLE01b");
//					
//					if (cigarettesPerDayNO > 99)
//						cigarettesPerDayNO = 99;
//					
//					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = Integer.toString(cigarettesPerDayNO);
//					
//					Pol_remarks.addpolicyRemarks("Cigarette > 30");
//				}
				
				
				//SMOKING QUESTION
				HasIndividualUWInfoInItemType.smokingQuestionIND = "N";
				HasIndividualUWInfoInItemType.healthQuestionIND = "N";
				if (is_1stparty || is_3rdparty) {
					if (Owner_insurability.has("LIFESTYLE01")) {
						HasIndividualUWInfoInItemType.smokingQuestionIND = Owner_insurability.getString("LIFESTYLE01");
						
//						if (Owner_insurability.getString("LIFESTYLE01").equals("Y"))
//							isPolicyPMED = true;
					}
				}

				//NO. OF CIGARETTE PER DAY
				//sample: HasIndividualUWInfoInItemType.cigarettesPerDayNO = "12";
				if (Owner_insurability.has("LIFESTYLE01b")) {
					int cigarettesPerDayNO = Owner_insurability.getInt("LIFESTYLE01b");
									
					if (cigarettesPerDayNO > 99)
						cigarettesPerDayNO = 99;
								
					HasIndividualUWInfoInItemType.cigarettesPerDayNO = Integer.toString(cigarettesPerDayNO);
					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = Integer.toString(cigarettesPerDayNO);
					
					if (cigarettesPerDayNO > 30) {
						isOWPMED = true;
						isPolicyPMED = true;
						
						Pol_remarks.addpolicyRemarks("PH: Cigarette > 30");
					}	
				}
				
				

				if (isOWPMED) {
					HasIndividualUWInfoInItemType.healthQuestionIND = "Y";
				}
			}
			
			if (isShield) {//For Shield
				HasIndividualUWInfoInItemType.declineQuestionIND = "";
				HasIndividualUWInfoInItemType.avocationQuestionIND = "";
				HasIndividualUWInfoInItemType.healthQuestionIND = "";
				HasIndividualUWInfoInItemType.previousHealthExamQuestionIND = "";
				HasIndividualUWInfoInItemType.previousHealthExamResultIND = "";
				HasIndividualUWInfoInItemType.previousHealthExamResultIND = "";
				HasIndividualUWInfoInItemType.smokingQuestionIND = "";
				HasIndividualUWInfoInItemType.cigarettesPerDayNO = "";
				HasIndividualUWInfoInItemType.alcoholQuestionIND = "";
				HasIndividualUWInfoInItemType.alcoholUnitsPerWeekNO = "";
				HasIndividualUWInfoInItemType.tobaccoQuestionIND = "";
				HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "";
			}
			
			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasIndividualUWInfoIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual - S

			//RECEIVE SMS FLAG
			if (isShield) 
				HasCustomerInformationInItemType.smsTextableFlg = "Y";
			else {
				if (Owner_personalInfo.has("mobileNo")) {
					HasCustomerInformationInItemType.smsTextableFlg = "Y";	
				} else {
					HasCustomerInformationInItemType.smsTextableFlg = "Y";
				}
			}	
						
			//DEPENDENT NO.
			HasCustomerInformationInItemType.dependentNO = "1";

			String ownerIND = null;
			String relationshipToOwnerCD = null;
			//OWNERSHIP
			//sample: HasCustomerInformationInItemType.ownerIND = "I";
			HasCustomerInformationInItemType.ownerIND = "I";
			

			if ((proposer_dependant != null)&&is_3rdparty){	
				//String dependent_code = "";
				
				if (proposer_dependant.has("relationship")){	
					switch(proposer_dependant.getString("relationship"))
					{
						case "SPO"://Spouse
							HasCustomerInformationInItemType.ownerIND = "S";
							relationshipToOwnerCD = "SPOUSE";
							break;
						case "MOT"://Mother
							HasCustomerInformationInItemType.ownerIND = "P";	
							relationshipToOwnerCD = "PARENT";
							break;
						case "FAT"://Father
							HasCustomerInformationInItemType.ownerIND = "P";
							relationshipToOwnerCD = "PARENT";
							break;
						case "DAU"://Daughter
							HasCustomerInformationInItemType.ownerIND = "P";
							relationshipToOwnerCD = "CHILD";
							break;
						case "SON"://Son
							HasCustomerInformationInItemType.ownerIND = "P";
							relationshipToOwnerCD = "CHILD";
							break;
						case "BRO"://Brother
							HasCustomerInformationInItemType.ownerIND = "P";
							relationshipToOwnerCD = "PARENT";
							break;
						case "SIS"://Sister
							HasCustomerInformationInItemType.ownerIND = "P";
							relationshipToOwnerCD = "PARENT";
							break;
						case "GMO"://Grand Mother
							HasCustomerInformationInItemType.ownerIND = "O";
							relationshipToOwnerCD = "OTHERS";
							break;
						case "GFA"://Grand Father
							HasCustomerInformationInItemType.ownerIND = "O";
							relationshipToOwnerCD = "OTHERS";
							break;
						case "GDA"://Grand Son
							HasCustomerInformationInItemType.ownerIND = "O";
							relationshipToOwnerCD = "CHILD";
							break;
						case "GSO"://Grand Daughter
							HasCustomerInformationInItemType.ownerIND = "O";
							relationshipToOwnerCD = "CHILD";
							break;
						default:
							break;
					}
				} else {
					HasCustomerInformationInItemType.ownerIND = "O";
					relationshipToOwnerCD = "OTHERS";
				}
				
				if (isShield)
					Pol_remarks.addpolicyRemarks("Relationship flag");
			}
			
			if (proposer_dependant == null) {
				HasCustomerInformationInItemType.ownerIND = "I";
				relationshipToOwnerCD = "INSURED";
			}
			ownerIND = HasCustomerInformationInItemType.ownerIND;
			
			if(isShield) {
				HasDetailsOfRisksInItemType.relationshipToOwnerCD = relationshipToOwnerCD;
			}
			//OWNER last name
			// sample:
			//if (quotation.has("pLastName")) {
			//	CanBeIndividualType.lastNM = quotation.getString("pLastName");
			// } else {
			//	throw new IOException("Mandatory field missing - owner last name");
			//}
			
			//CanBeIndividualType.lastNM = "";
			//if (is_1stparty || is_3rdparty)
			//{
			//	if (Owner_personalInfo.has("lastName"))
			//	{
			//		CanBeIndividualType.lastNM = Owner_personalInfo.getString("lastName");
			//	}
			//}
			
			CanBeIndividualType.lastNM = "";
			CanBeIndividualType.firstNM = "";

			String OW_fullNM = "";
			String OW_chineseNM = "";
			String OW_nationality = "" ;
			String OW_othName = "" ;
			String OW_lastNM = "";
			String OW_firstNM = "";
			
			if (is_3rdparty) {
//				String OW_lastNM = "";
//				String OW_firstNM = "";
				if (Owner_personalInfo.has("lastName"))
				{
					OW_lastNM = spaceRemover(truncateSpecialChar(Owner_personalInfo.getString("lastName").toUpperCase()));
				}
				if (Owner_personalInfo.has("firstName"))
				{
					OW_firstNM = spaceRemover(truncateSpecialChar(Owner_personalInfo.getString("firstName").toUpperCase()));
				}
				if (Owner_personalInfo.has("fullName")) {
					OW_fullNM = spaceRemover(truncateSpecialChar(Owner_personalInfo.getString("fullName").toUpperCase()));
				}
				if (Owner_personalInfo.has("hanyuPinyinName")) {
					OW_chineseNM = spaceRemover(truncateSpecialChar(Owner_personalInfo.getString("hanyuPinyinName").toUpperCase()));
				}
				if (Owner_personalInfo.has("othName")) {
					OW_othName = spaceRemover(truncateSpecialChar(Owner_personalInfo.getString("othName").toUpperCase()));
				}
				CanBeIndividualType.lastNM = OW_fullNM;
				CanBeIndividualType.firstNM = "";
			}
			
			//mandatory gen owner for API testing only.
			
			//OWNER HKID
			//sample: CanBeIndividualType.documentID = "S5678543";
			if (is_3rdparty) {
				if (Owner_personalInfo.has("idCardNo")) {
					CanBeIndividualType.documentID = truncateSpecialChar(Owner_personalInfo.getString("idCardNo"));
				}
			}
			
			//OWNER GENDER *
			if (is_3rdparty) {	
				CanBeIndividualType.genderCD = OW_Gender;
			}

			//OWNER BIRTH DATE
//			if (quotation.has("pDob")) {
//				Date pDob = new Date(quotation.getString("pDob"));	
//				CanBeIndividualType.birthDT = pDob;	
//			}
			
			//Smaple: CanBeIndividualType.birthDT = this.getDummyDate();//TODO
			CanBeIndividualType.birthDT = "";
			String OW_birthDate = "";
 			if (is_3rdparty) {
				if (Owner_personalInfo.has("dob")) {
					CanBeIndividualType.birthDT = Owner_personalInfo.getString("dob");
					OW_birthDate = CanBeIndividualType.birthDT;
				}
			}
			
			//HEIGHT IN METRE FOR OWNER 
			// sample : CanBeIndividualType.height = "0";
			if (is_3rdparty) {
				if (Owner_insurability.has("HW01") && !isShield) {//For Shield
					CanBeIndividualType.height = Double.toString(Owner_insurability.getDouble("HW01"));
				}
			}

			//WEIGHT IN KG FOR OWNER
			// sample :CanBeIndividualType.weight = "0";
			if (is_3rdparty && (OW_weight > 0) && !isShield) {//For Shield
				CanBeIndividualType.weight = Double.toString(OW_weight);
			}

			//OCC. CLASS FOR OWNER
			//CanBeIndividualType.stdOccupationCD = "1";
			CanBeIndividualType.stdOccupationCD = "";
			if (is_3rdparty) {
				if (Owner_personalInfo.has("occupation")) {
					CanBeIndividualType.stdOccupationCD = getOccupationClass(Owner_personalInfo.getString("occupation"),OW_issueage,OW_Gender);
				}
			}
			
			if (is_3rdparty && hasPayorBenefitRider) {
				if (CanBeIndividualType.weight != null && CanBeIndividualType.height != null) {
					if (!BMI(OW_issueage, CanBeIndividualType.weight, CanBeIndividualType.height)) {
						Pol_remarks.addpolicyRemarks("PH: Check BMI");
						isPolicyPMED = true;
					}
				}
			}
			
			//MONTHLY INCOME FOR PAYOR
			CanBeIndividualType.annualIncomeAMT = "0";
			
			if (is_3rdparty) {
				if (proposerClientDoc.has("allowance")) {
//					if (isShield) {//For Shield
//						double allowance = proposerClientDoc.getDouble("allowance");
//						CanBeIndividualType.annualIncomeAMT = Double.toString(Math.round(allowance * 100.0) / 100.0);
//					} else {
						CanBeIndividualType.annualIncomeAMT = proposerClientDoc.get("allowance").toString();
//					}
				}
				if("4".equals(CanBeIndividualType.stdOccupationCD) || "6".equals(CanBeIndividualType.stdOccupationCD)){
					CanBeIndividualType.annualIncomeAMT = "0";
				}
			}

			//COUNTRY OF ORIGIN
			//sample: CanBeIndividualType.countryOfOriginDESC = "Malaysia";
			if (is_1stparty || is_3rdparty) {
				if (Owner_personalInfo.has("nationality")) {
					if (NationalityConversion(Owner_personalInfo.getString("nationality"), false) != null) {
						CanBeIndividualType.countryOfOriginDESC = NationalityConversion(Owner_personalInfo.getString("nationality"), false); 		
					}
				}
			}
			
			//NATIONALITY
			//sample :CanBeIndividualType.nationality = "Indian";
			CanBeIndividualType.nationality = "";
	
			if (Owner_personalInfo.has("nationality")) {
				CanBeIndividualType.nationality = NationalityConversion(Owner_personalInfo.getString("nationality"), false); 
				OW_nationality = CanBeIndividualType.nationality;
			}
			
			//CKA fields
			//Default Y to broker channel
			CanBeIndividualType.ckaEducationFLG = "Y";
			CanBeIndividualType.ckaInvestmentExperienceFLG = "Y";
			CanBeIndividualType.ckaWorkExperienceFLG = "Y";
			CanBeIndividualType.ckaFLG = "Y";
			
			if (is_ILPProduct) {
				if (Owner_personalInfo.has("age") && Owner_personalInfo.getInt("age") > 65)
					CanBeIndividualType.vulnerableAgeFLG = "Y";	
				else 
					CanBeIndividualType.vulnerableAgeFLG = "N";
				
				if (Owner_personalInfo.has("education") && Owner_personalInfo.getString("education").toUpperCase().equals("BELOW"))
					CanBeIndividualType.vulnerableEducationFLG = "Y";	
				else
					CanBeIndividualType.vulnerableEducationFLG = "N";
			}
								
			if (isShield) {
				if (OW_nationality.equals("SINGAPOREAN"))		
					CanBeIndividualType.countryOfResidenceCD  = "LR";
				else
					CanBeIndividualType.countryOfResidenceCD  = "PR";
			}
		
			boolean OW_withCKA = false;
			JSONObject bundleFNADoc_ckaSection = new JSONObject();
			JSONObject bundleFNADoc_ckaSection_owner = new JSONObject();
			if(bundleFNADoc != null && bundleFNADoc.has("ckaSection")) {
				bundleFNADoc_ckaSection = bundleFNADoc.getJSONObject("ckaSection");
				if(bundleFNADoc_ckaSection.has("isValid")) {
					if(bundleFNADoc_ckaSection.getBoolean("isValid")) {
						if(bundleFNADoc_ckaSection.has("owner")) {
							bundleFNADoc_ckaSection_owner = bundleFNADoc_ckaSection.getJSONObject("owner");
							OW_withCKA = true;
						}
					}
				}
			}
			
			if (OW_withCKA && is_ILPProduct) {
				try {
					String bundleFNADoc_ckaSection_course = bundleFNADoc_ckaSection_owner.getString("course");
					if(bundleFNADoc_ckaSection_course != null){
						if ("notApplicable".equals(bundleFNADoc_ckaSection_course)) {
							CanBeIndividualType.ckaEducationFLG ="N";
						}
					}
				}catch(Exception CAK_e0001) {
					
				}
				
				try {
					boolean ckaInvestmentExperienceFLG = false;
					
					String bundleFNADoc_ckaSection_collectiveInvestment = bundleFNADoc_ckaSection_owner.getString("collectiveInvestment");
					if(bundleFNADoc_ckaSection_collectiveInvestment != null){
						if ("N".equals(bundleFNADoc_ckaSection_collectiveInvestment)) {
							CanBeIndividualType.ckaInvestmentExperienceFLG ="N";
							ckaInvestmentExperienceFLG = false;
						} else {
							CanBeIndividualType.ckaInvestmentExperienceFLG ="Y";
							ckaInvestmentExperienceFLG = true;
						}
					}
					
					if (!ckaInvestmentExperienceFLG) {
						String bundleFNADoc_ckaSection_insuranceInvestment = bundleFNADoc_ckaSection_owner.getString("insuranceInvestment");
						if(bundleFNADoc_ckaSection_insuranceInvestment != null){
							if ("N".equals(bundleFNADoc_ckaSection_insuranceInvestment)) {
								CanBeIndividualType.ckaInvestmentExperienceFLG ="N";
							} else {
								CanBeIndividualType.ckaInvestmentExperienceFLG ="Y";
							}
						}
					}
				}catch(Exception CAK_e0002) {
					
				}
				
				try {
					String bundleFNADoc_ckaSection_workingExperience = bundleFNADoc_ckaSection_owner.getString("profession");
					if(bundleFNADoc_ckaSection_workingExperience != null){
						if ("notApplicable".equals(bundleFNADoc_ckaSection_workingExperience)) {
							CanBeIndividualType.ckaWorkExperienceFLG ="N";
						}
					}
				}catch(Exception CAK_e0003) {
					
				}
				
				if("Y".equals(CanBeIndividualType.ckaEducationFLG) || "Y".equals(CanBeIndividualType.ckaInvestmentExperienceFLG) || "Y".equals(CanBeIndividualType.ckaWorkExperienceFLG)) {
					CanBeIndividualType.ckaFLG = "Y";
				} else {
					CanBeIndividualType.ckaFLG = "N";
					Pol_remarks.addpolicyRemarks("CKA Failed");
				}
			}
			
			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//forLifeUnitofExposure:S
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > forLifeUnitofExposure > hasCoverageDetailsIn > isAssociatedWithLifeInsuranceProduct - S

						
			//basic plan code
			
			HasCoverageDetailsInType.baseCoverageIndicatorCd = "P"; 
			IsAssociatedWithLifeInsuranceProductItemType.insuranceProductCd = Basic_planCD;
	
			//rider class code

			if(isShield)
				IsAssociatedWithLifeInsuranceProductItemType.planClassCD = "1";
			else
				IsAssociatedWithLifeInsuranceProductItemType.planClassCD = "S";

			//RecordApplicationRequest > policy > forLifeUnitofExposure > hasCoverageDetailsIn > isAssociatedWithLifeInsuranceProduct - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > forLifeUnitofExposure - S
			
			//LIFE SUM INSURED
			//sample: ForLifeUnitofExposureItemType.currAMT = "1000";
			if (quotation_BasicPlan.has("sumInsured")) {
				ForLifeUnitofExposureItemType.currAMT = quotation_BasicPlan.getBigDecimal("sumInsured").toString();
			}

			//BASIC PREMIUM RATE
			//Need to fill up until build complete
			if (quotation_BasicPlan.has("premRate")) {
				ForLifeUnitofExposureItemType.basicPremiumRt = quotation_BasicPlan.getBigDecimal("premRate").toString();
			}

			//ATTACH TO for rider only
			//ForLifeUnitofExposureItemType.referenceTO = "11.55"; 
			//ForLifeUnitofExposureItemType.referenceTO = IsAssociatedWithLifeInsuranceProductItemType.insuranceProductCd;
			String basic_product_planCD = IsAssociatedWithLifeInsuranceProductItemType.insuranceProductCd;
			
			//for basic plan, always blank;
			ForLifeUnitofExposureItemType.referenceTO = "";
			
			isAssociatedWithLifeInsuranceProductList.add(IsAssociatedWithLifeInsuranceProductItemType);
			HasCoverageDetailsInType.isAssociatedWithLifeInsuranceProduct = isAssociatedWithLifeInsuranceProductList;
			HasCoverageDetailsInType.baseCoverageIndicatorCd = "P"; 
			
			if (is_flexiProduct ||  is_awtProduct || is_pulsarProduct) {
				if (quotation.has("premium"))
					ForLifeUnitofExposureItemType.basicRegularPremiumAMT = Double.toString(quotation.getDouble("premium"));
			}

			if (isShield) {
				ForLifeUnitofExposureItemType.referenceTO = "";
				ForLifeUnitofExposureItemType.packagedTO = "";
				ForLifeUnitofExposureItemType.currAMT = "0";
				ForLifeUnitofExposureItemType.basicPremiumRt = "";
			}

			ForLifeUnitofExposureItemType.hasCoverageDetailsIn = HasCoverageDetailsInType;	
			ForLifeUnitofExposureItemTypeList.add(ForLifeUnitofExposureItemType); 

// for rider: 
			if (isShield == false || !applicationDoc.getString("policyNumber").equals(ShieldBasicPolicyNo)) {
				if (quotation_plans.length() > 1)
				{
					JSONObject quotation_RiderPlan = null;
					for(int rider_count = 1; rider_count < quotation_plans.length(); rider_count++)
					{
						if (isShield && rider_count == 1)
							rider_count += 1;
						
						HasCoverageDetailsInType _HasCoverageDetailsInType = obj.getHasCoverageDetailsInType();
						ForLifeUnitofExposureItemType = obj.getForLifeUnitofExposureItemType();
						
						List<IsAssociatedWithLifeInsuranceProductItemType> _isAssociatedWithLifeInsuranceProductList = new ArrayList<IsAssociatedWithLifeInsuranceProductItemType>();
						IsAssociatedWithLifeInsuranceProductItemType _IsAssociatedWithLifeInsuranceProductItemType = obj.getIsAssociatedWithLifeInsuranceProductItemType();
						
						try {
							quotation_RiderPlan = quotation_plans.getJSONObject(rider_count);
							boolean isWOP_rider = false;
							if (quotation_RiderPlan.has("planCode")) {
								_IsAssociatedWithLifeInsuranceProductItemType.insuranceProductCd = quotation_RiderPlan.getString("planCode");
								if(_IsAssociatedWithLifeInsuranceProductItemType.insuranceProductCd.contains("WOP")) {
									isWOP_rider = true;
								} 
								
								//TODO
								if(!isShield)
									ForLifeUnitofExposureItemType.referenceTO = packageToRider(Basic_planCD, quotation_RiderPlan.getString("planCode"));
							} else {
								throw new IOException("Mandatory field missing - basic plan code");
							}

							//rider class code  /rider class code
							_IsAssociatedWithLifeInsuranceProductItemType.planClassCD = riderClassCodeMapping(quotation_RiderPlan.getString("planCode"));


							if (quotation_RiderPlan.has("sumInsured")) {
								ForLifeUnitofExposureItemType.currAMT = quotation_RiderPlan.getBigDecimal("sumInsured").toString();
							}

							//ForLifeUnitofExposureItemType.basicPremiumRt = "0";
							if (quotation_RiderPlan.has("premRate")) {
								ForLifeUnitofExposureItemType.basicPremiumRt = quotation_RiderPlan.getBigDecimal("premRate").toString();
							}
							
							//ForLifeUnitofExposureItemType.referenceTO = basic_product_planCD;
							if(isWOP_rider) {
								ForLifeUnitofExposureItemType.referenceTO = Basic_planCD;
							}else {
								//ForLifeUnitofExposureItemType.referenceTO = "";
							}
							
							
							// add as element:
							//isAssociatedWithLifeInsuranceProductList.add(rider_count,IsAssociatedWithLifeInsuranceProductItemType);
							_isAssociatedWithLifeInsuranceProductList.add(_IsAssociatedWithLifeInsuranceProductItemType);
							_HasCoverageDetailsInType.isAssociatedWithLifeInsuranceProduct = _isAssociatedWithLifeInsuranceProductList;
							_HasCoverageDetailsInType.baseCoverageIndicatorCd = "R"; 
							ForLifeUnitofExposureItemType.hasCoverageDetailsIn = _HasCoverageDetailsInType;	
							ForLifeUnitofExposureItemTypeList.add(ForLifeUnitofExposureItemType);
						} catch  (Exception ex) {}
					}
				}
			}
			
			//RecordApplicationRequest > policy > forLifeUnitofExposure - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//forLifeUnitofExposure:E

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasLifePolicyTransactionDetailsIn - S

			//CASH AMT - K47CSHAMT 
			 
			HasLifePolicyTransactionDetailsInItemType.premiumAMT = "1000000.11"; 
			
			if (!isCreditCardPayment) {
				HasLifePolicyTransactionDetailsInItemType.premiumAMT = quotation.getBigDecimal("premium").toString();
			}
			
			//TRX CODE
			HasLifePolicyTransactionDetailsInItemType.transTypeCD = "1";
			//Payment CCY
			HasLifePolicyTransactionDetailsInItemType.transCurrencyCD = "SGD";

			//RecordApplicationRequest > policy > hasLifePolicyTransactionDetailsIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasLifePolicyTransactionDetailsIn > hasLifePolicyTransactionHeaderDetailsIn - S

			//CCP AMT
			//sample :HasLifePolicyTransactionHeaderDetailsInItemType.ccLocalAMT = "123456789876.08";


			//CREDITCARD NO
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.creditCardNO = "1234567898765432";

			//BANK ACC NO
			//mandatory field
			//shield - 'L-CHKS'
			//non-shield - 'L-CHK'
			HasLifePolicyTransactionHeaderDetailsInItemType.bankAccountNO =  "L-CHK";

			//BANK ACC NO
			//HasLifePolicyTransactionHeaderDetailsInItemType.cardExpirationDT = "999912";

			//AUTH CODE
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.authenticationCD = "000000";

			//REMARKS
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.paymentRemarks = "132";
			//"EASE - CC" when the payment is made via CC.
			//"EASE - eNets" when the payment is made via enets
			//"EASE - CC IPP" when the payment is made via Credit card IPP
			
			//BANK CODE
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.bankCD = "A12";
			

			//BRANCH CODE
			//HasLifePolicyTransactionHeaderDetailsInItemType.branchCD = "786TR4";

			//CHEQUE NUMBER
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.chequeNO = "710098761234";

			//CHEQUE DATE
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.chequeDT = this.getDummyDate();
			
			if (1==2)//Useless //TO be removed
			{
				HasLifePolicyTransactionHeaderDetailsInItemType.ccLocalAMT = quotation.getBigDecimal("premium").toString();
				HasLifePolicyTransactionHeaderDetailsInItemType.creditCardNO = "0000000000000000";
				HasLifePolicyTransactionHeaderDetailsInItemType.authenticationCD = "000000";
				
				if ("crCard".equals(paymentmethod)) {	
					HasLifePolicyTransactionHeaderDetailsInItemType.paymentRemarks = "EASE - CC";
					HasLifePolicyTransactionHeaderDetailsInItemType.bankCD = "DBS";
					HasLifePolicyTransactionHeaderDetailsInItemType.branchCD = "0";//TO DO: not in excel
					HasLifePolicyTransactionHeaderDetailsInItemType.cardExpirationDT = "999912";

					HasLifePolicyTransactionHeaderDetailsInItemType.chequeNO = "EASE CC";
					HasLifePolicyTransactionHeaderDetailsInItemType.chequeDT = getCurrentDate();
				}
				
				if ("eNets".equals(paymentmethod)) {
					HasLifePolicyTransactionHeaderDetailsInItemType.paymentRemarks = "EASE - eNets";
					HasLifePolicyTransactionHeaderDetailsInItemType.bankCD = "DBS";
					HasLifePolicyTransactionHeaderDetailsInItemType.branchCD = "0";//TO DO: not in excel
					HasLifePolicyTransactionHeaderDetailsInItemType.cardExpirationDT = "999912";
					HasLifePolicyTransactionHeaderDetailsInItemType.bankAccountNO =  "L-CHK";
					HasLifePolicyTransactionHeaderDetailsInItemType.chequeNO = "EASE Nets";
					HasLifePolicyTransactionHeaderDetailsInItemType.chequeDT = getCurrentDate();

				}
				
				if ("dbsCrCardIpp".equals(paymentmethod)) {
					HasLifePolicyTransactionHeaderDetailsInItemType.paymentRemarks = "EASE - CC IPP";
					HasLifePolicyTransactionHeaderDetailsInItemType.bankCD = "";
					HasLifePolicyTransactionHeaderDetailsInItemType.branchCD = "0";//TO DO: not in excel
					HasLifePolicyTransactionHeaderDetailsInItemType.cardExpirationDT = "999912";
					HasLifePolicyTransactionHeaderDetailsInItemType.bankAccountNO =  "L-CHK";
					HasLifePolicyTransactionHeaderDetailsInItemType.chequeNO = "EASE IPP";
					HasLifePolicyTransactionHeaderDetailsInItemType.chequeDT = getCurrentDate();
				}
				
				if (isShield) {
					HasLifePolicyTransactionHeaderDetailsInItemType.branchCD = "0";
				}
			}
			
			//RecordApplicationRequest > policy > hasLifePolicyTransactionDetailsIn > hasLifePolicyTransactionHeaderDetailsIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
			if (quotation.has("fund"))
			{
				JSONObject quotation_fund = quotation.getJSONObject("fund");
				JSONArray quotation_fund_funds = quotation_fund.optJSONArray("funds");

				if (quotation_fund_funds.length() > 0)
				{	
					
					List<HasInvestmentFundDetailsInItemType> HasInvestmentFundDetailsInItemTypeList = new ArrayList<HasInvestmentFundDetailsInItemType>();
					HasInvestmentFundDetailsInItemType HasInvestmentFundDetailsInItemType = obj.getHasInvestmentFundDetailsInItemType();

					List<HasAccountDetailsInItemType> HasAccountDetailsInItemTypeList = new ArrayList<HasAccountDetailsInItemType>();
					HasAccountDetailsInItemType HasAccountDetailsInItemType = obj.getHasAccountDetailsInItemType();

					for(int fund_count =0;fund_count < quotation_fund_funds.length();fund_count++)
					{
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn > hasInvestmentFundDetailsIn(InvestmentFund) - S

						//FUND CODE
						HasInvestmentFundDetailsInItemType.investmentFundCd = "IBR";//TODO

						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn > hasInvestmentFundDetailsIn(InvestmentFund) - E
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn - S

						//ALLOCATION %
						HasAccountDetailsInItemType.allocPct = "123.09";//TODO

						//FUND ALLOCATION % FOR LSP
						HasAccountDetailsInItemType.lumpsumAllocPCT = "123.09";//TODO

						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn - E
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					}
					HasInvestmentFundDetailsInItemTypeList.add(HasInvestmentFundDetailsInItemType);
					HasAccountDetailsInItemType.hasInvestmentFundDetailsIn = HasInvestmentFundDetailsInItemTypeList;
					HasAccountDetailsInItemTypeList.add(HasAccountDetailsInItemType);
					HasDetailsRecordInItemType.hasAccountDetailsIn = HasAccountDetailsInItemTypeList;
				} 
			}
*/
			Integer investment_fund = 0;
			
			if (appform_basicvalue_plandetails.has("fundList")) {
				JSONArray appform_basicvalue_plandetails_fundList = appform_basicvalue_plandetails.optJSONArray("fundList");

                if (appform_basicvalue_plandetails_fundList.length() > 0) {      
                	List<HasAccountDetailsInItemType> HasAccountDetailsInItemTypeList = new ArrayList<HasAccountDetailsInItemType>();
                         
         			JSONObject fund_allocation = null;
                     
     				for(int fund_count =0;fund_count < appform_basicvalue_plandetails_fundList.length();fund_count++)
                    {		
                	 	fund_allocation = appform_basicvalue_plandetails_fundList.getJSONObject(fund_count);
                        HasAccountDetailsInItemType HasAccountDetailsInItemType = obj.getHasAccountDetailsInItemType();
                        
                        List<HasInvestmentFundDetailsInItemType> HasInvestmentFundDetailsInItemTypeList = new ArrayList<HasInvestmentFundDetailsInItemType>();
                        HasInvestmentFundDetailsInItemType HasInvestmentFundDetailsInItemType = obj.getHasInvestmentFundDetailsInItemType();

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn > hasInvestmentFundDetailsIn(InvestmentFund) - S

                        //FUND CODE
                        if (fund_allocation.has("fundCode"))
                        {
                        	HasInvestmentFundDetailsInItemType.investmentFundCd = fund_allocation.getString("fundCode");
                        }
                        

                        //RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn > hasInvestmentFundDetailsIn(InvestmentFund) - E
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn - S

                        //ALLOCATION %
                        if (fund_allocation.has("alloc")) {	
                        	try {
                            	HasAccountDetailsInItemType.lumpsumAllocPCT = Double.toString(fund_allocation.getDouble("topUpAlloc"));
                        	} catch (Exception e) {}
                        	
                        	HasAccountDetailsInItemType.allocPct = Double.toString(fund_allocation.getDouble("alloc"));
                        }

                        //RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn - E
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                 
                        HasInvestmentFundDetailsInItemTypeList.add(HasInvestmentFundDetailsInItemType);
                        HasAccountDetailsInItemType.hasInvestmentFundDetailsIn = HasInvestmentFundDetailsInItemTypeList;
                        HasAccountDetailsInItemTypeList.add(HasAccountDetailsInItemType);
                        investment_fund = investment_fund + 1;
                     }
                     
     				HasDetailsRecordInItemType.hasAccountDetailsIn = HasAccountDetailsInItemTypeList;
                } 
            }

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasPolicyChargeInfoIn - S
			
			//CHARGE CODE 
			if (changibleIND_PROD) {
				//sales fee:				
				if (appform_basicvalue_plandetails.has("singlePremSalesCharge")) {
					//CHARGE %
					//sample: HasPolicyChargeInfoInItemType.chargePCT = "9876123476231.98";
					//HasPolicyChargeInfoInItemType.chargePCT = "0.00";
					
					HasPolicyChargeInfoInItemType.chargeCD = initialPayment.getULChargeCD();
					Double tmp_int_change = 0.00;
					tmp_int_change = Math.round(appform_basicvalue_plandetails.getDouble("singlePremSalesCharge")*100.0)/100.0;
					HasPolicyChargeInfoInItemType.chargePCT  = (Double.toString(tmp_int_change).concat("000")).substring(0,4);
					
					HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
				}
				
				//Sales Charge
				if (appform_basicvalue_plandetails != null && appform_basicvalue_plandetails.has("rspSelect")) { 
					try {
						if (appform_basicvalue_plandetails.getBoolean("rspSelect")) {
							String salesChargeCD = salesChargeMapping(Basic_planCD);
							
							if (!salesChargeCD.equals("")) {
								HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
								HasPolicyChargeInfoInItemType.chargeCD = salesChargeCD;
								
								Double tmp_int_change = 0.00;
								tmp_int_change = Math.round(appform_basicvalue_plandetails.getDouble("singlePremSalesCharge")*100.0)/100.0;
								HasPolicyChargeInfoInItemType.chargePCT  = (Double.toString(tmp_int_change).concat("000")).substring(0,4);
								
								HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
							}	
							
							if (is_flexiProduct) {
								HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
								HasPolicyChargeInfoInItemType.chargeCD = "TP05";
								HasPolicyChargeInfoInItemType.chargePCT = "5";
								HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
							}
						}
					} catch (Exception e) {}
				} else if (appform_basicvalue_plandetails != null && appform_basicvalue_plandetails.has("rspInd")) {
					try {
						if (appform_basicvalue_plandetails.getBoolean("rspInd")) {
							String salesChargeCD = salesChargeMapping(Basic_planCD);
							
							if (!salesChargeCD.equals("")) {
								HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
								HasPolicyChargeInfoInItemType.chargeCD = salesChargeCD;
								
								Double tmp_int_change = 0.00;
								tmp_int_change = Math.round(appform_basicvalue_plandetails.getDouble("singlePremSalesCharge")*100.0)/100.0;
								HasPolicyChargeInfoInItemType.chargePCT  = (Double.toString(tmp_int_change).concat("000")).substring(0,4);
								
								HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
							}	
							
							if (is_flexiProduct) {
								HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
								HasPolicyChargeInfoInItemType.chargeCD = "TP05";
								HasPolicyChargeInfoInItemType.chargePCT = "5";
								HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
							}//TODO- To be removed
						}
					} catch (Exception e) {}
				}
											
				//service fee:			
				if (appform_basicvalue_plandetails.has("serviceFee")) {			
					String serviceFreeChargeCD = serviceFeeMapping(Basic_planCD);
					
					if (!serviceFreeChargeCD.equals("")) {
						HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
						HasPolicyChargeInfoInItemType.chargeCD = serviceFreeChargeCD;
						
						Double tmp_int_change = 0.00;
						
						if (appform_basicvalue_plandetails.has("serviceFee")) {
							tmp_int_change = appform_basicvalue_plandetails.getDouble("serviceFee")*100.0;
							HasPolicyChargeInfoInItemType.chargePCT = (Double.toString(tmp_int_change).concat("000")).substring(0,4);
						}
						
						HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
					}						
				} else {
					String serviceFreeChargeCD = serviceFeeMapping(Basic_planCD);

					if (!serviceFreeChargeCD.equals("")) {
						HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
						HasPolicyChargeInfoInItemType.chargeCD = serviceFreeChargeCD;

						Double tmp_int_change = 0.00;
						HasPolicyChargeInfoInItemType.chargePCT = (Double.toString(tmp_int_change).concat("000")).substring(0,4);
						
						
						//if (tmp_int_change > 0)
							HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
					}						
				}
				
				if (is_flexiProduct) {
					//Top up
					if (appform_basicvalue_plandetails != null && appform_basicvalue_plandetails.has("topUpSelect") && appform_basicvalue_plandetails.getBoolean("topUpSelect")) {
						HasPolicyChargeInfoInItemType = obj.getHasPolicyChargeInfoInItemType();
						HasPolicyChargeInfoInItemType.chargeCD = "LS06";
						HasPolicyChargeInfoInItemType.chargePCT = "5";
						HasPolicyChargeInfoInItemTypeList.add(HasPolicyChargeInfoInItemType);
					}
				}
				
//Add the full list of charge fee:				
				obj.recordApplicationRequest.policy.hasPolicyChargeInfoIn = HasPolicyChargeInfoInItemTypeList;
			}


			//RecordApplicationRequest > policy > hasPolicyChargeInfoIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasAXAAsiaPolicyPaymentAccount - S

			//user input status of DA form
			HasAXAAsiaPolicyPaymentAccountItemType.sectionCD = "UW";
			HasAXAAsiaPolicyPaymentAccountItemType.statusCD = "";
			
			//For Monthly payment mode only
			if (obj.recordApplicationRequest.policy.paymentModeCD.equals("M")) {
				HasAXAAsiaPolicyPaymentAccountItemType.bankNO = "9999";
				HasAXAAsiaPolicyPaymentAccountItemType.branchNO = "999";
				HasAXAAsiaPolicyPaymentAccountItemType.factoringHouseCD = "O";
				
				if (isShield && !applicationDoc.getString("policyNumber").equals(ShieldBasicPolicyNo))
					HasAXAAsiaPolicyPaymentAccountItemType.statusCD = "V";					
			} else {
				HasAXAAsiaPolicyPaymentAccountItemType.bankNO = "";
				HasAXAAsiaPolicyPaymentAccountItemType.branchNO = "";
				HasAXAAsiaPolicyPaymentAccountItemType.factoringHouseCD = "";
			}
			
			HasAXAAsiaPolicyPaymentAccountItemType.validFromDTTM = this.getCurrentDate();//TODO - USELESS
			HasAXAAsiaPolicyPaymentAccountItemType.authProcessDT = this.getCurrentDate();//TODO - USELESS
			HasAXAAsiaPolicyPaymentAccountItemType.modDT = this.getCurrentDate();//TODO - USELESS
			HasAXAAsiaPolicyPaymentAccountItemType.paymentMethodChangeFLG = "0";//TODO - USELESS
			
			//RecordApplicationRequest > policy > hasAXAAsiaPolicyPaymentAccount - E
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasAXAAsiaPolicyPaymentAccount > hasAXAAsiaPartyAccount - S

			//Process Fund Call on Specific Date
			if (FundReqDate)
				HasAXAAsiaPartyAccountItemType.cpfBankEffectiveDT = "00000000";
			else 
				HasAXAAsiaPartyAccountItemType.cpfBankEffectiveDT = obj.recordApplicationRequest.policy.policySubmitDT;
			
			String bankCD = "";
			
			if (bankName.equals("dbs"))
				bankCD = "7171";
			else if (bankName.equals("uob"))
				bankCD = "7375";
			else if (bankName.equals("ocbc"))
				bankCD = "7339";
					
		  	HasAXAAsiaPartyAccountItemType.accountBankCD = bankCD;
		  	HasAXAAsiaPartyAccountItemType.cpfINVAccountNO = "";
			HasAXAAsiaPartyAccountItemType.mandateStatusCD = "0";//TODO-Unless 
			HasAXAAsiaPartyAccountItemType.creditCardNO = "0";//TODO-Unless 
			HasAXAAsiaPartyAccountItemType.cardExpirationDT = "0";//TODO-Unless
			//HasAXAAsiaPartyAccountItemType.cpfBankEffectiveDT = "0";//TODO-Unless
			HasAXAAsiaPartyAccountItemType.processDT = "";
			
		 	HasAXAAsiaPartyAccountItemType.bankID = truncateSpecialChar(LifeAssured_personalInfo.getString("idCardNo"));
			
			//For Monthly payment mode only
//			if (!Parm_paymode.equals("L")) {
//				HasAXAAsiaPartyAccountItemType.accountNO = "999-9999999";
//			} else {
//				HasAXAAsiaPartyAccountItemType.accountNO = "";
//			}
			
			if (Parm_paymode.equals("M") && obj.recordApplicationRequest.policy.singlePremiumPolicyFLG.isEmpty()) {
				HasAXAAsiaPartyAccountItemType.accountNO = "999-9999999";
			} else {
				HasAXAAsiaPartyAccountItemType.accountNO = "";
			}
							
		  	HasAXAAsiaPartyAccountItemType.cpfBankAbbr = "";
		  	HasAXAAsiaPartyAccountItemType.status = "B";
		  	HasAXAAsiaPartyAccountItemType.accountTypeCD = "";
			  
			if (obj.recordApplicationRequest.policy.paymentModeCD.equals("M")) {
				if (is_3rdparty) {
					HasAXAAsiaPartyAccountItemType.payorLastNM = OW_fullNM;
					HasAXAAsiaPartyAccountItemType.payorFirstNM = "";
			  	} else {
			  		HasAXAAsiaPartyAccountItemType.payorLastNM = LA_fullNM;
					HasAXAAsiaPartyAccountItemType.payorFirstNM = "";
			  	}
			} else {
				HasAXAAsiaPartyAccountItemType.payorLastNM = "";
				HasAXAAsiaPartyAccountItemType.payorFirstNM = "";
			}
			
			if (isShield && isCPFPayment) {
				HasAXAAsiaPartyAccountItemType.amalgametedAccountNO = "";
				HasAXAAsiaPartyAccountItemType.branchCD = "0";
		
				if (is_3rdparty) {
					HasAXAAsiaPartyAccountItemType.payorLastNM = spaceRemover(OW_fullNM);
					HasAXAAsiaPartyAccountItemType.accountNO = truncateSpecialChar(Owner_personalInfo.getString("idCardNo"));
					HasAXAAsiaPartyAccountItemType.cpfINVAccountNO = truncateSpecialChar(Owner_personalInfo.getString("idCardNo"));
				} else { 
					HasAXAAsiaPartyAccountItemType.payorLastNM = spaceRemover(LA_fullNM);
					HasAXAAsiaPartyAccountItemType.accountNO = truncateSpecialChar(LifeAssured_personalInfo.getString("idCardNo"));
					HasAXAAsiaPartyAccountItemType.cpfINVAccountNO = truncateSpecialChar(LifeAssured_personalInfo.getString("idCardNo"));
				}
				
				if (isAPPROVE_REJECT)
					HasAXAAsiaPartyAccountItemType.processDT = APPROVE_REJECT_date;
				else if (isINVALIDATED)
		        	HasAXAAsiaPartyAccountItemType.processDT = INVALIDATED_date;
				else if (isEXPIRED)
					HasAXAAsiaPartyAccountItemType.processDT = EXPIRED_date;
				
				HasAXAAsiaPartyAccountItemType.accountTypeCD = "CPF"; 
			}
			
			
			if (quotation_policyOptions.has("paymentMethod")) {
				if (isCPFPayment) {					
					switch(Payor_paymentMethod)
					{
					  case "cpfissa":
						  	//CPF BANK ABBRVIATION
						  	HasAXAAsiaPartyAccountItemType.cpfBankAbbr = "";
	
						  	//CPF BANK CODE can skip currently
						  	//HasAXAAsiaPartyAccountItemType.accountBankCD = "00TT";//TODO -useless
						
						  	//CPF INV A/C NO
						  	HasAXAAsiaPartyAccountItemType.cpfINVAccountNO = LifeAssured_personalInfo.getString("idCardNo");
						
						  	//BANK ID NO - can skip currently 
						  	//HasAXAAsiaPartyAccountItemType.bankID = "CITI987";
	
						  	//Debtor Name
						  	//HasAXAAsiaPartyAccountItemType.payorLastNM = Owner_personalInfo.getString("lastName");
						  	//HasAXAAsiaPartyAccountItemType.payorFirstNM = Owner_personalInfo.getString("firstName");
							
							//CPF A/C NO.
							HasAXAAsiaPartyAccountItemType.accountNO = CPFpaymentBlockNM.getString("idCardNo");
	
						  	//STATUS
						  	HasAXAAsiaPartyAccountItemType.status = "B";
						
						  	//Process Date - rls, add this for rls population
						  	HasAXAAsiaPartyAccountItemType.processDT = "";

						  	HasAXAAsiaPartyAccountItemType.accountTypeCD = "CPF";
						  	break;
					  case "cpfisoa":
						    if (withCPFAccount) {
						    	//CPF BANK ABBRVIATION
							  	if (CPFpaymentBlockNM.has(BankNamePicker_fieldNM)) {
								  	HasAXAAsiaPartyAccountItemType.cpfBankAbbr = (CPFpaymentBlockNM.getString(BankNamePicker_fieldNM).toUpperCase());
							  	} else {
									throw new IOException("BANK ABBRVIATION missing");
							  	}
			
							  	//Debtor Name
							  	//HasAXAAsiaPartyAccountItemType.payorLastNM = Owner_personalInfo.getString("lastName");
							  	//HasAXAAsiaPartyAccountItemType.payorFirstNM = Owner_personalInfo.getString("firstName");
								
								//CPF BANK CODE can skip currently
								//HasAXAAsiaPartyAccountItemType.accountBankCD = "00A";//TODO - useless
								//CPF INV A/C NO
							  	if (CPFpaymentBlockNM.has(InvmAcctNo_fieldNM)) {
							  		HasAXAAsiaPartyAccountItemType.cpfINVAccountNO = CPFpaymentBlockNM.getString(InvmAcctNo_fieldNM);
							  	}
								//BANK ID NO - can skip currently 
								//HasAXAAsiaPartyAccountItemType.bankID = "CITI987";
			
								//CPF A/C NO.
								HasAXAAsiaPartyAccountItemType.accountNO = CPFpaymentBlockNM.getString("idCardNo");
			
								//STATUS
								HasAXAAsiaPartyAccountItemType.status = "B";
								
								//Process Date - rls, add this for rls population
								HasAXAAsiaPartyAccountItemType.processDT = "";  

							  	HasAXAAsiaPartyAccountItemType.accountTypeCD = "CPF";
						    }
						  	
							break;
					  case "srs":
						  if (withCPFAccount) { 
							  //CPF BANK ABBRVIATION
							  if (CPFpaymentBlockNM.has(BankNamePicker_fieldNM)) {
								  HasAXAAsiaPartyAccountItemType.cpfBankAbbr = (CPFpaymentBlockNM.getString(BankNamePicker_fieldNM).toUpperCase());
							  } else {
								  throw new IOException("BANK ABBRVIATION missing");
							  }

							  //Debtor Name
							  //HasAXAAsiaPartyAccountItemType.payorLastNM = Owner_personalInfo.getString("lastName");
							  //HasAXAAsiaPartyAccountItemType.payorFirstNM = Owner_personalInfo.getString("firstName");

							  //CPF BANK CODE can skip currently
							  //HasAXAAsiaPartyAccountItemType.accountBankCD = "00A";//TODO - useless

							  //CPF INV A/C NO
							  if (CPFpaymentBlockNM.has(InvmAcctNo_fieldNM)) {
								  HasAXAAsiaPartyAccountItemType.cpfINVAccountNO = CPFpaymentBlockNM.getString(InvmAcctNo_fieldNM);
							  }

							  //BANK ID NO - can skip currently 
							  //HasAXAAsiaPartyAccountItemType.bankID = "CITI987";

							  //CPF A/C NO.
							  HasAXAAsiaPartyAccountItemType.accountNO = CPFpaymentBlockNM.getString("idCardNo");

							  //STATUS
							  HasAXAAsiaPartyAccountItemType.status = "B";

							  //Process Date - rls, add this for rls population
							  HasAXAAsiaPartyAccountItemType.processDT = "";
							  HasAXAAsiaPartyAccountItemType.accountTypeCD = "CPF";
						  }
							
						    break;
					  default:
						    break;
					}
				}
			}
			//RecordApplicationRequest > policy > HasCustomerInformationInItemType > canBeIndividual >hasCRSInformationIn - E
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			//FATCA /CRS
			HasCRSInformationInType.CRSType = ownerIND;
			HasCRSInformationInType.CRSStatus = "N";
			HasCRSInformationInType.followUpIndicator = "N";
			
			//BBBB
			//JSONObject tmp_json = new JSONObject();
			
			if (Owner_declaration.has("FATCA01") && "Y".equals(Owner_declaration.getString("FATCA01"))) {
				String Str_SINGAPORE = "SINGAPORE";
				countryOfTaxResidenceType.add(Str_SINGAPORE);
				//HasCRSInformationInType.countryOfTaxResidence= countryOfTaxResidenceType;
				
				if (Owner_declaration.has("FATCA01b")) {
					taxIdentificationNOType.add(truncateSpecialChar(Owner_declaration.getString("FATCA01b")));
					//HasCRSInformationInType.taxIdentificationNO = taxIdentificationNOType;
				}
			} 
			
			if (Owner_declaration.has("FATCA02") && "Y".equals(Owner_declaration.getString("FATCA02"))) {
				String Str_UNITEDSTATES = "UNITED STATES";
				countryOfTaxResidenceType.add(Str_UNITEDSTATES);
				//HasCRSInformationInType.countryOfTaxResidence= countryOfTaxResidenceType;
				
				if (Owner_declaration.has("FATCA03")) {
					taxIdentificationNOType.add(truncateSpecialChar(Owner_declaration.getString("FATCA03")));
					//HasCRSInformationInType.taxIdentificationNO = taxIdentificationNOType;
				}
				
				HasCRSInformationInType.followUpIndicator = "Y";
			}
			
			if (Owner_declaration.has("CRS01") && "Y".equals(Owner_declaration.getString("CRS01"))) {
				HasCRSInformationInType.CRSStatus = "Y";
				String tax_country = null;
				
				if (Owner_declaration.has("CRS01_DATA")) {
					JSONArray Owner_declaration_Array = Owner_declaration.getJSONArray("CRS01_DATA");
					
					JSONObject Owner_declaration_Object = null;
					for (int i = 0; i < Owner_declaration_Array.length(); i++) {
						Owner_declaration_Object = Owner_declaration_Array.getJSONObject(i);
						
						if (Owner_declaration_Object.has("CRS01a")) {
							tax_country = Owner_declaration_Object.getString("CRS01a");
							countryOfTaxResidenceType.add(CountryConversion(tax_country));
							//HasCRSInformationInType.countryOfTaxResidence = taxIdentificationNOType;
						}
						
						if (Owner_declaration_Object.has("CRS01b")) {
							taxIdentificationNOType.add(truncateSpecialChar(Owner_declaration_Object.getString("CRS01b")));
							//HasCRSInformationInType.taxIdentificationNO = taxIdentificationNOType;
						}
					}
				}
				
				HasCRSInformationInType.followUpIndicator = "Y";
			} else {
				countryOfTaxResidenceType.add("");
				taxIdentificationNOType.add("");
				HasCRSInformationInType.countryOfTaxResidence = countryOfTaxResidenceType;
				HasCRSInformationInType.taxIdentificationNO = taxIdentificationNOType;
			}
			
			HasCRSInformationInType.countryOfTaxResidence = countryOfTaxResidenceType;
			HasCRSInformationInType.taxIdentificationNO = taxIdentificationNOType;
			
			if (Owner_personalInfo.has("residenceCountry") && (!"R2".equals((Owner_personalInfo.getString("residenceCountry")))))
				HasCRSInformationInType.followUpIndicator = "Y";
						
			HasCRSInformationInType.FATCAStatus = "N";
			
			//If US Citizen /Tax Resident = Yes, 
			if (Owner_declaration.has("FATCA02")) {
				if ("Y".equals(Owner_declaration.getString("FATCA02")))
					HasCRSInformationInType.FATCAStatus = "Y";
			}
			
			//Country of Residential Address = United States
			if (Owner_personalInfo.has("residenceCountry")) {
				if ("R252".equals(Owner_personalInfo.getString("residenceCountry")))
					HasCRSInformationInType.FATCAStatus = "Y";
			}
			
			// Country of Mailing Address = United States
			if(Owner_personalInfo.has("addrCountry")) {
				if ("R252".equals(Owner_personalInfo.getString("addrCountry")))
					HasCRSInformationInType.FATCAStatus = "Y";
			}
			
			//Country of Employer Address = United States
			if (Owner_personalInfo.has("organizationCountry")) {
				if ("R252".equals(Owner_personalInfo.getString("organizationCountry")))
					HasCRSInformationInType.FATCAStatus = "Y";
			}
			
			//Country Telephone Code = +1
			if (Owner_personalInfo.has("mobileCountryCode")) {
				if ("+1".equals(Owner_personalInfo.getString("mobileCountryCode")) || "+1".equals(Owner_personalInfo.getString("otherMobileCountryCode")))
					HasCRSInformationInType.FATCAStatus = "Y";
			}
			
//			if (Owner_personalInfo.has("organizationCountry")) {
//				if ("R58".equals(Owner_personalInfo.getString("organizationCountry")) || "R56".equals(Owner_personalInfo.getString("organizationCountry")))
//					HasCRSInformationInType.FATCAStatus = "Y";
//			} else if(Owner_personalInfo.has("mAddrCountry")) {
//				if ("R58".equals(Owner_personalInfo.getString("mAddrCountry")) || "R56".equals(Owner_personalInfo.getString("mAddrCountry")))
//					HasCRSInformationInType.FATCAStatus = "Y";
//			} else if(Owner_personalInfo.has("AddrCountry")) {
//				if ("R58".equals(Owner_personalInfo.getString("AddrCountry")) || "R56".equals(Owner_personalInfo.getString("AddrCountry")))
//					HasCRSInformationInType.FATCAStatus = "Y";
//			} else if(Owner_personalInfo.has("mobileCountryCode")) {
//				if ("+1".equals(Owner_personalInfo.getString("mobileCountryCode")) || "+1".equals(Owner_personalInfo.getString("mobileCountryCode")))
//					HasCRSInformationInType.FATCAStatus = "Y";
//			} else if(Owner_personalInfo.has("otherMobileCountryCode")) {
//				if ("+1".equals(Owner_personalInfo.getString("otherMobileCountryCode")) || "+1".equals(Owner_personalInfo.getString("otherMobileCountryCode")))
//					HasCRSInformationInType.FATCAStatus = "Y";
//			}
				
			CanBeIndividualType.hasCRSInformationIn = HasCRSInformationInType;
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > HasCustomerInformationInItemType > canBeIndividual >hasCRSInformationIn - S
			


			//RecordApplicationRequest > policy > hasAssessmentHeadersIn > hasAssessmentDetailsIn - E
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			HasAddressesInItemTypeList.add(HasAddressesInItemType);
			CanBeIndividualType.hasAddressesIn = HasAddressesInItemTypeList;

			HasCustomerInformationInItemType.canBeIndividual = CanBeIndividualType;

			HasCustomerInformationInItemTypeList.add(HasCustomerInformationInItemType);

			//remove for looping
			//HasInvestmentFundDetailsInItemTypeList.add(HasInvestmentFundDetailsInItemType);
			//HasAccountDetailsInItemType.hasInvestmentFundDetailsIn = HasInvestmentFundDetailsInItemTypeList;
			//HasAccountDetailsInItemTypeList.add(HasAccountDetailsInItemType);
			//HasDetailsRecordInItemType.hasAccountDetailsIn = HasAccountDetailsInItemTypeList;

			//HasAssessmentDetailsInItemTypeList.add(HasAssessmentDetailsInItemType);

			HasIndividualUWInfoInItemTypeList.add(HasIndividualUWInfoInItemType);
			CanBeIndividualType.hasIndividualUWInfoIn = HasIndividualUWInfoInItemTypeList;

			HasBankReferrerInfoInItemTypeList.add(HasBankReferrerInfoInItemType);
			HasStaffInformationInItemType.hasBankReferrerInfoIn = HasBankReferrerInfoInItemTypeList;
			HasStaffInformationInItemTypeList.add(HasStaffInformationInItemType);		
			HasAssociationWithItemType.hasStaffInformationIn = HasStaffInformationInItemTypeList;
			HasAssociationWithItemTypeList.add(HasAssociationWithItemType);
			HasProducerInformationInItemType.hasAssociationWith = HasAssociationWithItemTypeList;

			HasAssessmentHeadersInItemType.hasAssessmentDetailsIn = HasAssessmentDetailsInItemTypeList;

			HasAssessmentHeadersInItemTypeList.add(HasAssessmentHeadersInItemType);
			obj.recordApplicationRequest.policy.hasAssessmentHeadersIn = HasAssessmentHeadersInItemTypeList;

			HasAXAAsiaPartyAccountItemTypeList.add(HasAXAAsiaPartyAccountItemType);

			HasAXAAsiaPolicyPaymentAccountItemType.hasAXAAsiaPartyAccount = HasAXAAsiaPartyAccountItemTypeList;
			
			HasAXAAsiaPolicyPaymentAccountItemTypeList.add(HasAXAAsiaPolicyPaymentAccountItemType);
			obj.recordApplicationRequest.policy.hasAXAAsiaPolicyPaymentAccount = HasAXAAsiaPolicyPaymentAccountItemTypeList;
			
			HasLifePolicyTransactionHeaderDetailsInItemTypeList.add(HasLifePolicyTransactionHeaderDetailsInItemType);		
			HasLifePolicyTransactionDetailsInItemType.hasLifePolicyTransactionHeaderDetailsIn = HasLifePolicyTransactionHeaderDetailsInItemTypeList;
			HasLifePolicyTransactionDetailsInItemTypeList.add(HasLifePolicyTransactionDetailsInItemType);

		//CR: remove the payment block:  refer mail Fri 10/20/2017 2:32 PM	
			//obj.recordApplicationRequest.policy.hasLifePolicyTransactionDetailsIn = HasLifePolicyTransactionDetailsInItemTypeList;
		
		//	remove for looping
		//	isAssociatedWithLifeInsuranceProductList.add(IsAssociatedWithLifeInsuranceProductItemType);
		//	HasCoverageDetailsInType.isAssociatedWithLifeInsuranceProduct = isAssociatedWithLifeInsuranceProductList;

		//	ForLifeUnitofExposureItemType.hasCoverageDetailsIn = HasCoverageDetailsInType;	
		//	ForLifeUnitofExposureItemTypeList.add(ForLifeUnitofExposureItemType);  

			HasDetailsRecordInItemTypeList.add(HasDetailsRecordInItemType);
			
			if (investment_fund > 0) {
					obj.recordApplicationRequest.policy.hasDetailsRecordIn = HasDetailsRecordInItemTypeList;
			} 
				
			obj.recordApplicationRequest.policy.forLifeUnitofExposure = ForLifeUnitofExposureItemTypeList;
			obj.recordApplicationRequest.policy.hasDetailsOfPolicyIn = HasDetailsOfPolicyInItemTypeList;
			//TO CHECK DATA CAN NOT PASS CONVERSION
			obj.recordApplicationRequest.policy.hasProducerInformationIn = HasProducerInformationInItemTypeList;
			obj.recordApplicationRequest.policy.hasDetailsOfRisksIn = HasDetailsOfRisksInItemTypeList;

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn > hasAddressIn - S

			HasAddressesInItemType = obj.getHasAddressesInItemType();
			HasAddressesInItemTypeList = new ArrayList<HasAddressesInItemType>();
			
			String ow_tmp2_block = "";
			String ow_tmp2_unit = "";	
			String ow_addressline1 = "";	
			String ow_addressline2 = "";
			String ow_addressline3 = "";	
			String ow_addressline4 = "";
			String ow_addressline5 = "";
			
			if (Owner_personalInfo.has("addrBlock")) {
				ow_tmp2_block = truncateSpecialChar(Owner_personalInfo.getString("addrBlock").toUpperCase());
			}
			
			if (Owner_personalInfo.has("unitNum")) {
				ow_tmp2_unit = truncateSpecialChar(Owner_personalInfo.getString("unitNum").toUpperCase());
			}
			
			if ("".equals(ow_tmp2_block)&&"".equals(ow_tmp2_unit)) {
				HasAddressesInItemType.addressLine1 = "";
			} else if ("".equals(ow_tmp2_block)){
				HasAddressesInItemType.addressLine1 = ow_tmp2_unit;
			} else if ("".equals(ow_tmp2_unit)) {
				HasAddressesInItemType.addressLine1 = ow_tmp2_block;
			} else {
				HasAddressesInItemType.addressLine1 = ow_tmp2_block + " " + ow_tmp2_unit;
			}
			ow_addressline1 = HasAddressesInItemType.addressLine1;
			
			//Address 2
			HasAddressesInItemType.addressLine2 = "";
			
			if (Owner_personalInfo.has("addrStreet")) {
				HasAddressesInItemType.addressLine2 = truncateSpecialChar(Owner_personalInfo.getString("addrStreet").toUpperCase());
			}
			ow_addressline2 = HasAddressesInItemType.addressLine2;
			
			//Address 3
			String ow_tmp2_building = "";
			String ow_tmp2_city = "";
			
			if (Owner_personalInfo.has("addrEstate")) {
				ow_tmp2_building = truncateSpecialChar(Owner_personalInfo.getString("addrEstate").toUpperCase());
			}
			
			if (Owner_personalInfo.has("addrCity")) {
				ow_tmp2_city = truncateSpecialChar(Owner_personalInfo.getString("addrCity").toUpperCase());
			}
			
			if ("".equals(ow_tmp2_building)&&"".equals(ow_tmp2_city)) {
				HasAddressesInItemType.addressLine3 = "";
			} else if ("".equals(ow_tmp2_building)){
				HasAddressesInItemType.addressLine3 = CityConversion(ow_tmp2_city);
			} else if ("".equals(ow_tmp2_city)) {
				HasAddressesInItemType.addressLine3 = ow_tmp2_building;
			} else {
				HasAddressesInItemType.addressLine3 = ow_tmp2_building + " " + CityConversion(ow_tmp2_city);
			}
			ow_addressline3 = HasAddressesInItemType.addressLine3;
			//Address 4
			String ow_tmp2_country = "";
			HasAddressesInItemType.addressLine4 = "";
			
			if (Owner_personalInfo.has("addrCountry")) {
				ow_tmp2_country = CountryConversion(truncateSpecialChar(Owner_personalInfo.getString("addrCountry").toUpperCase()));
				
				if (ow_tmp2_country.toLowerCase().contains("China".toLowerCase())) {
					HasAddressesInItemType.addressLine4 = "CHINA" + " " + truncateSpecialChar(Owner_personalInfo.getString("postalCode").toUpperCase());
				} else {
					HasAddressesInItemType.addressLine4 = ow_tmp2_country+ " " + truncateSpecialChar(Owner_personalInfo.getString("postalCode").toUpperCase());
				}
			}
			ow_addressline4 = HasAddressesInItemType.addressLine4;
			
			//Address 5
			HasAddressesInItemType.cityNM = "";
			
			if (Owner_personalInfo.has("addrCountry")) {
				HasAddressesInItemType.cityNM = getCountryCD(ow_tmp2_country); 
			}
			
			//MOBILE'NUMBER OF INSURED
			//HasAddressesInItemType.mobilePhoneNO = "9876512345";
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_personalInfo.has("mobileNo")) {
					try {
						HasAddressesInItemType.mobilePhoneNO = Long.toString(LifeAssured_personalInfo.getLong("mobileNo")); 
					} catch(Exception ex002) {
						HasAddressesInItemType.mobilePhoneNO = LifeAssured_personalInfo.getString("mobileNo");
					}
				}
			}

			//EMAIL ADDRESS OF INSURED
			/*
				HasAddressesInItemType.emailAddress = quotation.getString("iEmail");
			 */
			
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_personalInfo.has("email")) {
					HasAddressesInItemType.emailAddress = truncateSpecialChar(LifeAssured_personalInfo.getString("email").toUpperCase());
				}
			}
			
			//TRUST DECLARATION 
			HasIndividualUWInfoInItemType.trustDeclarationIND = "N";
			if (is_1stparty || is_3rdparty) {
				if (LA_issueage < 16) {
					HasIndividualUWInfoInItemType.trustDeclarationIND = "Y";
				}
			}

			HasAddressesInItemTypeList.add(HasAddressesInItemType);

			HasPersonalDetailsInItemType.hasAddressesIn = HasAddressesInItemTypeList;
			HasPersonalDetailsInItemTypeList.add(HasPersonalDetailsInItemType);

			HasDetailsOfRisksInItemType.hasPersonalDetailsIn = HasPersonalDetailsInItemTypeList;
			HasDetailsOfRisksInItemTypeList.add(HasDetailsOfRisksInItemType);
			
			HasDetailsOfPolicyInItemTypeList.add(HasDetailsOfPolicyInItemType);

			HasProducerInformationInItemTypeList.add(HasProducerInformationInItemType);

			obj.recordApplicationRequest.policy.hasCustomerInformationIn = HasCustomerInformationInItemTypeList;
			
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn > hasAddressIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn > hasIndividualUWInfoIn - S

			HasIndividualUWInfoInItemTypeList = new ArrayList<HasIndividualUWInfoInItemType>();
			HasIndividualUWInfoInItemType = obj.getHasIndividualUWInfoInItemType();
			
			HasPersonalDetailsInItemTypeList = new ArrayList<HasPersonalDetailsInItemType>();
			
			HasDetailsOfRisksInItemTypeList = new ArrayList<HasDetailsOfRisksInItemType>();
			
			if (!isShield)
				HasIndividualUWInfoInItemType.healthQuestionIND = "N";
			
			//CUSTOMER PROTECTION DECLARA
			HasIndividualUWInfoInItemType.customerProtectionDeclarationIND = "N";
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_policies.has("isProslReplace")) {
					HasIndividualUWInfoInItemType.customerProtectionDeclarationIND = LifeAssured_policies.getString("isProslReplace");
				}
			}  

			//AVOCATION QUESTION
			HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_insurability.has("LIFESTYLE05")) {
					if ("Y".equals(LifeAssured_insurability.getString("LIFESTYLE05"))) {
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
					} else {
						HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
					}
				}
			}
			
			//DECLINE QUESTION
			HasIndividualUWInfoInItemType.declineQuestionIND = "N";
			if (!isShield) {
				if (LifeAssured_insurability.has("INS01")) {
					HasIndividualUWInfoInItemType.declineQuestionIND = LifeAssured_insurability.getString("INS01");
					
					if ("Y".equals(LifeAssured_insurability.getString("INS01"))){
			//			isLAPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("LA: INS History = Y");
					}
				}
			}
			
			//For Shield
			if (isShield) {
				HasIndividualUWInfoInItemType.avocationQuestionIND = "N";
			
				if (LifeAssured_insurability.has("INS03")) {					
					if ("Y".equals(LifeAssured_insurability.getString("INS03"))){
						isPolicyPMED = true;
						HasIndividualUWInfoInItemType.declineQuestionIND = LifeAssured_insurability.getString("INS03");
						Pol_remarks.addpolicyRemarks("Substandard history"); 
					}
				}
				
				if (LifeAssured_insurability.has("INS04")) {
					if ("Y".equals(LifeAssured_insurability.getString("INS04"))){
						isPolicyPMED = true;
						isLAPMED = true;
					}
				}

				if (LifeAssured_insurability.has("INS05")) {
					if ("Y".equals(LifeAssured_insurability.getString("INS05"))){
						isPolicyPMED = true;
						isLAPMED = true;
						Pol_remarks.addpolicyRemarks("MSH 30% loading"); 
					}
				}
				
				
				HasIndividualUWInfoInItemType.diQuestionIND = ""; 
				HasIndividualUWInfoInItemType.trustDeclarationIND = ""; 
				
				HasIndividualUWInfoInItemType.customerProtectionDeclarationIND = HasPersonalDetailsInItemType.externalShieldIND;//TODO
				HasIndividualUWInfoInItemType.agentReportIND = "Y";
				
				HasIndividualUWInfoInItemType.dobProofIND = "Y";
				HasIndividualUWInfoInItemType.previousSumInsured = "0";
				
			}
			
			
			if (((!isLAPMED) || (!isPolicyPMED)) && !isShield) {
				if (LifeAssured_insurability.has("INS02")) {
					if ("Y".equals(LifeAssured_insurability.getString("INS02"))){
						isLAPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("LA: INS History = Y");
					}
				}
			}
			
			if (!isLAPMED && !isShield) {
				if (LifeAssured_insurability.has("LIFESTYLE06")) {
					if ("Y".equals(LifeAssured_insurability.getString("LIFESTYLE06"))){
						isLAPMED = true;
					}
				}
			}
			
			String LA_medicalQN = null;
			
			if (!isShield) {
				for(int QN_COUNT = 0; QN_COUNT < medicalQNString.length; QN_COUNT++) {
					LA_medicalQN = medicalQNString[QN_COUNT];
					
					if (LifeAssured_insurability.has(LA_medicalQN) && !LA_medicalQN.equals("INS02")) {
						if ("Y".equals(LifeAssured_insurability.getString(LA_medicalQN))){
							isLAPMED = true;
							isPolicyPMED = true;
							Pol_remarks.addpolicyRemarks("LA: Health Qn = Y");
														
							if (isLAPMED && isPolicyPMED)
								break;
						}
					}
				}
			} else {
				for(int QN_COUNT = 0; QN_COUNT < medicalShieldQNString.length; QN_COUNT++) {
					LA_medicalQN = medicalShieldQNString[QN_COUNT];
					
					if (LifeAssured_insurability.has(LA_medicalQN)) {
						if ("Y".equals(LifeAssured_insurability.getString(LA_medicalQN))){
							
							isPolicyPMED = true;
							
							if (isPolicyPMED)
								break;
						}
					}
				}
			}

			//Details of Regular Doctor
			if (LifeAssured_insurability.has("REG_DR01_DATA") && !isShield) {
				JSONArray REG_DR01_DATA_array = LifeAssured_insurability.getJSONArray("REG_DR01_DATA");
				
				for(int i = 0; i < REG_DR01_DATA_array.length(); i++) {
					JSONObject REG_DR01_DATA = REG_DR01_DATA_array.getJSONObject(i);
					
					if (REG_DR01_DATA.has("REG_DR01d")) {
						if (REG_DR01_DATA.getString("REG_DR01d").toUpperCase().equals("OTHER")) {
							isLAPMED = true;
							isPolicyPMED = true;
							
							Pol_remarks.addpolicyRemarks("LA: Check Reason of Consultation");
							break;
						}
					}
				}
			}
			
			
			Double LA_weight_change = 0.00;
			Double LA_weight_GainLost = 0.00;
			Double LA_weightchangePC = 0.0;
			
			//if (!isLAPMED || !isPolicyPMED) {
				if (LifeAssured_insurability.has("HW03")&&("Y".equals(LifeAssured_insurability.getString("HW03")))) {
					if (LifeAssured_insurability.has("HW03b")) {
						LA_weight_change = LifeAssured_insurability.getDouble("HW03b");
					}
					
					if (LA_weight_change > 0) {
						if ("L".equals(LifeAssured_insurability.getString("HW03a"))) {
							 LA_weight_GainLost = LA_weight_change *(-1);
						} else {
							 LA_weight_GainLost = LA_weight_change;
						}
					}
					LA_weightchangePC = LA_weight_change/(LA_weight + LA_weight_GainLost);
					
					if (LA_weightchangePC > 0.05) {
						isLAPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("LA: Weight change = Y");
					} else if (LifeAssured_insurability.has("HW03c")&&("OTHER".equals(LifeAssured_insurability.getString("HW03c").toUpperCase()))){
						isLAPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("LA: Weight change = Y");
					}			
				}
			//}
			
			HasIndividualUWInfoInItemType.smokingQuestionIND = "N";
			
			if (LifeAssured_insurability.has("LIFESTYLE01")) {
				HasIndividualUWInfoInItemType.smokingQuestionIND = LifeAssured_insurability.getString("LIFESTYLE01");
			}
			
			HasIndividualUWInfoInItemType.cigarettesPerDayNO = "0";
			
			if (!isShield) {
				//TOBACCO QUESTION FOR PAYOR
				HasIndividualUWInfoInItemType.tobaccoQuestionIND = "N";
				
				if (is_1stparty || is_3rdparty) {
					if (LifeAssured_insurability.has("LIFESTYLE01")) {
						HasIndividualUWInfoInItemType.tobaccoQuestionIND = LifeAssured_insurability.getString("LIFESTYLE01");
							
//						if (Owner_insurability.getString("LIFESTYLE01").equals("Y"))
//							isPolicyPMED = true;
					}
				}
				
				
				if (LifeAssured_insurability.has("LIFESTYLE01b")) {
					int cigarettesPerDayNO = LifeAssured_insurability.getInt("LIFESTYLE01b");
					
					if (cigarettesPerDayNO > 99)
						cigarettesPerDayNO = 99;
							
					HasIndividualUWInfoInItemType.cigarettesPerDayNO = Integer.toString(cigarettesPerDayNO);
					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = Integer.toString(cigarettesPerDayNO);
					
					if (cigarettesPerDayNO > 30) {
						isPolicyPMED = true;
						isLAPMED = true;
				
						Pol_remarks.addpolicyRemarks("LA: Cigarette > 30");
					}	
				}
			}
						
			if (is_inspireDuo || is_pulsarProduct) {
				if (quotation.has("iSmoke") && quotation.getString("iSmoke").equals("Y")) {
					HasIndividualUWInfoInItemType.smokingQuestionIND = "Y";
					HasIndividualUWInfoInItemType.tobaccoQuestionIND = "Y";
					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "1";
					HasIndividualUWInfoInItemType.cigarettesPerDayNO = "1";	
				}
			}
			
			if (isShield) {
				if (LifeAssured_insurability.has("LIFESTYLE01b")) {
					int cigarettesPerDayNO = LifeAssured_insurability.getInt("LIFESTYLE01b");
					int smokedYearNo = LifeAssured_insurability.getInt("LIFESTYLE01c");
					HasIndividualUWInfoInItemType.smokingQuestionIND = "Y";
					HasIndividualUWInfoInItemType.tobaccoQuestionIND = "Y";
					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = Integer.toString(cigarettesPerDayNO);
					HasIndividualUWInfoInItemType.cigarettesPerDayNO = Integer.toString(cigarettesPerDayNO);
																	
					double packsPerYear = (cigarettesPerDayNO * smokedYearNo) / 20.0;
					
					if (cigarettesPerDayNO > 25) {
						isPolicyPMED = true;
						//HasIndividualUWInfoInItemType.tobaccoQuestionIND = "Y";
						HasIndividualUWInfoInItemType.smokingQuestionIND = LifeAssured_insurability.getString("LIFESTYLE01");
			 
						Pol_remarks.addpolicyRemarks("Smoking Flag (" + cigarettesPerDayNO + " cigarettes per day / " + packsPerYear  + " pack years)");
					} else if (packsPerYear > 40) {
						isPolicyPMED = true;
						//HasIndividualUWInfoInItemType.tobaccoQuestionIND = "Y";
						HasIndividualUWInfoInItemType.smokingQuestionIND = LifeAssured_insurability.getString("LIFESTYLE01");
						Pol_remarks.addpolicyRemarks("Smoking Flag (" + cigarettesPerDayNO + " cigarettes per day / " + packsPerYear  + " pack years)"); 
					}
				} else {
					HasIndividualUWInfoInItemType.smokingQuestionIND = "N";
					HasIndividualUWInfoInItemType.tobaccoQuestionIND = "N";
					HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "0";
				}
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			String LA_residenceDeclCD = "";
			String OW_residenceDeclCD = "";
			String LA_countryOfResidenceCD = "";
			String OW_countryOfResidenceCD = "";
			String LA_medicalIND = "";
			String OW_medicalIND = "";
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Life Assured
			//residenceDeclCD
			//residing in Singapore or residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			if (LA_P6_Q1.equals("resided") || LA_P6_Q1.equals("outside"))
				LA_residenceDeclCD = "SIF";	
			else if (LA_P6_Q1.equals("outside5"))//residing outside Singapore. I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
				LA_residenceDeclCD = "OIF";	
			
			//Owner
			//residing in Singapore or residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			if (OW_P6_Q1.equals("resided") || OW_P6_Q1.equals("outside"))
				OW_residenceDeclCD = "SIF";	
			else if (OW_P6_Q1.equals("outside5"))//residing outside Singapore. I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
				OW_residenceDeclCD = "OIF";	
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//medicalIND
			//Life Assured
			//residing in Singapore 
			if (LA_P6_Q1.equals("resided"))
				LA_medicalIND = "N";	
			//I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
			//residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			else if (LA_P6_Q1.equals("outside5") || LA_P6_Q1.equals("outside"))//residing outside Singapore. 
				LA_medicalIND = "Y";	
			
			//Owner
			//residing in Singapore 
			if (OW_P6_Q1.equals("resided") )
				OW_medicalIND = "N";	
			//I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
			//residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			else if (OW_P6_Q1.equals("outside5") || OW_P6_Q1.equals("outside"))
				OW_medicalIND = "Y";	
					
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Life Assured
			//countryOfResidenceCD
			//LA_P6_Q2 = Have you resided in Singapore for at least 183 days in the 12 months preceding the date of proposal?
			//LA_P6_Q3 = Do you intend to stay in Singapore on a permanent basis?
			if (LA_P6_Q2.equals("Y") && LA_P6_Q3.equals("Y")) {
				LA_residenceDeclCD = "SIF";
				LA_countryOfResidenceCD = "LR";
				LA_medicalIND = "N";
			} else if (LA_P6_Q2.equals("N") && LA_P6_Q3.equals("Y")) {
				LA_residenceDeclCD = "OIF";
				LA_countryOfResidenceCD = "FR";
				LA_medicalIND = "Y";
			} else if (LA_P6_Q2.equals("Y") && LA_P6_Q3.equals("N")) {
				LA_residenceDeclCD = "SIF";
				LA_countryOfResidenceCD = "LR";
				LA_medicalIND = "Y";
			} else if (LA_P6_Q2.equals("N") && LA_P6_Q3.equals("N")) {
				LA_residenceDeclCD = "OIF";
				LA_countryOfResidenceCD = "FR";
				LA_medicalIND = "Y";
			}
			
			//Owner
			//OW_P6_Q2 = Have you resided in Singapore for at least 183 days in the 12 months preceding the date of proposal?
			//OW_P6_Q3 = Do you intend to stay in Singapore on a permanent basis?
			if (OW_P6_Q2.equals("Y") && OW_P6_Q3.equals("Y")) {
				OW_residenceDeclCD = "SIF";
				OW_countryOfResidenceCD = "LR";
				OW_medicalIND = "N";
			} else if (OW_P6_Q2.equals("N") && OW_P6_Q3.equals("Y")) {
				OW_residenceDeclCD = "OIF";
				OW_countryOfResidenceCD = "FR";
				OW_medicalIND = "Y";
			} else if (OW_P6_Q2.equals("Y") && OW_P6_Q3.equals("N")) {
				OW_residenceDeclCD = "SIF";
				OW_countryOfResidenceCD = "LR";
				OW_medicalIND = "Y";
			} else if (OW_P6_Q2.equals("N") && OW_P6_Q3.equals("N")) {
				OW_residenceDeclCD = "OIF";
				OW_countryOfResidenceCD = "FR";
				OW_medicalIND = "Y";
			}
										
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Have you resided in Singapore for at least 90 days in the last 12 months proceding the date of proposal?
			//Life Assured
			if (LA_P6_Q4.equals("Y")) {
				LA_residenceDeclCD = "SIF";
				LA_medicalIND = "Y";
			}
			else if (LA_P6_Q4.equals("N")) {
				LA_residenceDeclCD = "OIF";
				LA_medicalIND = "Y";
			}
					
			//Owner
			if (OW_P6_Q4.equals("Y")) {
				OW_residenceDeclCD = "SIF";
				OW_medicalIND = "Y";
			} else if (OW_P6_Q1.equals("N")) {
				OW_residenceDeclCD = "OIF";
				OW_medicalIND = "Y";
			}
					
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Life Assured
			if (LA_P6_Q1.equals("resided") || LA_P6_Q1.equals("outside") || LA_P6_Q1.equals("outside5"))
				LA_countryOfResidenceCD = "LR";	
			
			//Owner
			if (OW_P6_Q1.equals("resided") || OW_P6_Q1.equals("outside") || OW_P6_Q1.equals("outside5"))
				OW_countryOfResidenceCD = "LR";	
 
			//Life Assured
			if (LA_P6_Q4.equals("Y"))
				LA_countryOfResidenceCD = "FR";	
			else if (LA_P6_Q4.equals("N"))
				LA_countryOfResidenceCD = "FR";	
			
			//Owner
			if (OW_P6_Q4.equals("Y"))
				OW_countryOfResidenceCD = "FR";	
			else if (OW_P6_Q1.equals("N"))
				OW_countryOfResidenceCD = "FR";
			
			/////////////////////////////////////////////////////////////////////////////////////////////
			//Other Pass
			if (LA_residenceDeclCD.equals(""))
				LA_residenceDeclCD = "OIF";
			
			if (LA_countryOfResidenceCD.equals(""))
				LA_countryOfResidenceCD = "FR";
			
			if (LA_medicalIND.equals(""))
				LA_medicalIND = "Y";
			
			/////////////////////////////////////////////////////////////////////////////////////////////
			if (!isShield) {
				if (is_3rdparty) {
					//residenceDeclCD
					if (LA_residenceDeclCD.equals(OW_residenceDeclCD))
						HasIndividualUWInfoInItemType.residenceDeclCD = LA_residenceDeclCD;
					else
						HasIndividualUWInfoInItemType.residenceDeclCD = "OIF";
					
					//countryOfResidenceCD
					if (LA_countryOfResidenceCD.equals(OW_countryOfResidenceCD))
						HasPersonalDetailsInItemType.countryOfResidenceCD  = LA_countryOfResidenceCD;
					else
						HasPersonalDetailsInItemType.countryOfResidenceCD  = "FR";
					
					//countryOfResidenceCD
					if (LA_medicalIND.equals(OW_medicalIND)) 
						obj.recordApplicationRequest.policy.medicalIND = LA_medicalIND;	
					else 
						obj.recordApplicationRequest.policy.medicalIND = "Y";
					
//					if (OW_countryOfResidenceCD.equals("LR"))
//						CanBeIndividualType.countryOfOriginDESC = ""; 
				} else {
					HasIndividualUWInfoInItemType.residenceDeclCD =  LA_residenceDeclCD;
					HasPersonalDetailsInItemType.countryOfResidenceCD  = LA_countryOfResidenceCD;
					obj.recordApplicationRequest.policy.medicalIND = LA_medicalIND;
					
//					if (LA_countryOfResidenceCD.equals("LR"))
//						CanBeIndividualType.countryOfOriginDESC = ""; 
				}
				
				//Refer the logic from table value v10 Excel > RESDEC tab
//				String r_code = HasIndividualUWInfoInItemType.residenceDeclCD.toString();
//				String m_ind = obj.recordApplicationRequest.policy.medicalIND;
//				String c_code = HasPersonalDetailsInItemType.countryOfResidenceCD.toString();
//				
//				if (r_code.equals("SIF") && m_ind.equals("Y") && c_code.equals("LR") ||	
//					r_code.equals("SIF") && m_ind.equals("Y") && c_code.equals("FR") ||
//					r_code.equals("OIF") && m_ind.equals("Y") && c_code.equals("FR") ||
//					r_code.equals("OIF") && m_ind.equals("Y") && c_code.equals("LR")) {
					
				if (obj.recordApplicationRequest.policy.medicalIND.equals("Y")) {
					if (is_1stparty) {
						Pol_remarks.addpolicyRemarks("LA: RESDEC = Y");
					} else {
						if (OW_medicalIND.equals("Y")) {
							Pol_remarks.addpolicyRemarks("PH: RESDEC = Y");
						}
						
						if (LA_medicalIND.equals("Y")) {
							Pol_remarks.addpolicyRemarks("LA: RESDEC = Y");
						}
					}
				}
			}
			
			if (isShield) {
				HasIndividualUWInfoInItemType.residenceDeclCD = "SIF";
										
				if (LA_nationality.equals("SINGAPOREAN"))		
					HasPersonalDetailsInItemType.countryOfResidenceCD  = "LR";
				else
					HasPersonalDetailsInItemType.countryOfResidenceCD  = "PR";
			}
			
			if (HasPersonalDetailsInItemType.countryOfResidenceCD.equals("LR")) {
				CanBeIndividualType.countryOfOriginDESC = "";
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	//BD01-026		
			if (!isPolicyPMED && !isShield) {
				boolean isPassResidencyCHK = true;
				if (is_1stparty) {
					if (LifeAssured_residency.has("EAPP-P6-F1")) {
						if ("resided".equals(LifeAssured_residency.getString("EAPP-P6-F1"))) {
							isPassResidencyCHK = true;	
						} else {
							isPassResidencyCHK = false;
						}
					}	
					if (LifeAssured_residency.has("EAPP-P6-F4")) {
						//if ("Y".equals(LifeAssured_residency.getString("EAPP-P6-F4")) && "Y".equals(LifeAssured_residency.getString("EAPP-P6-F5")))	{
						if ("Y".equals(LifeAssured_residency.getString("EAPP-P6-F4")))	{//TODO
							isPassResidencyCHK = true;		
						} else {
							isPassResidencyCHK = false;
						}
					}	
				} 
				if (!isPassResidencyCHK) {
					isPolicyPMED = true; 
				}
//				if(is_3rdparty&&!isSameAnswer) {
//					isPolicyPMED = true;
//				}
			}

			//ALCOHOL QUESTION
			//sample: HasIndividualUWInfoInItemType.alcoholQuestionIND = "Y";
			
			boolean LA_is_drinker = false;
			HasIndividualUWInfoInItemType.alcoholQuestionIND = "N";
			
			if (is_1stparty || is_3rdparty) {
				if (LifeAssured_insurability.has("LIFESTYLE02")) {
					HasIndividualUWInfoInItemType.alcoholQuestionIND = LifeAssured_insurability.getString("LIFESTYLE02");
					
					if ("Y".equals(LifeAssured_insurability.getString("LIFESTYLE02"))) {
						LA_is_drinker = true;	
					}			
				}
			}
			
			if (!isShield) {
				if (LifeAssured_insurability.has("LIFESTYLE03")) {
					if (LifeAssured_insurability.getString("LIFESTYLE03").equals("Y")) {
						Pol_remarks.addpolicyRemarks("LA: Drug = Y");
						isLAPMED = true;
						isPolicyPMED = true;
					}
						
				}
				
				if (LifeAssured_insurability.has("LIFESTYLE04")) {
					if (LifeAssured_insurability.getString("LIFESTYLE04").equals("Y")) {
						isPolicyPMED = true;
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
						Pol_remarks.addpolicyRemarks("LA: Intention to reside outside = Y");
					}
				}
				
				if (LifeAssured_insurability.has("LIFESTYLE05")) {
					if (LifeAssured_insurability.getString("LIFESTYLE05").equals("Y")) {
						isPolicyPMED = true;
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
						Pol_remarks.addpolicyRemarks("LA: Avocation = Y");
					}
				}
				
				if (LifeAssured_insurability.has("LIFESTYLE06")) {
					if (LifeAssured_insurability.getString("LIFESTYLE06").equals("Y")) {
						isPolicyPMED = true;
						HasIndividualUWInfoInItemType.avocationQuestionIND = "Y";
						Pol_remarks.addpolicyRemarks("LA: Travel for > 3 months");
					}
				}
				
				if (LifeAssured_insurability.has("FAMILY01")) {
					if (LifeAssured_insurability.getString("FAMILY01").equals("Y")) {
						isLAPMED = true;
						isPolicyPMED = true;
						Pol_remarks.addpolicyRemarks("LA: Family Hx = Y");
					}
				}
			}
			
			//ALCOHOL UNITS/WEEK
			//HasIndividualUWInfoInItemType.alcoholUnitsPerWeekNO = "27";
			Integer LA_alcohol_unit_weekly = 0;
			
			if (LA_is_drinker) {
				if (LifeAssured_insurability.has("LIFESTYLE02a_1")) {
					if ("Y".equals(LifeAssured_insurability.getString("LIFESTYLE02a_1"))) {
						LA_alcohol_unit_weekly = LA_alcohol_unit_weekly + LifeAssured_insurability.getInt("LIFESTYLE02a_2");
					}	
					
					if ("Y".equals(LifeAssured_insurability.getString("LIFESTYLE02b_1"))) {
						LA_alcohol_unit_weekly = LA_alcohol_unit_weekly + LifeAssured_insurability.getInt("LIFESTYLE02b_2");
					}
					
					if ("Y".equals(LifeAssured_insurability.getString("LIFESTYLE02c_1"))) {
						LA_alcohol_unit_weekly = LA_alcohol_unit_weekly + LifeAssured_insurability.getInt("LIFESTYLE02c_2");
					}
					
					if (LA_alcohol_unit_weekly > 99)
						LA_alcohol_unit_weekly = 99;

					if (!isShield && LA_alcohol_unit_weekly > 30) {
						Pol_remarks.addpolicyRemarks("LA: Alcohol > 30");
						isLAPMED = true;
						isPolicyPMED = true;
					}
					
					if (isShield && LA_alcohol_unit_weekly >= 30)
						isPolicyPMED = true;
				}
			}	
			HasIndividualUWInfoInItemType.alcoholUnitsPerWeekNO = Integer.toString(LA_alcohol_unit_weekly);

//			if (!isShield) {
//				//TOBACCO QUESTION
//				HasIndividualUWInfoInItemType.tobaccoQuestionIND = "N";
//				
//				if (LifeAssured_insurability.has("LIFESTYLE01")) {
//					HasIndividualUWInfoInItemType.tobaccoQuestionIND = LifeAssured_insurability.getString("LIFESTYLE01");
//					
//					if (LifeAssured_insurability.getString("LIFESTYLE01").equals("Y"))
//						isPolicyPMED = true;
//				}
//			}

			//TOBACCO UNITS/DAY
			//sample: HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "23";
//			HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "0";
//					
//			if (LifeAssured_insurability.has("LIFESTYLE01b")) {
//				int cigarettesPerDayNO = LifeAssured_insurability.getInt("LIFESTYLE01b");
//				
//				if (cigarettesPerDayNO > 99)
//					cigarettesPerDayNO = 99;
//				
//				HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = Integer.toString(cigarettesPerDayNO);
//				
//				if (!isShield)
//					Pol_remarks.addpolicyRemarks("Cigarette > 30");
//			}	
			
//			if (isShield) {
//				HasIndividualUWInfoInItemType.tobaccoQuestionIND = "N";
//				HasIndividualUWInfoInItemType.tobaccoUnitsPerDayNO = "0";
//				HasIndividualUWInfoInItemType.previousHealthExamQuestionIND = "N";
//				HasIndividualUWInfoInItemType.previousHealthExamReasonIND = "0";
//				HasIndividualUWInfoInItemType.previousHealthExamResultIND = "0";
//			}
			
			//PREVIOUS EXAM FOR INSURED
			HasIndividualUWInfoInItemType.previousHealthExamQuestionIND = "N";

			//PREVIOUS EXAM REASON FOR IN
			HasIndividualUWInfoInItemType.previousHealthExamReasonIND = "";

			//PREVIOUS EXAM RESULT FOR IN
			HasIndividualUWInfoInItemType.previousHealthExamResultIND = "";
			
			//AGENT REPORT
			HasIndividualUWInfoInItemType.agentReportIND = "Y";
			
			if (isLAPMED) {  
				HasIndividualUWInfoInItemType.healthQuestionIND = "Y";
			} else {
				HasIndividualUWInfoInItemType.healthQuestionIND = "N";
			}

			//RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn > hasIndividualUWInfoIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			//!!!!---- Move to after all validations checking of "isPolicyPMED" ------ !!!!//
//			HasIndividualUWInfoInItemTypeList.add(HasIndividualUWInfoInItemType);
//			
//			HasPersonalDetailsInItemType.hasIndividualUWInfoIn = HasIndividualUWInfoInItemTypeList;
//			
//			HasPersonalDetailsInItemTypeList.add(HasPersonalDetailsInItemType);
//			
//			HasDetailsOfRisksInItemType.hasPersonalDetailsIn = HasPersonalDetailsInItemTypeList;
//			HasDetailsOfRisksInItemTypeList.add(HasDetailsOfRisksInItemType);
//			obj.recordApplicationRequest.policy.hasDetailsOfRisksIn = HasDetailsOfRisksInItemTypeList;
			//!!!!---- Move to after all validations checking of "isPolicyPMED" ------ !!!!//
			
			//Policy Level:   
			//policy remark summary& medical indicator
			//IF:
			//	"Supervisor Approval Date" already exceeded "Age Crossed Date"
			//OR
			//	"Age Crossed Date" - "Supervisory Approval Date" =< 3 Calendar days
			String ageCrossDate = "";  
			int passedDays = 0;
			boolean isCrossAge = false;
			
			if (isAPPROVE) {
				//riskCommenceDate = "2017-11-21";
				//LA_birthDate = "1993-8-29";
				
				//Signature_date = "2017-11-21";
//				
				//ageCrossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, false); 
					
//				String a= "";
//				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
//				long diff = format.parse(approveDate).getTime() - format.parse(LA_birthDate).getTime();
//				long diffDays = (diff / (24 * 60 * 60 * 1000)) + 3;
//				long diffYears = diffDays / 365;
//				int eSub_age = ((Number)diffYears).intValue();
				
//				approveDate = "2017-11-17";
//				LA_birthDate = "1987-5-18";
				
				//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
				
//				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//				Calendar cal  = Calendar.getInstance();
//				cal.setTime(df.parse(approveDate));
				
				//For testing
//				LA_birthDate = "1987-4-21";
//				approveDate = "2017-11-5";
//				LA_issueage = 31;
					
				if (isShield) {
//					String[] apprDateParts = approveDate.split("-");
//					Calendar apprDate = null;
//
//					apprDate = new GregorianCalendar(Integer.parseInt(apprDateParts[0]), Integer.parseInt(apprDateParts[1])-1, Integer.parseInt(apprDateParts[2]));								
//				
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//					String _approveDate3 = sdf.format(apprDate.getTime());
					 				
//					int eSub_age = ageCrossDateCal.calAgeNearestBirthday(approveDate, LA_birthDate);
//							
//					if (eSub_age > LA_issueage) {
//						isPolicyPMED = true;
//						Pol_remarks.addpolicyRemarks("Age crossed or will cross soon."); 
//					}
				} else {		
					//if (is_1stparty) {
						String[] apprDateParts = approveDate.split("-");
						Calendar apprDate = null;

						apprDate = new GregorianCalendar(Integer.parseInt(apprDateParts[0]), Integer.parseInt(apprDateParts[1])-1, Integer.parseInt(apprDateParts[2]));								
						apprDate.add(Calendar.DATE, 3);
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						String _approveDate3 = sdf.format(apprDate.getTime());
						
						int eSub_age = ageCrossDateCal.calAgeNearestBirthday(_approveDate3, LA_birthDate);
						
						if (eSub_age > LA_issueage) {
							isPolicyPMED = true;
							Pol_remarks.addpolicyRemarks("LA: Age crossed or will cross soon."); 
						}
						
						if (hasPayorBenefitRider && is_3rdparty) {								
							eSub_age = ageCrossDateCal.calAgeNearestBirthday(_approveDate3, OW_birthDate);
							
							int OW_nearAge = 0;
							
							if (Owner_personalInfo.has("nearAge")) {
								OW_nearAge = Owner_personalInfo.getInt("nearAge");
								
								if (eSub_age > OW_nearAge) {
									isPolicyPMED = true;
									Pol_remarks.addpolicyRemarks("PH: Age crossed or will cross soon."); 
								}
							}
								
						}
					//}
				}
				
				//approveDate- LA_birthDate				
				
				//Age Cross Date < Date of Submission
//				if (ageCrossDate.compareTo(Signature_date) <= 0) {
//				
//				} else {
					//ageCrossDate = "2017-10-20";
					//approveDate = "2017-11-21";
					
//					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
//					
//					ageCrossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, true); 
//					
//					long diff = format.parse(riskCommenceDate).getTime() - format.parse(ageCrossDate).getTime();
//				
//					long diffDays = diff / (24 * 60 * 60 * 1000);
//					passedDays  = ((Number)diffDays).intValue();
//													
//					if (is_1stparty || (hasPPEPS && is_3rdparty)) {
//						if (passedDays > 183) {
//							isPolicyPMED = true;
//							Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon."); 
//						}  else { 
//							String crossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, false);				
//							long diff1 = format.parse(crossDate).getTime() - format.parse(approveDate).getTime();
//							long diffDays1 = diff1 / (24 * 60 * 60 * 1000);
//							passedDays  = ((Number)diffDays1).intValue();
//							
//							if (passedDays != -1 && passedDays <= 3) {
//								isPolicyPMED = true;
//								Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
//							}
//						}
//					}
			//	}
					
				
				
							
//				if (hasPPEPS && is_3rdparty) {					
//					if (passedDays > 183) {
//						isPolicyPMED = true;
//						Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon."); 
//						passedDays = 0;
//					} else { 
//						String crossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, false);				
//						long diff1 = format.parse(crossDate).getTime() - format.parse(approveDate).getTime();
//						long diffDays1 = diff1 / (24 * 60 * 60 * 1000);
//						passedDays  = ((Number)diffDays1).intValue();
//						
//						if (passedDays != -1 && passedDays <= 14) {
//							isPolicyPMED = true;
//							Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
//						}
//					}
//				}
				
			}
			
			//IF:
			//	Payor = "Others" AND
			//	Relationship of Payor to the Proposer  = "Others".
			//  Remark: the payor always stored at proposer
			
//			boolean hasFundsDetails = false;
//			
//			if (Owner_declaration.has("FUND_SRC01")) {
//				if (Owner_declaration.getString("FUND_SRC01").toUpperCase().equals("FOREIGN")) {
//					Pol_remarks.addpolicyRemarks("Check source of funds details");
//					isPolicyPMED = true;
//					hasFundsDetails = true;
//				}
//			}
			
//			if (Owner_declaration.has("FUND_SRC02")) {
//				if (!Owner_declaration.getString("FUND_SRC02").toUpperCase().equals("OTHER")) {
//					if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("Y")) 
//						isPolicyPMED = true;	
//				}
//			} 
			
			
			
//			if (is_3rdparty) {
//				String iCid = clientDoc.getString("cid");
//				String pCid = proposerClientDoc.getString("cid");
//					
//				//Insured
//				if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(iCid)) {
//					if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("Y")) 
//						isPolicyPMED = true;		
//				} 
//				
//				//Proposer
//				if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
//					if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("N") && Owner_declaration.getString("FUND_SRC30").equals("N")) 
//						isPolicyPMED = true;		
//				} 
//			} else {
//				String iCid = clientDoc.getString("cid");
//				
//				//Proposer
//				if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(iCid)) {
//					if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("N") && Owner_declaration.getString("FUND_SRC30").equals("N")) 
//						isPolicyPMED = true;		
//				} 
//			}
			
		
//			if (Owner_declaration.has("FUND_SRC02") && ("OTHER".equals(Owner_declaration.getString("FUND_SRC02").toUpperCase()))) {
//				if (Owner_declaration.has("FUND_SRC09") && ("OTH".equals(Owner_declaration.getString("FUND_SRC09").toUpperCase()))) {
//					Pol_remarks.addpolicyRemarks("Check source of funds details");
//					isPolicyPMED = true;
//					hasFundsDetails = true;
//				}
//			}
			
//			//Payor is LA with source of wealth = OTHERS
//			if (!hasFundsDetails) {
//				//String iCid = clientDoc.getString("cid");
//				
//				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(iCid)) {
//					if (Owner_declaration.has("FUND_SRC30") &&  Owner_declaration.getString("FUND_SRC30").equals("N")) {
//						if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("Y")) { 
//							Pol_remarks.addpolicyRemarks("Check source of funds details");
//							isPolicyPMED = true;	
//							hasFundsDetails = true;
//						}
//					}	
//				//} 
//			}

			
//			//Payor is PH with source of funds = OTHERS & source of wealth NOT = OTHERS
//			if (!hasFundsDetails) {
//				String pCid = proposerClientDoc.getString("cid");
//							
//				//Proposer
//				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
//					if (Owner_declaration.has("FUND_SRC30") &&  Owner_declaration.getString("FUND_SRC30").equals("Y")) {
//						if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("N")) { 
//							Pol_remarks.addpolicyRemarks("Check source of funds details");
//							isPolicyPMED = true;	
//							hasFundsDetails = true;
//						}
//					}
//				//} 
//			}
			
//			//Payor is PH with source of wealth = OTHERS & source of funds NOT = OTHERS
//			if (!hasFundsDetails) {
//				String pCid = proposerClientDoc.getString("cid");							
//				
//				//Proposer
//				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
//					if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("Y")) { 
//						if (Owner_declaration.has("FUND_SRC30") && Owner_declaration.getString("FUND_SRC30").equals("N")) {
//							Pol_remarks.addpolicyRemarks("Check source of funds details");
//							isPolicyPMED = true;	
//							hasFundsDetails = true;
//						}
//					}
//				//} 
//			}
			
//			//Payor is PH with source of wealth & source of funds = OTHERS
//			if (!hasFundsDetails) {
//				//String pCid = proposerClientDoc.getString("cid");							
//				
//				//Proposer
//				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
//					if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("Y")) { 
//						if (Owner_declaration.has("FUND_SRC30") && Owner_declaration.getString("FUND_SRC30").equals("Y")) {
//							Pol_remarks.addpolicyRemarks("Check source of funds details");
//							isPolicyPMED = true;	
//							hasFundsDetails = true;
//						}
//					}
//				//} 
//			}
			
			
			//1st Party, payor = LA with Source of fund = OTHERS and source of wealth NOT = OTHERS
//			if (!hasFundsDetails) {
//				String iCid = clientDoc.getString("cid");							
//				
//				//Insured
//				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(iCid)) {
//					if (Owner_declaration.has("FUND_SRC30") &&  Owner_declaration.getString("FUND_SRC30").equals("Y")) {
//						if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.getString("FUND_SRC38").equals("N")) { 
//							Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
//							isPolicyPMED = true;	
//							hasFundsDetails = true;
//						}
//					}
//				//} 
//			}
			
			
			
			
//			if (hasFundsDetails == false) {
//				if (Owner_declaration.has("FUND_SRC30")) {//Source of funds
//					if (Owner_declaration.get("FUND_SRC30").equals("Y")) {
//						Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
//						isPolicyPMED = true;
//					}
//				} else if (Owner_declaration.has("FUND_SRC38")) {//Source of Wealth
//					if (Owner_declaration.get("FUND_SRC38").equals("Y")) {
//						Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
//						isPolicyPMED = true;
//					}
//				}	
//			}
					
			//IF:
			//Source of Wealth = "Others" OR
			//Source of Funds = "Others"
			JSONObject Owner_declaration_PEP01_DATA = new JSONObject();
//			try {
//				if (Owner_declaration.has("PEP01_DATA")) {
//					JSONArray Owner_declaration_PEP01_DATA_array = Owner_declaration.getJSONArray("PEP01_DATA");
//					Owner_declaration_PEP01_DATA = Owner_declaration_PEP01_DATA_array.getJSONObject(0);
//				}
//			}catch(Exception PEP01_e0001){
//				
//			}
			
				
//			if (Owner_declaration.has("PEP01_DATA")) {
//				if (Owner_declaration_PEP01_DATA.has("PEP01f6")&&("Y".equals(Owner_declaration_PEP01_DATA.getString("PEP01f6")))){
//					Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
//					isPolicyPMED = true;
//				}
//				if (Owner_declaration_PEP01_DATA.has("PEP01e6")&&("Y".equals(Owner_declaration_PEP01_DATA.getString("PEP01e6")))){
//					Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
//					isPolicyPMED = true;
//				}	
//			}
			
			//Bankruptcy
			if (Owner_declaration.has("BANKRUPTCY01") && ("Y".equals(Owner_declaration.getString("BANKRUPTCY01"))) && is_3rdparty) {
				Pol_remarks.addpolicyRemarks("PH: Bankrupt declaration");
				
				if (!isShield)
					isPolicyPMED = true;
			}
			
			if (LifeAssured_declaration.has("BANKRUPTCY01") && ("Y".equals(LifeAssured_declaration.getString("BANKRUPTCY01")))) {
				Pol_remarks.addpolicyRemarks("LA: Bankrupt declaration");
				
				if (!isShield)
					isPolicyPMED = true;
			} 

			//PEP
			if (Owner_declaration.has("PEP01") && ("Y".equals(Owner_declaration.getString("PEP01"))) && is_3rdparty) {
				Pol_remarks.addpolicyRemarks("PH: PEP declaration");
				isPolicyPMED = true;
			}
			
			if (LifeAssured_declaration.has("PEP01")&&("Y".equals(LifeAssured_declaration.getString("PEP01")))) {
				Pol_remarks.addpolicyRemarks("LA: PEP declaration");
				isPolicyPMED = true;
			}

			//Trusted Individual (if Proposer is a Selected Client )
			if (Owner_declaration.has("TRUSTED_IND01")) {
				if (!Owner_declaration.getString("TRUSTED_IND01").isEmpty()) {
					Pol_remarks.addpolicyRemarks("VC=Y, TI=Y");
					isPolicyPMED = true;
				} else {
					Pol_remarks.addpolicyRemarks("VC=Y, TI=N");
				}
			}	

			//Concurrent application
			if (Owner_policies.has("havPndinApp")&&("Y".equals(Owner_policies.getString("havPndinApp"))) && is_3rdparty) {
				Pol_remarks.addpolicyRemarks("PH: Concurrent submissions.");
				isPolicyPMED = true;
			}
			
			if (LifeAssured_policies.has("havPndinApp")&&("Y".equals(LifeAssured_policies.getString("havPndinApp")))) {
				Pol_remarks.addpolicyRemarks("LA: Concurrent submissions.");
				isPolicyPMED = true;
			}

			if (isShield) {
				//Total of DOB month
				int totalMonth = totalOfDOBMonth(LA_birthDate);
				
				if (totalMonth < 7 || totalMonth >= 720) {
					Pol_remarks.addpolicyRemarks("Non med limit");
					isPolicyPMED = true;
				}
			}
					
			//Upload document:
			//supporting document
			if (this.applicationDoc.has("supportDocuments")) {
				JSONObject supportDocuments = this.applicationDoc.getJSONObject("supportDocuments");
				JSONObject supportDocuments_values = new JSONObject();
				
				if(supportDocuments.has("values")) {
					supportDocuments_values = supportDocuments.getJSONObject("values");
				
					//LA's optional doc
					JSONObject supportDocuments_values_insured = supportDocuments_values.getJSONObject("insured");
					JSONObject supportDocuments_values_insured_optDoc = supportDocuments_values_insured.getJSONObject("optDoc");
					
					
					JSONObject supportDocuments_values_insured_otherDoc = new JSONObject();
					JSONObject supportDocuments_values_insured_otherDoc_values = new JSONObject();
							
					if(supportDocuments_values_insured.has("otherDoc")) {
						supportDocuments_values_insured_otherDoc = supportDocuments_values_insured.getJSONObject("otherDoc"); 
						
						if(supportDocuments_values_insured_otherDoc.has("values")) {
							supportDocuments_values_insured_otherDoc_values = supportDocuments_values_insured_otherDoc.getJSONObject("values");
						} 
					}		
					
					//proposer's optional doc
					JSONObject supportDocuments_values_proposer = supportDocuments_values.getJSONObject("proposer");
					JSONObject supportDocuments_values_proposer_optDoc = supportDocuments_values_proposer.getJSONObject("optDoc");
						
					//For Shield
					if(is_1stparty && supportDocuments_values_proposer.has("otherDoc")) {
						supportDocuments_values_insured_otherDoc = supportDocuments_values_proposer.getJSONObject("otherDoc"); 
						
						if(supportDocuments_values_insured_otherDoc.has("values")) {
							supportDocuments_values_insured_otherDoc_values = supportDocuments_values_insured_otherDoc.getJSONObject("values");
						} 
					}	
	
					//Other document:
					JSONObject supportDocuments_values_policyForm = supportDocuments_values.getJSONObject("policyForm");
					
					JSONObject supportDocuments_values_policyForm_otherDoc = new JSONObject();
					JSONObject supportDocuments_values_policyForm_otherDoc_values = new JSONObject();
					if(supportDocuments_values_policyForm.has("otherDoc")) {
						supportDocuments_values_policyForm_otherDoc = supportDocuments_values_policyForm.getJSONObject("otherDoc"); 
						
						if(supportDocuments_values_policyForm_otherDoc.has("values")) {
							supportDocuments_values_policyForm_otherDoc_values = supportDocuments_values_policyForm_otherDoc.getJSONObject("values");
						} 
					}		
					
					JSONObject supportDocuments_values_policyForm_mandDocs = new JSONObject();
					if(supportDocuments_values_policyForm.has("mandDocs")) {
						supportDocuments_values_policyForm_mandDocs = supportDocuments_values_policyForm.getJSONObject("mandDocs"); 
						if(supportDocuments_values_policyForm_mandDocs.has("healthDeclaration")) {
							if (supportDocuments_values_policyForm_mandDocs.getJSONArray("healthDeclaration").length() > 0)
								isPolicyPMED = true;
						} 
					}	
					
					//LA's MedicalTest
					if(supportDocuments_values_insured_optDoc.has("iMedicalTest")) {
						JSONArray supportDocuments_values_insured_optDoc_iMedicalTest = supportDocuments_values_insured_optDoc.getJSONArray("iMedicalTest");
						if(supportDocuments_values_insured_optDoc_iMedicalTest.length()>0) {
							for(int i = 0;i < supportDocuments_values_insured_optDoc_iMedicalTest.length(); i++){
								JSONObject LA_medical_check_doc = supportDocuments_values_insured_optDoc_iMedicalTest.getJSONObject(i);
								if(LA_medical_check_doc.has("uploadDate")) {
									if(approveDate.equals(LA_medical_check_doc.getString("uploadDate").substring(0,10))){
										Pol_remarks.addpolicyRemarks("Other docs uploaded");
										isPolicyPMED = true;
										break;
									}
								}
							}
						}
					}
					
					if(is_3rdparty) {
						if(supportDocuments_values_proposer_optDoc.has("pMedicalTest")) {
							JSONArray supportDocuments_values_proposer_optDoc_pMedicalTest = supportDocuments_values_proposer_optDoc.getJSONArray("pMedicalTest");
							if(supportDocuments_values_proposer_optDoc_pMedicalTest.length()>0) {
								for(int i = 0;i < supportDocuments_values_proposer_optDoc_pMedicalTest.length(); i++){
									JSONObject OW_medical_check_doc = supportDocuments_values_proposer_optDoc_pMedicalTest.getJSONObject(i);
									if(OW_medical_check_doc.has("uploadDate")) {
										if(approveDate.equals(OW_medical_check_doc.getString("uploadDate").substring(0,10))){
											Pol_remarks.addpolicyRemarks("Other docs uploaded");
											isPolicyPMED = true;
											break;
										}
									}
								}
								
							}
						}
					}
					
					if(supportDocuments_values_insured_optDoc.has("iJuvenileQuestions")) {
						JSONArray supportDocuments_values_insured_optDoc_iJuvenileQuestions = supportDocuments_values_insured_optDoc.getJSONArray("iJuvenileQuestions");
						if(supportDocuments_values_insured_optDoc_iJuvenileQuestions.length()>0) {
							for(int i = 0;i < supportDocuments_values_insured_optDoc_iJuvenileQuestions.length(); i++){
								JSONObject LA_medical_check_doc = supportDocuments_values_insured_optDoc_iJuvenileQuestions.getJSONObject(i);
								if(LA_medical_check_doc.has("uploadDate")) {
									if(approveDate.equals(LA_medical_check_doc.getString("uploadDate").substring(0,10))){
										Pol_remarks.addpolicyRemarks("Other docs uploaded");
										isPolicyPMED = true;
										break;
									}
								}
							}
						}
					}
					
					boolean isdocfound = false;
					//if (!isShield) {
						if(supportDocuments_values_policyForm_otherDoc.has("template")) {
							JSONArray supportDocuments_values_policyForm_otherDoc_template = supportDocuments_values_policyForm_otherDoc.getJSONArray("template");
							if(supportDocuments_values_policyForm_otherDoc_template.length()>0) {
								for(int i = 0;i < supportDocuments_values_policyForm_otherDoc_template.length(); i++){
									String otherDoc_id = (supportDocuments_values_policyForm_otherDoc_template.getJSONObject(i)).getString("id");
									JSONArray otherDoc_array = supportDocuments_values_policyForm_otherDoc.getJSONObject("values").getJSONArray(otherDoc_id);
									for(int j = 0;j < otherDoc_array.length(); j++){
										JSONObject otherDoc_array_record = otherDoc_array.getJSONObject(j);
										if(otherDoc_array_record.has("uploadDate")){
											if(approveDate.equals(otherDoc_array_record.getString("uploadDate").substring(0,10))){
												Pol_remarks.addpolicyRemarks("Other docs uploaded");
												isPolicyPMED = true;
												isdocfound = true;
												break;
											}
										}
									}
									if(isdocfound) {
										break;
									}
								}
							}
						}
					//}
					
					if (1 == 1) {
						isdocfound = false;
						if(supportDocuments_values_insured_otherDoc.has("template")) {
							JSONArray supportDocuments_values_insured_otherDoc_template = supportDocuments_values_insured_otherDoc.getJSONArray("template");
							if(supportDocuments_values_insured_otherDoc_template.length()>0) {
								for(int i = 0;i < supportDocuments_values_insured_otherDoc_template.length(); i++){
									String otherDoc_id = (supportDocuments_values_insured_otherDoc_template.getJSONObject(i)).getString("id");
									JSONArray otherDoc_array = supportDocuments_values_insured_otherDoc.getJSONObject("values").getJSONArray(otherDoc_id);
									for(int j = 0;j < otherDoc_array.length(); j++){
										JSONObject otherDoc_array_record = otherDoc_array.getJSONObject(j);
										if(otherDoc_array_record.has("uploadDate")){
											if(approveDate.equals(otherDoc_array_record.getString("uploadDate").substring(0,10))){
												Pol_remarks.addpolicyRemarks("Other docs uploaded");
												isPolicyPMED = true;
												isdocfound = true;
												break;
											}
										}
									}
									if(isdocfound) {
										break;
									}
								}
							}
						}
						
						//TODO
//						if(supportDocuments_values_policyForm_otherDoc.has("template") && !isdocfound) {
//							JSONArray supportDocuments_values_policyForm_otherDoc_template = supportDocuments_values_policyForm_otherDoc.getJSONArray("template");
//							if(supportDocuments_values_policyForm_otherDoc_template.length()>0) {
//								for(int i = 0;i < supportDocuments_values_policyForm_otherDoc_template.length(); i++){
//									String otherDoc_id = (supportDocuments_values_policyForm_otherDoc_template.getJSONObject(i)).getString("id");
//									JSONArray otherDoc_array = supportDocuments_values_policyForm_otherDoc.getJSONObject("values").getJSONArray(otherDoc_id);
//									for(int j = 0;j < otherDoc_array.length(); j++){
//										JSONObject otherDoc_array_record = otherDoc_array.getJSONObject(j);
//										if(otherDoc_array_record.has("uploadDate")){
//											if(approveDate.equals(otherDoc_array_record.getString("uploadDate").substring(0,10))){
//												Pol_remarks.addpolicyRemarks("Other docs uploaded");
//												isPolicyPMED = true;
//												isdocfound = true;
//												break;
//											}
//										}
//									}
//									if(isdocfound) {
//										break;
//									}
//								}
//							}
//						}
					}
				}
			}
			
			//Rule 14	Replacement of policies
			if(quotation !=null && quotation.has("clientChoice") && !isShield) {
				JSONObject clientChoice = quotation.getJSONObject("clientChoice");
				
				if(clientChoice !=null &&  clientChoice.has("recommendation")) {
					JSONObject recommendation = clientChoice.getJSONObject("recommendation");
					
					if(recommendation !=null &&  recommendation.has("rop")) {
						JSONObject rop = recommendation.getJSONObject("rop");
						
						if(rop !=null &&  rop.has("choiceQ1")) {
							String choiceQ1 = rop.getString("choiceQ1");
							
							if(choiceQ1!=null && choiceQ1.equals("Y")) {
								isPolicyPMED = true;

								Pol_remarks.addpolicyRemarks("Replacement of policy");
							}
						}
					}
				}
			}
			
			//Rule 2 Source of Payment/Funds 
			if (Owner_declaration.has("FUND_SRC01") && !isShield) {
				boolean canAutoUnderWriting = true;
				
				if (Owner_declaration.getString("FUND_SRC01").equals("sg")) { //Singapore Funds
					double premium = quotation.has("premium")?quotation.getDouble("premium"):0; //Premium (Both Single and Regular)
					
					boolean isOtherPayor = false;
					boolean isOtherRelationshipPayor = false;
					
					if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals("other")) {
						isOtherPayor = true;	
					}
					if (Owner_declaration.has("FUND_SRC09") && Owner_declaration.getString("FUND_SRC09").equals("OTH")) {
						isOtherRelationshipPayor = true;	
					}
					
					if(premium < 20000) {
						if(isOtherPayor && isOtherRelationshipPayor) {
							canAutoUnderWriting = false;
							Pol_remarks.addpolicyRemarks("Check payor details");
						}
					}else {
						boolean isOtherSourceOfFunds = false;
						boolean isOtherSourceOfWealth = false;
						
						if (Owner_declaration.has("FUND_SRC30") && Owner_declaration.has("FUND_SRC31")) {
							isOtherSourceOfFunds = Owner_declaration.getString("FUND_SRC30").equals("Y") && Owner_declaration.has("FUND_SRC31") && !Owner_declaration.getString("FUND_SRC31").isEmpty()?true:false;	
						}
						
						if (Owner_declaration.has("FUND_SRC38") && Owner_declaration.has("FUND_SRC39")) {
							isOtherSourceOfWealth = Owner_declaration.getString("FUND_SRC38").equals("Y") && Owner_declaration.has("FUND_SRC39") && !Owner_declaration.getString("FUND_SRC39").isEmpty()?true:false;	
						}
											
						if((isOtherPayor && isOtherRelationshipPayor) || isOtherSourceOfFunds || isOtherSourceOfWealth) {
							canAutoUnderWriting = false;
							Pol_remarks.addpolicyRemarks("Check source of funds details");
						}
					}
					
				}else { // Foreigner Funds
					canAutoUnderWriting = false;
					Pol_remarks.addpolicyRemarks("Check source of funds details");
				}
				
				if(!canAutoUnderWriting) {
					isPolicyPMED = true;
				}
			}
			
			//Rule 10  Pass expiry is within 3 months from date of Application
			if(Owner_personalInfo !=null && Owner_personalInfo.has("passExpDate") && is_3rdparty && !isShield) {
				String applicationSignedDateString = applicationDoc.getString("applicationSignedDate");
				
				ZonedDateTime applicationSignedDateTime = ZonedDateTime.parse(applicationSignedDateString, Function.DATE_FORMAT_ISO8601);
				
				long applicationSignedTimeStamp = Date.from(applicationSignedDateTime.toInstant()).getTime();
				
				long passTimeStampOwner = Owner_personalInfo.getLong("passExpDate");//Proposer
				boolean canAutoUnderWritingOwner = this.validatePassExpiryDate(applicationSignedTimeStamp,passTimeStampOwner);
				
				if (!canAutoUnderWritingOwner) {
					isPolicyPMED = true;
					Pol_remarks.addpolicyRemarks("PH: Pass Expiry Date is within 3 months");
				}
			}
			
			if (LifeAssured_personalInfo!= null && LifeAssured_personalInfo.has("passExpDate") && !isShield) {
				String applicationSignedDateString = applicationDoc.getString("applicationSignedDate");
				
				ZonedDateTime applicationSignedDateTime = ZonedDateTime.parse(applicationSignedDateString, Function.DATE_FORMAT_ISO8601);
				
				long applicationSignedTimeStamp = Date.from(applicationSignedDateTime.toInstant()).getTime();
								
				long passTimeStampLifeAssured = LifeAssured_personalInfo.getLong("passExpDate");//Life Assured
				boolean canAutoUnderWritingLifeAssured = this.validatePassExpiryDate(applicationSignedTimeStamp,passTimeStampLifeAssured);
								
				if (!canAutoUnderWritingLifeAssured) {
					isPolicyPMED = true;
					Pol_remarks.addpolicyRemarks("LA: Pass Expiry Date is within 3 months");
				}
			}
				
			
			//Rule 11 Underwriting Sum Assured amount exceeded Non-Medical Limit
			// USAR = [Sum Assured of current application] + 
			// (Total Sum Assured of All Existing Policies) + 
			// (Total Sum Assured of All Pending Applications) - 
			// (Total Sum Assured of All Policies to be Replaced)  
			if(quotation !=null && LifeAssured_policies != null && LifeAssured_personalInfo != null && !isShield) {
				double CIP_sumInsured = 0;//Critical Illness Plus rider
				
				if (quotation_plans.length() > 1) {	
					for(int i = 1; i < quotation_plans.length(); i++)
					{
						if (quotation_plans.getJSONObject(i).has("covCode")) {
							if (quotation_plans.getJSONObject(i).getString("covCode").equals("CIP"))
								if (quotation_plans.getJSONObject(i).has("sumInsured"))
									CIP_sumInsured = quotation_plans.getJSONObject(i).getDouble("sumInsured");
						}		
					}
				}
				
				double usar = this.calculateTotalSumAssured(quotation_BasicPlan, LifeAssured_policies, CIP_sumInsured);
				
				boolean isSingaporeResident = this.validateSingaporeResident(Owner_declaration,LA_countryOfResidenceCD);
				boolean canAutoUnderWriting = false;
				
				int nearAge = LifeAssured_personalInfo.getInt("nearAge");
				
				boolean isLifeExentialRelease3 = false; // release 3 product
				boolean isHealthProGrowthRelease3 = false; // release 3 product
				
				//USAR validation (involving NML Table) is only applicable for non-GIO products: Term Protector (Prime), Inspire Flexi Series, Life MultiProtect, EarlyStage Criticare and HealthPro Growth
				
				
				if(isLifeExentialRelease3 || isHealthProGrowthRelease3) {
					//TODO release 3 product only
					
					if(usar <= 100000
							|| (usar <=150000 && nearAge<=65)
							|| (usar <=200000 && nearAge<=60)
							|| (usar <=350000 && nearAge<=55)
							|| (usar <=500000 && nearAge<=50)
							|| (usar <=650000 && nearAge>=18 && nearAge<=50)
							) {
						canAutoUnderWriting = true;
					}
					
				}else {
					
					if (appForm_BasicPlan.has("covCode")) {
						String covCode = appForm_BasicPlan.getString("covCode");
						
						if(covCode !=null && (covCode.equals("TPX") || covCode.equals("TPPX") || covCode.equals("FPX") || covCode.equals("FSX"))) {// Term Protector (Prime), Inspire Flexi Series
							if(isSingaporeResident) {
								if(usar <= 100000
										|| (usar <=150000 && nearAge<=65)
										|| (usar <=200000 && nearAge<=60)
										|| (usar <=350000 && nearAge<=55)
										|| (usar <=500000 && nearAge<=50)
										|| (usar <=650000 && nearAge>=18 && nearAge<=50)
										|| (usar <=1000000 && nearAge>=18 && nearAge<=50)
										|| (usar <=1500000 && nearAge>=18 && nearAge<=50)
										) {
									canAutoUnderWriting = true;
								}
								
							}else {
								
								if(usar <= 100000
										|| (usar <=150000 && nearAge<=65)
										|| (usar <=200000 && nearAge<=60)
										|| (usar <=350000 && nearAge<=55)
										|| (usar <=500000 && nearAge<=50)
										|| (usar <=650000 && nearAge>=18 && nearAge<=50)
										|| (usar <=1000000 && nearAge>=18 && nearAge<=40)
										) {
									canAutoUnderWriting = true;
								}
								
							}
						}else {
							canAutoUnderWriting = true;
						}
					}else {
						canAutoUnderWriting = true;
					}
				}
				
				if(!canAutoUnderWriting) {
					isPolicyPMED = true;
					Pol_remarks.addpolicyRemarks("Exceeded NML");
				}
			}
			
			//Rule 13 TSAR, Total Sum Assured amount exceeded max cover limit
			String canAutoUnderWritingTSAR =  this.validateTSAR(LifeAssured_personalInfo, quotation, LifeAssured_declaration, LA_countryOfResidenceCD, app_plans, LifeAssured_policies);
			
			if (!canAutoUnderWritingTSAR.equals("")) {
				isPolicyPMED = true;
				Pol_remarks.addpolicyRemarks("Exceeded Maximum Cover for ["+canAutoUnderWritingTSAR+"]");
			}
			
			//*** ----- CR044 FailAutoUnderWriting RULES----- ***//
			boolean isThirdPartyCase = this.isThirdPartyCase(Owner);

			boolean canAutoUnderWriting = this.validateFailAutoUnderWritingRulesCR044(isThirdPartyCase,Owner, quotation, Owner_policies, Owner_personalInfo, Owner_declaration, LA_countryOfResidenceCD,Pol_remarks,LifeAssured_policies);
			
			if(!canAutoUnderWriting && !isShield) {
				isPolicyPMED = true;
			}
			//*** ----- CR044 FailAutoUnderWriting RULES ----- ***//
			
			// !!!! ---------------------- Division line -------------------------- !!!!//
			// !!!! All checking for "isPolicyPMED" should be done before this line !!!!//
			// !!!! ---------------------- Division line -------------------------- !!!!//
			
			//*** ----- CR044 set Insured's Height and Weight ----- ***//
			
			if(quotation != null) {
				if(quotation.has("baseProductCode")) {
					String baseProductCode = quotation.getString("baseProductCode");
					
					if(baseProductCode.contains("SAV") && isThirdPartyCase) { //Savvy Saver and is Third Party Case
						// covCode Mapping
						// PPEPS = Smart Payer PremiumEraser Plus
						// PPERA = Smart Payer PremiumEraser
						
						if(hasPayorBenefitRider) {
							HasPersonalDetailsInItemType.height = "1.5";
							HasPersonalDetailsInItemType.weight = "40";
						}
					}
				}
			}
			
			//*** ----- CR044 set Insured's Height and Weight ----- ***//
			
			HasIndividualUWInfoInItemTypeList.add(HasIndividualUWInfoInItemType);
			
			HasPersonalDetailsInItemType.hasIndividualUWInfoIn = HasIndividualUWInfoInItemTypeList;
			
			HasPersonalDetailsInItemTypeList.add(HasPersonalDetailsInItemType);
			
			HasDetailsOfRisksInItemType.hasPersonalDetailsIn = HasPersonalDetailsInItemTypeList;
			HasDetailsOfRisksInItemTypeList.add(HasDetailsOfRisksInItemType);
			obj.recordApplicationRequest.policy.hasDetailsOfRisksIn = HasDetailsOfRisksInItemTypeList;
		
			//policy medical flag:
			if (isPolicyPMED) {
				obj.recordApplicationRequest.policy.medicalIND = "Y";
			}
			
			//when "followUpIndicator": "Y", make medicalIND = Y
			if (HasCRSInformationInType.followUpIndicator.equals("Y")) {
				obj.recordApplicationRequest.policy.medicalIND = "Y";
				Pol_remarks.addpolicyRemarks("Check FATCA/CRS declaration");
			}
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasBenificialOwnerDetailsIn - S
			
			
			String la_tmp_block = "";
			String la_tmp_unit = "";
			String la_addressline1 = "";	
			String la_addressline2 = "";
			String la_addressline3 = "";	
			String la_addressline4 = "";
			
			if (LifeAssured_personalInfo.has("addrBlock")) {
				la_tmp_block = truncateSpecialChar(LifeAssured_personalInfo.getString("addrBlock"));
			}
			
			if (LifeAssured_personalInfo.has("unitNum")) {
				la_tmp_unit = truncateSpecialChar(LifeAssured_personalInfo.getString("unitNum"));
			}
			
			if ("".equals(la_tmp_block) &&"".equals(la_tmp_unit)) {
				la_addressline1 = "";
			} else if ("".equals(la_tmp_block)){
				la_addressline1 = la_tmp_unit;
			} else if ("".equals(la_tmp_unit)) {
				la_addressline1 = la_tmp_block;
			} else {
				la_addressline1 = la_tmp_block + " " + la_tmp_unit;
			}
			
			//Address 2
			la_addressline2 = "";
			
			if (LifeAssured_personalInfo.has("addrStreet")) {
				la_addressline2 = truncateSpecialChar(LifeAssured_personalInfo.getString("addrStreet"));
			}
			
			//Address 3
			String la_tmp_building = "";
			String la_tmp_city = "";
			
			if (LifeAssured_personalInfo.has("addrEstate")) {
				la_tmp_building = truncateSpecialChar(LifeAssured_personalInfo.getString("addrEstate"));
			}
			
			if (LifeAssured_personalInfo.has("addrCity")) {
				la_tmp_city = truncateSpecialChar(LifeAssured_personalInfo.getString("addrCity").toUpperCase());
			}
			
			if ("".equals(la_tmp_building)&&"".equals(la_tmp_city)) {
				la_addressline3 = "";
			} else if ("".equals(la_tmp_building)){
				la_addressline3 = CityConversion(la_tmp_city);
			} else if ("".equals(la_tmp_city)) {
				la_addressline3 = la_tmp_building;
			} else {
				la_addressline3 = la_tmp_building + " " + CityConversion(la_tmp_city);
			}
			
			//Address 4
			String la_tmp_country = "";
			la_addressline4 = "";
			if (LifeAssured_personalInfo.has("addrCountry")) {
				la_tmp_country = CountryConversion(truncateSpecialChar(LifeAssured_personalInfo.getString("addrCountry").toUpperCase()));
				if (la_tmp_country.toLowerCase().contains("China".toLowerCase())) {
					la_addressline4 = "China" + " " + truncateSpecialChar(LifeAssured_personalInfo.getString("postalCode").toUpperCase());
				} else {
					la_addressline4 = la_tmp_country + " " + truncateSpecialChar(LifeAssured_personalInfo.getString("postalCode").toUpperCase());
				}
			}
			
			//Address 5
			//HasAddressesInItemType.cityNM = "";
//			if (LifeAssured_personalInfo.has("addrCountry")) {
//				HasAddressesInItemType.cityNM = getCountryCD(la_tmp_country); 
//			}
			

			//BD19-008
			//LINE NO.
			String[] tmp_remarks =  Pol_remarks.getremarklist();
			for (int rm_count = 0; rm_count < tmp_remarks.length; rm_count++) {
				if(tmp_remarks[rm_count] == null || "".equals(tmp_remarks[rm_count])) {
					break;
				}
				HasAssessmentDetailsInItemType.lineNO = Integer.toString(rm_count+1);
				HasAssessmentDetailsInItemType.assessmentDetails = tmp_remarks[rm_count];
				HasAssessmentDetailsInItemTypeList.add(HasAssessmentDetailsInItemType);
				HasAssessmentDetailsInItemType = obj.getHasAssessmentDetailsInItemType();
			}
			
			
			if(is_1stparty||is_3rdparty) {
				//CanBeIndividualType = obj.getCanBeIndividualItemType();		
				CanBeIndividualItemTypeList = new ArrayList<CanBeIndividualItemType>();
				HasAddressesInItemTypeList = new ArrayList<HasAddressesInItemType>();
				CanBeIndividualType = obj.getCanBeIndividualType();
				
 
				HasBenificialOwnerDetailsInItemType.relationshipToInsuredCD = "LA";
				HasBenificialOwnerDetailsInItemType.ownershipFLG = "C";
				CanBeIndividualItemType = obj.getCanBeIndividualItemType();		
				//CanBeIndividualItemType.ownershipFLG = "C";
				CanBeIndividualItemType.firstNM = "";
				CanBeIndividualItemType.lastNM = spaceRemover(LA_fullNM);
				CanBeIndividualItemType.chineseNM = spaceRemover(LA_ChineseNM);
				CanBeIndividualItemType.birthDT = LA_birthDate;
				CanBeIndividualItemType.genderCD = LA_Gender;
				CanBeIndividualItemType.passportNo = truncateSpecialChar(LifeAssured_personalInfo.getString("idCardNo"));//NRIC/Passport No
				CanBeIndividualItemType.countryOfResidenceCD = LA_nationality;
				
				if (isShield) {
					if (OW_nationality.equals("SINGAPOREAN"))		
						CanBeIndividualItemType.countryOfResidenceCD  = "LR";
					else
						CanBeIndividualItemType.countryOfResidenceCD  = "PR";
				}
				
				CanBeIndividualItemType.nationality = NationalityConversion(LifeAssured_personalInfo.getString("nationality"), true);//Nationality
				
				//CanBeIndividualItemType ->  hasAddressesIn
					HasAddressesInItemType = obj.getHasAddressesInItemType();
					HasAddressesInItemType.addressLine1 = spaceRemover(LA_firstNM);//Given Name
					HasAddressesInItemType.addressLine2 = spaceRemover(LA_lastNM);//Surname
					HasAddressesInItemType.addressLine3 = spaceRemover(LA_othName);//English Name
					HasAddressesInItemType.addressLine4 = spaceRemover(LA_ChineseNM);//Han Yu Pin Yin Name
				
				CanBeIndividualItemType.hasAddressesIn = HasAddressesInItemTypeList;
				HasAddressesInItemTypeList.add(HasAddressesInItemType);
				CanBeIndividualItemTypeList.add(CanBeIndividualItemType);
				HasBenificialOwnerDetailsInItemType.canBeIndividual = CanBeIndividualItemTypeList;
				HasBenificialOwnerDetailsInItemTypeList.add(HasBenificialOwnerDetailsInItemType); 	
			}
			
			if(is_3rdparty) {
				//Initial CanBeIndividual array
				HasBenificialOwnerDetailsInItemType = obj.getHasBenificialOwnerDetailsInItemType();
				CanBeIndividualItemTypeList = new ArrayList<CanBeIndividualItemType>();
				HasAddressesInItemTypeList = new ArrayList<HasAddressesInItemType>();
				HasBenificialOwnerDetailsInItemType.relationshipToInsuredCD = "PO";
				HasBenificialOwnerDetailsInItemType.ownershipFLG = "C";
				//Initial 
				CanBeIndividualItemType = obj.getCanBeIndividualItemType();
				//CanBeIndividualItemType.ownershipFLG = "C";
				CanBeIndividualItemType.firstNM = "";
				CanBeIndividualItemType.lastNM = spaceRemover(OW_fullNM);
				CanBeIndividualItemType.chineseNM = spaceRemover(OW_chineseNM);
				CanBeIndividualItemType.birthDT = OW_birthDate;
				CanBeIndividualItemType.genderCD = OW_Gender;
				CanBeIndividualItemType.passportNo = truncateSpecialChar(Owner_personalInfo.getString("idCardNo"));
				CanBeIndividualItemType.countryOfResidenceCD = OW_nationality;
				
				if (isShield) {
					if (OW_nationality.equals("SINGAPOREAN"))		
						CanBeIndividualItemType.countryOfResidenceCD  = "LR";
					else
						CanBeIndividualItemType.countryOfResidenceCD  = "PR";
				}
								
				CanBeIndividualItemType.nationality = NationalityConversion(Owner_personalInfo.getString("nationality"), true);
				
				//CanBeIndividualItemType ->  hasAddressesIn
					HasAddressesInItemType = obj.getHasAddressesInItemType();
					HasAddressesInItemType.addressLine1 = spaceRemover(OW_firstNM);//Given Name
					HasAddressesInItemType.addressLine2 = spaceRemover(OW_lastNM);//Surname
					HasAddressesInItemType.addressLine3 = spaceRemover(OW_othName);//English Name
					HasAddressesInItemType.addressLine4 = spaceRemover(OW_chineseNM);//Han Yu Pin Yin Name
				
				CanBeIndividualItemType.hasAddressesIn = HasAddressesInItemTypeList;
				
				HasAddressesInItemTypeList.add(HasAddressesInItemType);
				CanBeIndividualItemTypeList.add(CanBeIndividualItemType);
				HasBenificialOwnerDetailsInItemType.canBeIndividual = CanBeIndividualItemTypeList;
				HasBenificialOwnerDetailsInItemTypeList.add(HasBenificialOwnerDetailsInItemType); 	
			}
			//not check in the has beneficial owner block:
			obj.recordApplicationRequest.policy.hasBenificialOwnerDetailsIn = HasBenificialOwnerDetailsInItemTypeList;
			
			//RecordApplicationRequest > policy > hasBenificialOwnerDetailsIn - E	
			
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasBenificialOwnerDetailsIn - S
					
			if (isShield) {
				if (!applicationDoc.getString("policyNumber").equals(ShieldBasicPolicyNo)) {
					obj.recordApplicationRequest.policy.policyNo_2 = ShieldBasicPolicyNo;
					HasPolicyAssociationDetailsInItemType.linkedPlanCode = ShieldBasicCode;
					HasPolicyAssociationDetailsInItemType.assocPolicyStatusCD = "MS";
					
					HasPolicyAssociationDetailsInItemTypeList.add(HasPolicyAssociationDetailsInItemType);
					obj.recordApplicationRequest.policy.hasPolicyAssociationDetailsIn = HasPolicyAssociationDetailsInItemTypeList;
				} else {
					//obj.recordApplicationRequest.policy.policyNo_2 = "";
					//HasPolicyAssociationDetailsInItemType.linkedPlanCode = "";
				}
									
				obj.recordApplicationRequest.policy.declineOptionIND = "N";
				
				if (isAPPROVE)
					obj.recordApplicationRequest.policy.projectionControlRequiredIND = "Y";
				else 
					obj.recordApplicationRequest.policy.projectionControlRequiredIND = "N";
			}
						
			//RecordApplicationRequest > policy > hasBenificialOwnerDetailsIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//RecordApplicationRequest > policy > hasAXAAsiaPolicyHeaderPaymentAccount - S
			
			if (obj.recordApplicationRequest.policy.paymentModeCD.equals("M")) {
				HasAXAAsiaPolicyHeaderPaymentAccountItemType.transCD = "DA";
				HasAXAAsiaPolicyHeaderPaymentAccountItemType.transDetailCD = "NFC007";
				HasAXAAsiaPolicyHeaderPaymentAccountItemType.detailTransactionDESC = "BANK ACCOUNT CHANGE FOR AUTO PAY OF PREMIUM";
				
				HasAXAAsiaPolicyHeaderPaymentAccountItemTypeList.add(HasAXAAsiaPolicyHeaderPaymentAccountItemType);
				
				obj.recordApplicationRequest.policy.hasAXAAsiaPolicyHeaderPaymentAccount = HasAXAAsiaPolicyHeaderPaymentAccountItemTypeList;
			}
			
			//RecordApplicationRequest > policy > hasAXAAsiaPolicyHeaderPaymentAccount - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} catch (Exception ex) {			
			throw ex;
		}

		return obj; 
	}
	
	private String getPaymentModeCD (String paymode) {
		if (paymode.equals("L"))
			return "A";
		else
			return paymode;
	}
	
	private String CityConversion (String city_code) { 
		//Get city json file - S
		JSONObject cityList = null;
		cityList = Constant.CBUTIL.getDoc("city");
			
		if (cityList == null) {
			Log.error("Error - city file missing ");
		}
		
		JSONArray city_list = cityList.getJSONArray("options");
		JSONObject city_record= null;
		JSONObject city_title = null;
		String city_option = null;
		String city_countryNM = null;

		if ((city_list.length() > 0) && (!city_code.isEmpty())) {
			for(int i = 0; i < city_list.length(); i++) {
				city_record = city_list.getJSONObject(i);
				city_option = city_record.getString("value");
			    
				if (city_option.equals(city_code)) {
					city_title = city_record.getJSONObject("title");
			    	city_countryNM	= city_title.getString("en");
			    	break;
			    }
			}	
		}
		
//		if (city_countryNM == null) {
//			if (city_code == "" || city_code == null) 
//				return "";
//			else
//				return city_code;
//		} else {
//			return city_countryNM;
//		}
		
		return "";//Requested by AXA to pass blank to city on release 1, refer mantis issue 0031811
	}
	
	private String CountryConversion (String Country_code) { 
		//Get residency json file - S
		JSONObject residencyList = null;
		residencyList = Constant.CBUTIL.getDoc("residency");
			
		if (residencyList == null) {
			Log.error("Error - residency file missing ");
		}
		
		JSONArray residency_list = residencyList.getJSONArray("options");
		JSONObject residency_record= null;
		JSONObject residency_title = null;
		String residency_option = null;
		String residency_countryNM = null;

		if ((residency_list.length() > 0) && (!Country_code.isEmpty())) {
			for(int i = 0; i < residency_list.length(); i++) {
				residency_record = residency_list.getJSONObject(i);
				residency_option = residency_record.getString("value");
			    
				if (residency_option.equals(Country_code)) {
			    	residency_title = residency_record.getJSONObject("title");
			    	residency_countryNM	= residency_title.getString("en");
			    	break;
			    }
			}	
		}
		
		if (residency_countryNM == null) {
			return "";
		} else {
			return residency_countryNM;
		}
	}

	private String NationalityConversion (String Nationality_code, boolean isConnectedParty) { 
		//Get nationality json file - S
		JSONObject nationalityList = null;
		nationalityList = Constant.CBUTIL.getDoc("nationality");
			
		if (nationalityList == null) {
			Log.error("Error - nationality file missing ");
		}
		
		JSONArray nationality_list = nationalityList.getJSONArray("options");
		JSONObject nationality_record= null;
		JSONObject nationality_title = null;
		String nationality_option = null;
		String nationality_countryNM = null;
		String nationality_countryNM_RLS = null;

		if ((nationality_list.length() > 0) && (!Nationality_code.isEmpty()) ){
			for(int i = 0; i < nationality_list.length(); i++) {
				nationality_record = nationality_list.getJSONObject(i);
				nationality_option = nationality_record.getString("value");
			    
				if (nationality_option.equals(Nationality_code)) {
			    	nationality_title = nationality_record.getJSONObject("title");
			    	nationality_countryNM = nationality_title.getString("en");
			    	nationality_countryNM_RLS = nationality_record.getString("rlsCode");
			    	break;
			    }
			}	
		}
				
		if (isConnectedParty) {
			if (nationality_countryNM_RLS == null) {
				return "";
			} else {
				return nationality_countryNM_RLS.toUpperCase();
			}
		} else {
			if (nationality_countryNM == null) {
				return "";
			} else {
				return nationality_countryNM.toUpperCase();
			}
		}
	}
	
	private String getCountryCD (String country_NM) { 
		JSONObject countryCDlist = null;
		countryCDlist = Constant.CBUTIL.getDoc("standard_country_code");
			
		if (countryCDlist == null) {
			Log.error("Error - country code file missing ");
		}
		
		JSONArray countryCD_list = countryCDlist.getJSONArray("options");
		JSONObject countryCD_record= null;
		JSONObject country_name = null;
		String countryCD_NM = null;
		String countryCD = null;

		if ((countryCD_list.length() > 0) && (!country_NM.isEmpty())) {
			for(int k = 0; k < countryCD_list.length(); k++) {
				countryCD_record = countryCD_list.getJSONObject(k);
				country_name = countryCD_record.getJSONObject("title");
				countryCD_NM	= country_name.getString("en");
			    
				if (countryCD_NM.toUpperCase().equals(country_NM.toUpperCase())) {
			    	countryCD = countryCD_record.getString("value");
			    	break;
			    }
			}	
		}
		
		if (countryCD == null) {
			return "";
		} else {
			return  countryCD;
		}
	}
	
	private String getBankCD (String BankName) { 
		JSONObject bankCDlist = null;
		bankCDlist = Constant.CBUTIL.getDoc("branches");
			
		if (bankCDlist == null) {
			Log.error("Error - bank code file missing ");
		}
		
		String bank_NM = null;
		String bank_CD = null;
		JSONObject bank_CD_record = null;
		JSONArray bankCDlistoptions = bankCDlist.getJSONArray("options");

		if ((bankCDlist.length() > 0) && (!BankName.isEmpty())) {
			for(int h = 0; h < bankCDlistoptions.length(); h++) {
				bank_CD_record = bankCDlistoptions.getJSONObject(h);
				bank_NM = bank_CD_record.getString("value");
				//bank_CD	= bank_CD_record.getString("rlscode");
			    
				if (bank_NM.equals(BankName)) {
			    	bank_CD = bank_CD_record.getString("rlscode");
			    	break;
			    }
			}	
		}
		
		if (bank_CD == null) {
			return "";
		} else {
			return bank_CD;
		}
	}
	
	private String getOccupationClass(String occupationCode,Integer parm_age,String parm_gender) {		
		JSONObject occupationCDlist = null;
		occupationCDlist = Constant.CBUTIL.getDoc("occupation");
			
		if (occupationCDlist == null) {
			Log.error("Error - occupation mapping file missing ");
		}
		
		String occupation_CD = null;
		String occupation_Class = null;
		JSONObject occupation_list_record = null;
		JSONArray occupationListoptions = occupationCDlist.getJSONArray("options");

		if ((occupationListoptions.length() > 0) && (!occupationCode.isEmpty())) {
			for(int h = 0; h < occupationListoptions.length(); h++) {
				occupation_list_record = occupationListoptions.getJSONObject(h);
				occupation_CD = occupation_list_record.getString("value");

				if (occupation_CD.equals(occupationCode)) {
					occupation_Class = Integer.toString(occupation_list_record.getJSONObject("class").getInt("all"));
			    	break;
			    }
			}	
		}
		
		//cr per mail: Wed 10/25/2017 5:07 PM
		if("O674".equals(occupationCode)&&"F".equals(parm_gender)) {
			occupation_Class = "5";
		}
		if("O675".equals(occupationCode)&&"M".equals(parm_gender)) {
			occupation_Class = "5";
		}
		if("O1321".equals(occupationCode)&&(parm_age < 18)) {
			occupation_Class = "5";
		}
		if("O1322".equals(occupationCode)&&(parm_age >= 18)) {
			occupation_Class = "5";
		}
		
		if (occupation_Class == null) {
			return "";
		} else {
			return occupation_Class;
		}
	}
			
	private String changeDateFormat(Date _date) {
		SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
	    String strDate = sdfDate.format(_date);
	    
		return strDate;
	}
	
	private String age_cross_date(Date _date) {
		SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
	    String strDate = sdfDate.format(_date);
	    
		return strDate;
	}
	
	private String add_leading_zero(String missing_zero_string) {
		missing_zero_string = "0000000000000000".concat(missing_zero_string);
		return missing_zero_string;
	}
	
	/*private String[] policyRemarks(String policyRemark) {
		String[] remarklist = new String[99];	
		
		for (int i = 0; i < remarklist.length; i++) {
			if (remarklist[i].equals(policyRemark)) {
				break;
			} 
			if ("".equals(remarklist[i])) {
				remarklist[i] =  policyRemark;
				break;
			}
		}
		return remarklist;
	}*/	

	private String getCurrentDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;		
	}
	
	private Integer getDaysPassed(String dateFrom, String dateTo) {
        Calendar calDateFrom = null;  // From Date
        Calendar calDateTo = null;      // To date
        Calendar lastBirthDT = null; 	// 
        Calendar nextBirthDT = null; // 
        
        String[] commDateFrom = null;
        String[] commDateTo = null;
        Integer daysPS = 0;

        try {
               if (dateFrom != null && dateTo != null) {
              	 commDateFrom = dateFrom.split("-");
              	 commDateTo = dateTo.split("-");
                     
                     // Assume Date String must be YYYY-MM-DD
                     calDateFrom = new GregorianCalendar(Integer.parseInt(commDateFrom[0]), Integer.parseInt(commDateFrom[1]), Integer.parseInt(commDateFrom[2]));
                     calDateTo = new GregorianCalendar(Integer.parseInt(commDateTo[0]), Integer.parseInt(commDateTo[1]), Integer.parseInt(commDateTo[2]));
                     
                     if(calDateFrom.compareTo(calDateTo) >= 0) {
                    	 return daysPS;
                     }else {
                         for(int i = 0;;i++) {
                             if (calDateFrom.before(calDateTo)) {
                            	 calDateFrom.add(Calendar.DAY_OF_MONTH, 1);  
                          	   daysPS ++;
                             }
                             else {
                          	   break; 
                             }
                         }
                     return daysPS;
                     }
               }
        } catch (Exception e) {
	  		Log.error(e);
        }
        return null;
    }
	

	//Block messages that contain a single-quote ('), hash mark (#), or string (--) anywhere within the messagec
	private String truncateSpecialChar(String data) {
		
		String result = data;
		result = result.replace("&", "&amp;");
		result = result.replace("#", "");
		result = result.replace("--", "");
		result = result.replace("'", "&apos;");
		result = result.replace("\"", "&quot;");
		result = result.replace("<", "&lt;");
		result = result.replace(">", "&gt;");
		
		return result;
	}
	
	
	//Remove space
	private String spaceRemover(String data) {
		String result = ""; 
		result = data.trim().replaceAll(" +", " ");
		
		return result;
	}
	
	private boolean isThirdPartyCase(JSONObject Owner) {
		boolean isThirdPartyCase = false;
		
		if(Owner !=null && Owner.has("extra")) {
			JSONObject extra = Owner.getJSONObject("extra");
			
			if(extra.has("isPhSameAsLa")) {
				isThirdPartyCase = "N".equals(extra.getString("isPhSameAsLa"));
			}
		}
		
		return isThirdPartyCase;
	}
	
	private String validateTSAR(JSONObject LA_personalInfo, JSONObject quotation,JSONObject LA_declaration, String LA_countryOfResidenceCD, JSONArray app_plans,JSONObject LifeAssured_policies) {
		//boolean canAutoUnderWriting = true;
		String canAutoUnderWriting = "";
		
		double annualIncome = 0;
		
		if(LA_personalInfo.has("allowance")) {
			double allowance = LA_personalInfo.getDouble("allowance");	
			annualIncome = allowance*12;
		}
		
		int iAge = quotation.has("iAge")?quotation.getInt("iAge"):0;
		
		boolean isNonIncomeEarner = this.isNonIncomeEarner(LA_personalInfo);
		//boolean isSingaporeResident = this.validateSingaporeResident(LA_declaration,LA_countryOfResidenceCD);
		boolean isSingaporeResident = true;
		
		if (LA_countryOfResidenceCD.equals("FR")) {
			isSingaporeResident = false;
		}
		
		double tsarLifeMax = 0;
		double tsarTPDMax = 0;
		double tsarCIMax = 0;
		double tsarPAMax = 0;
		double tsarADBMax = 0;
		
		if (isNonIncomeEarner) {
			tsarLifeMax = 500000;
			tsarTPDMax = 500000;
			tsarCIMax = 500000;
			tsarPAMax = 500000;
			tsarADBMax = 500000;
		} else {
			if (isSingaporeResident) {
				tsarLifeMax = 15000000;
				tsarTPDMax = 6000000;
				tsarCIMax = 3000000;
				tsarPAMax = 3500000;
				tsarADBMax = 3500000;
			} else {
				tsarLifeMax = 15000000;
				tsarTPDMax = 4000000;
				tsarCIMax = 3000000;
				tsarPAMax = 3500000;
				tsarADBMax = 3500000;
			}
		}
		
		//TSAR values
		double tsarLife = 0;
		double tsarTPD = 0;
		double tsarCI = 0;
		double tsarPA= 0;
		double tsarADB= 0;
		
		//Income Multiplier
		double incomeMultiplierLife= 0;
		double incomeMultiplierTPD = 0;
		double incomeMultiplierCI = 0;
		double incomeMultiplierPA= 0;
		double incomeMultiplierADB= 0;
		
		//Multiplier for Life , TPD & ADB
		if(iAge <=39) { 
			incomeMultiplierLife = 30;
			incomeMultiplierTPD = 30;
			incomeMultiplierADB = 30;
		}else if(iAge >=40 && iAge<=49) {
			incomeMultiplierLife = 25;
			incomeMultiplierTPD = 25;
			incomeMultiplierADB = 25;
		}else if(iAge >=50 && iAge<=59) {
			incomeMultiplierLife = 15;
			incomeMultiplierTPD = 15;
			incomeMultiplierADB = 15;
		}else if(iAge >=60) {
			incomeMultiplierLife = 10;
			incomeMultiplierTPD = 10;
			incomeMultiplierADB = 10;
		}
		
		//Multiplier for CI Riders (Accerlerated/Additional)
		if(iAge <=35) {
			incomeMultiplierCI = 20;
		}else if(iAge >=36 && iAge<=40) {
			incomeMultiplierCI = 12;
		}else if(iAge >=41 && iAge<=45) {
			incomeMultiplierCI = 10;
		}else if(iAge >=46 && iAge<=55) {
			incomeMultiplierCI = 8;
		}else if(iAge >=56 && iAge<=65) {
			incomeMultiplierCI = 4;
		}
		
		//TODO Standalone CI
		//boolean isStandaloneCI = false;
		
		//Multiplier for PA
		if(iAge >=20 && iAge<=29) {
			incomeMultiplierPA = 7;
		}else if(iAge >=30 && iAge<=39) {
			incomeMultiplierPA = 7;
		}else if(iAge >=40 && iAge<=49) {
			incomeMultiplierPA = 5;
		}else if(iAge >=50 && iAge<=59) {
			incomeMultiplierPA = 5;
		}else if(iAge >=60) {
			incomeMultiplierPA = 2.5;
		}
		
		for (int i = 0; i < app_plans.length(); i++) {
			JSONObject app = app_plans.getJSONObject(i);
			
			if(app.has("covCode")){
				
				String covCode = app.getString("covCode");
				
				if(app.has("sumInsured") && app.get("sumInsured")!=null && !app.get("sumInsured").toString().isEmpty() && (app.get("sumInsured") instanceof Integer || app.get("sumInsured") instanceof Long || app.get("sumInsured") instanceof Float || app.get("sumInsured") instanceof Double)) {
					
					double sumInsured = app.getDouble("sumInsured");
					
					switch (covCode) {
					case "BAA"://AXA Band Aid
						tsarPA+=sumInsured*1;
						break;
					case "FPX"://INSPIRE FlexiProtector
						tsarLife+=sumInsured*1;
						tsarTPD+=sumInsured*1;
						break;
					case "FSX"://INSPIRE FlexiSaver
						tsarLife+=sumInsured*1;
						tsarTPD+=sumInsured*1;
						break;
					case "SAV"://SavvySaver
						tsarLife+=sumInsured*1;
						break;
					case "TPX"://Term Protector
						tsarLife+=sumInsured*1;
						break;
					case "TPPX"://Term Protector Prime
						tsarLife+=sumInsured*1;
						break;
					case "CRX"://Advance CI Payout
						tsarCI+=sumInsured*1;
						break;
					case "DIS"://Advance TPD Payout
						tsarTPD+=sumInsured*1;
						break;
					case "CIP"://CRITICAL ILLNESS PLUS
						tsarCI+=sumInsured*1;
						break;
					case "AP"://PERSONAL ACCIDENT BENEFIT
						tsarPA+=sumInsured*1;
						break;
					case "DCB"://DisabilityCash Benefit
						tsarTPD+=sumInsured*Math.min(20, 65-iAge);
						break;
					case "CARB"://SUPP CHOICE ACCELERATOR       BENEFIT
						tsarCI+=sumInsured*1;
						break;
					case "LTT"://SUPP LEVEL TERM BENEFIT
						tsarLife+=sumInsured*1;
						tsarTPD+=sumInsured*1;
						break;
					case "LARB"://SUPP LIVING ACCELERATOR       BENEFIT
						tsarCI+=sumInsured*1;
						break;
					case "LVT"://SUPP LIVING TERM BENEFIT
						tsarLife+=sumInsured*1;
						tsarTPD+=sumInsured*1;
						tsarCI+=sumInsured*1;
						break;
					case "ADBR"://Accidental Death Benefit
						tsarADB+=sumInsured*1;
						break;
					}	
				}
			}
		}
		
		//Existing Sum Assured - S 
		if (LifeAssured_policies.has("existTpd")) {
			try {
				tsarTPD += LifeAssured_policies.getDouble("existTpd");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("existCi")) {
			try {
				tsarCI += LifeAssured_policies.getDouble("existCi");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("existLife")) {
			try {
				tsarLife += LifeAssured_policies.getDouble("existLife");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("existPaAdb")) {
			try {
				tsarPA += LifeAssured_policies.getDouble("existPaAdb");	
				tsarADB += LifeAssured_policies.getDouble("existPaAdb");	
			} catch(Exception ex) {}			
		}
		//Existing Sum Assured - E
		
		//Pending Sum Assured - S
		if (LifeAssured_policies.has("pendingTpd")) {
			try {
				tsarTPD += LifeAssured_policies.getDouble("pendingTpd");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("pendingCi")) {
			try {
				tsarCI += LifeAssured_policies.getDouble("pendingCi");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("pendingLife")) {
			try {
				tsarLife += LifeAssured_policies.getDouble("pendingLife");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("pendingPaAdb")) {
			try {
				tsarPA += LifeAssured_policies.getDouble("pendingPaAdb");	
				tsarADB += LifeAssured_policies.getDouble("pendingPaAdb");
			} catch(Exception ex) {}			
		}
		//Pending Sum Assured - E
		
		//Replaced Sum Assured - S
		if (LifeAssured_policies.has("replaceTpd")) {
			try {
				tsarTPD -= LifeAssured_policies.getDouble("replaceTpd");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("replaceCi")) {
			try {
				tsarCI -= LifeAssured_policies.getDouble("replaceCi");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("replaceLife")) {
			try {
				tsarLife -= LifeAssured_policies.getDouble("replaceLife");	
			} catch(Exception ex) {}
		}
		
		if (LifeAssured_policies.has("replacePaAdb")) {
			try {
				tsarPA -= LifeAssured_policies.getDouble("replacePaAdb");	
				tsarADB -= LifeAssured_policies.getDouble("replacePaAdb");
			} catch(Exception ex) {}			
		}
		//Replaced Sum Assured - E
				
		double valueIncomeMultiplierLife= annualIncome*incomeMultiplierLife;
		double valueIncomeMultiplierTPD = annualIncome*incomeMultiplierTPD;
		double valueIncomeMultiplierCI = annualIncome*incomeMultiplierCI;
		double valueIncomeMultiplierPA = annualIncome*incomeMultiplierPA;
		double valueIncomeMultiplierADB = annualIncome*incomeMultiplierADB;
								
		if (isNonIncomeEarner) {
			if ((tsarLife > tsarLifeMax))
				canAutoUnderWriting += "Life,";
				
			if ((tsarTPD > tsarTPDMax))
				canAutoUnderWriting += "TPD,";
			
			if ((tsarCI > tsarCIMax))
				canAutoUnderWriting += "CI,";
			
			if ((tsarPA > tsarPAMax))
				canAutoUnderWriting += "PA,";
			
			if ((tsarADB > tsarADBMax))
				canAutoUnderWriting += "ADB,";
		} else {
			if (valueIncomeMultiplierLife < tsarLifeMax)
				tsarLifeMax = valueIncomeMultiplierLife;
			
			if (valueIncomeMultiplierTPD < tsarTPDMax)
				tsarTPDMax = valueIncomeMultiplierTPD;

			if (valueIncomeMultiplierCI < tsarCIMax)
				tsarCIMax = valueIncomeMultiplierCI;
			
			if (valueIncomeMultiplierPA < tsarPAMax)
				tsarPAMax = valueIncomeMultiplierPA;
			
			if (valueIncomeMultiplierADB < tsarADBMax)
				tsarADBMax = valueIncomeMultiplierADB;

			if (tsarLife > tsarLifeMax)
				canAutoUnderWriting += "Life,";
				
			if (tsarTPD > tsarTPDMax)
				canAutoUnderWriting += "TPD,";
			
			if (tsarCI > tsarCIMax)
				canAutoUnderWriting += "CI,";
			
			if (tsarPA > tsarPAMax)
				canAutoUnderWriting += "PA,";
			
			if (tsarADB > tsarADBMax)
				canAutoUnderWriting += "ADB,";
		}
				
		if (canAutoUnderWriting.endsWith(","))
			canAutoUnderWriting= canAutoUnderWriting.substring(0, canAutoUnderWriting.length() - 1);
		
		return canAutoUnderWriting;
	}
	
	private boolean validateFailAutoUnderWritingRulesCR044(boolean isThirdPartyCase, JSONObject Owner,JSONObject quotation , JSONObject Owner_policies, JSONObject Owner_personalInfo, JSONObject Owner_declaration, String LA_countryOfResidenceCD, PolicyRemarks Pol_remarks,JSONObject LifeAssured_policies) {
		boolean canAutoUnderWriting = true;
		
		//*** ----- CR044 ----- ***//
		if(Owner != null && quotation !=null && Owner_policies !=null && LifeAssured_policies !=null) {
			
			String paymentMode = quotation.has("paymentMode")?quotation.getString("paymentMode"):null;
			boolean isSinglePremium = false;
			
			if(paymentMode != null) {
				if(paymentMode.equals("L")) { // Single premium
					isSinglePremium = true;
				}
			}
			
			double totalAnnualPremiumOwner =  this.calculateTotalAnnualPremium(quotation,Owner_policies);
			
			double totalAnnualPremiumLA =  this.calculateTotalAnnualPremium(quotation,LifeAssured_policies);
			
			
			if(Owner_personalInfo !=null && !isSinglePremium) {
				//------CR044 Rule 2 Start------//
				
				double annualIncome = 0;
				
				if(Owner_personalInfo.has("allowance")) {
					double allowance = Owner_personalInfo.getDouble("allowance");	
					annualIncome = allowance*12;
				}
				
				double totalSumAssured = this.calculateTotalSumAssured(quotation, Owner_policies, 0);
				
				boolean isNonIncomeEarner = this.isNonIncomeEarner(Owner_personalInfo);
				
				int premTermYr = 0;

				if (quotation.has("plans")) {
					if(quotation.getJSONArray("plans").getJSONObject(0).has("premTermYr")) {
						premTermYr = quotation.getJSONArray("plans").getJSONObject(0).getInt("premTermYr");
					}else if(quotation.getJSONArray("plans").getJSONObject(0).has("premTerm")) {
						premTermYr = quotation.getJSONArray("plans").getJSONObject(0).getInt("premTerm");
					}
				} else {
					if(quotation.has("premTermYr")) {
						premTermYr = quotation.getInt("premTermYr");
					}else if(quotation.has("premTerm")) {
						premTermYr = quotation.getInt("premTerm");
					}
				}
				
				int pAge = quotation.has("pAge")?quotation.getInt("pAge"):0;
				
				// Validation Rule 2
				if(!isNonIncomeEarner) { //Proposer is Income-Earner
					if((annualIncome>0 && annualIncome <=25000 && (totalAnnualPremiumOwner/annualIncome > 0.2)) //If Annual Income > 0 & <= S$ 25k, AND [Total Annual Premium / Annual Income] > 20%; 
							|| (annualIncome>25000 && annualIncome <=150000 && (totalAnnualPremiumOwner/annualIncome > 0.3)) //If Annual Income > S$ 26k & <= S$ 150k , AND [Total Annual Premium / Annual Income] > 30%; 
							|| (annualIncome>150000 && (pAge+premTermYr<65) && (totalAnnualPremiumOwner/annualIncome > 0.5)) //If Annual Income > S$ 150k , AND Current age + Premium paying term < 65 AND [Total Annual Premium / Annual Income] > 50%.  
							|| (annualIncome>150000 && (pAge+premTermYr>=65) && (totalAnnualPremiumOwner/annualIncome > 0.35)) //If Annual Income > S$150k, AND Current Age + Premium paying term >= 65 AND [Total Annual Premium / Annual Income] > 35%.
							) { 
						canAutoUnderWriting = false;
						Pol_remarks.addpolicyRemarks("Check for affordability");
					}
				}else { //Proposer is a Non-Income Earner
					if(totalSumAssured > 500000) { // If Total Sum Assured <= 500k, then skip Annual Premium validation. 
						//Total Sum Assured is over 500k
						if(
								(annualIncome>0 && annualIncome <=25000 && (totalAnnualPremiumOwner/annualIncome > 0.2)) //If Annual Income > 0 & <= S$ 25k, AND [Total Annual Premium / Annual Income] > 20%; 
								|| (annualIncome>25000 && annualIncome <=150000 && (totalAnnualPremiumOwner/annualIncome > 0.3)) //If Annual Income > S$ 26k & <= S$ 150k , AND [Total Annual Premium / Annual Income] > 30%; 
								|| (annualIncome>150000 && (pAge+premTermYr<65) && (totalAnnualPremiumOwner/annualIncome > 0.5)) //If Annual Income > S$ 150k , AND Current age + Premium paying term < 65 AND [Total Annual Premium / Annual Income] > 50%.  
								|| (annualIncome>150000 && (pAge+premTermYr>=65) && (totalAnnualPremiumOwner/annualIncome > 0.35)) //If Annual Income > S$150k, AND Current Age + Premium paying term >= 65 AND [Total Annual Premium / Annual Income] > 35%.
								|| (annualIncome == 0) //If Annual Income or Monthly Allowance = 0; 
								) {
							canAutoUnderWriting = false;
							Pol_remarks.addpolicyRemarks("Check for affordability");
						}
					}
				}
				//------CR044 Rule 2 End------//
				
			}
			
			//------CR044 Rule 3 Start------//
			if(Owner_declaration !=null && LA_countryOfResidenceCD != null && !isSinglePremium) {
				boolean isSingaporeResident = this.validateSingaporeResident(Owner_declaration,LA_countryOfResidenceCD);
				
				if(isSingaporeResident) { //Singapore Residents 
					if(totalAnnualPremiumLA > 75000) { //If Total Annual Premium > 75k (Regardless of currency)
						canAutoUnderWriting = false;
						Pol_remarks.addpolicyRemarks("Large RP");
					}
				}else{ //Non-Singapore Residents
					if(totalAnnualPremiumLA > 60000) { //If Total Annual Premium > 60k (Regardless of currency)
						canAutoUnderWriting = false;
						Pol_remarks.addpolicyRemarks("Large RP");
					}
				}
			}
			//------CR044 Rule 3 End------//
			
			//------CR044 Rule 4 Start------//
			if(isSinglePremium) { // Single premium
				double premium = quotation.has("premium")?quotation.getDouble("premium"):0;
				
				if(premium>500000) { //If Single Premium > 500k (Regardless of currency)
					canAutoUnderWriting = false;
					Pol_remarks.addpolicyRemarks("Large SP");
				}
			}
			//------CR044 Rule 4 End------//
		}
		
		//*** ----- CR044 ----- ***//
		
		
		return canAutoUnderWriting;
	}
	
	private boolean isNonIncomeEarner(JSONObject Owner_personalInfo) {
		// *** Non-Income Earner *** //
		//Housewife = O675
		//Househusband = O674
		//Student (Age 18 and above) = O1321
		//Student (Below age 18) = O1322
		//Child / Juvenile / Infant = O246
		//Unemployed = O1450
		//Retiree / Pensioner = O1132
		
		
		boolean isNonIncomeEarner = false;
		
		String occupation = Owner_personalInfo.has("occupation")?Owner_personalInfo.getString("occupation"): null;
		if(occupation != null) {
			String[] checkingValues = new String[] { "O675","O674","O1321","O1322","O246","O1450","O1450" };
			isNonIncomeEarner = Arrays.asList(checkingValues).contains(occupation);
		}
		
		return isNonIncomeEarner;
	}
	
	private boolean validateSingaporeResident(JSONObject Owner_declaration, String LA_countryOfResidenceCD) {
		boolean isSingaporeResident = true;

		if (LA_countryOfResidenceCD.equals("FR")) {
			isSingaporeResident = false;
		}
		
//		if (Owner_declaration.has("FUND_SRC01")) {
//			String FUND_SRC01 = Owner_declaration.getString("FUND_SRC01");
//
//			if (FUND_SRC01 != null) {
//				boolean isFundSG = FUND_SRC01.equals("sg");
//
//				if (LA_countryOfResidenceCD.equals("LR") && isFundSG) { //Singapore Residents 
//					isSingaporeResident = true;
//
//				} else if (LA_countryOfResidenceCD.equals("FR") || !isFundSG) { //Non-Singapore Residents
//					isSingaporeResident = false;
//				}
//			}
//		}

		return isSingaporeResident;
	}
	
	private double calculateTotalAnnualPremium(JSONObject quotation, JSONObject policies) {
		double totYearPrem = quotation.has("totYearPrem")?quotation.getDouble("totYearPrem"):0;
		
		double existTotalPrem = (policies.has("existTotalPrem") && policies.get("existTotalPrem")!=null && !policies.get("existTotalPrem").toString().isEmpty()) ?policies.getDouble("existTotalPrem"):0;
		double pendingTotalPrem = (policies.has("pendingTotalPrem") && policies.get("pendingTotalPrem")!=null && !policies.get("pendingTotalPrem").toString().isEmpty()) ?policies.getDouble("pendingTotalPrem"):0;
		double replaceTotalPrem = (policies.has("replaceTotalPrem") && policies.get("replaceTotalPrem")!=null && !policies.get("replaceTotalPrem").toString().isEmpty()) ?policies.getDouble("replaceTotalPrem"):0;
		
		return totYearPrem+existTotalPrem+pendingTotalPrem-replaceTotalPrem;
	}
	
	private double calculateTotalSumAssured(JSONObject quotation, JSONObject policies, double CIP_sumInsured) {
		double sumInsured = quotation.has("sumInsured")?quotation.getDouble("sumInsured"):0;
		
		sumInsured += CIP_sumInsured;
		
		// SumAssured of existing
		double existCi = (policies.has("existCi") && policies.get("existCi")!=null && !policies.get("existCi").toString().isEmpty()) ?policies.getDouble("existCi"):0;
		double existLife = (policies.has("existLife") && policies.get("existLife")!=null && !policies.get("existLife").toString().isEmpty()) ?policies.getDouble("existLife"):0;
		double existPaAdb = (policies.has("existPaAdb") && policies.get("existPaAdb")!=null && !policies.get("existPaAdb").toString().isEmpty()) ?policies.getDouble("existPaAdb"):0;
		double existTpd = (policies.has("existTpd") && policies.get("existTpd")!=null && !policies.get("existTpd").toString().isEmpty()) ?policies.getDouble("existTpd"):0;
		
		double totalExistSumAssured = existCi+existLife+existPaAdb+existTpd;
		
		// SumAssured of pending
		double pendingCi = (policies.has("pendingCi") && policies.get("pendingCi")!=null && !policies.get("pendingCi").toString().isEmpty()) ?policies.getDouble("pendingCi"):0;
		double pendingLife = (policies.has("pendingLife") && policies.get("pendingLife")!=null && !policies.get("pendingLife").toString().isEmpty()) ?policies.getDouble("pendingLife"):0;
		double pendingPaAdb = (policies.has("pendingPaAdb") && policies.get("pendingPaAdb")!=null && !policies.get("pendingPaAdb").toString().isEmpty()) ?policies.getDouble("pendingPaAdb"):0;
		double pendingTpd = (policies.has("pendingTpd") && policies.get("pendingTpd")!=null && !policies.get("pendingTpd").toString().isEmpty()) ?policies.getDouble("pendingTpd"):0;
		
		double totalPendingSumAssured = pendingCi+pendingLife+pendingPaAdb+pendingTpd;
		
		// SumAssured of replacement
		double replaceCi = (policies.has("replaceCi") && policies.get("replaceCi")!=null && !policies.get("replaceCi").toString().isEmpty()) ?policies.getDouble("replaceCi"):0;
		double replaceLife = (policies.has("replaceLife") && policies.get("replaceLife")!=null && !policies.get("replaceLife").toString().isEmpty()) ?policies.getDouble("replaceLife"):0;
		double replacePaAdb = (policies.has("replacePaAdb") && policies.get("replacePaAdb")!=null && !policies.get("replacePaAdb").toString().isEmpty()) ?policies.getDouble("replacePaAdb"):0;
		double replaceTpd = (policies.has("replaceTpd") && policies.get("replaceTpd")!=null && !policies.get("replaceTpd").toString().isEmpty()) ?policies.getDouble("replaceTpd"):0;
		
		double totalReplaceSumAssured = replaceCi+replaceLife+replacePaAdb+replaceTpd;
		
		return sumInsured+totalExistSumAssured+totalPendingSumAssured-totalReplaceSumAssured;
	}
	
	private boolean validatePassExpiryDate(long appSignedTimeStamp, long passExpiryTimestamp) {
		boolean canAutoUnderWriting = true;
		
		// 1 day = 86400
		// 90 days = 7776000
		
		appSignedTimeStamp/=1000;
		passExpiryTimestamp/=1000;
		
		if(passExpiryTimestamp <= appSignedTimeStamp+7776000) {
			canAutoUnderWriting = false;
		}
		
		return canAutoUnderWriting;
	}
	
	private boolean BMI(int age, String weight, String height) {
//		Build Adult - Age 18-99 =  BMI 17 - 31
//		Build Juvenile: Age 0 =  BMI  12.5 - 18.5
//		Build Juvenile: Age 1 - 3 =  BMI  12.5 - 18.5
//		Build Juvenile: Age 4 - 6 =  BMI 13.0 - 20.0
//		Build Juvenile: Age 7 - 9 =  BMI 13.0 - 20.0
//		Build Juvenile: Age 10 - 13 =  BMI 14.0 - 28.0
//		Build Juvenile: Age 14 - 17 =  BMI 14.0 - 28.0
//		BMI = Weight / Height2 (m2)
				
		double _weight = Double.parseDouble(weight);
		double _height = Double.parseDouble(height);
		double result  = _weight / (_height * _height);
		
		if (age >= 18 && age <= 99) {
			if (result >= 17 && result <= 31) 
				return true;
		} else if (age == 0) {
			if (result >= 12.5 && result <= 18.5) 
				return true;
		} else if (age >= 1 && age <= 3) {
			if (result >= 12.5 && result <= 18.5) 
				return true;
		} else if (age >= 4 && age <= 6) {
			if (result >= 13 && result <= 20) 
				return true;
		} else if (age >= 7 && age <= 9) {
			if (result >= 13 && result <= 20) 
				return true;
		} else if (age >= 10 && age <= 13) {
			if (result >= 14 && result <= 28) 
				return true;
		} else if (age >= 14 && age <= 17) {
			if (result >= 14 && result <= 28) 
				return true;
		}
		
		return false;
	}
	
	private boolean BMIforSheild(int age, String weight, String height) {
//		Build Adult - Age 18-99 =  BMI 17 - 33
//		Build Juvenile: Age 0 =  BMI  12.5 - 18.5
//		Build Juvenile: Age 1 - 3 =  BMI  12.5 - 18.5
//		Build Juvenile: Age 4 - 6 =  BMI 13.0 - 20.0
//		Build Juvenile: Age 7 - 9 =  BMI 13.0 - 20.0
//		Build Juvenile: Age 10 - 13 =  BMI 14.0 - 28.0
//		Build Juvenile: Age 14 - 17 =  BMI 14.0 - 28.0
//		BMI = Weight / Height2 (m2)
				
		double _weight = Double.parseDouble(weight);
		double _height = Double.parseDouble(height);
		double result  = _weight / (_height * _height);
				
		if (age >= 18 && age <= 99) {
			if (result >= 17 && result <= 33) 
				return true;
		} else if (age == 0) {
			if (result >= 12.5 && result <= 18.5) 
				return true;
		} else if (age >= 1 && age <= 3) {
			if (result >= 12.5 && result <= 18.5) 
				return true;
		} else if (age >= 4 && age <= 6) {
			if (result >= 13 && result <= 20) 
				return true;
		} else if (age >= 7 && age <= 9) {
			if (result >= 13 && result <= 20) 
				return true;
		} else if (age >= 10 && age <= 13) {
			if (result >= 14 && result <= 28) 
				return true;
		} else if (age >= 14 && age <= 17) {
			if (result >= 14 && result <= 28) 
				return true;
		}
		
		return false;
	}
	
	public int totalOfDOBMonth (String LA_dob) {
		int days = Integer.parseInt(LA_dob.substring(8, 10));
		int month = Integer.parseInt(LA_dob.substring(5, 7));
		int year = Integer.parseInt(LA_dob.substring(0, 4));
	
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		Date d = new Date();

		/*Calculating the num of year, month and date*/
		month = (12 - month) + localDate.getMonthValue();
		year = (d.getYear() + 1900) - year - 1;

		if (month >= 12)
		{
			year = year + 1;
			month = month - 12;
		}

		int TotalOfMonth = month + (year * 12);
		
		return TotalOfMonth;
	}
	
	private String getOccupationClassForSheild(String occupationCode, String income) {		
		String occupation_Class = "1";
		
		int _income = 0;
		
		if (income != null)
			_income = Integer.parseInt(income);
		
		//O1322 Student below 18 - No income	Map as occupation class 4
		if ("O1322".equals(occupationCode) && _income <= 0)
			occupation_Class = "4";
		
		//O1322 Student below 18 - with income	Map as occupation class 7
		else if ("O1322".equals(occupationCode) && _income > 0)
			occupation_Class = "7";
		
		//O1321 Student above 18 - No income	Map as occupation class 6
		else if ("O1321".equals(occupationCode) && _income <= 0)
			occupation_Class = "6";
		
		//O1321 Student above 18 - with income	Map as occupation class 1
		else if ("O1321".equals(occupationCode) && _income > 0)
			occupation_Class = "1";
		
		//O246 Child/juvenile/infant - no income	Map as occupation class 4
		else if ("O246".equals(occupationCode) && _income <= 0)
			occupation_Class = "4";
		
		//O246 Child/juvenile/infant - with income	Map as occupation class 7
		else if ("O246".equals(occupationCode) && _income > 0)
			occupation_Class = "7";
		
		//Housewife/house husband with or without income	Map as occupation class 4
		else if ("O674".equals(occupationCode) || "O675".equals(occupationCode))
			occupation_Class = "4";
		
		//O1132 Retiree/pensioner	Map as occupation class 6
		else if ("O1132".equals(occupationCode))
			occupation_Class = "6";
		
		//Default 1
		else
			occupation_Class = "1";
		
		return occupation_Class;
	}
		
	private String packageToRider(String basicCD, String riderCD) {
		JSONObject json = null;
		json = Constant.CBUTIL.getDoc("packageToRider");
		
		if (json == null)
			Log.error("Error - packageToRider json missing");
	
		JSONArray json_list = json.getJSONArray("options");
		String _riderCD = "";
		
		if (json_list.length() > 0) {
			for(int i = 0; i < json_list.length(); i++) {
				_riderCD = json_list.getJSONObject(i).getString("riderCD");
				
				if (riderCD.equals(_riderCD) && basicCD.equals(json_list.getJSONObject(i).getString("basicCD")))
					return json_list.getJSONObject(i).getString("basicCD");
			}	
		}
		
		return "";
	}
	
	private boolean isPayorBenefitRider(String basicCD, String riderCD) {
		try {
			JSONObject json = null;
			json = Constant.CBUTIL.getDoc("application_form_mapping");
			
			if (json == null)
				Log.error("Error - application_form_mapping json missing");
		
			JSONObject basicSections = json.getJSONObject(basicCD);
			JSONArray eFormSections = basicSections.getJSONArray("eFormSections");
			
			if (basicSections.has("eFormSections")) {
				eFormSections = basicSections.getJSONArray("eFormSections");
				
				for (int i = 0; i < eFormSections.length(); i++) {
					JSONObject _eFormSections = eFormSections.getJSONObject(i);
					
					if (_eFormSections.has("items")) {
						JSONArray items = _eFormSections.getJSONArray("items");
						
						for (int j = 0; j < items.length(); j++) {
							JSONObject _items = items.getJSONObject(j);
							
							if (_items.has("id")) {	
								if (_items.getString("id").equals("insurability")) {				
									if (_items.has("form")) {
										JSONObject form = _items.getJSONObject("form");
										JSONObject insurability = form.getJSONObject("ph");
										
										if (insurability.has(riderCD))
											return true;						
									}	
								}
							}
						}
					}
				}
			}
		} catch(Exception ex) {
			Log.error(ex);	
		}
		
		return false;
	}
	

	private String salesChargeMapping(String basicCD) {
		JSONObject json = null;
		json = Constant.CBUTIL.getDoc("salesChargeForRSP");
		
		if (json == null)
			Log.error("Error - salesChargeForRSP json missing");
	
		JSONArray json_list = json.getJSONArray("options");
		
		if (json_list.length() > 0) {
			for(int i = 0; i < json_list.length(); i++) {		
				if (basicCD.equals(json_list.getJSONObject(i).getString("basicCD")))
					return json_list.getJSONObject(i).getString("chargeCD");
			}	
		}
		
		return "";
	}
	
	private String serviceFeeMapping(String basicCD) {
		JSONObject json = null;
		json = Constant.CBUTIL.getDoc("serviceFreeRLSMapping");
		
		if (json == null)
			Log.error("Error - serviceFreeRLSMapping json missing");
	
		JSONArray json_list = json.getJSONArray("options");
		
		if (json_list.length() > 0) {
			for(int i = 0; i < json_list.length(); i++) {		
				if (basicCD.equals(json_list.getJSONObject(i).getString("basicCD")))
					return json_list.getJSONObject(i).getString("chargeCD");
			}	
		}
		
		return "";
	}
	
	
	private String riderClassCodeMapping(String riderCD) {
		JSONObject json = null;
		json = Constant.CBUTIL.getDoc("riderClassCodeRLSMapping");
		
		if (json == null)
			Log.error("Error - riderClassCodeRLSMapping json missing");
	
		JSONArray json_list = json.getJSONArray("options");
		
		if (json_list.length() > 0) {
			for(int i = 0; i < json_list.length(); i++) {		
				if (riderCD.equals(json_list.getJSONObject(i).getString("riderCD")))
					return json_list.getJSONObject(i).getString("classCD");
			}	
		}
		
		return "1";
	}
	
	private String getRLSPaymentMethodCD(String paymentCD, boolean isShort) {
		switch(paymentCD) {
			case "crCard"://Credit Card
				if (isShort)
					return "CC";
				else 
					return "Paid by CC";
			case "eNets"://eNETS
				if (isShort)
					return "Enets";
				else 
					return "Paid by eNETS";
			case "dbsCrCardIpp"://DBS Credit Card (Instalment Payment Plan)
				if (isShort)
					return "DBS IPP";
				else 
					return "Paid by DBS IPP";
			case "axasam"://AXS/SAM
				if (isShort)
					return "AXS/SAM";
				else 
					return "Paid by AXS/SAM";
			case "chequeCashierOrder"://Cheque/Cashier Order
				if (isShort)
					return "CQ/CO";
				else 
					return "Paid by Cheque / Cashier Order";
			case "cash"://Cash
				if (isShort)
					return "Cash";
				else 
					return "Paid by Cash";
			case "teleTransfter"://Telegraphic Transfer
				if (isShort)
					return "TT";
				else 
					return "Paid by TT";
			case "cpfisoa"://CPFIS-OA
				if (isShort)
					return "CPF OA";
				else 
					return "Paid by OA";
			case "cpfissa"://CPFIS-SA
				if (isShort)
					return "CPF SA";
				else 
					return "Paid by SA";
			case "srs"://SRS			  
				if (isShort)
					return "CPF SRS";
				else 
					return "Paid by SRS";
			case "payLater"://payLater
	
				if (isShort)
					return "Pay Later";
				else 
					return "To be paid later";
		}

		return "";
	}
	
	

	public boolean isDateValid(String date) 
	{
			String DATE_FORMAT = "yyyy-MM-dd";
		
	        try {
	            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
	            df.setLenient(false);
	            df.parse(date);
	            return true;
	        } catch (Exception e) {
	            return false;
	        }
	}

}
