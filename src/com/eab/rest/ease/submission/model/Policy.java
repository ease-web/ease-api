package com.eab.rest.ease.submission.model;

import java.util.Date;
import java.util.List;

public class Policy {

    public String applicationCaptureDT;
    public String companyCD;
    public String policyNO;
    public String sourceChannelCD;
    public String policySubmitDT;
    public String applicationSignatureDT;
    public String policyStatusCD;
    public String policyEffectiveDTTM;
    public String medicalIND = "N"; // Default N
    public String languageCD;
    public String paymentModeCD;
    public String paymentMethodCD;
    public String currencyCD;
    public String minPremiumAMT;
    public String premiumAMT;


    public String depositAMT;
    public Date policyExpirationDTTM;
    public String policyNo_2;
    public String autoSetFLG;
    public String communicationMode;
    public Date policySubmitDT1;
    public String initialLumpsumPremiumAMT;
    public String lumpsumPremiumAMT;
    public String topupRegularModalPremium;
    public String lobRk;
    public String applicationSubmitTM;
    public String declineOptionIND;
    public String termConversionFLG;
    public String termConversionPolicyNO;
    public String indexationFLG;
    public String nonForfeitureFLG;
    public String captureUserDeptNM;
    public String captureUserSourceNM;
    public String lastUpdateUserDeptNM;
    public String lastUpdateUserSourceNM;
    public String statusUpdateDeptNM;
    public String statusUpdateSourceNM;
    public String singlePremiumPolicyFLG;
    
    public HasCreditAuthorizationDetailsIn hasCreditAuthorizationDetailsIn = null;
    public List<HasDetailsOfPolicyIn> hasDetailsOfPolicyIn = null;
    public List<HasProducerInformationIn> hasProducerInformationIn = null;
    public List<HasDetailsOfRisksIn> hasDetailsOfRisksIn = null;
    public List<HasCustomerInformationIn> hasCustomerInformationIn = null;
    public List<HasPolicyChargeInfoIn> hasPolicyChargeInfoIn = null;
    public List<HasLifePolicyTransactionDetailsIn> hasLifePolicyTransactionDetailsIn = null;
    public List<ForLifeUnitofExposure> forLifeUnitofExposure = null;
    public List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProduct = null;
    public List<HasNotificationsDetailsIn> hasNotificationsDetailsIn = null;
    public List<HasSharedProducerInformationIn> hasSharedProducerInformationIn = null;
    public List<HasDetailsRecordIn> hasDetailsRecordIn = null;
    public List<HasPolicyRemarkDetailsIn> hasPolicyRemarkDetailsIn = null;
    public List<HasCaseUnderwriterDetailsIn> hasCaseUnderwriterDetailsIn = null;
    public List<HasPolicyAssociationDetailsIn> hasPolicyAssociationDetailsIn = null;
    public List<HasAssessmentHeadersIn> hasAssessmentHeadersIn = null;
    public List<HasAXAAsiaPolicyPaymentAccount> hasAXAAsiaPolicyPaymentAccount = null;
    public List<HasBenificialOwnerDetailsIn> hasBenificialOwnerDetailsIn = null;
    public List<HasAXAAsiaPolicyHeaderPaymentAccount> hasAXAAsiaPolicyHeaderPaymentAccount = null;
    public List<HasUnderwritingDecisionsIn> hasUnderwritingDecisionsIn = null;
    public List<HasDependentInformationIn> hasDependentInformationIn = null;
    public List<HasPreviousPolicyDetailsIn> hasPreviousPolicyDetailsIn = null;


}