package com.eab.rest.ease.submission.model;

public class HasNotificationsDetailsIn {

    public String recipientTypeCD;
    public String recipientEmailAddress;

    public HasNotificationsDetailsIn() {
    }
}