package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasBeneficiaryInformationIn {

    public String beneficiaryFLG;
    public List<CanBeIndividualItemType> canBeIndividual;
    public List<HasBeneficiaryXUoeDetailsIn> hasBeneficiaryXUoeDetailsIn;

    public HasBeneficiaryInformationIn() {
    }
}
