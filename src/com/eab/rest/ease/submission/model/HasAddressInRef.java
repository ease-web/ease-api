package com.eab.rest.ease.submission.model;

/**
 * Identifies the insured residential address
 */
public class HasAddressInRef {
    public String officeTelNo;
    public String addressLine1;
    public String addressLine2;
    public String addressLine3;
    public String addressLine4;
    public String cityNM;
    public String countryCD;
    public String stateRegionCd;
    public String telephoneNO;
    public String emailAddress;
    public String mobilePhoneNO;
    public String addressTypeInfoCD;
    public String addressTypeCD;
    public String postalCD;
    public String mailingStatusCD;
    public String phoneStatusCd;
    public String emailStatusCD;
    public String phoneTypeInfoCD;
    public String emailTypeInfoCD;

    public HasAddressInRef() {
    }
}
