package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasDependentInformationIn {

    public String dependentNO;
    public String planCD;
    public String classCD;
    public String premiumAMT;
    public String offerCD;
    public List<String> prevPolicyNO;
    public String familyMembersNO;
    public List<CanBeIndividualItemType> canBeIndividual;

    public HasDependentInformationIn() {
    }
}