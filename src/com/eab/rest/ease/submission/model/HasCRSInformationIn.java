package com.eab.rest.ease.submission.model;

import java.util.List;


/**
 * Identifies the details of the owner CRS information
 */
public class HasCRSInformationIn {

    public String CRSType;
    public String followUpIndicator;
    public List<String> taxIdentificationNO;
    public List<String> countryOfTaxResidence;
    public String remarks;
    public String entityFlag;
    public String CRSStatus;
    public String FATCAStatus;
    public String entityType;

    public HasCRSInformationIn() {
    }
}