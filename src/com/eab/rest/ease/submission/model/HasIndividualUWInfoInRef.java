package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasIndividualUWInfoInRef {
        public String declineQuestionIND;
        public String avocationQuestionIND;
        public String healthQuestionIND;
        public String previousHealthExamQuestionIND;
        public String previousHealthExamReasonIND;
        public String previousHealthExamResultIND;
        public String alcoholQuestionIND;
        public String alcoholUnitsPerWeekNO;
        public String tobaccoQuestionIND;
        public String tobaccoUnitsPerDayNO;
        public String smokingQuestionIND;
        public String cigarettesPerDayNO;
        public List<String> previousSubStdPNO;
        public String ePolicyDeliveryFLG;
        public String customerProtectionDeclarationIND;
        public String agentReportIND;
        public String residenceDeclCD;
        public String trustDeclarationIND;

        public HasIndividualUWInfoInRef() {}
}
