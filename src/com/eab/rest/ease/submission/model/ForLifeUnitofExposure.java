package com.eab.rest.ease.submission.model;

import java.util.Date;
import java.util.List;

public class ForLifeUnitofExposure {

    public String basicRegularPremiumAMT;
    public String currAMT;
    public String basicPremiumRt;
    public String loadingPremiumRt;
    public String tempLoadingRT;
    public Date tempExtraEndDt;
    public String tempLoadingYrCNT;
    public String referenceTO;
    public String packagedTO;
    public HasCoverageDetailsIn hasCoverageDetailsIn;
    public List<HasBeneficiaryInformationIn> hasBeneficiaryInformationIn;

    public ForLifeUnitofExposure() {
    }
}
