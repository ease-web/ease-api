package com.eab.rest.ease.submission.model;

public class HasPreviousPolicyDetailsIn {

    public String insuredIND;
    public String prevInsuredIND;
    public String dependentNO;
    public String prevPolicyNO;
    public String lifeNO;

    public HasPreviousPolicyDetailsIn() {
    }
}