package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasAssessmentHeadersIn {

    public String referenceNO;
    public List<HasAssessmentDetailsIn> hasAssessmentDetailsIn;

    public HasAssessmentHeadersIn() {
    }
}
