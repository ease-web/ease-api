package com.eab.rest.ease.submission.model;

import java.util.Date;

public class HasCaseUnderwriterDetailsIn {

    public String notificationTypeCD;
    public String referenceNO;
    public String remarkCD;
    public String remarkDESC;
    public String remarkDESC2;
    public String remarkDESC3;
    public String deptCD;
    public String assessorUserID;
    public Date lastUpdateDT;
    public Date lastUpdateUserID;
    public Date completionDT;
    public String completionStatusCD;
    public Date remarkIssueDT;

    public HasCaseUnderwriterDetailsIn() {
    }
}
