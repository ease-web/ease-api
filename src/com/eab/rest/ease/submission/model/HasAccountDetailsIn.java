package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasAccountDetailsIn {

    public String allocPct;
    public String lumpsumAllocPCT;

    public List<HasInvestmentFundDetailsIn> hasInvestmentFundDetailsIn;

    public HasAccountDetailsIn() {
    }
}