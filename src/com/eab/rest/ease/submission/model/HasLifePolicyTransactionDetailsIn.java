package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasLifePolicyTransactionDetailsIn {

    public String paymentDueDT;
    public String premiumAMT;
    public String transTypeCD;
    public String transCurrencyCD;
    public String receiptNO;
    public List<HasLifePolicyTransactionHeaderDetailsIn> hasLifePolicyTransactionHeaderDetailsIn;
    public List<HasPaymentDetailsIn> hasPaymentDetailsIn;

    public HasLifePolicyTransactionDetailsIn() {
    }
}
