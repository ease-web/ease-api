package com.eab.rest.ease.submission.model;

import java.util.List;

/**
 * Identifies the Primary Producer Infomation
 */
public class HasProducerInformationIn {

    public String producerID;
    public List<HasAssociationWith> hasAssociationWith;

    public HasProducerInformationIn() {
    }
}