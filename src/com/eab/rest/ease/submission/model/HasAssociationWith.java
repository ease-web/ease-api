package com.eab.rest.ease.submission.model;

import java.util.List;

/**
 * Identify the Producer's company information. Mainly for Singpost.
 */
public class HasAssociationWith {

    public String bankBranchCD;
    public String stateRegionCD;
    public String areaCD;
    public String bankCD;
    public List<HasStaffInformationIn> hasStaffInformationIn;

    public HasAssociationWith() {
    }
}