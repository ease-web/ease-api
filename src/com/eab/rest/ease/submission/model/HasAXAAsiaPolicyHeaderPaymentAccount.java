package com.eab.rest.ease.submission.model;

public class HasAXAAsiaPolicyHeaderPaymentAccount {

    public String transCD;
    public String transDetailCD;
    public String detailTransactionDESC;

    public HasAXAAsiaPolicyHeaderPaymentAccount() {
    }
}