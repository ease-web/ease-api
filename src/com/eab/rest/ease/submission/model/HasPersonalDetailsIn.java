package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasPersonalDetailsIn {

    public String stdBusinessCD;
    public String stdOccupationCD;
    public String industryCD;
    public String occupationClassCd;
    public String citizenCD;
    public String religionCD;
    public String firstNM;
    public String lastNM;
    public String genderCD;
    public String documentTypeCD;
    public String documentID;
    public String birthDT = "";
    public String maritalStatusCD;
    public String countryOfResidenceCD;
    public String height;
    public String weight;
    public String annualIncomeAMT;
    public List<HasAddressInRef> hasAddressesIn;
    public List<HasIndividualUWInfoInRef> hasIndividualUWInfoIn;

    public HasPersonalDetailsIn() {
    }
}
