package com.eab.rest.ease.rls.application;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.eab.auth.ease.Maam;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.API_RLS_REC_APP_TRX;
import com.google.gson.JsonObject;

public class RecordApplicationService {

	public HttpPost createRecordApplicationPostRequest(String msgid, JsonObject jsonBody, String lob) throws Exception {
		Log.info("Goto RecordApplicationUtil.createApplicationPostRequest");

		String recordAppHost = EnvVariable.get("WEB_API_RLS_APP");
		String recordAppCallbackHost = EnvVariable.get("WEB_API_RLS_APP_CALLBACK");

		HttpPost request = new HttpPost(recordAppHost);
		request.addHeader("Content-Type", "application/json");

		// Headers
		request.addHeader("Authorization", "Bearer " + Maam.getToken());
		request.addHeader("x-axa-entity", "SG");
		request.addHeader("x-axa-requesting-channel", "EASE");
		request.addHeader("x-axa-contextheader-customdata-sourcesystem", "EASE");
		request.addHeader("x-axa-contextheader-customdata-targetsystem", "FileNet");
		request.addHeader("x-axa-msgid", msgid);
		request.addHeader("x-axa-initialmsgid", msgid);
		request.addHeader("x-axa-lob", ("Health".equals(lob)||"Life".equals(lob))?lob:"");
		request.addHeader("x-axa-contextheader-contextversion", "1.0");
		
		JsonObject addressingJson = new JsonObject();
		addressingJson.addProperty("replyTo", recordAppCallbackHost);

		byte[] bytesEncoded = Function.StrToBase64(addressingJson.toString());
		String base64String = Function.ByteToBase64(bytesEncoded);

		Log.debug("**** base64String=" + base64String);
		request.addHeader("x-axa-async-addressing", base64String);
		
		request.addHeader("Accept", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

	public String getPolicyNumber(JsonObject bodyJson) {
		Log.info("Goto RecordApplicationUtil.getPolicyNumber");

		String policyNumberAsString = "";
		try {
			if (bodyJson.has("recordApplicationRequest")) {
				JsonObject recordApplicationRequest = bodyJson.get("recordApplicationRequest").getAsJsonObject();
				if (recordApplicationRequest.has("policy")) {
					JsonObject policy = recordApplicationRequest.get("policy").getAsJsonObject();
					if (policy.has("policyNO")) {
						policyNumberAsString = policy.get("policyNO").getAsString();
					}
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		return policyNumberAsString;
	}

	public boolean createTransactionRecord(String audID, String headerAsString, String body) throws Exception {
		Log.info("Goto RecordApplicationService.createTransactionRecord");
		API_RLS_REC_APP_TRX trxRecordAppDB = new API_RLS_REC_APP_TRX();
		return trxRecordAppDB.create(audID, headerAsString, body);
	}

	public boolean updateTransactionRecord(String audID, String policyNumberAsString, String requestHeader,
			String requestParam, String responseHeaderAsString, String responseAsString, String requestID,
			boolean isCompleted) throws Exception {
		Log.info("Goto RecordApplicationService.updateTransactionRecord");
		API_RLS_REC_APP_TRX trxRecordAppDB = new API_RLS_REC_APP_TRX();
		return trxRecordAppDB.update(audID, policyNumberAsString, requestHeader, requestParam, responseHeaderAsString,
				responseAsString, requestID, isCompleted);
	}
}
