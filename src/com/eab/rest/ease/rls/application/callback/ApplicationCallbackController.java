package com.eab.rest.ease.rls.application.callback;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonObject;

@Path("/esub/callback")
public class ApplicationCallbackController {
	
	private ApplicationCallbackService applicationCallbackService = new ApplicationCallbackService();
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response applicationNotification(@Context HttpHeaders headers, String body) {
		Log.info("RLS RecordApplication / callback");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers, body); // for debug
		
		boolean isCompleted = false;
		Response output = null;
		
		try {
			boolean result = applicationCallbackService.createTransactionRecord(audID, RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body);

			Log.debug("*****trxAppNotiDB create result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		
		try {
			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate Json format
			if (bodyJson == null) {
				// Request body is not in JSON format
				output = applicationCallbackService.createFailureResponse(audID, "Json Syntax Incorrect;");
			} else {
				
				String errorString = applicationCallbackService.validateCallbackContent(audID, bodyJson);
				
				if(errorString.length()>0) {
					output = applicationCallbackService.createFailureResponse(audID, errorString);
				}else {
					output = applicationCallbackService.createSuccessResponse(audID);
				}
			}
		}catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		
		try {
			isCompleted = output.getStatus() == 200 ? true : false;
			
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			
			String responseAsString = output.getEntity().toString();
			boolean result = applicationCallbackService.updateTransactionRecord(audID, responseHeaderAsString, responseAsString,isCompleted);

			Log.debug("*****trxAppNoti update result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}

}
