package com.eab.rest.ease.rls.application;

import java.util.Arrays;

import com.eab.common.Log;
import com.eab.rest.ease.validator.BaseValidator;
import com.google.gson.JsonObject;

public class RecordApplicationValidator extends BaseValidator {

	@Override
	protected String validateMandatoryFields(JsonObject json, String parentKey, String level) {
//		Log.info("Goto RecordApplicationUtil.validateFields");

		// Handle AXA cases that using same key but different models
		String[] splitLevels = level.split("[.]");
		if (splitLevels.length > 2) {
			String grantparentLevel = splitLevels[splitLevels.length - 2];
			String parentLevel = splitLevels[splitLevels.length - 1];

			if (grantparentLevel.equals("hasCustomerInformationIn") && parentLevel.equals("canBeIndividual")) {
				parentKey = "canBeIndividual";
			} else if (grantparentLevel.equals("hasBeneficiaryInformationIn")
					&& parentLevel.equals("canBeIndividual")) {
				parentKey = "canBeIndividualRef";
			} else if (grantparentLevel.equals("hasBenificialOwnerDetailsIn")
					&& parentLevel.equals("canBeIndividual")) {
				parentKey = "canBeIndividualReference";
			} else if (grantparentLevel.equals("hasDependentInformationIn") && parentLevel.equals("canBeIndividual")) {
				parentKey = "canBeIndividualRef2";
			}
		}

		String[] mandatoryFields = new String[] {};
		String localErrorString = "";

		switch (parentKey) {
		case "":
			mandatoryFields = new String[] { "recordApplicationRequest" };
			break;
		case "recordApplicationRequest":
			mandatoryFields = new String[] { "policy" };
			break;
		case "policy":
			mandatoryFields = new String[] { "currencyCD", "depositAMT", "paymentModeCD", "policyEffectiveDTTM",
					"policyNO", "applicationCaptureDT", "communicationMode", "companyCD", "languageCD",
					"policySubmitDT", "paymentMethodCD", "applicationSubmitTM" };
			break;
		case "hasPolicyChargeInfoIn":
			mandatoryFields = new String[] { "chargeCD" };
			break;

		case "hasLifePolicyTransactionDetailsIn":
			mandatoryFields = new String[] { "transTypeCD", "transCurrencyCD",
					"hasLifePolicyTransactionHeaderDetailsIn" };
			break;

		case "hasLifePolicyTransactionHeaderDetailsIn":
			mandatoryFields = new String[] { "bankAccountNO" };
			break;

		case "hasCustomerInformationIn":
			mandatoryFields = new String[] { "smsTextableFlg", "dependentNO", "canBeIndividual" };
			break;

		case "canBeIndividual":
			mandatoryFields = new String[] { "lastNM", "birthDT", "hasIndividualUWInfoIn", "hasAddressesIn" };
			break;

		case "hasAddressIn":
			mandatoryFields = new String[] { "addressLine1", "addressLine2", "addressLine3", "addressLine4",
					"addressTypeInfoCD" };
			break;

		case "hasCRSInformationIn":
			mandatoryFields = new String[] { "CRSType" };
			break;

		case "hasPersonalDetailsIn":
			mandatoryFields = new String[] { "lastNM", "genderCD", "documentID", "birthDT", "hasAddressesIn",
					"hasIndividualUWInfoIn" };
			break;

		case "hasAddressInRefhasAddressInRef":
			mandatoryFields = new String[] { "addressTypeInfoCD", "phoneTypeInfoCD", "emailTypeInfoCD" };
			break;

		case "hasIndividualUWInfoInRef":
			mandatoryFields = new String[] { "healthQuestionIND", "smokingQuestionIND", "cigarettesPerDayNO",
					"avocationQuestionIND", "declineQuestionIND", "diQuestionIND", "customerProtectionDeclarationIND",
					"agentReportIND", "alcoholQuestionIND", "alcoholUnitsPerWeekNO", "tobaccoQuestionIND",
					"tobaccoUnitsPerDayNO", "previousHealthExamQuestionIND", "previousHealthExamResultIND" };
			break;

		case "isAssociatedWithLifeInsuranceProduct":
			mandatoryFields = new String[] { "insuranceProductCd", "planClassCD" };
			break;

		case "hasBeneficiaryInformationIn":
			mandatoryFields = new String[] { "canBeIndividual", "hasBeneficiaryXUoeDetailsIn" };
			break;

		case "canBeIndividualRef":
			mandatoryFields = new String[] { "lastNM", "genderCD" };
			break;

		case "hasBeneficiaryXUoeDetailsIn":
			mandatoryFields = new String[] { "relationshipToInsuredCD", "benefitDistributionPCT", "beneficiaryTypeCD" };
			break;

		case "hasProducerInformationIn":
			mandatoryFields = new String[] { "producerID", "hasAssociationWith" };
			break;

		case "hasAssociationWith":
			mandatoryFields = new String[] { "bankBranchCD", "stateRegionCD", "areaCD", "bankCD",
					"hasStaffInformationIn" };
			break;

		case "hasStaffInformationIn":
			mandatoryFields = new String[] { "channelID", "staffCD", "hasBankReferrerInfoIn" };
			break;

		case "hasBankReferrerInfoIn":
			mandatoryFields = new String[] { "leadID", "leadCategoryCD" };
			break;

		case "hasDetailsRecordIn":
			mandatoryFields = new String[] { "hasAccountDetailsIn" };
			break;

		case "hasAccountDetailsIn":
			mandatoryFields = new String[] { "allocPct" };
			break;

		case "hasInvestmentFundDetailsIn":
			mandatoryFields = new String[] { "investmentFundCd" };
			break;

		case "hasPolicyRemarkDetailsIn":
			mandatoryFields = new String[] { "lineNO", "remarks" };
			break;

		case "hasAXAAsiaPolicyPaymentAccount":
			mandatoryFields = new String[] { "statusCD", "bankNO", "branchNO", "validFromDTTM", "factoringHouseCD",
					"authProcessDT", "sectionCD", "modDT", "paymentMethodChangeFLG", "hasAXAAsiaPartyAccount" };
			break;

		case "hasAXAAsiaPartyAccount":
			mandatoryFields = new String[] { "mandateStatusCD", "payorLastNM", "accountNO", "creditCardNO",
					"cardExpirationDT", "accountBankCD", "cpfBankEffectiveDT", "cpfBankAbbr", "cpfINVAccountNO",
					"status", "processDT" };
			break;

		case "hasBenificialOwnerDetailsIn":
			mandatoryFields = new String[] { "canBeIndividual" };
			break;

		case "canBeIndividualReference":
			mandatoryFields = new String[] { "firstNM", "lastNM", "countryOfResidenceCD" };
			break;

		case "hasCRSInformationInRef1":
			mandatoryFields = new String[] { "CRSType" };
			break;

		case "hasAXAAsiaPolicyHeaderPaymentAccount":
			mandatoryFields = new String[] { "transCD", "transDetailCD", "detailTransactionDESC" };
			break;
		case "hasDependentInformationIn":
			mandatoryFields = new String[] { "dependentNO", "planCD", "classCD", "premiumAMT", "offerCD",
					"canBeIndividual" };
			break;
		case "canBeIndividualRef2":
			mandatoryFields = new String[] { "lastNM", "genderCD", "birthDT", "documentID", "stdOccupationCD" };
			break;
		case "hasCRSInformationInRef2":
			mandatoryFields = new String[] { "CRSType" };
			break;
		case "hasCreditAuthorizationDetailsIn":
			mandatoryFields = new String[] { "caEffectiveDT", "status", "accountHolderFullName", "bankID", "branchCD",
					"accountNO", "accountBankCD", "processDT" };
			break;
		case "hasPreviousPolicyDetailsIn":
			mandatoryFields = new String[] { "insuredIND", "prevInsuredIND", "dependentNO", "lifeNO" };
			break;

		default:
			break;
		}

		// validate mandatory fields
		for (String key : mandatoryFields) {
			if (!json.has(key)) {
				localErrorString += "missing " + key + ";";
			}
		}

		return localErrorString;
	}

	@Override
	protected String validateField(JsonObject json, String currentKey, String parentKey) {
//		Log.info("Goto RecordApplicationUtil.validateField");
		
		String localErrorString = "";

		boolean valid = true;
		String[] optionValues = null;

		switch (currentKey) { // check possible values
		case "lob":
			optionValues = new String[] { "Life", "Health" };
			valid = Arrays.asList(optionValues).contains(json.get("lob").getAsString());

			break;
		default:
			break;
		}
		
		if(optionValues != null) {
			valid = Arrays.asList(optionValues).contains(json.get(currentKey).getAsString());
		}

		if (!valid) {
			localErrorString += "invalid " + currentKey + ";";
		}

		// Check JSON type for array fields
		String[] jsonArrayKeys = new String[] { "hasDetailsOfPolicyIn", "hasPolicyChargeInfoIn",
				"hasCustomerInformationIn", "hasLifePolicyTransactionDetailsIn", "hasDetailsOfRisksIn",
				"forLifeUnitofExposure", "isAssociatedWithLifeInsuranceProduct", "hasNotificationsDetailsIn",
				"hasProducerInformationIn", "hasSharedProducerInformationIn", "hasDetailsRecordIn",
				"hasPolicyRemarkDetailsIn", "hasCaseUnderwriterDetailsIn", "hasPolicyAssociationDetailsIn",
				"hasAssessmentHeadersIn", "hasAXAAsiaPolicyPaymentAccount", "hasBenificialOwnerDetailsIn",
				"hasAXAAsiaPolicyHeaderPaymentAccount", "hasUnderwritingDecisionsIn", "hasDependentInformationIn",
				"hasPreviousPolicyDetailsIn", "prevPolicyNO", "hasIndividualUWInfoIn", "hasAddressesIn",
				"hasLifePolicyTransactionHeaderDetailsIn", "hasPaymentDetailsIn", "insuredPrevPolicyNO",
				"hasPersonalDetailsIn", "previousSubStdPNO", "taxIdentificationNO", "countryOfTaxResidence",
				"remarksDESC", "hasBeneficiaryInformationIn", "canBeIndividual", "hasBeneficiaryXUoeDetailsIn",
				"hasAssociationWith", "hasStaffInformationIn", "hasBankReferrerInfoIn", "hasInvestmentFundDetailsIn",
				"hasAssessmentDetailsIn", "hasAXAAsiaPartyAccount" };

		boolean isJsonArrayKey = Arrays.asList(jsonArrayKeys).contains(currentKey);

		if (isJsonArrayKey) {
			if (!json.get(currentKey).isJsonArray()) {
				if (!(parentKey.equals("hasCustomerInformationIn") && currentKey.equals("canBeIndividual"))
						&& !(parentKey.equals("hasPreviousPolicyDetailsIn") && currentKey.equals("prevPolicyNO"))) {
					localErrorString += parentKey + "." + currentKey + " should be a JSON array;";
				}
			}
		}

		return localErrorString;
	}

	@Override
	protected boolean getLevelDisplay() {
		return true;
	}

}
