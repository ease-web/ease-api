package com.eab.rest.ease.rls.jobstatus;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/rlsJobStatus")
public class rlsJobStatus {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{searchingId}")
	public Response getISJobStatus(@Context HttpHeaders headers, @PathParam("searchingId") String searchingId) {
		Log.info("Landing /rlsJobStatus");
		Log.info(searchingId);
		String audID = headers.getRequestHeader("aud-id").get(0);
		Response output = null;
		try {
			
			JSONObject jobStatus = getJobStatus(searchingId);
			
			
			if (jobStatus == null) {
				jobStatus = RlsStatusUtil.createFailObject();
				output = RestUtil.toResponse(audID, Response.Status.ACCEPTED, jobStatus.toString());
			} else {
				jobStatus.put("success", true);
				output = RestUtil.toResponse(audID, Response.Status.ACCEPTED, jobStatus.toString());
			}
			
			 
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		
		return output;
	}
	
	private JSONObject getJobStatus(String searchingId) throws Exception {
		if (searchingId.indexOf("SA") > -1) {
			return getShieldJobStatus(searchingId);
		} else {
			BATCH_SUBMISSION_REQUEST bSubReq = new BATCH_SUBMISSION_REQUEST();		
			return bSubReq.selectRLSStatus(searchingId);
		}
	}
	
	private JSONObject getShieldJobStatus(String searchingId) throws Exception {
		JSONObject json = Constant.CBUTIL.getDoc(searchingId);
		JSONObject result = new JSONObject();
		JSONObject runResult = new JSONObject();
		String concatSqlParam = new String("");
		
		if (json != null) {
			Function func = new Function();
			JsonObject targetDoc = func.transformObject(json);
			JsonArray childIdsArr = (targetDoc.has("childIds")) ? targetDoc.get("childIds").getAsJsonArray() : new JsonArray();
			BATCH_SUBMISSION_REQUEST bSubReq = new BATCH_SUBMISSION_REQUEST();
			result =  bSubReq.selectRLSStatusForShieldFailedCase(childIdsArr);
			runResult = bSubReq.selectRLSStatusForShieldRunCase(childIdsArr);
			result = (!result.has("FAIL_CNT") || !runResult.has("RUN_CNT") || (result.has("FAIL_CNT") && result.getInt("FAIL_CNT") > 0 || runResult.getInt("RUN_CNT") == 0)) ? RlsStatusUtil.createFailObject() :
				RlsStatusUtil.createSuccessObject();
			return result;
		} else {
			return RlsStatusUtil.createFailObject();
		}
		
	}
}
