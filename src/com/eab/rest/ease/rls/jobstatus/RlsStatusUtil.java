package com.eab.rest.ease.rls.jobstatus;

import org.json.JSONObject;

public class RlsStatusUtil {
	public static JSONObject createFailObject() {
		JSONObject jobStatus = new JSONObject();
		jobStatus.put("success", false);
		jobStatus.put("SUBMISSION_STATUS", "NOT_FOUND");
		return jobStatus;
	}
	
	public static JSONObject createSuccessObject() {
		JSONObject jobStatus = new JSONObject();
		jobStatus.put("success", true);
		jobStatus.put("SUBMISSION_STATUS", "C");
		return jobStatus;
	}
}
