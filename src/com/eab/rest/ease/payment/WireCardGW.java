package com.eab.rest.ease.payment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_PAYMENT_STATUPD_TRX;
import com.eab.dao.API_PAYMENT_TRX;
import com.eab.database.jdbc.DBAccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Path("/wirecard")
public class WireCardGW {
	
	private WireCardCallbackService wireCardCallbackService = new WireCardCallbackService();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPaymentPath")
	public Response getPaymentPath(@Context HttpHeaders headers, String body) {
		Log.info("Landing /getPaymentPath");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		String headerStr = RestUtil.showLogRequest(headers, body); // for debug

		boolean hasError = false;
		String msgError = "";

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jobj = null;
		Response output = null;
		String trxNo = null;
		API_PAYMENT_TRX payTrx = new API_PAYMENT_TRX();
		Connection conn = null;

		try {
			JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
			if (checkParam(jsonObject) && checkSysParam()) {
				conn = DBAccess.getConnection();

				trxNo = jsonObject.get("trxNo").getAsString();
				String method = jsonObject.get("method").getAsString();
				double amt = jsonObject.get("amount").getAsDouble();
				String cur = jsonObject.get("currency").getAsString();
				String lob = jsonObject.has("lob")?jsonObject.get("lob").getAsString():"Life";
				boolean isShield = jsonObject.has("isShield")?jsonObject.get("isShield").getAsBoolean() : false;
				
				JsonArray relatedPolicyNumbers = (jsonObject.has("relatedPolicyNumber") && jsonObject.get("relatedPolicyNumber").getAsJsonArray().size() > 0)?
							jsonObject.get("relatedPolicyNumber").getAsJsonArray() : new JsonArray();
				JsonArray relatedApplicationNumbers = (jsonObject.has("relatedApplicationNumber") && jsonObject.get("relatedApplicationNumber").getAsJsonArray().size() > 0)?
						jsonObject.get("relatedApplicationNumber").getAsJsonArray() : new JsonArray();
							
				String paytype = "";
				String transtype = "sale";
				
				String host = EnvVariable.get("WEB_API_EASYPAY2_HOST");
				String mid = EnvVariable.get("WEB_API_EASYPAY2_MID");
				String returnUrl = EnvVariable.get("WEB_API_EASYPAY2_RETURN_URL");
				String statusUrl = EnvVariable.get("WEB_API_EASYPAY2_STATUS_URL");

				if ("crCard".equals(method)) {
					paytype = "";
//					transtype = "SALE";
				} else if ("eNets".equals(method)) {
					paytype = "41";
//					transtype = "sale";
					mid = EnvVariable.get("WEB_API_EASYPAY2_ENETS_MID");
				} else if ("dbsCrCardIpp".equals(method)) {
					paytype = "";
					mid = EnvVariable.get("WEB_API_EASYPAY2_IPP_MID");
				}
				
//				String paytype = jsonObject.has("paytype")?jsonObject.get("paytype").getAsString():"";
				java.util.Date timestamp = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime();
				

				boolean createResult = false;
				if (isShield) {
					createResult = seperateShieldPaymentCase(audID, trxNo,
							jsonObject.get("appId").getAsString(),
							jsonObject.get("policyNo").getAsString(),
							lob,
							method,
							cur, amt, conn, relatedPolicyNumbers, relatedApplicationNumbers);
				} else {
					createResult = payTrx.create(audID, trxNo,
							jsonObject.get("appId").getAsString(),
							jsonObject.get("policyNo").getAsString(),
							lob,
							method,
							cur, amt, conn, null, "I");
				}
				
				
				Log.debug("create trx result:" + createResult);

				if (createResult) {
					/* mid=20090922005
					 * &ref=test-20121228-110
					 * &amt=10.00
					 * &cur=SGD
					 * &rcard=64
					 * &returnurl=http://www.telemoneyworld.com
					 * &statusurl=http://www.telemoneyworld.com.sg
					 * &version=2
					 * &validity=2012-12-30-11:22:33
					 * &signature=706e16062f19170d75457f4e15d18551 */
//					if (method == "crCard") {
					
						
						jobj = new JsonObject();
						
						DecimalFormat df = new DecimalFormat("#.00"); 
						String strAmt = df.format(amt);

						JsonObject params = new JsonObject();					
						params.addProperty("mid", mid);
						params.addProperty("ref", trxNo);
						params.addProperty("amt", strAmt);
						params.addProperty("cur", cur);
//						if ("".equals(paytype)) {
//							params.addProperty("rcard", "64");
//						}
						params.addProperty("paytype", paytype);
						params.addProperty("transtype", transtype);
						params.addProperty("returnurl", returnUrl);
						params.addProperty("statusurl", statusUrl);
						
						String hashKey = EnvVariable.get("WEB_API_EASYPAY2_SECURITY_KEY");
						if (StringUtils.isNotEmpty(hashKey)) {																		
							int validLimit = Integer.parseInt(EnvVariable.get("WEB_API_EASYPAY2_TRX_TIME_LIMIT"));
							java.util.Date validTime = new java.util.Date(timestamp.getTime() + validLimit * 60 * 1000);
							SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
//							sdformat.setTimeZone(TimeZone.getTimeZone("utc".toUpperCase()));
							String validity = sdformat.format(validTime);

							params.addProperty("version", "2");
							params.addProperty("validity", validity);
							String secStr = strAmt + trxNo + cur + mid + transtype + hashKey;
							
							Log.debug("securitySeq:" + secStr);
							params.addProperty("signature", generateSignature(secStr));
						}

						jobj.addProperty("success", 1);						
						jobj.add("params", params);
						jobj.addProperty("url", host);
						jobj.addProperty("status", "I");
						jobj.addProperty("timestamp", timestamp.getTime());
						
						Log.debug("get wc url:"+ jobj.toString());

						return Response.ok(jobj.toString()).build();
//					}
				}
			}
			Log.error("missing env?");
			output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, msgError));
		} catch (Exception ex) {
			hasError = true;
			ex.printStackTrace();
			msgError = ex.getMessage();
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, msgError));
		} finally {
			if (hasError) {
				try {
					// Update Transaction Log in Database
					payTrx.updateStatus(trxNo, "F", msgError, "", null, null, conn);
				} catch (Exception ex) {
					Log.error(ex);
				}
			}
			gson = null;
			jobj = null;
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (Exception e) {
			}
			conn = null;
		}

		return output;
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/callback")
	public Response updateStatus(@Context HttpHeaders headers, MultivaluedMap<String, String> params) {
		Log.info("Landing /ccStatusUpdate");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter
//		String headerStr = RestUtil.showLogRequest(headers, body); // for debug
		java.util.Date timestamp = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime();
		
		Response output = null;
		Connection conn = null;
		boolean result = false;
		boolean audCreated = false;
		String trxNo = "MISSING REFERENCE NO";
		JSONObject logObj = new JSONObject();

		API_PAYMENT_STATUPD_TRX psDao = new API_PAYMENT_STATUPD_TRX();
		
		String mid = null;
		
		try {

			if (params.containsKey("TM_RefNo")) {
				trxNo = params.getFirst("TM_RefNo");
				
				conn = DBAccess.getConnection();
				audCreated = psDao.create(audID, trxNo, conn);
	
				String approvalCode = params.getFirst("TM_ApprovalCode");
				mid = params.getFirst("TM_MCode");
				String status = params.getFirst("TM_Status");
				
				String amt = params.getFirst("TM_DebitAmt");
				String cur = params.getFirst("TM_Currency");
				String paytype = params.getFirst("TM_PaymentType");
				String transtype = params.getFirst("TM_TrnType");
				String errorCode = params.getFirst("TM_Error");
				String errorMsg = params.getFirst("TM_ErrorMsg");
				String signature = params.getFirst("TM_Signature");
				String hashKey = EnvVariable.get("WEB_API_EASYPAY2_SECURITY_KEY");
				String receiptNo = params.getFirst("TM_ApprovalCode");
				

				// create object for log
				logObj.put("TM_ApprovalCode", receiptNo);
				logObj.put("TM_MCode", mid);
				logObj.put("TM_Status", status);
				
				logObj.put("TM_DebitAmt", amt);
				logObj.put("TM_Currency",  cur);
				logObj.put("TM_PaymentType",  paytype);
				logObj.put("TM_TrnType", transtype);
				logObj.put("TM_Error", errorCode);
				logObj.put("TM_ErrorMsg", errorMsg);
				logObj.put("TM_Signature", signature);
				
			
				String secStr = "", secSeq = "";
				if (StringUtils.isNotEmpty(hashKey)) {
					secSeq = amt + trxNo + cur + mid + transtype + status + errorCode + hashKey;
					secStr = generateSignature(secSeq);
				}
				if (signature != null) {
					signature = signature.toUpperCase();
				}
				Log.debug("*** signature="+signature);
				
				logObj.put("secSeq", secSeq);
				logObj.put("secStr", secStr);

				if (StringUtils.isEmpty(secStr) || (StringUtils.isNotEmpty(signature) && signature.equals(secStr))) {
					JsonObject bodyJson = new JsonObject();

					bodyJson.addProperty("TM_DebitAmt", amt);
					bodyJson.addProperty("TM_Currency", cur);
					bodyJson.addProperty("TM_PaymentType", paytype);
					bodyJson.addProperty("TM_ApprovalCode", approvalCode);
					
					API_PAYMENT_TRX payTrx = new API_PAYMENT_TRX();
					JSONObject paymentRecord = payTrx.selectPaymentRecordByTrxNo(trxNo);

					Log.debug("*** trxNo="+trxNo);
					Log.debug("*** trx="+paymentRecord);
					if (paymentRecord != null) {
						String trxStatus = wireCardCallbackService.convertStatus(status, errorCode);
						
						try {
							String appId = paymentRecord.has("APP_ID")?paymentRecord.getString("APP_ID"):null;
							Log.info("*** set trxEnquiryStatus:"+  appId);
	
							// update application in couchbse
							if (StringUtils.isNotEmpty(appId)) {
								JSONObject json = Constant.CBUTIL.getDoc(appId);
								JSONObject payment = json.has("payment")?json.getJSONObject("payment"):null;
								
								boolean isInitialPaymentCompleted = json.has("isInitialPaymentCompleted")?json.getBoolean("isInitialPaymentCompleted"):false; 
								
								if (payment != null && !isInitialPaymentCompleted) {
									JSONObject paymentResult = new JSONObject();
									paymentResult.put("trxStatus", trxStatus);
									paymentResult.put("trxRemark", errorMsg);
									paymentResult.put("trxAmount", amt);
									paymentResult.put("trxCcy", cur);
									paymentResult.put("trxTime", timestamp.getTime());
									paymentResult.put("trxPayType", paytype);
									paymentResult.put("receiptNo", receiptNo);
	
					                if ("I".equals(trxStatus) ) {
					                	paymentResult.put("trxStatusRemark", "O");
					                } else {
					                	paymentResult.put("trxStatusRemark", trxStatus);
					                }
					                
					                if(payment.has("trxEnquiryStatus") && "No".equals(payment.getString("trxEnquiryStatus"))) {
					                	paymentResult.put("trxEnquiryStatus", "");
									}
					                
					                boolean updateSuccess = wireCardCallbackService.updateApplication(appId, json, payment, paymentResult, status);
					                if (!updateSuccess) {
					                	throw new Exception("WireCard Callback Update application Json Failed :" + appId);
					                }
					                
								} else {
									if (isInitialPaymentCompleted) {
										throw new Exception("duplicate update on payment complated: new status = " + status);
									}  else {
										throw new Exception("Invalid Application :" + appId);
									}
								}
							} else {
								throw new Exception("Update application failure "+  appId);
							}
						} catch (Exception ex) {
							throw new Exception("Update application failure " + ex.getMessage());
						}
						
						String relatedPolicyNumbersString = paymentRecord.has("RELATED_POLICY_NUMBERS")?paymentRecord.getString("RELATED_POLICY_NUMBERS"):null;

						if(status.equals("YES")) {
							
							JsonArray pendingToSendJsonArray = new JsonArray();
							
							if(relatedPolicyNumbersString == null) { // Non-shield case
								
								JsonObject paymentRecordInGsonFormat = Function.validateJsonFormatForString(paymentRecord.toString());
								
								if(paymentRecordInGsonFormat != null) {
									pendingToSendJsonArray.add(paymentRecordInGsonFormat);
								}
								
							}else { //shield case
								JsonObject relatedPolicyNumbersObject = Function.validateJsonFormatForString(relatedPolicyNumbersString);
								
								JsonArray relatedPolicyNumberArray = relatedPolicyNumbersObject.has("Policy_Numbers")?relatedPolicyNumbersObject.get("Policy_Numbers").getAsJsonArray():null;
								
								if(relatedPolicyNumberArray!=null) {
									int i = 0;
									String[] policyStringArray = new String[relatedPolicyNumberArray.size()];
									for(JsonElement element : relatedPolicyNumberArray) {
										String policyNumberString = element.getAsString();
										
										policyStringArray[i] = policyNumberString;
										i++;
									}
									
									if(policyStringArray.length >0) {
										JSONArray paymentRecordArray = payTrx.selectPaymentRecordsByPolicyNumbers(policyStringArray);
										
										if(paymentRecordArray !=null) {
											JsonArray paymentRecordArrayGson = Function.validateJsonArrayFormatForString(paymentRecordArray.toString());
											pendingToSendJsonArray.addAll(paymentRecordArrayGson);
										}
									}
								}
								
							}
							
							for(JsonElement element : pendingToSendJsonArray) {
								JsonObject jsonObj = element.getAsJsonObject();
								
								//Call RSL Record document					
								String policyNO = jsonObj.has("POLICY_NO")?jsonObj.get("POLICY_NO").getAsString():null;
								String amount = jsonObj.has("AMOUNT")?jsonObj.get("AMOUNT").getAsString():null;
								String currency = jsonObj.has("CURRENCY")?jsonObj.get("CURRENCY").getAsString():null;
								String lob = jsonObj.has("POL_TYPE")?jsonObj.get("POL_TYPE").getAsString():null;
								String method = paymentRecord.has("METHOD")?paymentRecord.getString("METHOD"):null;
								
								JsonObject recordPaymentJson = PaymentSubmissionUtil.createRecordPaymentJson(policyNO, amount, currency, lob, PaymentSubmissionUtil.convertPaymentRemarksByPayMehtod(method), false);
								Log.debug("*** recordPaymentJson="+recordPaymentJson.toString());
								
								HttpPost postRequest = PaymentSubmissionUtil.createRecordPaymentPostRequest(recordPaymentJson);
								
								Response recordPaymentOutput = HttpUtil.send(postRequest);
								String responseAsString = recordPaymentOutput.getEntity().toString();
								Log.debug("****postToRecordPaymentAPI responseAsString=" + responseAsString);
								
							}
							
						}
						
						// update payment trx if not error 
						result = payTrx.updateStatus(trxNo, trxStatus, errorMsg, bodyJson.toString(), null, null, conn);
						
					} else {
//						Log.error("Invalid reference number :" + trxNo);
//						output = Response.status(Response.Status.BAD_REQUEST).build();
						throw new Exception("Invalid reference number :" + trxNo);
					}
				} else {
//					Log.error();
//					output = Response.status(Response.Status.BAD_REQUEST).build();
					throw new Exception("Signature does not match:" + signature + " - " + secStr);
				}
				
				psDao.updateStatus(audID, "success", logObj.toString(), conn);
			} else {
				throw new Exception("Invalid reference number :" + trxNo);
			}
		} catch (Exception e) {
			Log.error(e);
			try {
				if (!audCreated) {
					audCreated = psDao.create(audID, trxNo, "failure", e.getMessage() + " - request para:"  + logObj.toString(), conn);
				} else {
					psDao.updateStatus(audID, "failure", e.getMessage()+ " - request para:"  + logObj.toString(), conn);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				Log.error(e1);
			}
			output = Response.status(Response.Status.BAD_REQUEST).build();
		} finally {

			// return acknowledge to wirecard
			String ack = "NO";
			if (result) {
				ack = "YES";
			}
			
			String respStr = "mid="+mid
						+"&ref=" + trxNo
						+"&ack=" + ack;
			
			output = Response.ok(respStr).build();
			
			try {
				if (conn != null) {
					if (!audCreated) {
						psDao.create(audID, trxNo, "failure", "Invalid Request.", conn);
					}
					conn.close();
					conn = null;
				}
			} catch (Exception ex) {
				Log.error(ex);
			}
		}

		return output;
	}
	
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getPaymentState")
    public Response getPaymentState(@Context HttpHeaders headers, String body) {
		Log.info("Landing /getPaymentState");
		
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter
		String headerStr = RestUtil.showLogRequest(headers, body); // for debug

		boolean hasError = false;
		String msgError = "";

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Response output = null;
		String trxNo = null;
		API_PAYMENT_TRX payTrx = new API_PAYMENT_TRX();
		Connection conn = null;
		
		JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
		
		Log.debug("jsonObject:"+ jsonObject.toString());
		if (jsonObject.has("trxNo")) {
			trxNo = jsonObject.get("trxNo").getAsString();
			Log.debug("Reteieve transcation no:"+ trxNo);
			try {
				conn = DBAccess.getConnection();
				JsonObject doc = payTrx.retrieveByTrxNo(trxNo, conn);
	            if (doc != null) {
					Log.debug("Reteieve transcation record:"+ doc.toString());
					
	            		JsonObject result = new JsonObject();
	            		result.addProperty("success", true);
	            		result.add("trxRecord", doc);
	                return RestUtil.toResponse(audID, Response.Status.OK, result.toString());
	            }
				Log.debug("Reteieve transcation failure:"+ trxNo);
	            output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Failure to retrieve Transaction"));
			} catch (Exception ex) {
				hasError = true;
				msgError = ex.getMessage();
				Log.error("Get status failure:"+trxNo);
				Log.error(ex);
				output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
						RestUtil.genErrorJsonStr(false, msgError));
			} finally {
				gson = null;
				try {
					if (conn != null && !conn.isClosed())
						conn.close();
				} catch (Exception ex) {
					Log.error(ex);
				}
				conn = null;
			}
        }
        Log.debug("failure to reteieve transcation.:"+ trxNo);
        return output;
    }

	private boolean checkParam(JsonObject jsonObject) {
		return jsonObject.has("amount") && jsonObject.has("currency") && jsonObject.has("trxNo")
				&& jsonObject.has("method") && jsonObject.has("policyNo");
	}

	private boolean checkSysParam() {
		return StringUtils.isNotEmpty(EnvVariable.get("WEB_API_EASYPAY2_HOST"))
				&& StringUtils.isNotEmpty(EnvVariable.get("WEB_API_EASYPAY2_MID"))
				&& StringUtils.isNotEmpty(EnvVariable.get("WEB_API_EASYPAY2_RETURN_URL"))
				&& StringUtils.isNotEmpty(EnvVariable.get("WEB_API_EASYPAY2_STATUS_URL"))
				;
	}
	
	private String generateSignature(String data) throws NoSuchAlgorithmException {
		MessageDigest hash = MessageDigest.getInstance("SHA-512");
		if (StringUtils.isNotEmpty(data)) {
			hash.update(data.getBytes());
			byte result[] = hash.digest();
			StringBuffer signature = new StringBuffer();
			for (int i = 0; i < result.length; i++) {
				String s = Integer.toHexString(result[i]);
				int length = s.length();
				if (length >= 2) {
					signature.append(s.substring(length - 2, length));
				} else {
					signature.append("0");
					signature.append(s);
				}
			}
			Log.debug("encoded signature:" + signature.toString().toUpperCase());
			return signature.toString().toUpperCase();
		}
		return null;
	}
	
	private boolean seperateShieldPaymentCase(String audID, String trxNo, String appId, String policyNo, String lob, String method, String cur, 
			double amt, Connection conn, JsonArray relatedPolicyNumbers, JsonArray relatedApplicationNumbers) throws SQLException, Exception {
		
		Log.info("Seperate The Payment transcation for RLS with different Policy Numbers");
		API_PAYMENT_TRX payTrx = new API_PAYMENT_TRX();
		JsonArray policyNumbersWithCash = new JsonArray();
		try {			
			boolean createChildResult = true;
			
			if (relatedApplicationNumbers.size() > 0) {
				String childPostfix = String.valueOf(Calendar.getInstance(TimeZone.getTimeZone(Constant.ZONE_ID)).getTimeInMillis());
				
				for (JsonElement ele: relatedApplicationNumbers) {
					boolean insertDbChildresult = true;
					String childAppId = ele.getAsString();
					JsonObject childJson = Function.transformObject(Constant.CBUTIL.getDoc(childAppId));
					
					if (childJson != null) {
						String childPolicyNo = (childJson.has("policyNumber")) ? childJson.get("policyNumber").getAsString() : null;
						double cashPortion = Function.transformEleToDouble(Function.getAsPath(childJson, "payment/cashPortion"));
						
						// Insert to Table for sending to RLS when there is cash portion paid.

						if (cashPortion > 0 && childPolicyNo !=null) {
							insertDbChildresult = payTrx.create(audID, childPolicyNo + "-" + childPostfix,
									appId,
									childPolicyNo,
									lob,
									method,
									cur, cashPortion, conn, null, "X");
							policyNumbersWithCash.add(childPolicyNo);
						}
						
						if (childPolicyNo == null || insertDbChildresult == false) {
							createChildResult = false;
						}
					} else {
						createChildResult = false;
					}
				}
			}

			JsonObject relatedPolNums = new JsonObject();
			relatedPolNums.add("Policy_Numbers", policyNumbersWithCash);
			
			boolean createMasterResult = payTrx.create(audID, trxNo,
					appId,
					policyNo,
					lob,
					method,
					cur, amt, conn, relatedPolNums.toString(), "I");
			
			return createMasterResult && createChildResult;
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (Exception e) {
			}
			conn = null;
		}
	}

}
