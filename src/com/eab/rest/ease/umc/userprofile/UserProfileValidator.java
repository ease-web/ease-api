package com.eab.rest.ease.umc.userprofile;

import java.util.Arrays;

import org.apache.commons.validator.routines.EmailValidator;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.rest.ease.validator.BaseValidator;
import com.google.gson.JsonObject;

public class UserProfileValidator extends BaseValidator {
	
	@Override
	protected String validateMandatoryFields(JsonObject json, String parentKey, String level) {
		Log.info("Goto UserProfileValidator.validateMandatoryFields");

		String[] mandatoryFields = new String[] {};
		String localErrorString = "";

		switch (parentKey) {
		case "":
			mandatoryFields = new String[] { "userId", "fullName", "email", "agentCode", "userStatus" };
			break;
		default:
			break;
		}

		for (String key : mandatoryFields) { // validate mandatory fields
			if (!json.has(key)) {
				localErrorString += "missing " + key + ";";
			}
		}

		return localErrorString;
	}

	@Override
	protected String validateField(JsonObject json, String currentKey, String parentKey) {
		Log.info("Goto UserProfileValidator.validateField");

		String localErrorString = "";

		boolean valid = true;
		String[] optionValues = null;

		switch (currentKey) { // check possible values
		case "email":
			valid = EmailValidator.getInstance().isValid(json.get(currentKey).getAsString());
			break;
		case "userStatus":
			optionValues = new String[] { "A", "T" };
			break;
		case "title":
			optionValues = new String[] { "Ms.", "Mrs.", "Mr." };
			break;
		case "userRole":
			optionValues = new String[] { "A", "MA", "MM1", "MM2" };
			break;
		case "userCat":
			optionValues = new String[] { "AGENT-A", "AGENT-MA", "AGENT-MM1", "AGENT-MM2", "BF-MM2", "BF-MM1",
					"BF-MA", "BF-A", "PO-MA", "PO-MM1", "PO-MM2", "PO-A", "WS-A", "WS-MM2", "WS-MA", "WS-MM1", "GI-MM1",
					"GI-MA", "GI-A", "GI-MM2", "BA-MM2", "BA-MM1", "BA-MA", "BA-A" };
			break;
		case "faAdvisorRole":
			optionValues = new String[] { "DM", "DS", "DA" };
			break;
		case "authGroup":
			optionValues = new String[] { "AG001", "FU004", "WS005", "PO006", "SY002", "BF003", "GI001",
			"BA001" };
			break;
		case "authorizedHI":
			optionValues = new String[] { "Y", "N" };
			break;
		case "authorizedM8":
			optionValues = new String[] { "Y", "N" };
			break;
		case "authorizedM8A":
			optionValues = new String[] { "Y", "N" };
			break;
		case "authorizedIFAST":
			optionValues = new String[] { "Y", "N" };
			break;
		case "userType":
			optionValues = new String[] { "AGENT", "BF", "PO", "WS", "GI", "BA" };
			break;
		case "proxyStartDate":
			valid = Function.dateFormatValidator(json.get(currentKey).getAsString(), Constant.DATETIME_PATTERN_AS_DATE_ONLY);
			break;
		case "proxyEndDate":
			valid = Function.dateFormatValidator(json.get(currentKey).getAsString(), Constant.DATETIME_PATTERN_AS_DATE_ONLY);
			break;
		case "suspStartDate":
			valid = Function.dateFormatValidator(json.get(currentKey).getAsString(), Constant.DATETIME_PATTERN_AS_DATE_ONLY);
			break;
		case "suspEndDate":
			valid = Function.dateFormatValidator(json.get(currentKey).getAsString(), Constant.DATETIME_PATTERN_AS_DATE_ONLY);
			break;
		default:
			break;
		}
		
		if(optionValues != null) {
			valid = Arrays.asList(optionValues).contains(json.get(currentKey).getAsString());
		}

		if (!valid) {
			localErrorString += "invalid " + currentKey + ";";
		}

		return localErrorString;
	}
	
	@Override
	protected boolean getLevelDisplay() {
		return false;
	}
}
