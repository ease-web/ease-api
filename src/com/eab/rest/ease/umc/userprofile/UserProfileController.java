package com.eab.rest.ease.umc.userprofile;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonObject;

@Path("/userprofile/update")
public class UserProfileController {
	
	private UserProfileService userProfileService = new UserProfileService();
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response userProfileController(@Context HttpHeaders headers, String body) {
		Log.info("UserProfileController / userProfileController");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers, body); // for debug
		
		Response output = null;
		
		boolean isCompleted = false;
		
		try {
			boolean createResult = userProfileService.createTransactionRecord(audID, RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body);
			Log.debug("*****trxProfileDB create result="+createResult);
			
			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate JSON format

			if (bodyJson == null) { // Request body is not in JSON format
				output = userProfileService.createErrorResponse(audID, "Json Syntax Incorrect;");
			} else {
				output = userProfileService.validateContent(audID, bodyJson); // validate incoming user profile
			}
			
		}catch(Exception ex) {
			Log.error(ex);
			output = userProfileService.createErrorResponse(audID, ex.getMessage());
		}
		
		try {
			String responseAsString = output.getEntity().toString(); //update response to DB
			isCompleted = output.getStatus() == 200? true:false;
			
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			boolean updateResult = userProfileService.updateTransactionRecord(audID, responseHeaderAsString, responseAsString,isCompleted);
			Log.debug("*****trxProfileDB update result="+updateResult);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}
}
