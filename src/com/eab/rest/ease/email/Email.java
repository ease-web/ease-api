package com.eab.rest.ease.email;

import java.io.StringReader;
import java.sql.Connection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_EMAIL_TRX;
import com.eab.database.jdbc.DBAccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

/*
 * URL--> http://ease-api:8080/ease-api/email/sendEmail
 * Expected Body -->
 * {
 *   "from": "xxxxx@eabystems.com",
 *   "to": "abcdefg@eabystems.com",
 *   "cc": "strvwxg@eabystems.com",
 *   "title": "email subject",
 *   "content": "email body",
 *   "attachments": [
 *     {"fileName": "attachment file name", "data": "base64", "cid": "cid_data"},
 *     {"fileName": "attachment file name", "data": "base64", "cid": "cid_data"}
 *   ]}
 * }  
 */

@Path("/email")
public class Email {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendEmail")
	public Response sendEmail(@Context HttpHeaders headers, String body) {
		Log.info("Landing /sendEmail");
		String audID = headers.getRequestHeader("aud-id").get(0);		// provided by filter
		
		String headerStr = RestUtil.showLogRequest(headers, body);		// for debug
		
		boolean hasError = false;
		String msgError = "";
		Gson gson = new GsonBuilder().setLenient().create();
		JsonElement jelem = null;
		JsonObject jobj = null;
		Connection conn = null;
		Response output = null;
		JsonElement jattach = null;
		JsonArray jarray = null;
		EmailValidator evalid = new EmailValidator();
		API_EMAIL_TRX dao = new API_EMAIL_TRX();
		List<String> filenameList = null;
		
		try {
			conn = DBAccess.getConnection();
			
			// Convert JSON Body to Object
			if (!hasError) {
				Gson mygson = new Gson();
				JsonReader reader = new JsonReader(new StringReader(body));
				reader.setLenient(true);
				jelem = mygson.fromJson(reader, JsonElement.class);
//				jelem = gson.fromJson(body, JsonElement.class);
				if (jelem != null) {
					jobj = jelem.getAsJsonObject();
					if (jobj == null) {
						hasError = true;
						msgError += "INVALID BODY;";
					}
				} else {
					hasError = true;
					msgError += "EMPTY BODY;";
				}
			}

			String from = jobj.get("from")!=null?jobj.get("from").getAsString():"";
			String to = jobj.get("to")!=null?jobj.get("to").getAsString():"";
			String cc = jobj.get("cc")!=null?jobj.get("cc").getAsString():"";
			String title = jobj.get("title")!=null?jobj.get("title").getAsString():"";
			String content = jobj.get("content")!=null?jobj.get("content").getAsString():"";
			jattach = jobj.get("attachments")!=null?jobj.get("attachments"):null;
			jarray = jattach!=null?jattach.getAsJsonArray():null;
			
			// Extract Filename from Attachment
			filenameList = EmailUtil.getAttachFilename(jarray);
			String nameList = null;
			if (filenameList != null && filenameList.size() > 0)
				nameList = String.join(",", filenameList);
			
			// Create Transaction Log into Database
			dao.create(audID, from, to, title, cc, nameList, conn);
			
			// Get known JSON objects
			if (!hasError) {
				
				// Parmater Validation
				if (!hasError) {
					if (from == null || !evalid.validate(from)) {
						hasError = true;
						msgError += "INVALID from;";
					}
					
					if (to == null) {
						hasError = true;
						msgError += "INVALID to;";
					} else {
						String[] splitTo = to.trim().split("\\s*,\\s*");
						for (String temp : splitTo) {
							if (!evalid.validate(temp)) {
								hasError = true;
								msgError += "INVALID to;";
								break;
							}
						}
					}
					
					if (cc != null && cc.length() > 0) {
						String[] splitCc = cc.trim().split("\\s*,\\s*");
						for (String temp : splitCc) {
							if (!evalid.validate(temp)) {
								hasError = true;
								msgError += "INVALID cc;";
								break;
							}
						}
					}
					
					if (title == null || title.length() == 0) {
						hasError = true;
						msgError += "INVALID title;";
					}
				}
				
				// Send Email
				if (!hasError) {
					String mailError = EmailUtil.sendEmail(from, to, cc, title, content, jarray);
					
					if (mailError == null || mailError.length() == 0) {
						hasError = false;
						msgError = "";
						
						// Update Transaction Log in Database
						dao.update(audID, "C", " ", conn);
					} else {
						hasError = true;
						msgError += mailError;
					}
				}
			}
			
			if (hasError) {
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, msgError));
			}
		} catch (Exception ex) {
			Log.error(ex);
			hasError = true;
			msgError = ex.getMessage();
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, msgError));
		} finally {
			if (hasError) {
				try {
					// Update Transaction Log in Database
					dao.update(audID, "F", msgError, conn);
				} catch(Exception ex) {
					Log.error(ex);
				}
			}
			gson = null;
			jelem = null;
			jobj = null;
			jattach = null;
			jarray = null;
			evalid = null;
			dao = null;
			
			try {if (conn != null && !conn.isClosed()) conn.close();} catch(Exception e) {}
			conn = null;
		}
		
		return output;
	}
}
