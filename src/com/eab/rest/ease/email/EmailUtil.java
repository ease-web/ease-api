package com.eab.rest.ease.email;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.time.StopWatch;

import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class EmailUtil {
	public static String sendEmail(String from, String to, String subject, String text) throws AddressException, MessagingException {
		return sendEmail(from, to, null, subject, text, null);
	}
	
	public static String sendEmail(String from, String to, String subject, String text, JsonArray attachs) throws AddressException, MessagingException {
		return sendEmail(from, to, null, subject, text, attachs);
	}
	
	public static String sendEmail(String from, String to, String cc, String subject, String text, JsonArray attachs) throws AddressException, MessagingException {
		Log.info("sendEmail | Start");
		BodyPart messageBodyPart = null;
		Multipart multipart = null;
		JsonObject jobj = null;
		Properties props = new Properties();
		StopWatch stopWatch = new StopWatch();
		boolean status = false;
		String mailError = null;
		
		try {
			stopWatch.start();
			String host = EnvVariable.get("WEB_API_SMTP_HOST");
			String port = EnvVariable.get("WEB_API_SMTP_PORT");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			Log.debug("mail.smtp.host=" + host);
			Log.debug("mail.smtp.port=" + port);
			
			Session session = Session.getInstance(props);
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			Log.debug("from=" + from);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			Log.debug("to=" + to);
			if (cc != null && cc.length() > 0) {
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
				Log.debug("cc=" + cc);
			}
//			message.setSubject(subject);
			message.setSubject(MimeUtility.encodeText(subject, "utf-8", "Q"));
			
			Log.debug("subject=" + subject);
			
			if (attachs != null && attachs.size() > 0) {
				// Create a multipart for message
				multipart = new MimeMultipart();
	
				// Set the actual message if having
				if (text != null && text.length() > 0) {
					messageBodyPart = new MimeBodyPart();		// Create the message part
//					messageBodyPart.setText(text);
					messageBodyPart.setContent(text, "text/html; charset=utf-8");		// HTML Body
					Log.debug("text(1)=" + text);
					multipart.addBodyPart(messageBodyPart);		// Set text message part
				}
				
				for(JsonElement attach : attachs) {
					jobj = attach.getAsJsonObject();
					String fileName = jobj.get("fileName").getAsString();
					String data = jobj.get("data").getAsString();
					String cid = jobj.get("cid")!=null?jobj.get("cid").getAsString():null;
					
					
					if (cid != null) {
						messageBodyPart = new MimeBodyPart();	
						messageBodyPart.setContent(text, "text/html; charset=utf-8");
						messageBodyPart.setText("image");
						messageBodyPart.setHeader("Content-ID", cid);
						byte[] encodedImage = DatatypeConverter.parseBase64Binary(data);
						DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(encodedImage, "image/*"));
						messageBodyPart.setDataHandler(dataHandler);
					} else {
						// For Base64 attachment
						messageBodyPart = new PreencodedMimeBodyPart("base64");	
						messageBodyPart.setText(data);
					}
					
					messageBodyPart.setFileName(fileName);
					Log.debug("attach --> fileName=" + fileName);
					
					multipart.addBodyPart(messageBodyPart);
				}
				
				// Send the complete message parts
				message.setContent(multipart);
			} else {
//				message.setText(text);
				message.setContent(text, "text/html; charset=utf-8");		// HTML Body
				Log.debug("text(2)=" + text);
			}
			
			Transport.send(message);
			status = true;
			
			Log.info("sendEmail | Sent success");
		} catch(Exception e) {
			status = false;
			Log.error(e, false);		//Don't send error in Email function.  This is common sense.
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			mailError = errors.toString();
			if (mailError != null && mailError.length() > 495)
				mailError = mailError.substring(0, 494) + "...";
			errors = null; 
		} finally {
			stopWatch.stop();
			Log.info("sendEmail | End, Elapsed time: " + stopWatch.getTime() + " mills");
			
			messageBodyPart = null;
			multipart = null;
			jobj = null;
			props = null;
			stopWatch = null;
		}
		
		return mailError;
	}
	
	public static List<String> getAttachFilename(JsonArray attachs) {
		List<String> output = null;
		JsonObject jobj = null;
		
		try {
			if (attachs != null && attachs.size() > 0) {
				output = new ArrayList<String>();
				
				for(JsonElement attach : attachs) {
					jobj = attach.getAsJsonObject();
					output.add(jobj.get("fileName").getAsString());
				}
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			jobj = null;
		}
		
		return output;
	}
	
}
