package com.eab.rest.ease.wfi.document.callback;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonObject;

@Path("/uploaddoc/callback")
public class DocumentsCallbackController {
	
	private DocumentsCallbackService documentsCallbackService = new DocumentsCallbackService();
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response documentsCallbackController(@Context HttpHeaders headers, String body){
		Log.info("DocumentsCallbackController / documentsCallbackController");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers, body); // for debug
		
		boolean isCompleted = false;
		
		Response output = null;
		
		try {
			//DB Data
			boolean result = documentsCallbackService.createTransactionRecord(audID, RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body);
			
			Log.debug("*****trxDocNotiDB create result="+result);
		} catch (Exception ex) {
			Log.error(ex);
			output = documentsCallbackService.createErrorResponse(audID, ex.getMessage(), "", "Exception");
		}
		
		String policyNumberAsString = "";
		JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate JSON format

		if (bodyJson == null) {
			// Request body is not in JSON format
			output = documentsCallbackService.createErrorResponse(audID, "Json Syntax Incorrect;", "", "Exception");
		} else {
			policyNumberAsString = documentsCallbackService.getPolicyNumber(bodyJson);
			output = documentsCallbackService.createSuccessResponse(audID);
		}
		
		try {
			
			isCompleted = output.getStatus() == 200? true:false;
			
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			String responseAsString = output.getEntity().toString();
			
			//DB Data
			boolean result = documentsCallbackService.updateTransactionRecord(audID, policyNumberAsString, responseHeaderAsString, responseAsString, isCompleted);
			
			Log.debug("*****trxDocNotiDB update result="+result);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return output;
	}

}
