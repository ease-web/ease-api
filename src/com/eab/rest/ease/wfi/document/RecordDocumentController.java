package com.eab.rest.ease.wfi.document;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.http.client.methods.HttpPost;

import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonObject;

@Path("/document")
public class RecordDocumentController {
	
	private RecordDocumentService recordDocumentService = new RecordDocumentService();
	private RecordDocumentValidator recordDocumentValidator = new RecordDocumentValidator();
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response recordDocumentController(@Context HttpHeaders headers, String body){
		Log.info("RecordDocumentController / recordDocumentController");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers, body); // for debug
		
		Response output = null;
		boolean isCompleted = false;
		
		String bodyAsString = "";
		String policyNumberAsString = "";
		
		JsonObject bodyJsonForAXA = null;
		HttpPost postRequest = null;
		
		try {
			JsonObject jsonBodyForTransaction = Function.validateJsonFormatForString(body); // create JSON object from original body
			
			if(jsonBodyForTransaction != null) {
				jsonBodyForTransaction.remove("contentData"); //remove contentData for transaction 
				bodyAsString = jsonBodyForTransaction.toString();
				
				if(jsonBodyForTransaction.has("policyNumber")) { //try to get the policy number
					try {
						policyNumberAsString = jsonBodyForTransaction.get("policyNumber").getAsString(); // get policyNumber
					} catch (Exception ex) {
						Log.error(ex);
					}
				}
			}else {
				bodyAsString = body; //Use original body for transaction if it's not in JSON format
			}
			
			boolean result = recordDocumentService.createTransactionRecord(audID, RestUtil.convertHeaderToStr(headers.getRequestHeaders()), bodyAsString, policyNumberAsString); //Create transaction
			Log.debug("*****trxRecordDocDB create result="+result);
			
			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate JSON format

			if (bodyJson == null) {
				// Request body is not in JSON format
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, "Json Syntax Incorrect;"));
			}else {

				String errorString = recordDocumentValidator.validate(bodyJson);
				boolean isFail = errorString.length() > 0 ? true : false;
				
				if (isFail) {
					//Invalid content
					output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, errorString));
				} else {
					try {
						bodyJsonForAXA = recordDocumentService.convertJsonBodyForRecordDocument(bodyJson);
						boolean isHealth = "MI".equals(bodyJson.get("channelCode").getAsString()) ? true : false;
						postRequest = recordDocumentService.createDocumentPostRequest(audID, bodyJsonForAXA, isHealth);
						
						output = HttpUtil.send(postRequest); // Send document to AXA
						
						Log.debug("***output="+output);
					} catch (Exception ex) {
						Log.error(ex);
						errorString += "cannot convert JsonBody for RecordDocument:"+ ex.getMessage();
						output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, errorString));
					}
				}
			}
			
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		
		try {
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			String responseAsString = output.getEntity().toString(); //update response to DB
			Log.debug("*****responseAsString="+responseAsString);
			
			String requestHeader = postRequest == null? "":HttpUtil.convertHeaderToStr(postRequest.getAllHeaders());
			String requestParam = "";
			if(bodyJsonForAXA !=null) {
				if(bodyJsonForAXA.has("RecordDocumentRequest")) {
					//remove contentData for transaction
					JsonObject recordDocumentRequest = bodyJsonForAXA.get("RecordDocumentRequest").getAsJsonObject();
					recordDocumentRequest.remove("contentData");
					bodyJsonForAXA.add("RecordDocumentRequest", recordDocumentRequest);
					requestParam = bodyJsonForAXA.toString();
				}
			}
			
			isCompleted = output.getStatus() == 200? true:false;
			
			boolean updateResult = recordDocumentService.updateTransactionRecord(audID, requestHeader, requestParam, responseHeaderAsString, responseAsString,isCompleted);
			Log.debug("*****trxProfileDB update result="+updateResult);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return output;
	}

}
