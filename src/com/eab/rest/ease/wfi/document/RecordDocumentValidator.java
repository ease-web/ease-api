package com.eab.rest.ease.wfi.document;

import java.util.Arrays;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.rest.ease.validator.BaseValidator;
import com.google.gson.JsonObject;

public class RecordDocumentValidator extends BaseValidator {

	@Override
	protected boolean getLevelDisplay() {
		return false;
	}

	@Override
	protected String validateMandatoryFields(JsonObject json, String parentKey, String level) {
		Log.info("Goto RecordDocumentValidator.validateMandatoryFields");

		String[] mandatoryFields = new String[] {};
		String localErrorString = "";

		switch (parentKey) {
		case "":
			mandatoryFields = new String[] { "MIMEType", "creationDate", "documentTitle", "documentType", "insuredName","policyNumber","primaryDocumentFlag","productType","channelCode","contentData"};
			break;
		default:
			break;
		}
		
		for (String key : mandatoryFields) { // validate mandatory fields
			if (!json.has(key)) {
				localErrorString += "missing " + key + ";";
			}
		}
		
		return localErrorString;
	}

	@Override
	protected String validateField(JsonObject json, String currentKey, String parentKey) {
		Log.info("Goto RecordDocumentValidator.validateField");
		
		String localErrorString = "";

		boolean valid = true;
		String[] optionValues = null;

		switch (currentKey) { // check possible values
		case "MIMEType":
			optionValues = new String[] { "image/tiff", "application/illustrator", "application/pdf" };
			break;
		case "creationDate":
			valid = Function.dateFormatValidator(json.get(currentKey).getAsString(), Constant.DATETIME_PATTERN_AS_ISO8601);
			break;
		case "primaryDocumentFlag":
			optionValues = new String[] { "YES", "NO" };
			break;
		case "productType":
			optionValues = new String[] { "TL", "UL" };
			break;
		case "channelCode":
			optionValues = new String[] { "AG", "MI" };
			break;
		case "contentData":
			String documentContent = json.get(currentKey).getAsString();
			if (documentContent == null) {
				localErrorString += "empty contentData;";
			}
			break;
		default:
			break;
		}
		
		if(optionValues != null) {
			valid = Arrays.asList(optionValues).contains(json.get(currentKey).getAsString());
		}

		if (!valid) {
			localErrorString += "invalid " + currentKey + ";";
		}
		
		return localErrorString;
	}

}
