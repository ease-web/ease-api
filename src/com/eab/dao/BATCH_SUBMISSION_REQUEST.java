package com.eab.dao;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DataType;
import com.google.gson.JsonArray;

public class BATCH_SUBMISSION_REQUEST extends BaseDao{

	public boolean create(String appNo, String statusToTrigger, int batchNo, String submissionStatus)
			throws Exception {
		
		init();
		
		Date currentDate = new Date();

		String sqlStatement = "insert into BATCH_SUBMISSION_REQUEST(EAPP_NO, CREATE_DATE, TRIGGER_BY_PROCESS, CREATE_BY_BATCH_NO, SUBMISSION_DATE_ST, SUBMISSION_STATUS) "
				+  "values (" + dbm.param(appNo, DataType.TEXT) + ", "
				+ dbm.param(Function.convertJavaDateToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(statusToTrigger, DataType.TEXT) + ", "
				+ dbm.param(batchNo, DataType.INT) + ", "
				+ dbm.param(Function.convertJavaDateToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(submissionStatus, DataType.TEXT) + ")";

		return insert(sqlStatement);
	}

	public boolean update(String eappId, Date subDateEnd, int noOfRec, String batchStatus) throws Exception {
		
		init();

		String sqlStatement = "UPDATE BATCH_SUBMISSION_REQUEST SET SUBMISSION_DATE_END = " + dbm.param(Function.convertJavaDateToSqlDate(subDateEnd), DataType.DATE)
				+ " , NUM_OF_DOCS = " + dbm.param(noOfRec, DataType.INT)
				+ " , SUBMISSION_STATUS = " + dbm.param(batchStatus, DataType.TEXT)
				+ " WHERE EAPP_NO = "
				+ dbm.param(eappId, DataType.TEXT);

		return update(sqlStatement);
	}
	
	
	public boolean updateRecordtoTake(int bNo, int noOfRecordToTake) throws Exception {
		init();
		
		String sqlStatement = "UPDATE batch_submission_request set taken_by_batch_no = " +  dbm.param(bNo, DataType.INT) + ", SUBMISSION_STATUS = 'P' where EAPP_NO in "
				+ "(SELECT * FROM (SELECT EAPP_NO FROM batch_submission_request where taken_by_batch_no is null ORDER BY create_date ) WHERE ROWNUM <= " + dbm.param(noOfRecordToTake, DataType.INT) + ")";
		Log.debug(sqlStatement);
		return update(sqlStatement);
	}
	
	public JSONObject selectRLSStatus(String polNum) throws Exception {
		init();
		
		String sqlStatement = "select SUBMISSION_STATUS from batch_submission_request where eapp_no in " + 
				"(select eapp_no from EAPP_MST where POL_NUM = " +  dbm.param(polNum, DataType.TEXT) + ")";
		
		return selectSingleRecord(sqlStatement); 
	}
	
	public JSONObject selectRLSStatusForShieldFailedCase(JsonArray childIdsArr) throws Exception {
		init();
		String dbmParam = "";
		for (int i =0; i < childIdsArr.size(); i++) {
			if (i == 0) {
				dbmParam += (childIdsArr.get(i) != null) ? dbm.param(childIdsArr.get(i).getAsString(), DataType.TEXT) : "";
			} else {
				dbmParam +=  (childIdsArr.get(i) != null) ? "," + dbm.param(childIdsArr.get(i).getAsString(), DataType.TEXT) : "";
			}
		}
		String sqlStatement = "SELECT COUNT(*) AS FAIL_CNT from batch_submission_request where eapp_no in (" +  dbmParam + ") and  submission_status <> 'C'";
		
		return selectSingleRecord(sqlStatement); 
	}
	
	public JSONObject selectRLSStatusForShieldRunCase(JsonArray childIdsArr) throws Exception {
		init();
		String dbmParam = "";
		for (int i =0; i < childIdsArr.size(); i++) {
			if (i == 0) {
				dbmParam += (childIdsArr.get(i) != null) ? dbm.param(childIdsArr.get(i).getAsString(), DataType.TEXT) : "";
			} else {
				dbmParam +=  (childIdsArr.get(i) != null) ? "," + dbm.param(childIdsArr.get(i).getAsString(), DataType.TEXT) : "";
			}
		}
		String sqlStatement = "SELECT COUNT(*) AS RUN_CNT from batch_submission_request where eapp_no in (" +  dbmParam + ")";
		
		return selectSingleRecord(sqlStatement); 
	}
	
	public int selectSeq() throws Exception{
		init();
		
		String sqlStatement = "select batch_sub_request_seq.nextval from dual";
		
		return selectSingleRecord(sqlStatement).getInt("NEXTVAL");
	}
	
	public JSONObject singlerecordquerySQL(String Sql) throws Exception {
		init();
		
		return selectSingleRecord(Sql); 
	}
	
	public JSONArray querySQL(String Sql) throws Exception {
		init();
		
		return selectMultipleRecords(Sql); 
	}
}
