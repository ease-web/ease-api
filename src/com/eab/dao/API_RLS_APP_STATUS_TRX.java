package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class API_RLS_APP_STATUS_TRX extends BaseDao{

	public boolean create(String audID, String headerAsString, String queryAsString, String requestID) throws Exception {

		init();

		String sqlStatement = "insert into API_RLS_APP_STATUS_TRX(AUD_ID, HEADER, QUERY, REQ_ID, CREATE_DT)"
				+ " values(" + dbm.param(audID, DataType.TEXT) + ", " + dbm.param(headerAsString, DataType.CLOB) 
				+ ", " + dbm.param(queryAsString, DataType.TEXT) 
				+ ", " + dbm.param(requestID, DataType.TEXT)
				+ ", sysdate" + ")";

		return insert(sqlStatement);
	}

	public boolean update(String audID, String requestHeader, String responseHeaderAsString, String responseAsString,
			boolean isCompleted) throws Exception {

		init();

		String status = isCompleted ? "C" : "F";

		String sqlStatement = "UPDATE API_RLS_APP_STATUS_TRX SET REQ_HEADER = "
				+ dbm.param(requestHeader, DataType.TEXT) + " , RESP_HEADER = "
				+ dbm.param(responseHeaderAsString, DataType.TEXT) + " , RESPONSE = "
				+ dbm.param(responseAsString, DataType.TEXT) + " , STATUS = " + dbm.param(status, DataType.TEXT)
				+ " WHERE AUD_ID = " + dbm.param(audID, DataType.TEXT);

		return update(sqlStatement);
	}

}
