package com.eab.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class API_RLS_ESUB_TRX {
	
	public boolean create(String audID, String header, String policyNumber, String status, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "insert into API_RLS_ESUB_TRX (aud_id, pol_num, status, create_dt) "
					+ "values ("
					+ " " + dbm.param(audID, DataType.TEXT)
					+ ", " + dbm.param(policyNumber, DataType.TEXT)
					+ ", " + dbm.param(status, DataType.TEXT)
					+ ", sysdate"
					+ ")";
			result = dbm.insert(sql, conn);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
	
	public boolean update(String audID, String response, String status, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "update API_RLS_ESUB_TRX"
					+ " set response=" + dbm.param(response, DataType.CLOB)
					+ ", status=" + dbm.param(status, DataType.TEXT)
					+ " where aud_id=" + dbm.param(audID, DataType.TEXT)
					;
			
			int count = dbm.update(sql, conn);
			
			if (count == 1)
				result = true;
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}

}
