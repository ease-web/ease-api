package com.eab.dao;

import org.json.JSONArray;

import com.eab.database.jdbc.DataType;

public class POOL_SHIELD extends BaseDao{
	
	public boolean updateTargetPolicyNumber(String audID) throws Exception {
		init();
		
		String sqlStatement = "UPDATE POOL_SHIELD SET AUD_ID = "+ dbm.param(audID, DataType.TEXT) + " WHERE POL_NUM = (SELECT MIN(POL_NUM) FROM POOL_SHIELD WHERE AUD_ID IS NULL) AND AUD_ID IS NULL";
		
		return update(sqlStatement);

	}
	
	public boolean rollbackTargetPolicyNumber(String audID) throws Exception {
		init();
		
		String sqlStatement = "UPDATE POOL_SHIELD SET AUD_ID = NULL WHERE AUD_ID = "+ dbm.param(audID, DataType.TEXT);
		
		return update(sqlStatement);

	}
	
	public JSONArray selectTargetPolicyNumbers(String audID) throws Exception {
		init();
		
		String sqlStatement = "SELECT POL_NUM FROM POOL_SHIELD WHERE AUD_ID = " + dbm.param(audID, DataType.TEXT);
		
		return selectMultipleRecords(sqlStatement);

	}
	
	public boolean removeRecords(String audID) throws Exception {
		init();
		
		String sqlStatement = "DELETE FROM POOL_SHIELD WHERE AUD_ID = "+ dbm.param(audID, DataType.TEXT);

		return delete(sqlStatement);
	}
}
