package com.eab.dao;

import java.util.ArrayList;
import java.util.List;

import com.eab.database.jdbc.DataType;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class POOL_USED_NON_SHIELD extends BaseDao {
	
	public boolean insertRecordsFromArray(String audID, JsonArray jsonArray) throws Exception {
		init();
		
		String sqlStatement = "insert into POOL_USED_NON_SHIELD(POL_NUM,AUD_ID) values(?,?)";
		
		List<Integer> dataTypeList = new ArrayList<>(); // data type of that statement columns.
		dataTypeList.add(DataType.TEXT);
		dataTypeList.add(DataType.TEXT);
		
		List<Object> sqlStatementDataObjList = new ArrayList<>();
		
		for (JsonElement item : jsonArray) {
			String policyNumber = item.getAsString();
			
			List<Object> dataObjList = new ArrayList<>();
			dataObjList.add(policyNumber);
			dataObjList.add(audID);
			
			sqlStatementDataObjList.add(dataObjList); //each statement will have a single object list.
			
		}

		return insertBatch(sqlStatement, sqlStatementDataObjList, dataTypeList);
	}

}