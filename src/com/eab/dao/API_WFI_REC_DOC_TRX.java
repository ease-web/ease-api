package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class API_WFI_REC_DOC_TRX extends BaseDao {

	public boolean create(String audID, String headerAsString, String bodyAsString, String policyNumberAsString)
			throws Exception {

		init();

		String sqlStatement = "insert into API_WFI_REC_DOC_TRX(AUD_ID, HEADER, PARAM, POL_NUM, CREATE_DT)" + " values("
				+ dbm.param(audID, DataType.TEXT) + ", " + dbm.param(headerAsString, DataType.CLOB) + ", "
				+ dbm.param(bodyAsString, DataType.CLOB) + ", " + dbm.param(policyNumberAsString, DataType.TEXT)
				+ ", sysdate" + ")";

		return insert(sqlStatement);

	}

	public boolean update(String audID, String requestHeader, String requestParam, String responseHeaderAsString,
			String responseAsString, boolean isCompleted) throws Exception {
		init();

		String status = isCompleted ? "C" : "F";

		String sqlStatement = "UPDATE API_WFI_REC_DOC_TRX SET REQ_HEADER = " + dbm.param(requestHeader, DataType.TEXT)
				+ " , REQ_PARAM = " + dbm.param(requestParam, DataType.TEXT) + " , RESP_HEADER = "
				+ dbm.param(responseHeaderAsString, DataType.CLOB) + " , RESPONSE = "
				+ dbm.param(responseAsString, DataType.CLOB) + " , STATUS = " + dbm.param(status, DataType.TEXT)
				+ " WHERE AUD_ID = " + dbm.param(audID, DataType.TEXT);

		return update(sqlStatement);

	}
}