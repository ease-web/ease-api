package com.eab.dao;

import org.json.JSONObject;

import com.eab.database.jdbc.DataType;

public class API_RLS_REC_APP_TRX extends BaseDao{

	public boolean create(String audID, String headerAsString, String bodyAsString) throws Exception {

		init();

		String sqlStatement = "insert into API_RLS_REC_APP_TRX(AUD_ID, HEADER, PARAM, CREATE_DT)"
				+ " values(" + dbm.param(audID, DataType.TEXT) + ", " + dbm.param(headerAsString, DataType.CLOB) + ", "
				+ dbm.param(bodyAsString, DataType.CLOB) + ", sysdate" + ")";

		return insert(sqlStatement);
	}

	public boolean update(String audID, String policyNumberAsString, String requestHeader, String requestParam,
			String responseHeaderAsString, String responseAsString, String requestID, boolean isCompleted)
			throws Exception {

		init();

		String status = isCompleted ? "C" : "F";

		String sqlStatement = "UPDATE API_RLS_REC_APP_TRX SET POL_NUM = "
				+ dbm.param(policyNumberAsString, DataType.TEXT) + " , REQ_HEADER = "
				+ dbm.param(requestHeader, DataType.TEXT) + " , REQ_PARAM = " + dbm.param(requestParam, DataType.TEXT)
				+ " , RESP_HEADER = " + dbm.param(responseHeaderAsString, DataType.TEXT) + " , RESPONSE = "
				+ dbm.param(responseAsString, DataType.TEXT) + " , REQ_ID = " + dbm.param(requestID, DataType.TEXT)
				+ " , STATUS = " + dbm.param(status, DataType.TEXT) + " WHERE AUD_ID = "
				+ dbm.param(audID, DataType.TEXT);

		return update(sqlStatement);
	}
	
	public JSONObject getPolicyNumber(String requestID)
			throws Exception {

		init();

		String sqlStatement = "SELECT POL_NUM FROM API_RLS_REC_APP_TRX WHERE REQ_ID = "+ dbm.param(requestID, DataType.TEXT);

		return selectSingleRecord(sqlStatement);
	}

}
