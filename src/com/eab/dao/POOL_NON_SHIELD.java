package com.eab.dao;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.eab.database.jdbc.DataType;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class POOL_NON_SHIELD extends BaseDao{
	
	public boolean updateTargetPolicyNumber(String audID) throws Exception {
		init();
		
		String sqlStatement = "UPDATE POOL_NON_SHIELD SET AUD_ID = "+ dbm.param(audID, DataType.TEXT) + " WHERE POL_NUM = (SELECT MIN(POL_NUM) FROM POOL_NON_SHIELD WHERE AUD_ID IS NULL) AND AUD_ID IS NULL";
		
		return update(sqlStatement);

	}
	
	public boolean rollbackTargetPolicyNumber(String audID) throws Exception {
		init();
		
		String sqlStatement = "UPDATE POOL_NON_SHIELD SET AUD_ID = NULL WHERE AUD_ID = "+ dbm.param(audID, DataType.TEXT);
		
		return update(sqlStatement);

	}
	
	public JSONArray selectTargetPolicyNumbers(String audID) throws Exception {
		init();
		
		String sqlStatement = "SELECT POL_NUM FROM POOL_NON_SHIELD WHERE AUD_ID = " + dbm.param(audID, DataType.TEXT);
		
		return selectMultipleRecords(sqlStatement);

	}
	
	public boolean removeRecords(String audID) throws Exception {
		init();
		
		String sqlStatement = "DELETE FROM POOL_NON_SHIELD WHERE AUD_ID = "+ dbm.param(audID, DataType.TEXT);

		return delete(sqlStatement);
	}

}
