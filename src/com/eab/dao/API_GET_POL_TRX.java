package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class API_GET_POL_TRX extends BaseDao{

	public boolean create(String audID, String headerAsString, String bodyAsString) throws Exception {

		init();

		String sqlStatement = "insert into API_GET_POL_TRX(AUD_ID, HEADER, PARAM)"
				+ " values(" + dbm.param(audID, DataType.TEXT) 
				+ ", " + dbm.param(headerAsString, DataType.CLOB) 
				+ ", " + dbm.param(bodyAsString, DataType.CLOB) 
				+ ")";

		return insert(sqlStatement);
	}

	public boolean update(String audID, String responseHeaderAsString, String responseAsString, int responsePolicyNumCount,
			boolean isCompleted) throws Exception {

		init();

		String status = isCompleted ? "C" : "F";

		String sqlStatement = "UPDATE API_GET_POL_TRX SET RESP_HEADER = "
				+ dbm.param(responseHeaderAsString, DataType.CLOB) + 
				" , RESP_BODY = " + dbm.param(responseAsString, DataType.CLOB) 
				+ " , RESP_POL_COUNT = " + dbm.param(responsePolicyNumCount, DataType.INT) 
				+ " , STATUS = " + dbm.param(status, DataType.TEXT)
				+ " WHERE AUD_ID = " + dbm.param(audID, DataType.TEXT);

		return update(sqlStatement);
	}

}
