package com.eab.dao;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.database.jdbc.DataType;

public class USER_PROFILE extends BaseDao{
	
	public boolean create(String user_id, String title, String name, String mobile_no, String office_no, String email, String role, String user_cat, String agent_no, String fa_role, String dir_firm, String distrib_no, String unit_no, String auth_group, String user_status, String auth_hi, String auth_m8, String auth_m8a, String auth_ifast, String rank, String position, String upline_no_1, String upline_no_2, String proxy_user_1, String proxy_user_2, String proxy_start_date, String proxy_end_date, String susp_start_date, String susp_end_date, String user_type) throws Exception {

		init();

		String sqlStatement = "insert into USER_PROFILE(USER_ID,TITLE,NAME,MOBILE_NO,OFFICE_NO,EMAIL,ROLE,USER_CAT,AGENT_NO,FA_ROLE,DIR_FIRM,DISTRIB_NO,UNIT_NO,AUTH_GROUP,USER_STATUS,AUTH_HI,AUTH_M8,AUTH_M8A,AUTH_IFAST,RANK,POSITION,UPLINE_NO_1,UPLINE_NO_2,PROXY_USER_1,PROXY_USER_2,PROXY_START_DATE,PROXY_END_DATE,SUSP_START_DATE,SUSP_END_DATE,USER_TYPE)"
				+ " values(" 
				+ dbm.param(user_id, DataType.TEXT) 
				 + ", " + dbm.param(title, DataType.TEXT)
				 + ", " + dbm.param(name, DataType.TEXT)
				 + ", " + dbm.param(mobile_no, DataType.TEXT)
				 + ", " + dbm.param(office_no, DataType.TEXT)
				 + ", " + dbm.param(email, DataType.TEXT)
				 + ", " + dbm.param(role, DataType.TEXT)
				 + ", " + dbm.param(user_cat, DataType.TEXT)
				 + ", " + dbm.param(agent_no, DataType.TEXT)
				 + ", " + dbm.param(fa_role, DataType.TEXT)
				 + ", " + dbm.param(dir_firm, DataType.TEXT)
				 + ", " + dbm.param(distrib_no, DataType.TEXT)
				 + ", " + dbm.param(unit_no, DataType.TEXT)
				 + ", " + dbm.param(auth_group, DataType.TEXT)
				 + ", " + dbm.param(user_status, DataType.TEXT)
				 + ", " + dbm.param(auth_hi, DataType.TEXT)
				 + ", " + dbm.param(auth_m8, DataType.TEXT)
				 + ", " + dbm.param(auth_m8a, DataType.TEXT)
				 + ", " + dbm.param(auth_ifast, DataType.TEXT)
				 + ", " + dbm.param(rank, DataType.TEXT)
				 + ", " + dbm.param(position, DataType.TEXT)
				 + ", " + dbm.param(upline_no_1, DataType.TEXT)
				 + ", " + dbm.param(upline_no_2, DataType.TEXT)
				 + ", " + dbm.param(proxy_user_1, DataType.TEXT)
				 + ", " + dbm.param(proxy_user_2, DataType.TEXT)
				 + ", " + dbm.param(proxy_start_date, DataType.TEXT)
				 + ", " + dbm.param(proxy_end_date, DataType.TEXT)
				 + ", " + dbm.param(susp_start_date, DataType.TEXT)
				 + ", " + dbm.param(susp_end_date, DataType.TEXT)
				 + ", " + dbm.param(user_type, DataType.TEXT)
				+ ")";

		return insert(sqlStatement);
	}
	
	public boolean delete(String agent_no) throws Exception {
		init();

		String sqlStatement = "DELETE FROM USER_PROFILE WHERE AGENT_NO = " + dbm.param(agent_no, DataType.TEXT);

		return insert(sqlStatement);
	}
	
	public JSONObject selectRecordByAgentNo(String agent_no) throws Exception {
		init();
		
		String sqlStatement = "SELECT AGENT_NO, NAME, DISTRIB_NO FROM USER_PROFILE WHERE AGENT_NO = "
				+ dbm.param(agent_no, DataType.TEXT);

		return selectSingleRecord(sqlStatement);

	}
	
	public JSONArray selectDirectReports(String agent_no) throws Exception {
		init();
		
		String sqlStatement = "SELECT USER_ID FROM USER_PROFILE WHERE UPLINE_NO_1 = "
				+ dbm.param(agent_no, DataType.TEXT);

		return selectMultipleRecords(sqlStatement);

	}
	
	public JSONArray selectAgencyDownlineByUserId(String user_id) throws Exception {
		init();
		
		String sqlStatement = "SELECT t5.agent_no,t5.name,t5.upline_1_no,t5.upline_1_name,t5.upline_2_no,(SELECT name FROM user_profile WHERE agent_no = t5.upline_2_no ) upline_2_name FROM (SELECT t0.agent_no,t0.name,t1.agent_no upline_1_no,t1.name upline_1_name,t1.upline_no_1 as upline_2_no FROM user_profile t0,user_profile t1, (SELECT t3.agent_no FROM user_profile t3 WHERE t3.user_id = "+ dbm.param(user_id, DataType.TEXT) +") t2 WHERE t0.upline_no_1 = t1.agent_no AND (t0.upline_no_1 =  t2.agent_no OR t0.upline_no_2 =  t2.agent_no)) t5";
		
		return selectMultipleRecords(sqlStatement);

	}
}
