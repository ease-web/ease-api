package com.eab.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class AUD_WEB_ACCESS  extends BaseDao {
	public boolean createAccessLog(long timestamp, String userId, String action, String token, String extraData) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(timestamp);
		Date date = cal.getTime();
		java.sql.Timestamp loginDT = new java.sql.Timestamp(date.getTime());  
		
		try {
			String sql = "INSERT INTO AUD_WEB_ACCESS (user_id, token, action, extra_data, access_dt) "
					+ "values("
					+ dbm.param(userId, DataType.TEXT) + ", "
					+ dbm.param(token, DataType.TEXT) + ", "
					+ dbm.param(action, DataType.TEXT) + ", "
					+ dbm.param(extraData, DataType.TEXT) + ", "
					+ dbm.param(loginDT, DataType.TIMESTAMP)
					+ ")";
			result = dbm.insert(sql);
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return result;
	}
	
}
