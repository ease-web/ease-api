package com.eab.dao;

import org.json.JSONObject;

import com.eab.database.jdbc.DataType;

public class LOOKUP_AGENT_RANK extends BaseDao{
	
	public JSONObject select(String type, String rank) throws Exception {
			init();
			
			String sqlStatement = "SELECT NAME FROM LOOKUP_AGENT_RANK WHERE TYPE = " + dbm.param(type, DataType.TEXT)
			+ " AND RANK = " + dbm.param(rank, DataType.TEXT);
			
			return selectSingleRecord(sqlStatement);
	}

}
