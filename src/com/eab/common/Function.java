package com.eab.common;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Function {
	
	public static String GenerateRandomString(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return new Base64().encodeAsString(bytes);
	}
	
	public static String GenerateRandomASCII(int length) {
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		// Random rng = new Random();
		java.security.SecureRandom rng = new java.security.SecureRandom();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}
	
	public static String ByteToStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		for (byte b : bytes)
			sb.append((char) b);
		return sb.toString();
	}

	public static byte[] StrToByte(String text) {
		return text.getBytes();
	}

	public static String ByteToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static byte[] HexToByte(String s) {
		return new BigInteger(s, 16).toByteArray();
	}

	public static byte[] StrToBase64(String text) {
		return text.getBytes();
	}

	public static String Base64ToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}
	
	public static String ByteToBase64(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}

	public static byte[] HexToStr(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
		}
		return data;
	}

	private static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static String StrToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}
	
	public static String getClientIp(HttpServletRequest request) {
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}
		return remoteAddr;
	}
	
	public static String getServerIp() {
    	String ipAddr = "0.0.0.0";
    	
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
                Enumeration<InetAddress> nias = ni.getInetAddresses();
                while(nias.hasMoreElements()) {
                    InetAddress ia= (InetAddress) nias.nextElement();
                    if (!ia.isLinkLocalAddress() 
                     && !ia.isLoopbackAddress()
                     && ia instanceof Inet4Address) {
                    	return ia.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
        	Log.error(e);
        }
        return ipAddr;
    }
	
	public static String getServerIp(HttpServletRequest request) {
		String ipAddr = (request != null) ? request.getLocalAddr() : "0.0.0.0";
		return ipAddr;
	}
	
	public static String getBaseUrl(HttpServletRequest request) {
		return (request.getRequestURL()).substring(0, (request.getRequestURL()).length()
				- (request.getRequestURI()).length() + (request.getContextPath()).length()) + "/";
	}

	public static String getPathUrl(HttpServletRequest request) {
		return request.getServletPath();
	}
	
	public static ZonedDateTime getDateUTCNow() {
		return ZonedDateTime.now(ZoneOffset.UTC);
	}

	public static final DateTimeFormatter DATE_FORMAT_ISO8601 = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_ISO8601);
	public static final DateTimeFormatter DATE_FORMAT_AXA = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_AXA);

	public static String getDateUTCNowStr() {
		return DATE_FORMAT_ISO8601.format(getDateUTCNow());
	}
	
	public static boolean dateFormatValidator(String value, String format) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			dateFormat.parse(value);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static String convertDateTimeZoneString(String fromDateStringAsISO8601, String targetTimeZoneId,
			DateTimeFormatter format) {

		// Convert to target time zone.
		// For example, targetTimeZoneId is Singapore, then 2017-09-03T09:46:07Z to be
		// 2017-09-03 17:46:07
		ZonedDateTime currentDateTime = ZonedDateTime.parse(fromDateStringAsISO8601, DATE_FORMAT_ISO8601);
		ZonedDateTime convertedDateTime = currentDateTime.withZoneSameInstant(ZoneId.of(targetTimeZoneId));
		String convertedDateString = format.format(convertedDateTime);

		Log.debug("*** fromDateStringAsISO8601=" + fromDateStringAsISO8601 + ", targetTimeZoneId=" + targetTimeZoneId
				+ ", convertedDateString=" + convertedDateString);

		return convertedDateString;
	}

	public static String convertDateString(String value, String fromFormat, String toFormat) throws ParseException {

		String convertedDateString = "";

		SimpleDateFormat fromDateFormatter = new SimpleDateFormat(fromFormat);

		java.util.Date date = fromDateFormatter.parse(value);

		SimpleDateFormat toDateFormatter = new SimpleDateFormat(toFormat);
		
		convertedDateString = toDateFormatter.format(date);

		return convertedDateString;
	}
	
	public static JSONObject merge2JSONObject(JSONObject json1, JSONObject json2) {
		JSONObject mergedJSON = new JSONObject();
		try {
			mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
			for (String crunchifyKey : JSONObject.getNames(json2)) {
				mergedJSON.put(crunchifyKey, json2.get(crunchifyKey));
			}
 
		} catch (Exception e) {
			Log.error(e);
		}
		return mergedJSON;
	}
	
	public static JsonObject validateJsonFormatForString(String jsonString) {
		JsonObject jsonBody = null;
		
		try {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
			jsonBody = jsonElement.getAsJsonObject();

		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return jsonBody;
	}
	
	public static JsonArray validateJsonArrayFormatForString(String jsonString) {
		JsonArray jsonBody = null;
		
		try {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
			jsonBody = jsonElement.getAsJsonArray();

		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return jsonBody;
	}
	
	public static String extractException(Exception e) {
		String message = e.toString() + "\n";
		StackTraceElement[] trace = e.getStackTrace();
		for (int i = 0; i < trace.length; i++) {
			message += trace[i].toString() + "\n";
		}
		
		return message;
	}
	
	public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}
	
	public static java.sql.Date sqlDateFromString(String value, String dateFormat, String targetTimeZoneId) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setTimeZone(TimeZone.getTimeZone(targetTimeZoneId));
		java.util.Date parsed = format.parse(value);
		
		return convertJavaDateToSqlDate(parsed);
	}
	
	public static boolean isNumeric(String text) {
		boolean result = false;
		try {
			Integer.parseInt(text);
			result = true;
		} catch(Exception e) {
			result = false;
		}
		
		return result;
	}
	
	public static boolean isASCII(String text) {
		boolean result = false;
		if (text != null)
			result = text.matches("\\p{ASCII}*");
		
		return result;
	}
	
	public static String getServerName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			Log.error(e);
		}
		return "";
	}
	
	public static JsonElement getAsPath(JsonElement e, String path){
    	JsonElement current = e;
        String ss[] = path.split("/");
        for (int i = 0; i < ss.length; i++) {
        	if (current != null) {
        		current = current.getAsJsonObject().get(ss[i]);
        	}
        }
        return current;
    }
	
	public static JsonObject transformObject(JSONObject obj) {
    	if (obj == null) {
    		return null;
    	}
		JsonParser jsonParser = new JsonParser();
		JsonObject transformedObj;
		transformedObj = (JsonObject)jsonParser.parse(obj.toString());
		return transformedObj;
    }
	
	public static double transformEleToDouble(JsonElement ele) {
    	if (ele == null ){
    		return 0;
    	} else {
    		return ele.getAsDouble();
    	}
    }
	
	public static String transformEleToString(JsonElement ele) {
		if (ele == null ){
    		return "";
    	} else {
    		return ele.getAsString();
    	}
    }
}
