package com.eab.common;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.http.Header;

import com.google.gson.JsonObject;

public class RestUtil {
	
	/***
	 * Show HTTP Header and Body into Log
	 * @param headers - HTTP Header
	 * @param body - HTTP Body
	 * @return HTTP Header in String format
	 */
	public static String showLogRequest(HttpHeaders headers, String body) {
		String output = "";
		
		try {
			String headerList = "";
			for(String header : headers.getRequestHeaders().keySet()) {
				headerList += header + ": ";
				for(String item : headers.getRequestHeader(header)) {
					headerList += item + "\n";
				}
			}
			Log.debug("headers:\n" + headerList);
			Log.debug("body:\n" + (body!=null && body.length()>=100?body.substring(0, 100):body));
			
			output = headerList;
		} catch(Exception e) {
			Log.error(e);
		}
		
		return output;
	}
	
	/***
	 * Show HTTP Header and Body into Log
	 * @param headers - HTTP Header
	 * @param body - HTTP Body
	 * @return HTTP Header in String format
	 */
	public static String showLogRequest(Header[] headers, String body) {
		String output = "";
		
		Log.debug("HTTP Request (preview):");
		
		try {
			String headerList = "";
			for(Header header : headers){
				headerList += header.getName() + ": " + header.getValue() + "\n";
			}
			Log.debug("headers:\n" + headerList);
			Log.debug("body:\n" + (body!=null && body.length()>=100?body.substring(0, 100):body));
			
			output = headerList;
		} catch(Exception e) {
			Log.error(e);
		}
		
		return output;
	}
	
	/***
	 * Show HTTP Header and Body into Log
	 * @param headers - HTTP Header
	 * @return HTTP Header in String format
	 */
	public static String showLogRequest(HttpHeaders headers) {
		String output = "";
		
		try {
			String headerList = "";
			for(String header : headers.getRequestHeaders().keySet()) {
				headerList += header + ": ";
				for(String item : headers.getRequestHeader(header)) {
					headerList += item + "\n";
				}
			}
			Log.debug("headers:\n" + headerList);
			
			output = headerList;
		} catch(Exception e) {
			Log.error(e);
		}
		
		return output;
	}
	
	/***
	 * Generate HTTP Response
	 * @param audID - Audit ID
	 * @param status - HTTP Status
	 * @param body - Response Body
	 * @return HTTP Response
	 */
	public static Response toResponse(String audID, Status status, String body) {
		ResponseBuilder rb = Response.status(status);
		if (audID != null)
			rb.header("Aud-ID", audID);
		if (body != null)
			rb.entity(body);
		return rb.build();
	}
	
	/***
	 * Convert HTTP Header to multi-line string
	 * @param headers - HTTP Header
	 * @return Header in each line
	 */
	public static String convertHeaderToStr(MultivaluedMap<String, String> headers) {
		String output = "";
		
		for(String header : headers.keySet()) {
			output += header + ": ";
			for(String item : headers.get(header)) {
				output += item + "\n";
			}
		}
		
		return output;
	}
	
	/***
	 * Generate Error JSON for response
	 * @param status - False for having Error
	 * @param message - Error Message
	 * @return JSON String
	 */
	public static String genErrorJsonStr(boolean status, String message) {
		JsonObject errorJson = new JsonObject();
		errorJson.addProperty("success", false);
		errorJson.addProperty("status", status);
		errorJson.addProperty("message", message);
		
		return errorJson.toString();
	}
	
	/***
	 * Generate Error JSON for response
	 * @param status - Status in Text format
	 * @param message - Error Message
	 * @return JSON String
	 */
	public static String genErrorJsonStr(String status, String message) {
		JsonObject errorJson = new JsonObject();
		errorJson.addProperty("status", status);
		errorJson.addProperty("message", message);
		
		return errorJson.toString();
	}
}
