package com.eab.common;

import java.security.KeyStore;

import org.hibernate.SessionFactory;

import com.eab.couchbase.CBUtil;
import com.realobjects.pdfreactor.PDFreactor;

public class Constant {
	public static String APP_NAME = "EASE-API";
	public static String APP_IP = "127.0.0.1";
	public static String APP_HOSTNAME = "";
	public static String DATABASE_TYPE = "oracle";
	public static String DATASOURCE_STRING = "";
	public static SessionFactory SESSION_FACTORY = null;
	public static String ZONE_ID = "Singapore";
	public static String DATETIME_PATTERN_AS_ISO8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	public static String DATETIME_PATTERN_AS_AXA = "dd-MM-yyyy HH:mm:ss";
	public static String DATETIME_PATTERN_AS_DATE_ONLY ="yyyy-MM-dd";
	public static String DATETIME_PATTERN_AS_SQL_DATE ="yyyy-MM-dd HH:mm:ss";
	public static boolean EANBLE_BATCH_EMAIL = true;
	
	public static CBUtil CBUTIL = null;
	public static KeyStore SSL_TRUST_STORE = null;
	public static KeyStore SSL_KEY_STORE = null;
	public static PDFreactor pdfReactor = null;
}
