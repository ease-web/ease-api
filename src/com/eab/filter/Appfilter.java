package com.eab.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.lang3.time.StopWatch;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class Appfilter implements Filter {
	
	public void init(FilterConfig config) throws ServletException {	
		// Do Nothing
	}
	
	public void destroy() {
		// Do Nothing
	}
	
	public void doFilter(ServletRequest request, ServletResponse response,	FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = null;
		HttpServletResponse httpResponse = null;
		String path = null;
		String localname = null;
		StopWatch stopWatch = null;
		String userID = null;
		String remoteIP = null;
		HeaderMapRequestWrapper requestWrapper = null;
		CharResponseWrapper wrappedResponse = null;
		
		// Initialization
		stopWatch = new StopWatch();
		httpRequest = (HttpServletRequest)request;
		httpResponse = (HttpServletResponse)response;
		path = getPath(httpRequest);
		userID = (httpRequest.getHeader("User-ID")!=null)?httpRequest.getHeader("User-ID"):"undefined";
		remoteIP = Function.getClientIp(httpRequest);
		
		// Override Request
		requestWrapper = new HeaderMapRequestWrapper(httpRequest);
		// Override Response
		wrappedResponse = new CharResponseWrapper( (HttpServletResponse)response );
		
		// #Process Start
		Log.info("remoteIP: " + remoteIP);
		Log.info("path: "+ path);
		Log.info("userID: "+ userID);
		
		String audid = genAudID();
		requestWrapper.addHeader("aud-id", audid);	//set Audit ID into HTTP Request Header

		/*********************
		 * Authorization
		 *********************/
		String authResult = checkAuthorize(requestWrapper);
		if (authResult != null && !authResult.equals("PASSED")) {
			userID = authResult;
		}
		
		// Create Audit Log record
		createAudit(audid, path, userID, remoteIP);
		
		stopWatch.start();
		Log.info("aud-id: " + audid + " --> START");
		
		/*********************
		 * Go forward to RESTful service or Reject
		 *********************/
		if (authResult != null) {
			chain.doFilter(requestWrapper, wrappedResponse);
			
			byte[] bytes = wrappedResponse.getByteArray();
			response.getOutputStream().write(bytes);
			
			// Update Audit Log record
			updateAudit(audid, "C", "");
		} else {
			((HttpServletResponse) response).setHeader("Content-Type", "application/json");
			((HttpServletResponse) response).setStatus(401);
			response.getOutputStream().write(RestUtil.genErrorJsonStr("UNAUTHORIZED", "Unauthorized access EASE internal API").getBytes());
			
			// Update Audit Log record
			updateAudit(audid, "F", "Unauthorized Access");
		}
		
		stopWatch.stop();
		Log.info("aud-id: " + audid + " --> END, Elapsed time: " + stopWatch.getTime() + " mills.");
		
		// #Process End
	}
	
	private String genAudID() {
		Instant ins = Instant.now(Clock.systemUTC());
		String rand = Function.GenerateRandomASCII(6);
		String result = ins.toEpochMilli() + "-" + rand;
		ins = null;
		
		return result;
	}
	
	private String getPath(HttpServletRequest req) {
		String path = req.getRequestURI();
		if (req.getContextPath() != null && !req.getContextPath().equals("/"))
			path = path.replaceFirst("("+req.getContextPath()+")", "");
		return path;
	}
	
	private String checkAuthorize(HttpServletRequest req) {
		// Call from AXA
		String path = getPath(req);
		
		Log.debug("Auth Check Path: " + path);
		if ( AuthorizeList.match(path) ) {
			String authCode = req.getHeader( AuthorizeList.heaherName );
			String userName = AuthorizeList.auth(authCode);

			Log.debug("Auth Check Username: " + userName);
			return userName;
		}
		Log.debug("Auth Check Username: " + "PASSED");
		return "PASSED";
	}
	
	private void createAudit(String audid, String urlPath, String userID, String remoteIP) {
		Instant ins = Instant.now(Clock.systemUTC());
		Timestamp ts = Timestamp.from(ins);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		
		executor.submit(() -> {
			StopWatch stopWatch = new StopWatch();
		    String threadName = Thread.currentThread().getName();
		    DBManager dbm = new DBManager();

		    stopWatch.start();
		    Log.info("createAudit | " + threadName + " --> " + audid + " | Start");
		    try {
			    String sqlInsert = "insert into AUD_API_ACCESS (aud_id, access_url, user_id, remote_host, local_host, start_dt)"
			    		+ "values ("
			    		+ "" + dbm.param(audid, DataType.TEXT)
			    		+ ", " + dbm.param(urlPath, DataType.TEXT)
			    		+ ", " + dbm.param(userID, DataType.TEXT)
			    		+ ", " + dbm.param(remoteIP, DataType.TEXT)
			    		+ ", " + dbm.param(Constant.APP_IP, DataType.TEXT)
			    		+ ", " + dbm.param(ts, DataType.TIMESTAMP)
			    		+ ")";
				dbm.insert(sqlInsert);
			} catch (SQLException e) {
				Log.error(e);
			} catch (Exception e) {
				Log.error(e);
			} finally {
				dbm = null;
			}
		    stopWatch.stop();
		    Log.info("createAudit | " + threadName + " --> " + audid + " | End, Elapsed time: " + stopWatch.getTime() + " mills");
		});
	}
	
	private void updateAudit(String audid, String status, String failReason) {
		Instant ins = Instant.now(Clock.systemUTC());
		Timestamp ts = Timestamp.from(ins);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		
		executor.submit(() -> {
			StopWatch stopWatch = new StopWatch();
			String threadName = Thread.currentThread().getName();
		    DBManager dbm = new DBManager();
		    
		    // Suspend 5 seconds before execute
		    try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				Log.error(e);
			}
		    
		    stopWatch.start();
		    Log.info("updateAudit | " + threadName + " --> " + audid + " | Start");
		    
		    try {
			    String sqlUpdate = "update AUD_API_ACCESS "
			    		+ "set END_DT=" + dbm.param(ts, DataType.TIMESTAMP)
			    		+ ", STATUS=" + dbm.param(status, DataType.TEXT)
			    		+ ", FAIL_REASON=" + dbm.param((failReason!=null)?failReason:"", DataType.TEXT)
			    		+ " where AUD_ID=" + dbm.param(audid, DataType.TEXT)
			    		;
			    int hitCount = 0;
			    hitCount = dbm.update(sqlUpdate);
			    Log.debug("updateAudit | hitCount=" + hitCount);
			    
			    for (int i=0; i<10 && hitCount == 0; i++) {
			    	Thread.sleep(2000);
			    	hitCount = dbm.update(sqlUpdate);
			    }
		    } catch (SQLException e) {
				Log.error(e);
			} catch (Exception e) {
				Log.error(e);
			} finally {
				dbm = null;
			}
		    stopWatch.stop();
		    Log.info("updateAudit | " + threadName + " --> " + audid + " | End, Elapsed time: " + stopWatch.getTime() + " mills");
		});
	}
	
    public class HeaderMapRequestWrapper extends HttpServletRequestWrapper {
        /**
         * construct a wrapper for this request
         * 
         * @param request
         */
        public HeaderMapRequestWrapper(HttpServletRequest request) {
            super(request);
        }

        private Map<String, String> headerMap = new HashMap<String, String>();

        /**
         * add a header with given name and value
         * 
         * @param name
         * @param value
         */
        public void addHeader(String name, String value) {
            headerMap.put(name, value);
        }

        @Override
        public String getHeader(String name) {
            String headerValue = super.getHeader(name);
            if (headerMap.containsKey(name)) {
                headerValue = headerMap.get(name);
            }
            return headerValue;
        }

        /**
         * get the Header names
         */
        @Override
        public Enumeration<String> getHeaderNames() {
            List<String> names = Collections.list(super.getHeaderNames());
            for (String name : headerMap.keySet()) {
                names.add(name);
            }
            return Collections.enumeration(names);
        }

        @Override
        public Enumeration<String> getHeaders(String name) {
            List<String> values = Collections.list(super.getHeaders(name));
            if (headerMap.containsKey(name)) {
                values.add(headerMap.get(name));
            }
            return Collections.enumeration(values);
        }
    }
    
    private static class ByteArrayServletStream extends ServletOutputStream
    {
    	ByteArrayOutputStream baos;

    	ByteArrayServletStream(ByteArrayOutputStream baos) {
    		this.baos = baos;
    	}

    	public void write(int param) throws IOException {
    		baos.write(param);
    	}

		@Override
		public boolean isReady() {
			return true;
		}

		@Override
		public void setWriteListener(WriteListener arg0) {
			throw new RuntimeException("Not implemented");
		}
    }

    private static class ByteArrayPrintWriter {
    	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	private PrintWriter pw = new PrintWriter(baos);
    	private ServletOutputStream sos = new ByteArrayServletStream(baos);
    	
    	public PrintWriter getWriter() {
    		return pw;
	    }
	
	    public ServletOutputStream getStream() {
	    	return sos;
	    }
	
	    byte[] toByteArray() {
	    	return baos.toByteArray();
	    }
    }

    public class CharResponseWrapper extends HttpServletResponseWrapper {
    	private ByteArrayPrintWriter output;
    	private boolean usingWriter;

    	public CharResponseWrapper(HttpServletResponse response) {
    		super(response);
    		usingWriter = false;
    		output = new ByteArrayPrintWriter();
    	}

    	public byte[] getByteArray() {
    		return output.toByteArray();
    	}

    	@Override
    	public ServletOutputStream getOutputStream() throws IOException {
    		// will error out, if in use
    		if (usingWriter) {
    			super.getOutputStream();
    		}
    		usingWriter = true;
    		return output.getStream();
    	}

    	@Override
    	public PrintWriter getWriter() throws IOException {
    		// will error out, if in use
    		if (usingWriter) {
    			super.getWriter();
    		}
    		usingWriter = true;
    		return output.getWriter();
    	}

    	public String toString() {
    		return output.toString();
    	}
    }
    
}
