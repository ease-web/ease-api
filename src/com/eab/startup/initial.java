package com.eab.startup;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;

import com.eab.auth.ease.MaamUtil;
import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.database.jdbc.DBAccess;
import com.eab.filter.AuthorizeList;
import com.realobjects.pdfreactor.Configuration;
import com.realobjects.pdfreactor.PDFreactor;

public class initial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void destroy() {
		//Nothing
	}
	
	public void init(ServletConfig config) throws ServletException {
		Log.info("Initialzation ......Start");
		
		super.init(config);
		try {
			// Get App Server IP Address & Hostname
			Constant.APP_IP = Function.getServerIp();
			Constant.APP_HOSTNAME = Function.getServerName();
			
			// Load Oracle Datasource
			Context context = new InitialContext(); 
			Constant.DATABASE_TYPE = config.getInitParameter("dbtype");
			Log.info("Database Type: " + Constant.DATABASE_TYPE);
			Constant.DATASOURCE_STRING = config.getInitParameter("datasource");
			Log.info("DataSource: " + Constant.DATASOURCE_STRING);
			
			try {
				DBAccess.setDatasource((DataSource) context.lookup(Constant.DATASOURCE_STRING));
			} catch(Exception e) {
				Log.error("[Initial Error] Unable to connect database " + Constant.DATABASE_TYPE + " | " + Constant.DATASOURCE_STRING);
			}
			
	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			// Load Coucbhase Sync Gateway Connection
			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			
			// Hibernate SessionFactory
//			Constant.SESSION_FACTORY = (SessionFactory) getServletContext().getAttribute("SessionFactory");

			// Filter Authorization
			AuthorizeList.init();
			
			// Initial MAAM Token
			MaamUtil.updateToken(this.getClass().getName());
			
			// Load Mutual SSL Keystore
			loadTrustStore();
			loadKeyStore();
			
			// Initial PDFReactor
			Constant.pdfReactor = new PDFreactor();
			// warm up PDFReactor
			Configuration prconfig = new Configuration();
			prconfig.setDocument("<html><head></head><body>test</body></html>");
			Constant.pdfReactor.convert(prconfig);
			
		}  catch(Exception e) {
			Log.error(e);
		} finally {
			///TODO
		}
		
		Log.info("Initialzation ......End");
	}
	
	private void loadTrustStore() {
		String trustFile = System.getProperty("jboss.server.config.dir") + File.separator + EnvVariable.get("SSL_TRUSTSTORE_FILE");
		KeyStore trustStore = null;
		FileInputStream instream = null;
		String trustStorePwd = EnvVariable.get("SSL_TRUSTSTORE_PWD");
		File trustFileObj = null;
		
		Log.info("Loading Trust Store: " + trustFile);
		try {
			trustFileObj = new File(trustFile);
			if (trustFileObj.exists() && trustFileObj.isFile()) {
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				instream = new FileInputStream(trustFileObj);
				trustStore.load(instream, trustStorePwd.toCharArray());
				Constant.SSL_TRUST_STORE = trustStore;
				Log.info("[!]...Trust Store file is loaded");
			} else {
				Log.info("[!]...No Trust Store file");
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			try {if(instream!=null)instream.close();}catch(Exception e) {}
			instream = null;
//			trustFileObj = null;
		}
	}
	
	private void loadKeyStore() {
		String keyFile = System.getProperty("jboss.server.config.dir") + File.separator + EnvVariable.get("SSL_KEYSTORE_FILE");
		KeyStore keyStore = null;
		FileInputStream instream = null;
		String keyStorePwd = EnvVariable.get("SSL_KEYSTORE_PWD");
		File keyFileObj = null;
		
		Log.info("Loading Key Store: " + keyFile);
		try {
			keyFileObj = new File(keyFile);
			if (keyFileObj.exists() && keyFileObj.isFile()) {
				keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				instream = new FileInputStream(keyFileObj);
				keyStore.load(instream, keyStorePwd.toCharArray());
				Constant.SSL_KEY_STORE = keyStore;
				Log.info("[!]...Key Store file is loaded");
			} else {
				Log.info("[!]...No Key Store file");
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			try {if(instream!=null)instream.close();}catch(Exception e) {}
			instream = null;
//			keyFileObj = null;
		}
	}
}
